<?php
return [
	// system params
    'adminEmail' => 'support@rekamy.com',
	'version'=>'3.0',

	// company params
    'appNameShort' => 'eStore', //size 132x132
    'appNameFull' => 'eStore MPSP', //size 132x132
    'companyNameShort' => 'MPSP',
    'companyNameFull' => 'Majlis Perbandaran Seberang Perai',
    'companyWebsite' => 'http://www.mpsp.gov.my/',

	// assets param

    // 'iconPath' => '/img/FEMS_logo/fems_icon_HD.ico', //size 132x132
    // 'companyLogoIcon' => '/img/FEMS_logo/FEMS-white-logo-250x100.png', //size 32x32
    // 'companyLogo' => '/img/FEMS_logo/FEMS-white-logo-250x100.png', //size 32x32
    // 'companyLogoAlt' => '/img/FEMS_logo/FEMS-black-logo-250x100.png', //size 32x32

    'iconPath' => '/img/favicon-32x32.png', //size 132x132
    'companyLogoIcon' => '/img/MPSP.png', //size 32x32
    'companyLogo' => '/img/MPSP.png', //size 32x32
    'companyLogoAlt' => '/img/MPSP.png', //size 32x32
];
