<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php',
    require __DIR__ . '/container.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','debug'],
    // 'bootstrap' => ['log','debug','settings'],
    'controllerNamespace' => 'frontend\controllers',
    // 'language' => 'ms-MY',
    // 'sourceLanguage' => 'en-US',
    'container' => $container,
    'components' => [
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/language',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@frontend/views' => '@frontend/themes/adminlte/views',
                    // '@frontend/views' => '@frontend/themes/gentella/views',
                    // '@frontend/views' => '@vendor/yiister/yii2-gentelella/views',
                    // '@frontend/views' => '@frontend/themes/sbadmin2/views',
                    // '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                ],
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'class' => 'common\components\User',
            'identityClass' => 'common\models\identity\Credential',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'estor4-frontend',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
/*        'access' => [
            'class' => 'common\components\AccessRule',
        ],
        'assetManager' => [
            'linkAssets' => true,
            'appendTimestamp' => true,
        ],*/
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
        ],
        // If you use tree table
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        // see settings on http://demos.krajee.com/tree-manager#module
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
            // oracle format
            'saveSettings' => [
                'date' => 'php:d-M-y',
                'time' => 'php:h:i:s a',
                'datetime' => 'php:d-M-Y h:i:s a',
            ],
            'displaySettings' => [
                'date' => 'php:d M Y',
                'time' => 'php:h:i:s a',
                'datetime' => 'php:d M Y h:i:s a',
            ],
            'autoWidgetSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => [
                    'type'=>3,
                    // 'value' =>date('d-M-y'),
                    'pluginOptions'=>[
                        'calendarWeeks' => true,
                        'daysOfWeekDisabled' => [0, 6],
                        'todayHighlight' => true,
                        // 'startDate' => 'today',
                        'todayBtn' => 'linked',
                        'todayHighlight' => 'true',
                        'daysOfWeekHighlighted' => [0, 6],
                        'defaultViewDate' => 'today',
                        'assumeNearbyYear' => 'true',
                        'autoclose' => true
                    ]
                ],
                \kartik\datecontrol\Module::FORMAT_DATETIME => [],
                \kartik\datecontrol\Module::FORMAT_TIME => [],
            ],
        ],
        'dynagrid' => [
            'class' => '\kartik\dynagrid\Module',
            'maxPageSize' => 200,
            'defaultPageSize' => 10,
            'dynaGridOptions' => [
                'storage' => kartik\dynagrid\DynaGrid::TYPE_SESSION,
                'gridOptions' => [],
                'matchPanelStyle' => false,
                'toggleButtonGrid' => [],
                'options' => [],
                'sortableOptions' => [],
                'userSpecific' => true,
                'columns' => [],
                'submitMessage' => Yii::t('kvdynagrid', 'Saving and applying configuration') . ' &hellip;',
                'deleteMessage' => Yii::t('kvdynagrid', 'Trashing all personalizations') . ' &hellip;',
                'deleteConfirmation' => Yii::t('kvdynagrid', 'Are you sure you want to delete the setting?'),
                'messageOptions' => [],
                'gridOptions'=>[
                    'filterSelector' => 'select[name="per-page"]',
                    'responsiveWrap'=>false,
                    // 'showPersonalize'=>true,
                ],
            ],
        ],
        'debug' => ['class' => 'yii\debug\Module'], //remove on production
    ],
    // 'as access' => [
    //     'class' => 'mdm\admin\components\AccessControl',
    //     'allowActions' => [
    //         'site/*',
    //         'admin/*',
    //         'some-controller/some-action',
    //         // The actions listed here will be allowed to everyone including guests.
    //         // So, 'admin/*' should not appear here in the production, of course.
    //         // But in the earlier stages of your development, you may probably want to
    //         // add a lot of actions here until you finally completed setting up rbac,
    //         // otherwise you may not even take a first step.
    //     ]
    // ],
    'params' => $params,
];
