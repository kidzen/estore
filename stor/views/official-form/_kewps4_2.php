<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\EstorInventories */

//$this->title = $inventory->ID;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estor Inventories'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//var_dump($inventory);
//var_dump($listItem[0]);die();
//die();
?>

<tbody>
    <?php
    // if (sizeof($transactions) >= 1) {
    if ($transactions) {
        foreach ($transactions as $i => $transaction) {
            ?>
            <?php
            if ($i == 0) { ?>
            <!-- if (sizeof($transactions) >= 1) { ?> -->
            <tr>
                <td class="text-center">/</td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="3" class="pull-right ">Baki Dibawa Kehadapan</td>
                <?php if ($transaction) { ?>
                <td class="text-center"><?= $transaction['COUNT_CURRENT'] - $transaction['COUNT_IN'] + $transaction['COUNT_OUT'] ?></td>
                <?php } else { ?>
                <td class="text-center">0</td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td class="text-center"><?= $i + 1 ?></td>
                <td class="text-center nowrap"><?= $transaction['CHECK_DATE']; ?></td>
                <td class="text-center"><?php
                    if ($transaction['TYPE'] == 1) {
                        echo 'IN';
                    } else {
                        echo 'OUT';
                    }
                    ?>
                </td>
                <td class="text-center" style="font-size: 10;width: 15%;white-space: nowrap;"><?= mb_strimwidth($transaction['REFFERENCE'],0,35,'...'); ?></td>
                <td class="text-center"><?= $transaction['COUNT_IN']; ?></td>
                <td class="pull-right"><?php
                    if ($transaction['TYPE'] == 1) {
                        echo $transaction['PRICE_IN'] / $transaction['COUNT_IN'];
                        ;
                    } else {
                        echo $transaction['PRICE_OUT'] / $transaction['COUNT_OUT'];
                        ;
                    }
                    ?>
                </td>
                <td class="text-center"><?= $transaction['COUNT_OUT']; ?></td>
                <td class="text-center"><?= $transaction['COUNT_CURRENT']; ?></td>
                <td style="font-size: 10;width: 30%;" class="text-center"><?= mb_strimwidth($transaction['checkBy']['mpspProfile']['NAMA'],0,30,'...') ?></td>
            </tr>
            <?php } ?>
            <?php for ($j = sizeof($transactions); $j < 27; $j++) { ?>
            <tr>
                <td class="text-center"><?= $j + 1 ?></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td></td>
                <td class="text-center"></td>
                <td class="pull-right"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center">&nbsp;</td>
            </tr>
            <?php } ?>
            <!-- avoid errors (untested) -->
            <?php } else { ?>
            <?php for ($j = 0; $j < 27; $j++) { ?>
            <tr>
                <td class="kv-align-center"><?= $j + 1 ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <?php } ?>
            <!-- end avoid errors (untested) -->
            <?php } ?>
        </tbody>
