<?php

use yii\helpers\Html;die;

/* @var $this yii\web\View */
/* @var $model common\models\OrderItems */

$this->title = Yii::t('app', 'Cetak Borang');
?>
<div class="form-generator">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?=
        $this->render('_form', [
            'model' => $model,
            'formId' => $formId,
            'yearList' => $yearList,
            'monthList' => $monthList,
            'ordersArray' => $ordersArray,
            'inventoriesArray' => $inventoriesArray,
        ])
        ?>

    </div>
</div>
