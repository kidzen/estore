<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;die;
    // var_dump(url::to(['/official-form/form-options']));die();
$script = <<<JS
// $(document).on('submit', 'form[data-pjax]', function(event) {

//   $.pjax.submit(event, '#pjax-container')
// });
$('#formgenerator-form_id').on('change', function() {
    var formId = $(this).val();
//  iv_id,year,month
    if (formId == 0) {
        $('#inventory_section').hide(800);
        $('#inventories_section').show(800);
        $('#order_section').hide(800);
        $('#timeframe_section').show(800);
        $('#month_section').show(800);
//  iv_id,year,month
    } else if (formId == 4) {
        $('#inventory_section').show(800);
        $('#inventories_section').hide(800);
        $('#order_section').hide(800);
        $('#timeframe_section').show(800);
        $('#month_section').show(800);
//  o_id,year,month
    } else if (formId == 0) {
        $('#inventory_section').hide(800);
        $('#inventories_section').hide(800);
        $('#order_section').show(800);
        $('#timeframe_section').show(800);
        $('#month_section').show(800);
//  o_id
    } else if ($.inArray(formId , ["10","11"] ) > -1) {
        // alert('success');
        $('#inventory_section').hide(800);
        $('#inventories_section').hide(800);
        $('#order_section').show(800);
        $('#timeframe_section').hide(800);
        $('#month_section').hide(800);
//  iv_id
    } else if ($.inArray(formId , ["5","8","9","17","18"] ) > -1) {
        $('#inventory_section').hide(800);
        $('#inventories_section').show(800);
        $('#order_section').hide(800);
        $('#timeframe_section').hide(800);
        $('#month_section').hide(800);
//  o_id,year
    } else if (formId == 0) {
        $('#inventory_section').hide(800);
        $('#inventories_section').hide(800);
        $('#order_section').show(800);
        $('#timeframe_section').show(800);
        $('#month_section').hide(800);
//  iv_id,year
    } else if ($.inArray(formId , ["7","14"] ) > -1) {
        $('#inventory_section').hide(800);
        $('#inventories_section').show(800);
        $('#order_section').hide(800);
        $('#timeframe_section').show(800);
        $('#month_section').hide(800);
//  year
    } else if (formId == 13) {
        $('#inventory_section').hide(800);
        $('#inventories_section').hide(800);
        $('#order_section').hide(800);
        $('#timeframe_section').show(800);
        $('#month_section').hide(800);
//  hide all
    } else {
        $('#inventory_section').hide(800);
        $('#inventories_section').hide(800);
        $('#order_section').hide(800);
        $('#timeframe_section').hide(800);
        $('#month_section').hide(800);
    }
});
JS;

/* @var $this yii\web\View */
/* @var $model common\models\OrderItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-items-form">

    <?php
    // Pjax::begin();
    $this->registerJs($script);
    ?>
    <?php $form = ActiveForm::begin(['id' => 'configform']);?>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <?php echo $form->field($model, 'form_id')->widget(kartik\select2\Select2::className(),
                    [
                    'data'          => $formId,
                        // 'theme' => 'default',
                    'options'       => ['placeholder' => 'Select an inventory ...'],
                    'pluginOptions' => [
                    'allowClear' => true,
                    ]
                    ]);
                    ?>
                </div>
            </div>
            <div class="row" id="inventory_section" style="display: none">
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'inventory_id')->widget(kartik\select2\Select2::className(), [
                        'data'          => $inventoriesArray,
    // 'theme' => 'default',
                        'options'       => ['placeholder' => 'Select an inventory ...'],
                        'pluginOptions' => [
                        'allowClear' => true,
                        ],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="row" id="inventories_section" style="display: none">
                    <div class="col-md-4">
                        <?=
                        $form->field($model, 'inventory_list')->widget(kartik\select2\Select2::className(),
                            [
                            'data'          => $inventoriesArray,
        // 'theme' => 'default',
                            'options'       => ['placeholder' => 'Select an inventory ...', 'multiple' => true],
                            'pluginOptions' => [
                            'allowClear' => true,
                            ],
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="row" id="order_section" style="display: none">
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'order_id')->widget(kartik\select2\Select2::className(),
                                [
                                'data'          => $ordersArray,
        // 'theme' => 'default',
                                'options'       => ['placeholder' => 'Select an inventory ...'],
                                'pluginOptions' => [
                                'allowClear' => true,
                                ],
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="row" id="timeframe_section" style="display: none">
                            <div class="col-md-4" id="year_section">
                                <?=
                                $form->field($model, 'YEAR')->widget(kartik\select2\Select2::className(),
                                    [
                                    'data'          => $yearList,
        // 'theme' => 'default',
                                    'options'       => ['placeholder' => 'Select an inventory ...'],
                                    'pluginOptions' => [
                                    'allowClear' => true,
                                    ],
                                    ]);
                                    ?>
                                </div>
                                <div class="col-md-4" id="month_section" style="display: none">
                                    <?=
                                    $form->field($model, 'month')->widget(kartik\select2\Select2::className(),
                                        [
                                        'data'          => $monthList,
        // 'theme' => 'default',
                                        'options'       => ['placeholder' => 'Select an inventory ...'],
                                        'pluginOptions' => [
                                        'allowClear' => true,
                                        ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div>
                                    <?=Html::a(Yii::t('app', 'Isi Semula'), ['/official-form/form-generator'], ['id'=>'reset','class' => 'btn btn-default']);
                                    ?>
                                    <?=Html::submitButton(Yii::t('app', 'Cetak'), ['class' => 'btn btn-primary', 'formtarget' => '_blank']);
                                    ?>
                                </div>
                            </div>
                            <?php ActiveForm::end();?>
                            <?php
                            // Pjax::end();
                            ?>
                        </div>
