            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $i => $item) { ?>
                        <tr>
                            <td class="text-center"><?= $i + 1 ?></td>
                            <td class="" colspan="2"><?= $item['inventory']['DESCRIPTION'] ?></td>
                            <td class="text-center" ><?= $item['RQ_QUANTITY'] ?></td>
                            <td class="text-center" ><?= $item['APP_QUANTITY'] ?></td>
                            <td class="text-center" ><?= $item['CURRENT_BALANCE'] ?></td>
                            <td class=""></td>
                        </tr>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 5; $j++) { ?>
                        <tr>
                            <td class="text-center"> <?= $j + 1 ?></td>
                            <td class="text-center" colspan="2"> </td>
                            <td class="text-center" > </td>
                            <td class="text-center" > </td>
                            <td class="text-center" > </td>
                            <td class="text-center">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 5; $j++) { ?>
                        <tr>
                            <td class="text-center"> <?= $j + 1 ?></td>
                            <td class="text-center" colspan="2"> </td>
                            <td class="text-center" > </td>
                            <td class="text-center" > </td>
                            <td class="text-center" > </td>
                            <td class="text-center">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            <tbody>
