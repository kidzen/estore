


<div class="box-body table-responsive">
    <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap kewps8">
        <!--<table bordered>-->
        <tbody>
            <tr>
                <td class="text-center no-border-bottom pull-right form-label" colspan="2">
                    <strong>KEW.PS-8</strong>
                </td>
            </tr>
            <tr>
                <td class="text-center no-border-top" colspan="2">
                    <br>&nbsp;
                    <br>&nbsp;
                    <strong>LABEL MASUK-DAHULU-KELUAR DAHULU (MDKD)</strong>
                    <br>&nbsp;
                    <br>&nbsp;
                    <br>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="text-center no-border-bottom pull-left" colspan="2"><strong>PERIHAL STOK:</strong>
                </td>
            </tr>
            <tr>
                <td class="text-center no-border-top" colspan="2">
                    <?= $item->DESCRIPTION ?><br>
                    <strong>&nbsp;</strong>
                </td>
            </tr>
            <tr>
                <td class="text-center no-border-bottom pull-left"><strong>NO.KOD:</strong>
                </td>
                <td class="text-center no-border-bottom pull-left"><strong>TARIKH DIBUAT:</strong>
                </td>
            </tr>
            <tr>
                <td class="text-center no-border-top">
                    <?= $item->CODE_NO ?><br>&nbsp;
                </td>
                <td class="text-center no-border-top">
                    <br>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="text-center no-border-bottom pull-left"><strong>NO. LOKASI STOK:</strong>
                </td>
                <td class="text-center no-border-bottom pull-left"><strong>TARIKH LUPUT:</strong>
                </td>
            </tr>
            <tr>
                <td class="text-center no-border-top">
                    <?= $item->LOCATION ?><br>&nbsp;
                </td>
                <td class="text-center no-border-top">
                    <br>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="text-center no-border-bottom pull-left"><strong>NO. PEMBUAT/PENGENALAN:</strong>
                </td>
                <td class="text-center no-border-bottom pull-left"><strong>TARIKH DITERIMA:</strong>
                </td>
            </tr>
            <tr>
                <td class="text-center no-border-top">
                    <br>&nbsp;
                </td>
                <td class="text-center no-border-top">
                    <br>&nbsp;
                </td>
            </tr>
            <tbody>
            </table>

        </div>
