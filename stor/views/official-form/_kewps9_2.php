            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $i=>$item) { ?>
                        <tr>
                            <td class="center"><?= $i + 1 ?> </td>
                            <td class="width-md"><?= $item['DESCRIPTION'] ?></td>
                            <td class="center"><?= $item['CODE_NO'] ?></td>
                            <td class="center"><?= $item['LOCATION'] ?></td>
                            <td class="center"><?= $item['QUANTITY'] ?></td>
                            <td class="center width-sm"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="width-sm">&nbsp;</td>
                        </tr>
                        <?php
                        if ($i + 1 == 5) {
                            break;
                        }
                        ?>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 5; $j++) { ?>
                        <tr>
                            <td class="center"><?= $j + 1 ?> </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 5; $j++) { ?>
                        <tr>
                            <td class="center"><?= $j + 1 ?> </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            <tbody>
