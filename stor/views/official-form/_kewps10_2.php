


<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN A</strong><br><strong>KEW.PS-10</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>BORANG PESANAN DAN PENGELUARAN STOK</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 pull-left" colspan="4"><span class="row-1-label">Daripada :</span><br><span class="row-1-data">&nbsp;</span></th>
                    <th class="col-2 pull-left" colspan="7"><span class="row-1-label">Kepada :</span><br><span class="row-1-data"></span></th>
                </tr>
                <tr>
                    <th class="text-center default col-1 row-2" colspan="4">Dilengkapkan Oleh Stor Pesanan</th>
                    <th class=" right-border-bold col-2 row-2" class="text-center default" colspan="7">Dilengkapkan Oleh Stor Pengeluar</th>
                </tr>
                <tr>
                    <th class="col-1 row-3" colspan="4">No Pemesanan : <?= $model->ORDER_NO ?></th>
                    <th class="col-2 row-3 right-border-bold" colspan="7">No Pengeluaran : <?= $model->ORDER_NO ?></th>
                </tr>
                <tr>
                    <th class="col-1 row-4" colspan="4">Tarikh Bekalan Dikehendaki : <?= $model->REQUIRED_DATE ?></th>
                    <th class="col-2 row-4 text-center default right-border-bold" colspan="5">BAHAGIAN BEKALAN,KAWALAN DAN AKAUN</th>
                    <th class="col-3 row-4 text-center default" colspan="2">BAHAGIAN SIMPANAN</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center default" rowspan="2">No. Kod</th>
                    <th class="col-2 vertical-align-top text-center text-center default" colspan="2" rowspan="2">Perihal Stok</th>
                    <th class="col-3 vertical-align-top text-center text-center default" rowspan="2">Kuantiti</th>
                    <th class="col-4 vertical-align-top text-center text-center default" colspan="2">Kad Kawalan Stok</th>
                    <th class="col-5 vertical-align-top text-center text-center default" rowspan="2">Kuantiti Diluluskan</th>
                    <th class="col-6 vertical-align-top text-center text-center default" colspan="2">Harga (RM)</th>
                    <th class="col-7 vertical-align-top text-center text-center default" rowspan="2">Kuantiti Dikeluarkan</th>
                    <th class="no-border-right text-center vertical-align-top text-center text-center default" rowspan="2">Catatan</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center default">No. Kad</th>
                    <th class="col-2 vertical-align-top text-center text-center default">Baki Sedia Ada</th>
                    <th class="col-3 vertical-align-top text-center text-center default">Seunit</th>
                    <th class="col-4 vertical-align-top text-center text-center default">Jumlah</th>
                </tr>
            </thead>
            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $i => $item) { ?>
                        <tr>
                            <td class="col-1 pull-left"><?= $item->inventory->CODE_NO ?></td>
                            <td class="width-lg" colspan="2"><?= $item->inventory->DESCRIPTION ?></td>
                            <td class="col-3 pull-left"><?= $item->RQ_QUANTITY ?></td>
                            <td class="col-4 pull-left"><?= $item->inventory->CARD_NO ?></td>
                            <td class="col-5 pull-left"><?= $item->CURRENT_BALANCE ?></td>
                            <td class="col-6 pull-left"><?= $item->APP_QUANTITY ?></td>
                            <td class="col-7 pull-left"><?= $item->UNIT_PRICE / $item->APP_QUANTITY ?></td>
                            <td class="no-border-right text-center pull-left"><?= $item->UNIT_PRICE ?></td>
                            <td class="col-9 pull-left"><?= $item->APP_QUANTITY ?></td>
                            <td class="col-10 pull-left width-sm"></td>
                        </tr>
                        <?php
                        if ($i + 1 == 4) {
                            break;
                        }
                        ?>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 4; $j++) { ?>
                        <tr>
                            <td class="col-1 pull-left"> </td>
                            <td class="col-2 pull-left" colspan="2"> </td>
                            <td class="col-3 pull-left"> </td>
                            <td class="col-4 pull-left"> </td>
                            <td class="col-5 pull-left"> </td>
                            <td class="col-6 pull-left"> </td>
                            <td class="col-7 pull-left"> </td>
                            <td class="no-border-right text-center pull-left"> </td>
                            <td class="col-9 pull-left"> </td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 3; $j++) { ?>
                        <tr>
                            <td class="col-1 pull-left"> </td>
                            <td class="col-2 pull-left" colspan="2"> </td>
                            <td class="col-3 pull-left"> </td>
                            <td class="col-4 pull-left"> </td>
                            <td class="col-5 pull-left"> </td>
                            <td class="col-6 pull-left"> </td>
                            <td class="col-7 pull-left"> </td>
                            <td class="no-border-right text-center pull-left"> </td>
                            <td class="col-9 pull-left"> </td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            <tbody>
            <tfoot>
                <tr>
                    <td class="foot-head-col-1 foot-head-row-1" colspan="2">Pegawai Pemesan :</td>
                    <td class="foot-head-col-2 foot-head-row-1" colspan="2">Pegawai Penerima :</td>
                    <td class="foot-head-col-3 foot-head-row-1" colspan="5">Tarikh Diluluskan dan Direkodkan Oleh :</td>
                    <td class="foot-head-col-4 foot-head-row-1" colspan="2">Dikeluarkan dan Direkodkan Oleh :</td>
                </tr>
                <tr>
                    <td class="no-border-right foot-row-2"><strong>Nama :</strong></td>
                    <td class="no-border-right text-center foot-row-2"><?= strtoupper($model->ORDERED_BY) ?></td>
                    <td class="no-border-right foot-row-2"><strong>Nama :</strong></td>
                    <td class="no-border-right text-center foot-row-2">&nbsp;</td>
                    <td class="no-border-right foot-row-2"><strong>Nama :</strong></td>
                    <td class="no-border-right text-center foot-row-2" colspan="4"><?= strtoupper(isset($model->approvedBy->NAME) ? $model->approvedBy->NAME : $model->approvedBy->USERNAME); ?></td>
                    <td class="no-border-right foot-row-2"><strong>Nama :</strong></td>
                    <td class="no-border-right text-center foot-row-2"><?= strtoupper(isset($model->creator->NAME) ? $model->creator->NAME : $model->creator->USERNAME); ?></td>
                </tr>
                <tr>
                    <td class="no-border-right foot-row-3"><strong>Jawatan :</strong></td>
                    <td class="no-border-right text-center foot-row-3">&nbsp;</td>
                    <td class="no-border-right foot-row-3"><strong>Jawatan :</strong></td>
                    <td class="no-border-right text-center foot-row-3">&nbsp;</td>
                    <td class="no-border-right foot-row-3"><strong>Jawatan :</strong></td>
                    <td class="no-border-right text-center foot-row-3" colspan="4"><?= strtoupper(isset($model->approvedBy->POSITION) ? $model->approvedBy->POSITION : $model->approvedBy->ROLE); ?></td>
                    <td class="no-border-right foot-row-3"><strong>Jawatan :</strong></td>
                    <td class="no-border-right text-center foot-row-3"><?= strtoupper(isset($model->creator->NAME) ? $model->creator->NAME : $model->creator->ROLE); ?></td>
                </tr>
                <tr>
                    <td class="no-border-right foot-row-4"><strong>Jabatan :</strong></td>
                    <td class="no-border-right text-center foot-row-4">&nbsp;</td>
                    <td class="no-border-right foot-row-4"><strong>Jabatan :</strong></td>
                    <td class="no-border-right text-center foot-row-4">&nbsp;</td>
                    <td class="no-border-right foot-row-4"><strong>Jabatan :</strong></td>
                    <td class="no-border-right text-center foot-row-4" colspan="4"><?= strtoupper(isset($model->approvedBy->DEPARTMENT) ? $model->approvedBy->DEPARTMENT : ''); ?></td>
                    <td class="no-border-right foot-row-4"><strong>Jabatan :</strong></td>
                    <td class="no-border-right text-center foot-row-4"><?= strtoupper(isset($model->creator->DEPARTMENT) ? $model->creator->DEPARTMENT : ''); ?></td>
                </tr>
                <tr>
                    <td class="no-border-right foot-vertical-align-top text-center"><strong>Tarikh :</strong></td>
                    <td class="no-border-right text-center foot-vertical-align-top text-center">&nbsp;</td>
                    <td class="no-border-right foot-vertical-align-top text-center"><strong>Tarikh :</strong></td>
                    <td class="no-border-right text-center foot-vertical-align-top text-center">&nbsp;</td>
                    <td class="no-border-right foot-vertical-align-top text-center"><strong>Tarikh :</strong></td>
                    <td class="no-border-right text-center foot-vertical-align-top text-center width-sm" colspan="4"><?= $model->APPROVED_AT; ?></td>
                    <td class="no-border-right foot-vertical-align-top text-center width-sm-8"><strong>Tarikh :</strong></td>
                    <td class="no-border-right text-center foot-row-4" style="width:16%;"><?= $model->CREATED_AT; ?></td>
                </tr>
                <tr>
                    <td class="foot-note-col-1 foot-note-vertical-align-top text-center" colspan="4" rowspan="8">
                        <br><br>
                        <p>Nota</p>
                        <p>Salinan 1 - Disimpan oleh pemesan</p>
                        <p>Salinan 2 - Bahagian Bekalan, Kawalan Dan Akaun</p>
                        <p>Salinan 3 - Bahagian Simpanan</p>
                        <p>Salinan 4 - Bahagian Bungkusan Dan Penghantaran</p>
                        <p>Salinan 5 – Disimpan oleh  pemesan setelah stok diterima</p>
                    </td>
                    <td class="no-border-right text-center foot-vertical-align-top text-center foot-deliver-head text-center default" colspan="7"><strong>BAHAGIAN BUNGKUSAN DAN,PENGHANTARAN</strong></td>
                </tr>
                <tr>
                    <td class="foot-head-col-1 foot-head-row-7" colspan="2" rowspan="7"><strong>Butir-Butir Bungkusan</strong></td>
                    <td class="foot-head-col-2 foot-head-row-7" colspan="3" rowspan="7"><strong>Butir-Butir Penghantaran</strong></td>
                    <td class="foot-head-col-3 foot-head-row-7" colspan="2">Telah dibungkus dan dihantar oleh :</td>
                </tr>
                <tr>
                    <td class="no-border-right foot-row-8"><strong>Nama : </strong></td>
                    <td class="no-border-right text-center foot-row-8">&nbsp;</td>
                </tr>
                <tr>
                    <td class="no-border-right foot-row-9"><strong>Jawatan : </strong></td>
                    <td class="no-border-right text-center foot-row-9">&nbsp;</td>
                </tr>
                <tr>
                    <td class="no-border-right foot-row-10"><strong>Jabatan : </strong></td>
                    <td class="no-border-right text-center foot-row-10">&nbsp;</td>
                </tr>
                <tr>
                    <td class="no-border-right foot-row-11"><strong>Tarikh : </strong></td>
                    <td class="no-border-right text-center foot-row-11">&nbsp;</td>
                </tr>
            </tfoot>
        </table>

    </div>
</div>
