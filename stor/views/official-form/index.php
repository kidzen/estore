<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
?>
<h1>official-form/index</h1>

<p>
    You may change the content of this page by modifying
    the file <code><?= __FILE__; ?></code>.
</p>
<ol>
    <li><?= Html::a('KEW PS 3',['pdf-kewps3'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 4',['pdf-kewps4'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 5',['pdf-kewps5'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 7',['pdf-kewps7'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 8',['pdf-kewps8'],['target'=>'_blank'])?></li>
    <!--<li><?= Html::a('KEW PS 82',['pdf-kewps82'],['target'=>'_blank'])?></li>-->
    <li><?= Html::a('KEW PS 9',['pdf-kewps9'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 10',['pdf-kewps10'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 11',['pdf-kewps11'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 13',['pdf-kewps13'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 14',['pdf-kewps14'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 17',['pdf-kewps17'],['target'=>'_blank'])?></li>
    <li><?= Html::a('KEW PS 18',['pdf-kewps18'],['target'=>'_blank'])?></li>
    <li><?= Html::a('Cetak Borang',['form-generator'])?></li>
</ol>
