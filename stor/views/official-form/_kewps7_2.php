
            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $i => $item) { ?>
                        <tr>
                            <td class="center"><?= $i + 1 ?> </td>
                            <td class="center"><?= $item['CODE_NO'] ?></td>
                            <td><?= $item['DESCRIPTION'] ?></td>
                            <td class="center"><?= isset($item['APPROVED_AT']) ? '(' . $item['APPROVED_BY'] . ')' . $item['APPROVED_AT'] : '' ?></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                        </tr>
                        <?php
                        if ($i + 1 == 5) {
                            break;
                        }
                        ?>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 5; $j++) { ?>
                        <tr>
                            <td class="center"><?= $j + 1 ?></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 5; $j++) { ?>
                        <tr>
                            <td class="center"><?= $j + 1 ?></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            <tbody>
