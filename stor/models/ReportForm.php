<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\InventoryCheckin;
use common\models\ActivityLog;
use common\models\KewpsForm;

/**
 * ContactForm is the model behind the contact form.
 */
class ReportForm extends Model
{
    public $form_id;
    public $kewps_id;
    public $orders_id;
    public $inventory_id;
    public $year;
    public $month;
    public $inventories_id;

    const FORM_TYPE_KEWPS = 1;
    const FORM_TYPE_FREQUENCY = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_id'], 'required'],
            [['form_id','kewps_id','inventory_id','year','month'], 'integer'],
            // ['inventories_id','orders_id', 'each', 'rule' => ['integer']],
            [['inventories_id','orders_id',], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'form_id' => Yii::t('app','Form Id'),
        ];
    }

    /**
     * Submit checkin items request.
     *
     * @param object $model object of checkin model
     * @return bool whether the transaction was succes
     */
    public static function kewpsList()
    {
        $kewpsList = [
            // 3 => 'KEW.PS-3 ()',
            4 => 'KEW.PS-4 (KAD PETAK)',
            5 => 'KEW.PS-5 (SENARAI DAFTAR KAD KAWALAN STOK)',
            7 => 'KEW.PS-7 (PENENTUAN KUMPULAN STOK)',
            8 => 'KEW.PS-8 (LABEL FIFO)',
            9 => 'KEW.PS-9 (SENARAI STOK BERTARIKH LUPUT)',
            10 => 'KEW.PS-10 (BORANG PESANAN DAN PENGELUARAN STOK)',
            11 => 'KEW.PS-11 (BORANG PEMESANAN STOK)',
            13 => 'KEW.PS-13 (LAPORAN KEDUDUKAN STOK)',
            14 => 'KEW.PS-14 (LAPORAN PEMERIKSAAN / VERIFIKASI STOR)',
            17 => 'KEW.PS-17 (PENYATA PELARASAN STOK)',
            18 => 'KEW.PS-18 (PERAKUAN AMBIL ALIH)',
        ];
        return $kewpsList;
    }

    /**
     * Submit checkin items request.
     *
     * @param object $model object of checkin model
     * @return bool whether the transaction was succes
     */
    public function generateKewps()
    {
        switch ($this->kewps_id) {
            case 4:
            KewpsForm::pdfKewps4(
                $id = $this->inventory_id,
                $year = $this->year,
                $month = $this->month,
                $printAll = 0
            );
            break;
            case 5:
            KewpsForm::pdfKewps5(
                $id = $this->inventories_id
            );
            break;
            case 7:
            KewpsForm::pdfKewps7(
                $id = $this->inventories_id,
                $year = $this->year
            );
            break;
            case 8:
            KewpsForm::pdfKewps8(
                $id = $this->inventories_id
            );
            break;
            case 9:
            KewpsForm::pdfKewps9(
                $id = $this->inventories_id
            );
            case 10:
            KewpsForm::pdfKewps10(
                $id = $this->orders_id
            );
            break;
            case 11:
            KewpsForm::pdfKewps11(
                $id = $this->orders_id
            );
            break;
            case 13:
            KewpsForm::pdfKewps13(
                $year = $this->year
            );
            break;
            case 14:
            KewpsForm::pdfKewps14(
                $id = $this->inventories_id,
                $year = $this->year
            );
            break;
            case 17:
            KewpsForm::pdfKewps17(
                $id = $this->inventories_id
            );
            case 18:
            KewpsForm::pdfKewps18(
                $id = $this->inventories_id
            );
            default:
            throw new Exception("Error Processing Request", 1);
            break;
        }
    }

    /**
     * Submit checkin items request.
     *
     * @param object $model object of checkin model
     * @return bool whether the transaction was succes
     */
    public function submit()
    {
        switch ($this->form_id) {
            case 1:
            $this->generateKewps();
            break;

            default:
            throw new Exception("Error Processing Request", 1);
            break;
        }
        if ($model->FORM_ID == 3) {
                // KewpsForm::pdfKewps4(
                //     $id = $model->INVENTORY_ID,
                //     $year = $model->YEAR,
                //     $month = $model->MONTH
                //     );
        } else if ($model->FORM_ID == 5) {
            KewpsForm::pdfKewps5(
                $id = $model->INVENTORY_LIST
            );
        } else if ($model->FORM_ID == 7) {
            KewpsForm::pdfKewps7(
                $id = $model->INVENTORY_LIST,
                $year = $model->YEAR
            );
        } else if ($model->FORM_ID == 8) {
            KewpsForm::pdfKewps8(
                $id = $model->INVENTORY_LIST
            );
        } else if ($model->FORM_ID == 9) {
            KewpsForm::pdfKewps9(
                $id = $model->INVENTORY_LIST
            );
        } else if ($model->FORM_ID == 10) {
            KewpsForm::pdfKewps10(
                $id = $model->ORDER_ID
            );

        } else if ($model->FORM_ID == 11) {
            KewpsForm::pdfKewps11(
                $id = $model->ORDER_ID
            );
        } else if ($model->FORM_ID == 13) {
            KewpsForm::pdfKewps13(
                $year = $model->YEAR
            );
        } else if ($model->FORM_ID == 14) {
            KewpsForm::pdfKewps14(
                $id = $model->INVENTORY_LIST,
                $year = $model->YEAR
            );
        } else if ($model->FORM_ID == 17) {
            KewpsForm::pdfKewps17(
                $id = $model->INVENTORY_LIST
            );
        } else if ($model->FORM_ID == 18) {
            KewpsForm::pdfKewps18(
                $id = $model->INVENTORY_LIST
            );
        }
    }
}
