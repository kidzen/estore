<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ReportForm;
use common\models\search\InventoryCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Exception;
use yii\helpers\Url;

class ReportController extends \yii\web\Controller
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index', 'view', 'create', 'update', 'delete', 'add-inventory'],
						'roles' => ['@']
					],
					[
						'allow' => true
					]
				]
			]
		];
	}

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionKewps()
	{
		$model = new ReportForm();

		if ($model->load(Yii::$app->request->post())) {
			$model->form_id = 1;
			$model->submit();
			// return $this->render('kewps',['model' => $model]);
		}


		return $this->render('kewps',['model' => $model]);
	}

	public function actionFrequency()
	{
		$model = new ReportForm();

		if ($model->load(Yii::$app->request->post())) {
			$model->form_id = 1;
			$model->submit();
			// return $this->render('kewps',['model' => $model]);
		}


		return $this->render('kewps',['model' => $model]);
	}

	public function actionYearly()
	{
		$model = new ReportForm();

		if ($model->load(Yii::$app->request->post())) {
			$model->form_id = 1;
			$model->submit();
			// return $this->render('kewps',['model' => $model]);
		}


		return $this->render('kewps',['model' => $model]);
	}

    public function actionSystemMaintence()
    {
        $searchModel = new \common\models\search\AllMaintenanceCheckSearch();
        var_dump($searchModel);die;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/all-maintenance-check/index.php', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUsage()
    {
        $searchModel = new \common\models\search\UsageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/usage/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAuditLog()
    {
        $searchModel = new \common\models\search\LogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/log/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStore()
    {
        $searchModel = new \common\models\search\StoreSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/store/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
