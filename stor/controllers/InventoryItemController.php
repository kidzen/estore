<?php

namespace frontend\controllers;

use Yii;
use common\models\InventoryItem;
use common\models\search\InventoryItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Exception;
use yii\helpers\Url;

/**
 * InventoryItemController implements the CRUD actions for InventoryItem model.
 */
class InventoryItemController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }
    /*
    public function beforeAction($action)
    {
        $toRedir = [
            'update' => 'view',
            'create' => 'view',
            'delete' => 'index',
        ];

        if (isset($toRedir[$action->id])) {
            Yii::$app->response->redirect(Url::to([$toRedir[$action->id]]), 301);
            Yii::$app->end();
        }
        return parent::beforeAction($action);
    }
    */

    /**
     * Lists all InventoryItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchActive = new InventoryItemSearch();
        $dataProviderActive = $searchActive->search(Yii::$app->request->queryParams);
        $dataProviderActive->query->andWhere(['inventory_item.checkout_id'=>null]);
        $dataProviderActive->query->andWhere(['inventory_item.dispose_id'=>null]);

/*        $searchCheckouted = new InventoryItemSearch();
        $dataProviderCheckouted = $searchCheckouted->search(Yii::$app->request->queryParams);
        $dataProviderCheckouted->query->andWhere(['not',['inventory_item.checkout_id'=>null]]);

        $searchDisposed = new InventoryItemSearch();
        $dataProviderDisposed = $searchDisposed->search(Yii::$app->request->queryParams);
        $dataProviderDisposed->query->andWhere(['not',['inventory_item.dispose_id'=>null]]);

        $searchDeleted = new InventoryItemSearch();
        $dataProviderDeleted = $searchDeleted->search(Yii::$app->request->queryParams);
        $dataProviderDeleted->query->where(['not',['inventory_item.deleted_by'=>0]]);
*/
/*        return $this->render('index-all', [
            'searchActive' => $searchActive,
            'dataProviderActive' => $dataProviderActive,
            'searchCheckouted' => $searchCheckouted,
            'dataProviderCheckouted' => $dataProviderCheckouted,
            'searchDisposed' => $searchDisposed,
            'dataProviderDisposed' => $dataProviderDisposed,
            'searchDeleted' => $searchDeleted,
            'dataProviderDeleted' => $dataProviderDeleted,
        ]);*/
        return $this->render('index', [
            'searchModel' => $searchActive,
            'dataProvider' => $dataProviderActive,
        ]);
    }

    /**
     * Displays a single InventoryItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new InventoryItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InventoryItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing InventoryItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InventoryItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);
            if($model->checkout_id) {
                throw new Exception(Yii::t('app',"This item has been checkouted"), 1);
            }
            if($model->dispose_id) {
                throw new Exception(Yii::t('app',"This item has been disposed"), 1);
            }
            $model->deleted_by = Yii::$app->user->id;
            $model->deleted_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
            $model->save();

            Yii::$app->notify->success();
        } catch (Exception $e) {
            Yii::$app->notify->fail(['message' => $e->getMessage()]);
        }
        return $this->redirect(['index']);

    }

    /**
     * Deletes an existing InventoryItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRecover($id)
    {
        $model = $this->findModel($id);
        $model->deleted_by = 0;
        $model->deleted_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Permanently deletes an existing InventoryItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletePermanent($id)
    {

        $model = $this->findModel($id);
        if($model->deleted_by != 0) {
            if($model->delete()) {
                Yii::$app->notify->success();
                return $this->redirect(['index']);
            }
        }
        Yii::$app->notify->fail();
        return $this->redirect(['index']);
    }


    /**
     * Finds the InventoryItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InventoryItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InventoryItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
