<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AllMaintenanceCheck */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'All Maintenance Check',
]) . ' ' . $model->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Maintenance Check'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', ]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="all-maintenance-check-update">
	<div class="panel panel-primary">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
