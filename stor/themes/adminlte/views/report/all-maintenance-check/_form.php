<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AllMaintenanceCheck */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="all-maintenance-check-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'type')->textInput(['maxlength' => true, 'placeholder' => 'Type']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'table_name')->textInput(['maxlength' => true, 'placeholder' => 'Table Name']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'col_name')->textInput(['maxlength' => true, 'placeholder' => 'Col Name']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'data_id')->textInput(['placeholder' => 'Data']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'fault_value')->textInput(['maxlength' => true, 'placeholder' => 'Fault Value']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'true_value')->textInput(['maxlength' => true, 'placeholder' => 'True Value']) ?>
</div>

    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
