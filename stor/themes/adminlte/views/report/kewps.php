<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model common\models\Profile */

$this->title = Yii::t('app', 'KEWPS Report');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profile'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<?php

    // var_dump(url::to(['/official-form/form-options']));die();
$script = <<<JS
// $(document).on('submit', 'form[data-pjax]', function(event) {

//   $.pjax.submit(event, '#pjax-container')
// });
$('#reportform-kewps_id').on('change', function() {
	var formId = $(this).val();
//  iv_id,year,month
	if (formId == 0) {
		$('#inventory_section').hide(800);
		$('#inventories_section').show(800);
		$('#order_section').hide(800);
		$('#year_section').show(800);
		$('#month_section').show(800);
		$('#kewps4_hint').hide(800);
//  iv_id,year,month
	} else if (formId == 4) {
		$('#inventory_section').show(800);
		$('#inventories_section').hide(800);
		$('#order_section').hide(800);
		$('#year_section').show(800);
		$('#month_section').show(800);
		$('#kewps4_hint').show(800);
//  o_id,year,month
	} else if (formId == 0) {
		$('#inventory_section').hide(800);
		$('#inventories_section').hide(800);
		$('#order_section').show(800);
		$('#year_section').show(800);
		$('#month_section').show(800);
		$('#kewps4_hint').hide(800);
//  o_id
	} else if ($.inArray(formId , ["10","11"] ) > -1) {
        // alert('success');
		$('#inventory_section').hide(800);
		$('#inventories_section').hide(800);
		$('#order_section').show(800);
		$('#year_section').hide(800);
		$('#month_section').hide(800);
		$('#kewps4_hint').hide(800);
//  iv_id
	} else if ($.inArray(formId , ["5","8","9","17","18"] ) > -1) {
		$('#inventory_section').hide(800);
		$('#inventories_section').show(800);
		$('#order_section').hide(800);
		$('#year_section').hide(800);
		$('#month_section').hide(800);
		$('#kewps4_hint').hide(800);
//  o_id,year
	} else if (formId == 0) {
		$('#inventory_section').hide(800);
		$('#inventories_section').hide(800);
		$('#order_section').show(800);
		$('#year_section').show(800);
		$('#month_section').hide(800);
		$('#kewps4_hint').hide(800);
//  iv_id,year
	} else if ($.inArray(formId , ["7","14"] ) > -1) {
		$('#inventory_section').hide(800);
		$('#inventories_section').show(800);
		$('#order_section').hide(800);
		$('#year_section').show(800);
		$('#month_section').hide(800);
		$('#kewps4_hint').hide(800);
//  year
	} else if (formId == 13) {
		$('#inventory_section').hide(800);
		$('#inventories_section').hide(800);
		$('#order_section').hide(800);
		$('#year_section').show(800);
		$('#month_section').hide(800);
		$('#kewps4_hint').hide(800);
//  hide all
	} else {
		$('#inventory_section').hide(800);
		$('#inventories_section').hide(800);
		$('#order_section').hide(800);
		$('#year_section').hide(800);
		$('#month_section').hide(800);
		$('#kewps4_hint').hide(800);
	}
});
JS;
$this->registerJs($script);

/* @var $this yii\web\View */
/* @var $model common\models\OrderItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-items-form">

	<div class="profile-create">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
			</div>
			<div class="panel-body">

				<?php $form = ActiveForm::begin();
				?>

				<?= $form->errorSummary($model); ?>

				<div class="col-md-4">
					<?= $form->field($model, 'kewps_id')->widget(\kartik\widgets\Select2::classname(), [
						'data' => $model->className()::kewpsList(),
						'options' => ['placeholder' => Yii::t('app', 'Choose KEWPS ID')],
						'pluginOptions' => [
							'allowClear' => true
						],
					]);
					?>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-4" id="inventory_section" style="display: none">
					<?=
					$form->field($model, 'inventory_id')->widget(kartik\select2\Select2::className(), [
						'data'          => \common\models\Inventory::arrayList(),
						'options'       => ['placeholder' => 'Select an inventory ...'],
						'pluginOptions' => [
							'allowClear' => true,
						],
					]);
					?>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-4" id="inventories_section" style="display: none">
					<?=
					$form->field($model, 'inventories_id')->widget(kartik\select2\Select2::className(), [
						'data'          => \common\models\Inventory::arrayList(),
						'options'       => ['placeholder' => 'Select an inventory ...', 'multiple' => true],
						'pluginOptions' => [
							'allowClear' => true,
						],
					]);
					?>
				</div>
				<div class="col-md-4" id="order_section" style="display: none">
					<?=
					$form->field($model, 'orders_id')->widget(kartik\select2\Select2::className(), [
						'data'          => \common\models\Order::arrayList(),
						'options'       => ['placeholder' => 'Select an order no ...', 'multiple' => true],
						'pluginOptions' => [
							'allowClear' => true,
						],
					]);
					?>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-3" id="year_section" style="display: none">
					<?= $form->field($model, 'year')->widget(\kartik\widgets\DatePicker::classname(), [
						'type' => 2,
						'options' => [
							'placeholder' => Yii::t('app', 'Choose KEWPS ID')
						],
						'pluginOptions' => [
							'allowClear' => true,
							'format' => 'yyyy',
							// 'todayBtn' => true,
							// 'daysOfWeekHighlighted' => true,
							'maxViewMode' => 'decade',
							'minViewMode' => 'decade',
						],
					]);
					?>
				</div>
				<div class="col-md-3" id="month_section" style="display: none">
					<?= $form->field($model, 'month')->widget(\kartik\widgets\DatePicker::classname(), [
						'type' => 2,
						'options' => [
							'placeholder' => Yii::t('app', 'Choose KEWPS ID')
						],
						'pluginOptions' => [
							'allowClear' => true,
							'format' => 'm',
							// 'todayBtn' => true,
							// 'defaultViewDate' => 'year',
							'maxViewMode' => 'year',
							'minViewMode' => 'year',
						],
					]);
					?>
				</div>
				<div class="clearfix"></div>
				<dl class="col-md-6" id="kewps4_hint" style="display: none">
					<dt>Nota</dt>
					<dd>Kosongkan Tahun dan Bulan untuk cetak semua</dd>
					<dd>Kosongkan Bulan untuk cetak data tahunan</dd>
					<dd>Isi Tahun dan Bulan untuk cetak data bulanan</dd>
				</dl>
				<div class="clearfix"></div>
				<div class="col-md-4">
					<div class="form-group">
						<?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-success', 'formtarget' => '_blank']) ?>
						<?= Html::a(Yii::t('app', 'Reset'), ['/report/kewps'], ['id'=>'reset','class' => 'btn btn-default']); ?>
						<?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
					</div>
				</div>


			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
</div>
