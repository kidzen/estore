


<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN B</strong><br><strong>KEW.PS-9</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>SENARAI STOK BERTARIKH LUPUT</strong></p>
        <p ><strong>Kementerian/Jabatan : </strong></p>
        <p ><strong>Kategori Stor : </strong></p>
        <p ><strong>Bulan : </strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Bil</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Perihal Stok</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">No. Kod</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Lokasi Stok</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Kuantiti Terimaan</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Tarikh Luput</th>
                    <th class="col-2 vertical-align-top text-center text-center info" colspan="6">Baki Stok Bagi 6 Bulan Sebelum Tamat Tempoh Luput<br>*(Kuantiti dan Tarikh Kemaskini)</th>
                    <th class="col-2 vertical-align-top text-center text-center info" rowspan="2">Catatan</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info">6 bulan</th>
                    <th class="col-1 vertical-align-top text-center text-center info">5 bulan</th>
                    <th class="col-1 vertical-align-top text-center text-center info">4 bulan</th>
                    <th class="col-1 vertical-align-top text-center text-center info">3 bulan</th>
                    <th class="col-1 vertical-align-top text-center text-center info">2 bulan</th>
                    <th class="col-1 vertical-align-top text-center text-center info">1 bulan</th>
                </tr>
            </thead>
