


<!--<div>
    <p class="pull-right form-lampiran"><strong>LAMPIRAN B</strong><br><strong>KEW.PS-7</strong></p>
    <div class="clearfix"></div>
    <p class="text-center form-name"><strong>PENENTUAN KUMPULAN STOK</strong></p>
    <p ><strong>Seksyen : ....</strong></p>
</div>-->
<div class="box-body table-responsive">
    <!--<htmlpageheader name="header1">-->
    <p class="pull-right form-lampiran"><strong>LAMPIRAN B</strong><br><strong>KEW.PS-7</strong></p>
    <p class="text-center form-name"><strong>PENENTUAN KUMPULAN STOK</strong></p>
    <p ><strong>Seksyen : ....</strong></p>
    <!--</htmlpageheader>-->
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Bil</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">No. Kod</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Perihal Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info" colspan="2">Jumlah Nilai Pembelian bagi 2 Tahun Lepas</th>
                    <th class="col-2 vertical-align-top text-center text-center info" rowspan="2">Purata Nilai Pembelian</th>
                    <th class="no-border-right text-center vertical-align-top text-center text-center info" rowspan="2">Peratusan</th>
                    <th class="no-border-right text-center vertical-align-top text-center text-center info" rowspan="2">Kumpulan Barang A atau B</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info"><?= $year1 ?>(RM)</th>
                    <th class="col-2 vertical-align-top text-center text-center info"><?= $year2 ?>(RM)</th>
                </tr>
            </thead>
