
            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $i => $item) { ?>
                        <tr>
                            <td class="text-center"><?= $i + 1 ?> </td>
                            <td class="text-center"><?= $item['code_no'] ?></td>
                            <td><?= $item['description'] ?></td>
                            <td class="text-center"><?= $item[$year1] ?></td>
                            <td class="text-center"><?= $item[$year2] ?></td>
                            <td class="text-center"><?= $item['avg_unit_price'] ?></td>
                            <td class="text-center"><?= $item['total_buy_percentage'] ?></td>
                            <td class="text-center">
                                <?php
                                if ($item['total_buy_percentage'] <= 30) {
                                    echo 'A';
                                } else {
                                    echo 'B';
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                        if ($i + 1 == 5) {
                            break;
                        }
                        ?>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 5; $j++) { ?>
                        <tr>
                            <td class="text-center"><?= $j + 1 ?></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 5; $j++) { ?>
                        <tr>
                            <td class="text-center"><?= $j + 1 ?></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            <tbody>
