<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\InventorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inventory-search">
    <div class="row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]);
        ?>

        <div class="col-md-4">
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'category_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\InventoryCategory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Inventory category')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'card_no')->textInput(['maxlength' => true, 'placeholder' => 'Card No']) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'code_no')->textInput(['maxlength' => true, 'placeholder' => 'Code No']) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

    </div>

    <?php /* echo $form->field($model, 'quantity')->textInput(['placeholder' => 'Quantity']) */ ?>

    <?php /* echo $form->field($model, 'min_stock')->textInput(['placeholder' => 'Min Stock']) */ ?>

    <?php /* echo $form->field($model, 'location')->textInput(['maxlength' => true, 'placeholder' => 'Location']) */ ?>

    <?php /* echo $form->field($model, 'approved')->widget(\kartik\widgets\Select2::classname(), [
            'data' => [1 => 'Approved', 0 => 'Inactive', 2 => 'Pending', 8 => 'Rejected', ],
            'options' => ['placeholder' => 'Select approved ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); */ ?>

    <?php /* echo $form->field($model, 'status')->widget(\kartik\widgets\Select2::classname(), [
            'data' => [1 => 'Active', 0 => 'Inactive'],
            'options' => ['placeholder' => 'Select status ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); */ ?>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
