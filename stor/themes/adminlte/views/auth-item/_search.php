<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AuthItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-auth-item-search">
    <div class="row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]);
        ?>

        <div class="col-md-4">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'type')->textInput(['placeholder' => 'Type']) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'rule_name')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\AuthRule::find()->orderBy('name')->asArray()->all(), 'name', 'name'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Auth rule')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'data')->textInput(['placeholder' => 'Data']) ?>

    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
