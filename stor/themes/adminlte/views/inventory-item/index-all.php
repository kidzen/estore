<?php
use kartik\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Active')),
        'content' => $this->render('index', [
            'searchModel' => $searchActive,
            'dataProvider' => $dataProviderActive,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Checkouted')),
        'content' => $this->render('index', [
            'searchModel' => $searchCheckouted,
            'dataProvider' => $dataProviderCheckouted,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Disposed')),
        'content' => $this->render('index', [
            'searchModel' => $searchDisposed,
            'dataProvider' => $dataProviderDisposed,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Deleted')),
        'content' => $this->render('index', [
            'searchModel' => $searchDeleted,
            'dataProvider' => $dataProviderDeleted,
        ]),
    ],
];
echo TabsX::widget([
    'items' => $items,
    // 'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    // 'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
