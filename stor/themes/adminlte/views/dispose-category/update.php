<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DisposeCategory */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dispose Category',
]) . ' ' . $model->name;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispose Category'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dispose-category-update">
	<div class="panel panel-danger">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
