<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DisposeDetailKey */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dispose Detail Key',
]) . ' ' . $model->key;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispose Detail Key'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->key, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dispose-detail-key-update">
	<div class="panel panel-danger">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
