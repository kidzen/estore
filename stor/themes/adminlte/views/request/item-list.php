<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\InventoryItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Inventory Items');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="inventory-items-index">

    <?php Pjax::begin(); ?>            <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            ['content' =>
                Html::a('<i class="glyphicon glyphicon-arrow-left"></i>', Yii::$app->request->referrer, ['class' => 'btn btn-success', 'title' => Yii::t('app', 'Kembali ke menu utama'),]) //. ' ' .
                // Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => Yii::t('app', 'Create inventory-items'),])//. ' ' .
            //                Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) //. ' ' .
            //                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => Yii::t('app', 'Add Book'), 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' ' .
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
        // 'exportConfig' => [
        //     'pdf' => $pdf,
        //     'csv' => '{csv}',
        //     'xls' => '{xls}',
        // ],
        'export' => [
            'fontAwesome' => true,
            'target' => '_self',
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //        'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => $this->title,
        ],
        'responsiveWrap' => false,
        'persistResize' => false,
        //        'exportConfig' => $exportConfig,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // [
            //     'attribute' => 'id',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            // [
            //     'attribute' => 'inventory.description',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            // [
            //     'attribute' => 'checkin_id',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            // [
            //     'attribute' => 'checkout_id',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            [
                'attribute' => 'sku',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'attribute' => 'unit_price',
                'hAlign' => 'center', 'vAlign' => 'middle',
                'format' => ['decimal', 2],
            ],
            // [
            //     'attribute' => 'deleted_by',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('role_officer_store'),
                'template' => '{view}',
                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['inventory-items/view', 'id' => $model->id], ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip']);
                    },
                            'recover' => function ($url, $model) {
                        if ($model->deleted_by) {
                            return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip','data-method' => 'POST']);
                        }
                    },
                            'delete' => function ($url, $model) {
                        if (!$model->deleted_by) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                                        'data-method' => 'post']);
                        }
                    },
                        ],
                    ],
                    [
                        'header' => 'Permanent Delete',
                        'class' => 'kartik\grid\ActionColumn',
                        'template' => '{delete-permanent}',
                        'buttons' => [
                            'delete-permanent' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                                            'title' => Yii::t('yii', 'Permanent Delete'), 'data-toggle' => 'tooltip','data-method' => 'POST'
                                ]);
                            }
                                ],
                                'visible' => Yii::$app->user->can('admin'),
                            ],
                        ],
                    ]);
                    ?>
                    <?php Pjax::end(); ?></div>
