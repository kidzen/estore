<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Order');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";
$this->registerJs($search);
?>
<div class="order-index">

    <div class="search-form" style="display:none">
        <?=  $this->render('/order/_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('/order/_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true,
            'visible' => false
        ],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'transaction_id',
            'label' => Yii::t('app', 'Transaction'),
            'value' => function($model){
                if ($model->transaction)
                    {return $model->transaction->id;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Transaction::find()->asArray()->all(), 'id', 'id'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Transaction', 'id' => 'grid-order-search-transaction_id']
        ],
        'instruction_id',
        'order_date',
        [
            'attribute' => 'ordered_by',
            'label' => Yii::t('app', 'Ordered By'),
            'value' => function($model){
                if ($model->orderedBy)
                    {return $model->orderedBy->username;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-order-search-ordered_by']
        ],
        'ordered_by_temp',
        'order_no',
        [
            'attribute' => 'usage_id',
            'label' => Yii::t('app', 'Usage'),
            'value' => function($model){
                if ($model->usage)
                    {return $model->usage->id;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Usage::find()->asArray()->all(), 'id', 'id'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Usage', 'id' => 'grid-order-search-usage_id']
        ],
        'required_date',
        'checkout_date',
        [
            'attribute' => 'checkout_by',
            'label' => Yii::t('app', 'Checkout By'),
            'value' => function($model){
                if ($model->checkoutBy)
                    {return $model->checkoutBy->username;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-order-search-checkout_by']
        ],
        'approved',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?= DynaGrid::widget([
        'columns'=>$gridColumn,
        'storage'=>DynaGrid::TYPE_COOKIE,
        'theme'=>'panel-primary',
        'showPersonalize'=>true,
        'gridOptions'=>[
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            // 'showPageSummary'=>true,
            //'floatHeader'=>true,
            'responsiveWrap'=>false,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-order']],
            'panel' => [
                'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
                'before' =>  '<div style="padding-top: 7px;"><em>* The table header sticks to the top in this demo as you scroll</em></div>',
                'after' => false,
            ],
            // 'export' => false,
            // your toolbar can include the additional full export menu
            'toolbar' => [
                ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title'=>Yii::t('app', 'Create Order')]) . ' '.
                Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button'])
            ],
            ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
            '{export}',
            '{toggleData}',
            'exportConfig' => [
                    // ExportMenu::FORMAT_PDF => false
            ]
        ],
    ],
        'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
        ]); ?>

    </div>
