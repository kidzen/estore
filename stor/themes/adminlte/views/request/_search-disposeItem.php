<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\InventoryItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inventory-item-search">

    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => ['dispose'],
            'method' => 'get',
        ]);
        ?>
        <div class="col-md-4">
            <?= $form->field($model, 'inventory_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \common\models\Inventory::arrayList(),
                'options' => ['placeholder' => Yii::t('app', 'Choose Inventory')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-md-4">
            <?php  echo $form->field($model, 'sku')->textInput(['maxlength' => true, 'placeholder' => 'Sku'])  ?>
        </div>

        <div class="col-md-3">
            <?php  echo $form->field($model, 'unit_price')->textInput(['maxlength' => true, 'placeholder' => 'Unit Price'])  ?>
        </div>

        <?php /* echo $form->field($model, 'status')->textInput(['placeholder' => 'Status']) */ ?>
        <div class="clearfix"></div>
        <div class="col-md-3">

            <div class="form-group">
                <?= Html::a(Yii::t('app', 'Search'),['request/dispose'], [
                    'title' => Yii::t('app', 'Search'),
                    'class' => 'btn btn-primary',
                    'onclick'=>"$('#pjaxload').dialog('open');//for jui dialog in my page
                    $.ajax({
                        type     :'POST',
                        cache    : false,
                        url  : 'request/dispose',
                        success  : function(response) {
                            $('#close').html(response);
                        }
                    });return false;",
                ]);
                ?>
                <?= Html::button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        <div class="clearfix"></div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

