<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Dispose */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispose'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dispose-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Dispose').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'refference_no',
        [
                'attribute' => 'transaction.id',
                'label' => Yii::t('app', 'Transaction'),
            ],
        [
                'attribute' => 'category.name',
                'label' => Yii::t('app', 'Category'),
            ],
        [
                'attribute' => 'method.name',
                'label' => Yii::t('app', 'Method'),
            ],
        [
                'attribute' => 'reason.name',
                'label' => Yii::t('app', 'Reason'),
            ],
        'quantity',
        'current_revenue',
        'cost',
        'returns',
        'description',
        'requested_at',
        [
                'attribute' => 'requestedBy.username',
                'label' => Yii::t('app', 'Requested By'),
            ],
        'disposed_at',
        [
                'attribute' => 'disposedBy.username',
                'label' => Yii::t('app', 'Disposed By'),
            ],
        'approved',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <?php if ($model->transaction) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">Transaction<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnTransaction = [
        ['attribute' => 'id', 'visible' => false],
        'store_id',
        'type',
        'check_date',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->transaction,
        'attributes' => $gridColumnTransaction    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->category) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">DisposeCategory<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnDisposeCategory = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->category,
        'attributes' => $gridColumnDisposeCategory    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->method) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">DisposeMethod<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnDisposeMethod = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->method,
        'attributes' => $gridColumnDisposeMethod    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->reason) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">DisposeReason<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnDisposeReason = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->reason,
        'attributes' => $gridColumnDisposeReason    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->updatedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->createdBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->requestedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->requestedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->disposedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->disposedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->approvedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->approvedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>

        <div class="col-sm-12">
<?php
if($providerInventoryItem->totalCount){
    $gridColumnInventoryItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'inventory.description',
                'label' => Yii::t('app', 'Inventory')
            ],
            [
                'attribute' => 'checkin.id',
                'label' => Yii::t('app', 'Checkin')
            ],
            [
                'attribute' => 'checkout.id',
                'label' => Yii::t('app', 'Checkout')
            ],
                        'sku',
            'unit_price',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInventoryItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inventory-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Inventory Item')),
        ],
        'export' => false,
        'columns' => $gridColumnInventoryItem
    ]);
}
?>

        </div>
    </div>
    </div>
    </div>
    </div>
</div>
