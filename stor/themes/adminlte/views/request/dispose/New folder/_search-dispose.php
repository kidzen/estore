<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\DisposeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-dispose-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'refference_no')->textInput(['maxlength' => true, 'placeholder' => 'Refference No']) ?>

    <?= $form->field($model, 'transaction_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\Transaction::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Transaction')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 ?>

    <?= $form->field($model, 'category_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeCategory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Dispose category')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 ?>

    <?= $form->field($model, 'method_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeMethod::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Dispose method')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 ?>

    <?php /* echo $form->field($model, 'reason_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeReason::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Dispose reason')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 */ ?>

    <?php /* echo $form->field($model, 'quantity')->textInput(['placeholder' => 'Quantity']) */ ?>

    <?php /* echo $form->field($model, 'current_revenue')->textInput(['placeholder' => 'Current Revenue']) */ ?>

    <?php /* echo $form->field($model, 'cost')->textInput(['placeholder' => 'Cost']) */ ?>

    <?php /* echo $form->field($model, 'returns')->textInput(['placeholder' => 'Returns']) */ ?>

    <?php /* echo $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) */ ?>

    <?php /* echo $form->field($model, 'requested_at')->textInput(['placeholder' => 'Requested At']) */ ?>

    <?php /* echo $form->field($model, 'requested_by')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
                'options' => ['placeholder' => Yii::t('app', 'Choose User')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 */ ?>

    <?php /* echo $form->field($model, 'disposed_at')->textInput(['placeholder' => 'Disposed At']) */ ?>

    <?php /* echo $form->field($model, 'disposed_by')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
                'options' => ['placeholder' => Yii::t('app', 'Choose User')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 */ ?>

    <?php /* echo $form->field($model, 'approved')->textInput(['placeholder' => 'Approved']) */ ?>

    <?php /* echo $form->field($model, 'status')->textInput(['placeholder' => 'Status']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
