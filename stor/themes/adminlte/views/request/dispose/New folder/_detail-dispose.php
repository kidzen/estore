<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Dispose */

?>
<div class="dispose-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->description) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'refference_no',
        [
                'attribute' => 'transaction.id',
                'label' => Yii::t('app', 'Transaction'),
            ],
        [
                'attribute' => 'category.name',
                'label' => Yii::t('app', 'Category'),
            ],
        [
                'attribute' => 'method.name',
                'label' => Yii::t('app', 'Method'),
            ],
        [
                'attribute' => 'reason.name',
                'label' => Yii::t('app', 'Reason'),
            ],
        'quantity',
        'current_revenue',
        'cost',
        'returns',
        'description',
        'requested_at',
        [
                'attribute' => 'requestedBy.username',
                'label' => Yii::t('app', 'Requested By'),
            ],
        'disposed_at',
        [
                'attribute' => 'disposedBy.username',
                'label' => Yii::t('app', 'Disposed By'),
            ],
        'approved',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>