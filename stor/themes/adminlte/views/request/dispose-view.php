<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Dispose */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispose'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dispose-view">

    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title"><?= Yii::t('app', 'Dispose').' '. Html::encode($this->title) ?></h2>
                </div>
                <div class="box-body">
                    <div class="col-sm-4">
                        <?= Html::a(Yii::t('app', 'Back'), [Yii::$app->request->referrer], ['class' => 'btn btn-primary']) ?>
                        <?php if(!$model->approved) : ?>
                            <?= Html::a(Yii::t('app', 'Update'), ['dispose-approval', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?php endif; ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['dispose-delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>

                    <div class="col-sm-12">
                        <?php
                        $gridColumn = [
                            ['attribute' => 'id', 'visible' => false],
                            'refference_no',
                            [
                                'attribute' => 'transaction.id',
                                'label' => Yii::t('app', 'Transaction'),
                            ],
                            [
                                'attribute' => 'category.name',
                                'label' => Yii::t('app', 'Category'),
                            ],
                            [
                                'attribute' => 'method.name',
                                'label' => Yii::t('app', 'Method'),
                            ],
                            [
                                'attribute' => 'reason.name',
                                'label' => Yii::t('app', 'Reason'),
                            ],
                            'quantity',
                            'current_revenue',
                            'cost',
                            'returns',
                            'description',
                            'requested_at',
                            [
                                'attribute' => 'requestedBy.username',
                                'label' => Yii::t('app', 'Requested By'),
                            ],
                            'disposed_at',
                            [
                                'attribute' => 'disposedBy.username',
                                'label' => Yii::t('app', 'Disposed By'),
                            ],
                            'approved',
                            'status',
                        ];
                        echo DetailView::widget([
                            'model' => $model,
                            'attributes' => $gridColumn
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-12">
                        <?php
                        if($providerInventoryItem->totalCount){
                            $gridColumnInventoryItem = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                [
                                    'attribute' => 'inventory.description',
                                    'label' => Yii::t('app', 'Inventory')
                                ],
                                [
                                    'attribute' => 'checkin.id',
                                    'label' => Yii::t('app', 'Checkin')
                                ],
                                [
                                    'attribute' => 'checkout.id',
                                    'label' => Yii::t('app', 'Checkout')
                                ],
                                'sku',
                                'unit_price',
                                'status',
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerInventoryItem,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inventory-item']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Inventory Item')),
                                ],
                                'export' => false,
                                'columns' => $gridColumnInventoryItem
                            ]);
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
