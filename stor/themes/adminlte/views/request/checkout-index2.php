<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
$actions = [
    'kewps10' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span> '.Yii::t('yii', 'KEW-PS-10'), ['/request/print-kewps10','id'=>$model['order']['id']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']),['class'=>'text-center']);
        }
    },
    'kew2' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-file" style="color:green;"> </span> '.Yii::t('yii', 'KEW-PS-10'), $url, ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'checkout-view' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-eye-open"> </span> '.Yii::t('yii', 'View KEW-PS-10'), ['checkout-view', 'id' => $model['order']['id']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'smart-approve' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-check" style="color:maroon;"> </span> '.Yii::t('yii', 'FIFO Approval'), ['/request/smart-approve', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'FIFO Approval'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'approval' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-check" style="color:green;"> </span> '.Yii::t('yii', 'Approval'), ['approval', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'Approval'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'reject' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"> </span> '.Yii::t('yii', 'Reject'), ['reject', 'id' => $model['order']['id']], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'item-list' => function ($url, $model) {
        return Html::tag('div',Html::a('<span class="glyphicon glyphicon-list"> </span> '.Yii::t('yii', 'Item List'), $url, ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
    },
    'update' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model->order->approved === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-pencil"> </span> '.Yii::t('yii', 'Update'), ['/request/update-checkout','id'=>$model['order']['id']], ['title' => Yii::t('yii', 'Update'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'recover' => function ($url, $model) {
        if ($model->deleted_by === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-refresh"> </span> '.Yii::t('yii', 'Recover'), $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip', 'data-method' => 'POST']),['class'=>'text-center']);;
        }
    },
    'delete' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model->order->approved === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-trash"> </span> '.Yii::t('yii', 'Delete'), $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                'data-method' => 'post']),['class'=>'text-center']);;
        }
    },
];

$this->title = Yii::t('app', 'Order');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";
$this->registerJs($search);
?>
<div class="order-index">

    <div class="search-form" style="display:none">
        <?php  /*$this->render('/order-item/_search', ['model' => $searchModel]);*/ ?>
        <?=  $this->render('_search-checkout2', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        // [
        //     'class' => 'kartik\grid\ExpandRowColumn',
        //     'width' => '50px',
        //     'value' => function ($model, $key, $index, $column) {
        //         return GridView::ROW_COLLAPSED;
        //     },
        //     'detail' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_expand-checkout', ['model' => $model]);
        //     },
        //     'headerOptions' => ['class' => 'kartik-sheet-style'],
        //     'expandOneOnly' => true
        // ],
        [
            'attribute' => 'order.order_no',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'value' => function($model) {
                return 'NO PESANAN: ' . $model->order->order_no;
            },
            'group' => true,
            'groupedRow' => true, // move grouped column to a single grouped row
            'groupOddCssClass' => 'kv-grouped-row', // configure odd group cell css class
            'groupEvenCssClass' => 'kv-grouped-row',
            // 'groupEvenCssClass' => 'maroon',
        ],
        [
            'attribute' => 'id',
            'visible' => false,
            'hAlign' => 'center',
            'vAlign' => 'middle',
        ],
        [
            'attribute'=>'inventoriesDetail',
            'label' => Yii::t('app','Inventories Detail'),
            'format' => 'raw',
            // 'hAlign' => 'center',
            // 'vAlign' => 'middle',
        ],
        [
            'attribute' => 'order.transaction_id',
            'label' => Yii::t('app', 'Transaction'),
            'value' => function($model){
                if ($model->order->transaction)
                    {return $model->order->transaction->id;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Transaction::find()->asArray()->all(), 'id', 'id'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Transaction', 'id' => 'grid-order-search-transaction_id'],
            'visible'=>false,
            'hAlign' => 'center',
            'vAlign' => 'middle',
        ],
        [
            'attribute'=>'order.instruction_id',
            'label' => Yii::t('app','Instruction Order Id'),
            'format' => 'raw',
            'value' => function($model) {
                return isset($model->order->instruction_id) ? Html::a($model->order->instruction_id,['/sisken/view','id'=>$model->order->instruction_id]) : null;
            },
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'visible'=>false,
        ],
/*        [
            'attribute'=>'ordersDetail',
            'label' => Yii::t('app','Orders Detail'),
            'format' => 'raw',
            'value' => function($model) {
                $instructionOrders = isset($model->order->instruction_id) ? Html::a($model->order->instruction_id,['/sisken/view','id'=>$model->order->instruction_id]) : null;
                $instructionOrders = Html::tag('div',Yii::t('app','Instruction Order').' : '.$instructionOrders);
                $instructionOrders = isset($model->order->instruction_id) ? $instructionOrders : null;
                $requestQuantitiesDetail = $model->requestQuantitiesDetail;
                return $instructionOrders . $requestQuantitiesDetail;
            },
            'hAlign' => 'center',
            'vAlign' => 'middle',
        ],*/
        [
            'attribute'=>'requestQuantitiesDetail',
            'label' => Yii::t('app','Quantity'),
            'format' => 'raw',
            'hAlign' => 'center',
            'vAlign' => 'middle',
        ],
        [
            'attribute'=>'order.order_date',
            'format' => 'raw',
            'value' => function($model){
                $date = Html::tag('div',$model->order->order_date);
                // $orderedBy = isset($model->order->orderedBy) ? Html::tag('div', Yii::t('app','Ordered By : ') . $model->order->orderedBy->profile->name : null;
                // $date = isset($model->order->order_date) ? $model->order->order_date : null;
                $orderedBy = isset($model->order->orderedBy) ? $model->order->orderedBy->profile->name : null;
                return $date.$orderedBy;
            },
            'hAlign' => 'center',
            'vAlign' => 'middle',
        ],
        [
            'attribute'=>'order.required_date',
            'format' => 'raw',
            // 'width' => '50px',
            'hAlign' => 'center',
            'vAlign' => 'middle',
        ],
        [
            'attribute'=>'current_balance',
            'format' => 'raw',
            'width' => '80px',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'visible' => false,
        ],
        [
            'attribute'=>'unit_price',
            // 'value' => 'approvedLabel',
            // 'label' => Yii::t('app','Approved'),
            'format' => 'currency',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'visible' => false,
        ],
        [
            'attribute'=>'usage.detail',
            // 'value' => 'approvedLabel',
            // 'label' => Yii::t('app','Approved'),
            'format' => 'raw',
            'hAlign' => 'center',
            'vAlign' => 'middle',
        ],
        [
            'attribute'=>'order.approved',
            'value' => 'approvedLabel',
            'label' => Yii::t('app','Approved'),
            'format' => 'raw',
            'hAlign' => 'center',
            'vAlign' => 'middle',
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            // 'template' => '{kewps10} {smart-approve} {approval} {reject} {item-list} {checkout-view} {update} {delete} {recover}',
            'template' => '{kewps10} {smart-approve} {approval} {reject} {item-list} {checkout-view} {update}',
            'dropdown' => true,
            'buttons' => $actions
        ],
        [
            'header' => 'Permanent Delete',
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{delete-permanent}',
            'buttons' => [
                'delete-permanent' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                        'title' => Yii::t('yii', 'Permanent Delete'), 'data-toggle' => 'tooltip', 'data-method' => 'POST'
                    ]);
                }
            ],
            'visible' => Yii::$app->user->can('admin'),
        ]
    ];
    ?>
    <?= DynaGrid::widget([
        'columns'=>$gridColumn,
        // 'storage'=>DynaGrid::TYPE_COOKIE,
        'theme'=>'panel-primary',
        'showPersonalize'=>true,
        'gridOptions'=>[
            'dataProvider' => $dataProvider,
            // // 'filterModel' => $searchModel,
            // 'filterSelector' => 'select[name="per-page"]',
            // 'showPageSummary'=>true,
            // 'floatHeader'=>true,
            'rowOptions'=>function ($model) {
                switch ($model['order']['approved']) {
                    case 1:
                    return ['class'=>'success'];
                    break;
                    case 2:
                    return ['class'=>'warning'];
                    break;
                    case 8:
                    return ['class'=>'danger'];
                    break;

                    default:
                    return ['class'=>'default'];
                        # code...
                    break;
                }
            },
            'responsiveWrap'=>false,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-order']],
            'panel' => [
                'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
                // 'before' =>  '<div style="padding-top: 7px;"><em>* The table header sticks to the top in this demo as you scroll</em></div>',
                'after' => false,
            ],
            // 'export' => false,
            // your toolbar can include the additional full export menu
            'toolbar' => [
                ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title'=>Yii::t('app', 'Create Order')]) . ' '.
                Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button'])
            ],
            ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
            '{export}',
            '{toggleData}',
            'exportConfig' => [
                    // ExportMenu::FORMAT_PDF => false
            ]
        ],
    ],
        'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
        ]); ?>

    </div>
