<?php
$actions = [
    'kewps10' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span> '.Yii::t('yii', 'KEW-PS-10'), ['/request/print-kewps10','id'=>$model['order']['id']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']),['class'=>'text-center']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'kew2' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-file" style="color:green;"> </span> '.Yii::t('yii', 'KEW-PS-10'), $url, ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'view' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-eye-open"> </span> '.Yii::t('yii', 'View KEW-PS-10'), ['view', 'id' => $model['order']['id']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'smart-approve' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-check" style="color:maroon;"> </span> '.Yii::t('yii', 'FIFO Approval'), ['/request/smart-approve', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'FIFO Approval'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'approval' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-check" style="color:green;"> </span> '.Yii::t('yii', 'Approval'), ['approval', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'Approval'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'reject' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
//                                    return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->order->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"> </span> '.Yii::t('yii', 'Reject'), ['reject', 'id' => $model->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'item-list' => function ($url, $model) {
        return Html::tag('div',Html::a('<span class="glyphicon glyphicon-list"> </span> '.Yii::t('yii', 'Item List'), $url, ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
    },
//                                    'item-list' => function ($url, $model) {
//                                return Html::a('<span class="glyphicon glyphicon-list"></span>', ['item-list', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
//                            },
    'update' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model->order->approved === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-pencil"> </span> '.Yii::t('yii', 'Update'), $url, ['title' => Yii::t('yii', 'Update'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'recover' => function ($url, $model) {
        if ($model->deleted_by === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-refresh"> </span> '.Yii::t('yii', 'Recover'), $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip', 'data-method' => 'POST']),['class'=>'text-center']);;
        }
    },
    'delete' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model->order->approved === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-trash"> </span> '.Yii::t('yii', 'Delete'), $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                'data-method' => 'post']),['class'=>'text-center']);;
        }
    },
];
$actions1 = [
    'kewps10' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span>', ['/request/print-kewps10','id'=>$model['order']['id']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'kew2' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span>', $url, ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'view' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model['order']['id']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'smart-approve' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
            return Html::a('<span class="glyphicon glyphicon-check" style="color:maroon;"></span>', ['/request/smart-approve', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'FIFO Approval'), 'data-toggle' => 'tooltip']);
        }
    },
    'approval' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
            return Html::a('<span class="glyphicon glyphicon-check" style="color:green;"></span>', ['approval', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'Approval'), 'data-toggle' => 'tooltip']);
        }
    },
    'reject' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
//                                    return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->order->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
            return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
        }
    },
    'item-list' => function ($url, $model) {
        return Html::a('<span class="glyphicon glyphicon-list"></span>', $url, ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
    },
//                                    'item-list' => function ($url, $model) {
//                                return Html::a('<span class="glyphicon glyphicon-list"></span>', ['item-list', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
//                            },
    'update' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model->order->approved === 2) {
            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'), 'data-toggle' => 'tooltip']);
        }
    },
    'recover' => function ($url, $model) {
        if ($model->deleted_by === 1) {
            return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip', 'data-method' => 'POST']);
        }
    },
    'delete' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model->order->approved === 2) {
            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                'data-method' => 'post']);
        }
    },
];
?>
