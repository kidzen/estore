<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use \kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Dispose */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="dispose-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <div class="col-md-4">
        <?= $form->field($model, 'refference_no')->textInput(['maxlength' => true, 'placeholder' => 'Refference No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'category_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeCategory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Dispose category')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'method_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeMethod::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Dispose method')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'reason_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeReason::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Dispose reason')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'current_revenue')->textInput(['placeholder' => 'Current Revenue']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'cost')->textInput(['placeholder' => 'Cost']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'returns')->textInput(['placeholder' => 'Returns']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'requested_at')->textInput(['placeholder' => 'Requested At']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'requested_by')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
            'options' => ['placeholder' => Yii::t('app', 'Choose User')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'disposed_at')->textInput(['placeholder' => 'Disposed At']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'disposed_by')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
            'options' => ['placeholder' => Yii::t('app', 'Choose User')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12">
        <?php
        $forms = [
            [
                'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'InventoryItem')),
                'content' => $this->render('_formInventoryItem-dispose'),
                // 'content' => $this->render('_formInventoryItem-dispose', [
                //     'row' => \yii\helpers\ArrayHelper::toArray($model->inventoryItems),
                // ]),
            ],
        ];
        echo kartik\tabs\TabsX::widget([
            'items' => $forms,
            'position' => kartik\tabs\TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'pluginOptions' => [
                'bordered' => true,
                'sideways' => true,
                'enableCache' => false,
            ],
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
