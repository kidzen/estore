<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Order'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Order').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'transaction.id',
                'label' => Yii::t('app', 'Transaction'),
            ],
        'instruction_id',
        'order_date',
        [
                'attribute' => 'orderedBy.username',
                'label' => Yii::t('app', 'Ordered By'),
            ],
        'ordered_by_temp',
        'order_no',
        [
                'attribute' => 'usage.id',
                'label' => Yii::t('app', 'Usage'),
            ],
        'required_date',
        'checkout_date',
        [
                'attribute' => 'checkoutBy.username',
                'label' => Yii::t('app', 'Checkout By'),
            ],
        'approved',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <?php if ($model->updatedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->createdBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->transaction) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">Transaction<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnTransaction = [
        ['attribute' => 'id', 'visible' => false],
        'store_id',
        'type',
        'check_date',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->transaction,
        'attributes' => $gridColumnTransaction    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->orderedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->orderedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->usage) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">Usage<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUsage = [
        ['attribute' => 'id', 'visible' => false],
        'category_id',
        'item_id',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->usage,
        'attributes' => $gridColumnUsage    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->checkoutBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->checkoutBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->approvedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->approvedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>

        <div class="col-sm-12">
<?php
if($providerOrderItem->totalCount){
    $gridColumnOrderItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'inventory.description',
                'label' => Yii::t('app', 'Inventory')
            ],
                        'rq_quantity',
            'app_quantity',
            'current_balance',
            'unit_price',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerOrderItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-order-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Order Item')),
        ],
        'export' => false,
        'columns' => $gridColumnOrderItem
    ]);
}
?>

        </div>

        <div class="col-sm-12">
<?php
if($providerPackage->totalCount){
    $gridColumnPackage = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'detail',
            'delivery',
                        'package_date',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPackage,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-package']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Package')),
        ],
        'export' => false,
        'columns' => $gridColumnPackage
    ]);
}
?>

        </div>
    </div>
    </div>
    </div>
    </div>
</div>
