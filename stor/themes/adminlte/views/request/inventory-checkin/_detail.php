<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InventoryCheckin */

?>
<div class="inventory-checkin-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'transaction.id',
                'label' => Yii::t('app', 'Transaction'),
            ],
        [
                'attribute' => 'budget.name',
                'label' => Yii::t('app', 'Budget'),
            ],
        [
                'attribute' => 'store.name',
                'label' => Yii::t('app', 'Store'),
            ],
        'inventory_id',
        [
                'attribute' => 'vendor.name',
                'label' => Yii::t('app', 'Vendor'),
            ],
        'items_quantity',
        'items_total_price',
        'check_date',
        'approved',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>