<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Order');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";
$this->registerJs($search);
?>
<div class="order-index">

<!--    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
-->

<p>
    <?= Html::a(Yii::t('app', 'Create Order'), ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?>
</p>
<div class="search-form" style="display:none">
    <?=  $this->render('_search-checkout', ['model' => $searchModel]); ?>
</div>
<?php
$actions = [
    'kewps10' => function ($url, $model) {
        if ($model['order']['APPROVED'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span> '.Yii::t('yii', 'KEW-PS-10'), ['/request/print-kewps10','id'=>$model['order']['ID']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']),['class'=>'text-center']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'kew2' => function ($url, $model) {
        if ($model['order']['APPROVED'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-file" style="color:green;"> </span> '.Yii::t('yii', 'KEW-PS-10'), $url, ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'view' => function ($url, $model) {
        if ($model['order']['APPROVED'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-eye-open"> </span> '.Yii::t('yii', 'View KEW-PS-10'), ['view', 'id' => $model['order']['ID']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'smart-approve' => function ($url, $model) {
        if ($model->DELETED === 0 && $model['order']['APPROVED'] === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-check" style="color:maroon;"> </span> '.Yii::t('yii', 'FIFO Approval'), ['/request/smart-approve', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'FIFO Approval'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'approval' => function ($url, $model) {
        if ($model->DELETED === 0 && $model['order']['APPROVED'] === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-check" style="color:green;"> </span> '.Yii::t('yii', 'Approval'), ['approval', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'Approval'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'reject' => function ($url, $model) {
        if ($model->DELETED === 0 && $model['order']['APPROVED'] === 2) {
//                                    return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->order->ID], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"> </span> '.Yii::t('yii', 'Reject'), ['reject', 'id' => $model->ID], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'item-list' => function ($url, $model) {
        return Html::tag('div',Html::a('<span class="glyphicon glyphicon-list"> </span> '.Yii::t('yii', 'Item List'), $url, ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
    },
//                                    'item-list' => function ($url, $model) {
//                                return Html::a('<span class="glyphicon glyphicon-list"></span>', ['item-list', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
//                            },
    'update' => function ($url, $model) {
        if ($model->DELETED === 0 && $model->order->APPROVED === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-pencil"> </span> '.Yii::t('yii', 'Update'), $url, ['title' => Yii::t('yii', 'Update'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'recover' => function ($url, $model) {
        if ($model->DELETED === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-refresh"> </span> '.Yii::t('yii', 'Recover'), $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip', 'data-method' => 'POST']),['class'=>'text-center']);;
        }
    },
    'delete' => function ($url, $model) {
        if ($model->DELETED === 0 && $model->order->APPROVED === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-trash"> </span> '.Yii::t('yii', 'Delete'), $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                'data-method' => 'post']),['class'=>'text-center']);;
        }
    },
];
$actions1 = [
    'kewps10' => function ($url, $model) {
        if ($model['order']['APPROVED'] === 1) {
            return Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span>', ['/request/print-kewps10','id'=>$model['order']['ID']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'kew2' => function ($url, $model) {
        if ($model['order']['APPROVED'] === 1) {
            return Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span>', $url, ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'view' => function ($url, $model) {
        if ($model['order']['APPROVED'] === 1) {
            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model['order']['ID']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'smart-approve' => function ($url, $model) {
        if ($model->DELETED === 0 && $model['order']['APPROVED'] === 2) {
            return Html::a('<span class="glyphicon glyphicon-check" style="color:maroon;"></span>', ['/request/smart-approve', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'FIFO Approval'), 'data-toggle' => 'tooltip']);
        }
    },
    'approval' => function ($url, $model) {
        if ($model->DELETED === 0 && $model['order']['APPROVED'] === 2) {
            return Html::a('<span class="glyphicon glyphicon-check" style="color:green;"></span>', ['approval', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'Approval'), 'data-toggle' => 'tooltip']);
        }
    },
    'reject' => function ($url, $model) {
        if ($model->DELETED === 0 && $model['order']['APPROVED'] === 2) {
//                                    return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->order->ID], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
            return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->ID], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
        }
    },
    'item-list' => function ($url, $model) {
        return Html::a('<span class="glyphicon glyphicon-list"></span>', $url, ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
    },
//                                    'item-list' => function ($url, $model) {
//                                return Html::a('<span class="glyphicon glyphicon-list"></span>', ['item-list', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
//                            },
    'update' => function ($url, $model) {
        if ($model->DELETED === 0 && $model->order->APPROVED === 2) {
            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'), 'data-toggle' => 'tooltip']);
        }
    },
    'recover' => function ($url, $model) {
        if ($model->DELETED === 1) {
            return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip', 'data-method' => 'POST']);
        }
    },
    'delete' => function ($url, $model) {
        if ($model->DELETED === 0 && $model->order->APPROVED === 2) {
            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                'data-method' => 'post']);
        }
    },
];

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'id',
        'hAlign' => 'center', 'vAlign' => 'middle',
    ],
    [
        'attribute' => 'order.order_no',
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'value' => function($model) {
            return 'NO PESANAN: ' . $model->order->ORDER_NO;
        },
        'group' => true,
            'groupedRow' => true, // move grouped column to a single grouped row
            'groupOddCssClass' => 'kv-grouped-row', // configure odd group cell css class
            'groupEvenCssClass' => 'kv-grouped-row',
//                'groupEvenCssClass' => 'maroon',
        ],
        [
            'label' => 'No Arahan Kerja',
            'attribute' => 'arahankerja.no_kerja',
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
        [
            'label' => 'Inventory Details',
            'attribute' => 'inventory_details',
            'format' => 'raw',
            'value' => function($model) {
                $card = 'Card No : ' . $model->inventory->CARD_NO;
                $code = 'Code No : ' . $model->inventory->CODE_NO;
                $description = 'Description : <br>' . $model->inventory->DESCRIPTION;
                return Html::tag('span', $card . '<br>' . $code . '<br>' . $description);
            },
            'hAlign' => 'left', 'vAlign' => 'middle',
        ],
//            [
//                'attribute' => 'current_balance',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'transaction.check_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'transaction.check_date',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
        [
            'attribute' => 'order.order_date',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::tag('span', 'Date : ' . $model->order->ORDER_DATE . '<br>' . 'Order by : ' . $model->order->ORDERED_BY);
            },
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
        [
            'attribute' => 'order.required_date',
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
//            [
//                'attribute' => 'order.ordered_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
        [
            'label' => 'Kuantiti Stor',
            'attribute' => 'inventory.quantity',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
//            [
//                'attribute' => 'rq_quantity',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
        [
            'label' => 'Kuantiti Permohonan',
            'attribute' => 'quantity',
            'format' => 'raw',
            'value' => function ($model) {
                if ($model->APP_QUANTITY) {
                    return Html::tag('span', 'Requested : ' . $model->RQ_QUANTITY . '<br>' . 'Approved : ' . $model->APP_QUANTITY);
                } else {
                    return Html::tag('span', 'Requested : ' . $model->RQ_QUANTITY . '<br>' . 'Approved : ' . 0);
                }
            },
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
//            [
//
//                'attribute' => 'items',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//                'format' => 'raw',
//                'value' => function($model) {
//                    $data = \yii\helpers\ArrayHelper::map($model->items, 'ID', 'SKU');
//                    return implode("<br>", $data);
//                },
//            ],
        [
            'label' => 'Jumlah Harga (RM)',
            'attribute' => 'unit_price',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'label' => 'Usage',
            'attribute' => 'usage_detail',
            'format' => 'raw',
            'value' => function($model) {
                $type = 'ID : ' . $model['vehicle']['ID'];
                $regNo = 'Reg No : ' . $model['vehicle']['REG_NO'];
                $description = 'Model : ' . $model['vehicle']['MODEL'];
                return Html::tag('span', $regNo . '<br>' . $description);
            },
            'hAlign' => 'left', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
//            [
//                'attribute' => 'order.checkout_date',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'order.checkout_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'created_at',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'updated_at',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'created_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'updated_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'deleted_at',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
        [
            'attribute' => 'order.approved',
            'format' => 'raw',
            'value' => function($model) {
                if ($model['order']['APPROVED'] == 2) {
                    return Html::label('MENUNGGU', null, ['class' => 'label label-warning']);
                } else if ($model['order']['APPROVED'] == 1) {
                    return Html::label('LULUS', null, ['class' => 'label label-success']);
                } else if ($model['order']['APPROVED'] == 8) {
                    return Html::label('DITOLAK', null, ['class' => 'label label-danger']);
                } else {
                    return Html::label('TIDAK BERKENAAN', null, ['class' => 'label label-primary']);
                }
            },
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
////            [
////                'attribute' => 'order.APPROVED_BY',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'order.APPROVED_AT',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('role_officer_store'),
//                        'group' => true,
//                        'subGroupOf' => 2,
            'template' => '{kewps10} {smart-approve} {approval} {reject} {item-list} {view} {update} {delete} {recover}',
            'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
            'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
//                        'group' => true, 'subGroupOf' => 2,
            'dropdown' => true,
            'buttons' => $actions
        ],
        [
            'header' => 'Permanent Delete',
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{delete-permanent}',
            'buttons' => [
                'delete-permanent' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                        'title' => Yii::t('yii', 'Permanent Delete'), 'data-toggle' => 'tooltip', 'data-method' => 'POST'
                    ]);
                }
            ],
            'visible' => Yii::$app->user->can('admin'),
        ]
    ]
            // ]

    ;
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        // 'columns' => $gridColumn,
        'columns' => $columns,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-order']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
        ]); ?>

    </div>
