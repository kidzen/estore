<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\ActivityLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-activity-log-search">
    <div class="row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]);
        ?>

        <div class="col-md-4">
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'remote_ip')->textInput(['maxlength' => true, 'placeholder' => 'Remote Ip']) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'action')->textInput(['maxlength' => true, 'placeholder' => 'Action']) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'controller')->textInput(['maxlength' => true, 'placeholder' => 'Controller']) ?>

    </div>

    <?php /* echo $form->field($model, 'params')->textInput(['maxlength' => true, 'placeholder' => 'Params']) */ ?>

    <?php /* echo $form->field($model, 'route')->textInput(['maxlength' => true, 'placeholder' => 'Route']) */ ?>

    <?php /* echo $form->field($model, 'status')->widget(\kartik\widgets\Select2::classname(), [
            'data' => [1 => 'Active', 0 => 'Inactive'],
            'options' => ['placeholder' => 'Select status ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); */ ?>

    <?php /* echo $form->field($model, 'messages')->textInput(['maxlength' => true, 'placeholder' => 'Messages']) */ ?>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
