<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UsageDetailValue */

$this->title = Yii::t('app', 'Create Usage Detail Value');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usage Detail Value'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usage-detail-value-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
