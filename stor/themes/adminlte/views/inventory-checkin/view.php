<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\InventoryCheckin */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Checkin'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-checkin-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Inventory Checkin').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">

            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'transaction.id',
            'label' => Yii::t('app', 'Transaction'),
        ],
        [
            'attribute' => 'budget.name',
            'label' => Yii::t('app', 'Budget'),
        ],
        [
            'attribute' => 'store.name',
            'label' => Yii::t('app', 'Store'),
        ],
        [
            'attribute' => 'inventory.description',
            'label' => Yii::t('app', 'Inventory'),
        ],
        [
            'attribute' => 'vendor.name',
            'label' => Yii::t('app', 'Vendor'),
        ],
        'items_quantity',
        'items_total_price',
        'check_date',
        [
                'attribute' => 'approved',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['approved']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <?php if ($model->budget) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title"><?= Yii::t('app', 'Budget') ?></h4>
    <?php
    $gridColumnBudget = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'revenue',
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model->budget,
        'attributes' => $gridColumnBudget    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->transaction) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title"><?= Yii::t('app', 'Transaction') ?></h4>
    <?php
    $gridColumnTransaction = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'store.name',
            'label' => Yii::t('app', 'Store'),
        ],
        'type',
        'check_date',
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model->transaction,
        'attributes' => $gridColumnTransaction    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->store) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title"><?= Yii::t('app', 'Store') ?></h4>
    <?php
    $gridColumnStore = [
        ['attribute' => 'id', 'visible' => false],
        'category.name',
        'name',
        'address',
        'contact_no',
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model->store,
        'attributes' => $gridColumnStore    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->inventory) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title"><?= Yii::t('app', 'Inventory') ?></h4>
    <?php
    $gridColumnInventory = [
        ['attribute' => 'id', 'visible' => false],
        'category.name',
        'card_no',
        'code_no',
        'description',
        'quantity',
        'min_stock',
        'location',
        [
                'attribute' => 'approved',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['approved']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model->inventory,
        'attributes' => $gridColumnInventory    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->vendor) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title"><?= Yii::t('app', 'Vendor') ?></h4>
    <?php
    $gridColumnVendor = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'address',
        'contact_no',
        'email',
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model->vendor,
        'attributes' => $gridColumnVendor    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->checkBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title"><?= Yii::t('app', 'Check By') ?></h4>
    <?php
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model->checkBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>

        <div class="col-sm-12">
<?php
if($providerInventoryItem->totalCount){
    $gridColumnInventoryItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'inventory.description',
                'label' => Yii::t('app', 'Inventory')
            ],
                        [
                'attribute' => 'checkout.id',
                'label' => Yii::t('app', 'Checkout')
            ],
            [
                'attribute' => 'dispose.description',
                'label' => Yii::t('app', 'Dispose')
            ],
            'sku',
            'unit_price',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInventoryItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inventory-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Inventory Item')),
        ],
        'export' => false,
        'columns' => $gridColumnInventoryItem
    ]);
}
?>

        </div>
    </div>
    </div>
    </div>
    </div>
</div>
