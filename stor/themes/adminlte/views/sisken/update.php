<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sisken */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sisken',
]) . ' ' . $model->tahun;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sisken'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->tahun, 'url' => ['view', ]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sisken-update">
	<div class="panel panel-primary">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
