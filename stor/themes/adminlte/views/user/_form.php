<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'StoreAssignment',
        'relID' => 'store-assignment',
        'value' => \yii\helpers\Json::encode($model->storeAssignments),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]
);
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <div class="col-md-4">
        <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'placeholder' => 'Username']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'profile_pic')->textInput(['maxlength' => true, 'placeholder' => 'Profile Pic']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'role_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\common\models\Role::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Role')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
        ); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status')->widget(\kartik\widgets\Select2::classname(), [
            'data' => [1 => 'Active', 0 => 'Inactive'],
            'options' => ['placeholder' => 'Select status ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]
        ); ?>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12">
        <?php
        $forms = [
            [
                'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'StoreAssignment')),
                'content' => $this->render('_formStoreAssignment', [
                    'row' => \yii\helpers\ArrayHelper::toArray($model->storeAssignments),
                ]),
            ],
        ];
        echo kartik\tabs\TabsX::widget([
            'items' => $forms,
            'position' => kartik\tabs\TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'pluginOptions' => [
                'bordered' => true,
                'sideways' => true,
                'enableCache' => false,
            ],
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
