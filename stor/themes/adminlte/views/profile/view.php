<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profile'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-view">

    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title"><?= Yii::t('app', 'Profile').' '. Html::encode($this->title) ?></h2>
                </div>
                <div class="box-body">

                    <div class="col-sm-12">
                        <?php
                        $gridColumn = [
                            ['attribute' => 'id', 'visible' => false],
                            'staff_no',
                            'name',
                            'position',
                            'department',
                        ];
                        echo DetailView::widget([
                            'model' => $model,
                            'attributes' => $gridColumn
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-12">
                        <?php
                        if($providerStoreAssignment->totalCount){
                            $gridColumnStoreAssignment = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                'staff_no',
                                'store.name',
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerStoreAssignment,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-store-assignment']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Store Assignment')),
                                ],
                                'export' => false,
                                'columns' => $gridColumnStoreAssignment
                            ]);
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
