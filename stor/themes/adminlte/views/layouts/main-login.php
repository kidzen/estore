<?php
use backend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$css = "
.img-layout {
    background: url('".Yii::getAlias('@web/dist')."/img/MPSP2.jpg');
    /*background: url('../img/MPSP5.jpg');*/
    /*background: url('../img/3.jpg');*/
    /*background: url('../img/2.png');*/

    /*background-repeat:no-repeat;*/
    /*background-size:auto 100%;*/
    /*background-size:100% auto;*/
    /*background-size:90% 90%;*/
    background-size:100% 100%;


/*    -webkit-background-size:cover;
    -moz-background-size:cover;
    -o-background-size:cover;
    background-size:cover;*/
    /*background-position:center;*/

    background-repeat:no-repeat;
    -webkit-background-size:cover;
    -moz-background-size:cover;
    -o-background-size:cover;
    background-size:cover;
    background-position:center;
}
";
$this->registerCss($css);
dmstr\web\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login-page img-layout">

<?php $this->beginBody() ?>

    <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
