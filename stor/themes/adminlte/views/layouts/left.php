<?php
switch (Yii::$app->db->driverName) {
    case 'mysql':
    $script = "SELECT table_name as TABLE_NAME
    FROM information_schema.tables
    WHERE table_schema = 'estor4'";
    break;
    case 'oci':
    $script = "
    SELECT
    TABLE_NAME
    FROM USER_TABLES
    UNION ALL
    SELECT
    VIEW_NAME AS TABLE_NAME
    FROM USER_VIEWS
    UNION ALL
    SELECT
    MVIEW_NAME AS TABLE_NAME
    FROM USER_MVIEWS
    ORDER BY TABLE_NAME
    ";

    break;
    default:
    $script = false;
    break;
}
if($script){
    $query = Yii::$app->db->createCommand($script)->queryAll();
    foreach ($query as $key => $value) {
        $url = '/'.strtr($value['TABLE_NAME'], '_', '-');
        $label = strtr($value['TABLE_NAME'], '_', ' ');
        $items[] = ['label' => $label, 'url' => [$url]];
    }
}
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div style="text-align: center">
                <!-- width 200px -->
                <img src="<?= Yii::$app->params['leftMenuImg'] ?>" style="width: 90%" alt="logo-sb-pdrm"/>
            </div>
        </div>

        <!-- search form -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
    -->        <!-- /.search form -->
    <?= dmstr\widgets\Menu::widget(
        [
            'options' => ['class' => 'sidebar-menu tree delay', 'data-widget'=> 'tree'],
            'items' => [
                ['label' => Yii::t('app','Request'), 'options' => ['class' => 'header']],
                [
                    'label' => Yii::t('app','Login'), 'url' => ['/site/login'],
                    'visible' => Yii::$app->user->isGuest
                ],
                [
                    'label' => Yii::t('app','Stock In'),
                    'items' => [
                        ['label' => Yii::t('app','Stock Registration'), 'icon' => 'file-code-o', 'url' => ['/inventory-checkin/create'], 'visible' => Yii::$app->user->can('checkin_stock')],
                        ['label' => Yii::t('app','Card Registration'), 'icon' => 'file-code-o', 'url' => ['/inventory/create'], 'visible' => Yii::$app->user->can('register_inventory')],
                        ['label' => Yii::t('app','Transaction In'), 'icon' => 'file-code-o', 'url' => ['/inventory-checkin/index'], 'visible' => Yii::$app->user->can('checkin_stock')],
                            // ['label' => Yii::t('app','Transaction In'), 'icon' => 'file-code-o', 'url' => ['/transaction/checkin']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Stock Out'),
                    'items' => [
                        ['label' => Yii::t('app','Checkout'), 'icon' => 'file-code-o', 'url' => ['/request/checkout']],
                        // ['label' => Yii::t('app','Checkout'), 'icon' => 'file-code-o', 'url' => ['/request/checkout'], 'visible' => empty(Yii::$app->user->storeList)],
                        // ['label' => Yii::t('app','Instruction Order'), 'icon' => 'file-code-o', 'url' => ['/request/instruction']],
                        ['label' => Yii::t('app','Transaction Out'), 'icon' => 'file-code-o', 'url' => ['/order/index'], 'visible' => Yii::$app->user->can('checkout_stock')],

                        // ['label' => Yii::t('app','Checkout'), 'icon' => 'file-code-o', 'url' => ['/request/checkout']],
                        // ['label' => Yii::t('app','Instruction Order'), 'icon' => 'file-code-o', 'url' => ['/request/instruction']],
                        // ['label' => Yii::t('app','Transaction Out'), 'icon' => 'file-code-o', 'url' => ['/request/checkout-list']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Disposal'),
                    'items' => [
                        ['label' => Yii::t('app','Dispose'), 'icon' => 'file-code-o', 'url' => ['/request/dispose'], 'visible' => Yii::$app->user->can('dispose_stock')],
                        ['label' => Yii::t('app','Disposal List'), 'icon' => 'file-code-o', 'url' => ['/request/dispose-index'], 'visible' => Yii::$app->user->can('dispose_stock')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Budget'),
                    'items' => [
                        ['label' => Yii::t('app','Budget Registration'), 'icon' => 'file-code-o', 'url' => ['/budget/create'], 'visible' => Yii::$app->user->can('create_budget')],
                        // ['label' => Yii::t('app','Budget Grant'), 'icon' => 'file-code-o', 'url' => ['/budget/grant'], 'visible' => Yii::$app->user->can('view_budget')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                ['label' => Yii::t('app','Admin Section'), 'options' => ['class' => 'header']],
                ['label' => Yii::t('app','My Profile'), 'url' => ['/profile/view', 'id' => Yii::$app->user->id]],
                [
                    'label' => Yii::t('app','Store Management'),
                    'items' => [
                        ['label' => Yii::t('app','Category'), 'icon' => 'file-code-o', 'url' => ['/inventory-category/index'], 'visible' => Yii::$app->user->can('view_category')],
                        ['label' => Yii::t('app','Inventory Card'), 'icon' => 'file-code-o', 'url' => ['/inventory/index'], 'visible' => Yii::$app->user->can('view_inventory')],
                        ['label' => Yii::t('app','Stock'), 'icon' => 'file-code-o', 'url' => ['/inventory-item/index'], 'visible' => Yii::$app->user->can('view_inventory_item')],
                        ['label' => Yii::t('app','Store'), 'icon' => 'file-code-o', 'url' => ['/store/index'], 'visible' => Yii::$app->user->can('view_store')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','List Management'),
                    'items' => [
                        ['label' => Yii::t('app','Vendor'), 'icon' => 'file-code-o', 'url' => ['/vendor/index'], 'visible' => Yii::$app->user->can('view_vendor')],
                        ['label' => Yii::t('app','Vehicle'), 'icon' => 'file-code-o', 'url' => ['/vehicle/index'], 'visible' => Yii::$app->user->can('view_vehicle')],
                        ['label' => Yii::t('app','Workshop'), 'icon' => 'file-code-o', 'url' => ['/workshop/index'], 'visible' => Yii::$app->user->can('view_workshop')],
                        ['label' => Yii::t('app','Usage Category'), 'icon' => 'file-code-o', 'url' => ['/usage-category/index'], 'visible' => Yii::$app->user->can('view_usage_category')],
                        ['label' => Yii::t('app','Usage List'), 'icon' => 'file-code-o', 'url' => ['/usage-list/index'], 'visible' => Yii::$app->user->can('view_usage')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','System Management'),
                    'items' => [
                        ['label' => Yii::t('app','MPSP Profile'), 'icon' => 'file-code-o', 'url' => ['/profile/index'], 'visible'=>Yii::$app->user->can('admin_user')],
                        ['label' => Yii::t('app','My Profile'), 'icon' => 'file-code-o', 'url' => ['/profile/view','id'=>Yii::$app->user->profile->staff_no], 'visible' => Yii::$app->user->can('viewOwn_profile')],
                        ['label' => Yii::t('app','Activity Log'), 'icon' => 'file-code-o', 'url' => ['/activity-log/index'], 'visible' => Yii::$app->user->can('view_activity_log')],
                        ['label' => Yii::t('app','System Maintenance'), 'icon' => 'file-code-o', 'url' => ['/site/maintenance']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Access Management'), 'items' => [
                        ['label' => Yii::t('app','User'), 'icon' => 'fa', 'url' => ['/user/index'],'visible'=>Yii::$app->user->can('admin_user')],
                        ['label' => Yii::t('app','User Profile'), 'icon' => 'fa', 'url' => ['/profile/index'],'visible'=>Yii::$app->user->can('admin_user')],
                        ['label' => Yii::t('app','Assignment'), 'icon' => 'fa', 'url' => ['/admin/assignment'],'visible'=>Yii::$app->user->can('admin_user')],
                        ['label' => Yii::t('app','Permission'), 'icon' => 'fa', 'url' => ['/admin/permission'],'visible'=>Yii::$app->user->can('admin_user')],
                        ['label' => Yii::t('app','Route'), 'icon' => 'fa', 'url' => ['/admin/route'],'visible'=>Yii::$app->user->can('admin_user')],
                        ['label' => Yii::t('app','Role'), 'icon' => 'fa', 'url' => ['/admin/role'],'visible'=>Yii::$app->user->can('admin_user')],
                        ['label' => Yii::t('app','Rules'), 'icon' => 'fa', 'url' => ['/auth-rule/index'],'visible'=>Yii::$app->user->can('admin_user')],
                        // ['label' => Yii::t('app','Auth Link'), 'icon' => 'fa', 'url' => ['/auth-item/index'],'visible'=>Yii::$app->user->can('admin_user')],
                        ['label' => Yii::t('app','Store Assignment'), 'icon' => 'file-code-o', 'url' => ['/store-assignment/index'], 'visible' => Yii::$app->user->can('view_store_assignment')],
                    ]
                ],
                ['label' => Yii::t('app','Reporting'), 'options' => ['class' => 'header'],'visible' => Yii::$app->user->can('generate_report'),],
                [
                    'label' => Yii::t('app','Form'),
                    'visible' => Yii::$app->user->can('generate_report'),
                    'items' => [
                        ['label' => Yii::t('app','KEWPS Form'), 'icon' => 'file-code-o', 'url' => ['/official-form/form-generator'],
                        'visible' => Yii::$app->user->can('generate_report'),],
                        // ['label' => Yii::t('app','KEWPA Form'), 'icon' => 'file-code-o', 'url' => ['/request/checkin']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Report'),
                    'visible' => Yii::$app->user->can('generate_report'),
                    'items' => [
                        ['label' => Yii::t('app','All Report'), 'icon' => 'file-code-o', 'url' => ['/report/index']],
                        ['label' => Yii::t('app','KEWPS Report'), 'icon' => 'file-code-o', 'url' => ['/report/kewps']],
                        ['label' => Yii::t('app','Yearly Report'), 'icon' => 'file-code-o', 'url' => ['/report/yearly']],
                        ['label' => Yii::t('app','Supplier Report'), 'icon' => 'file-code-o', 'url' => ['/vendor-usage/index']],
                        ['label' => Yii::t('app','Vehicle Report'), 'icon' => 'file-code-o', 'url' => ['/vehicle-usage/index']],
                        ['label' => Yii::t('app','Budget Report'), 'icon' => 'file-code-o', 'url' => ['/budget-usage/index']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                ['label' => Yii::t('app','Module Developer'), 'options' => ['class' => 'header'],'visible' => false,],
                [
                    'label' => Yii::t('app','Developer'), 'icon' => 'file-code-o',
                    'visible' => false,
                    'items' => [
                        ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                        ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                        [
                            'label' => 'Under Development', 'icon' => 'file-code-o',
                            'items' => [
                                ['label' => Yii::t('app','Workshop Report'), 'icon' => 'file-code-o', 'url' => ['/workshop-usage/index']],
                                ['label' => Yii::t('app','Store Report'), 'icon' => 'file-code-o', 'url' => ['/store-usage/index']],
                            ]
                        ],
                        [
                            'label' => 'Database', 'icon' => 'database',
                            'items' => $items
                        ],
                    ],
                    // 'visible' => Yii::$app->user->can('admin'),
                ],
            ],
        ]
        ) ?>

    </section>

</aside>
