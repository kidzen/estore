<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VendorUsage */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Vendor Usage',
]) . ' ' . $model->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vendor Usage'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', ]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="vendor-usage-update">
	<div class="panel panel-primary">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
