<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VendorUsage */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="vendor-usage-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'vendor')->textInput(['maxlength' => true, 'placeholder' => 'Vendor']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'year')->textInput(['maxlength' => true, 'placeholder' => 'Year']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'month')->textInput(['maxlength' => true, 'placeholder' => 'Month']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'placeholder' => 'Date']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'inventory_id')->textInput(['placeholder' => 'Inventory']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'items_total_price')->textInput(['maxlength' => true, 'placeholder' => 'Items Total Price']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'by_year')->textInput(['maxlength' => true, 'placeholder' => 'By Year']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'by_vendor')->textInput(['maxlength' => true, 'placeholder' => 'By Vendor']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'by_year_vendor')->textInput(['maxlength' => true, 'placeholder' => 'By Year Vendor']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'by_month_vendor')->textInput(['maxlength' => true, 'placeholder' => 'By Month Vendor']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'by_date_vendor')->textInput(['maxlength' => true, 'placeholder' => 'By Date Vendor']) ?>
</div>

    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
