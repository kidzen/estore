<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VendorUsage */

$this->title = Yii::t('app', 'Create Vendor Usage');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vendor Usage'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-usage-create">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
