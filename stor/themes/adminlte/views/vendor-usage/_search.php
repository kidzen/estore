<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\VendorUsageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-vendor-usage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'vendor')->textInput(['maxlength' => true, 'placeholder' => 'Vendor']) ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true, 'placeholder' => 'Year']) ?>

    <?= $form->field($model, 'month')->textInput(['maxlength' => true, 'placeholder' => 'Month']) ?>

    <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'placeholder' => 'Date']) ?>

    <?php /* echo $form->field($model, 'inventory_id')->textInput(['placeholder' => 'Inventory']) */ ?>

    <?php /* echo $form->field($model, 'items_total_price')->textInput(['maxlength' => true, 'placeholder' => 'Items Total Price']) */ ?>

    <?php /* echo $form->field($model, 'by_year')->textInput(['maxlength' => true, 'placeholder' => 'By Year']) */ ?>

    <?php /* echo $form->field($model, 'by_vendor')->textInput(['maxlength' => true, 'placeholder' => 'By Vendor']) */ ?>

    <?php /* echo $form->field($model, 'by_year_vendor')->textInput(['maxlength' => true, 'placeholder' => 'By Year Vendor']) */ ?>

    <?php /* echo $form->field($model, 'by_month_vendor')->textInput(['maxlength' => true, 'placeholder' => 'By Month Vendor']) */ ?>

    <?php /* echo $form->field($model, 'by_date_vendor')->textInput(['maxlength' => true, 'placeholder' => 'By Date Vendor']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
