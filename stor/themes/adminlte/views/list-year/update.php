<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ListYear */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'List Year',
]) . ' ' . $model->year;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List Year'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->year, 'url' => ['view', ]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="list-year-update">
	<div class="panel panel-primary">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
