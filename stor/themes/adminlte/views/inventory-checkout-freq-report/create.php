<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InventoryCheckoutFreqReport */

$this->title = Yii::t('app', 'Create Inventory Checkout Freq Report');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Checkout Freq Report'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-checkout-freq-report-create">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
