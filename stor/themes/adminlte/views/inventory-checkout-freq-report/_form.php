<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InventoryCheckoutFreqReport */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="inventory-checkout-freq-report-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'inventory_id')->textInput(['placeholder' => 'Inventory']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'year')->textInput(['maxlength' => true, 'placeholder' => 'Year']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'month')->textInput(['maxlength' => true, 'placeholder' => 'Month']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'placeholder' => 'Date']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'by_inventory')->textInput(['maxlength' => true, 'placeholder' => 'By Inventory']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'by_year')->textInput(['maxlength' => true, 'placeholder' => 'By Year']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'by_month')->textInput(['maxlength' => true, 'placeholder' => 'By Month']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'by_date')->textInput(['maxlength' => true, 'placeholder' => 'By Date']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'rank_inventory')->textInput(['maxlength' => true, 'placeholder' => 'Rank Inventory']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'rank_year')->textInput(['maxlength' => true, 'placeholder' => 'Rank Year']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'rank_month')->textInput(['maxlength' => true, 'placeholder' => 'Rank Month']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'rank_date')->textInput(['maxlength' => true, 'placeholder' => 'Rank Date']) ?>
</div>

    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
