<?php
use yii\helpers\Html;
$appNameShort = Yii::t('app',Yii::$app->params['appNameShort']);
$appNameFull = Yii::t('app',Yii::$app->params['appNameFull']);
?>
<?= Yii::t('app','') ?>


<!-- Content Header (Page header) -->
<div class="content-header">
  <h1>
    <?= $appNameFull ?>
    <small>Version <?= Yii::$app->params['version'] ?></small>
</h1>
</div>

<!-- Main content -->
<div class="content body">

  <section id="introduction">
    <h2 class="page-header"><a href="#introduction"><?= Yii::t('app','Introduction') ?></a></h2>
    <p class="lead">
      <b><?= $appNameFull ?></b> <?= Yii::t('app','is a centeralized system to manage Store\'s stock records') ?>.</p>
  </section><!-- /#introduction -->


  <!-- ============================================================= -->
  <section id="download">
    <!-- <h2 class="page-header"><a href="#download">Download</a></h2> -->
<!--     <p class="lead">
        <?= $appNameFull ?> <?= Yii::t('app','have several module') ?>.
    </p> -->
      <?php /*
      <div class="row">
        <div class="col-sm-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= Yii::t('app','Module') ?> <i><?= Yii::t('app','Administrator')."\n" ?>.</i></h3>
              <span class="label label-primary pull-right"><i class="fa fa-lock"></i></span>
            </div><!-- /.box-header -->
            <div class="box-body">
              <p><?= Yii::t('app','This module is for managing record') ?>.</p>
              <?= Html::a('<i class="fa fa-download"></i> Akses Modul <i><?= Yii::t('app','Administrator')."\n" ?>.</i> sekarang',yii\helpers\Url::to(strtr($_SERVER['PHP_SELF'], ['frontend'=>'backend'])),['class'=>'btn btn-primary']) ?>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
        <div class="col-sm-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Modul Pengguna</h3>
              <span class="label label-danger pull-right"><i class="fa fa-users"></i></span>
            </div><!-- /.box-header -->
            <div class="box-body">
              <p>Modul ini adalah untuk kegunaan <b>Pengguna Jabatan</b> dan <b>Pentadbir Jabatan Undang-undang.</b></p>
              <?= Html::a('Akses Modul Pengguna sekarang',['site/index'],['class'=>'btn btn-info']) ?>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
      */?>
      <h3><?= Yii::t('app','System Module WorkFlow') ?></h3>
      <pre class="hierarchy bring-up"><code class="language-bash" data-lang="bash">Carta Alir Modul Sistem

    <?= $appNameFull ?><!--
        ├── Modul <?= Yii::t('app','Administrator')."\n" ?>. (Akses <?= Yii::t('app','Administrator')."\n" ?>. Sahaja)
        │   ├── Pengkalan Data
        │   ├── Konfigurasi
        │
-->
    ├── <?= Yii::t('app','Role Base Accees Control Module (RBAC)')."\n" ?>
    │   ├── <?= Yii::t('app','Administrator')."\n" ?>
    │   │   ├── <?= Yii::t('app','System Configuration')."\n" ?>
    │   │   ├── <?= Yii::t('app','All user profile')."\n" ?>
    │   │   ├── <?= Yii::t('app','All store profile')."\n" ?>
    │   │   ├── <?= Yii::t('app','All store category')."\n" ?>
    │   │   ├── <?= Yii::t('app','All inventory category')."\n" ?>
    │   │   ├── <?= Yii::t('app','All inventory card')."\n" ?>
    │   │   ├── <?= Yii::t('app','All stock')."\n" ?>
    │   │   ├── <?= Yii::t('app','All order')."\n" ?>
    │   │
    │   ├── <?= Yii::t('app','Store Admin')."\n" ?>
    │   │   ├── <?= Yii::t('app','Personal Profile')."\n" ?>
    │   │   ├── <?= Yii::t('app','Store details')."\n" ?>
    │   │   ├── <?= Yii::t('app','Stock check in and check out record')."\n" ?>
    │   │   ├── Template Draf
    │   │
    │   ├── <?= Yii::t('app','Store Officer')."\n" ?>
    │   │   ├── <?= Yii::t('app','Personal Profile')."\n" ?>
    │   │   ├── <?= Yii::t('app','Store details')."\n" ?>
    │   │   ├── <?= Yii::t('app','Branch details')."\n" ?>
    │   │   ├── <?= Yii::t('app','Location details')."\n" ?>
    │   │   ├── <?= Yii::t('app','Efies & maintenance record')."\n" ?>
    │   │   ├── Template Draf
    │   │
    │   ├── <?= Yii::t('app','Store User')."\n" ?>
    │       ├── <?= Yii::t('app','Personal Profile')."\n" ?>
    │       ├── <?= Yii::t('app','Store details')."\n" ?>
    │       ├── <?= Yii::t('app','Branch details')."\n" ?>
    │       ├── <?= Yii::t('app','Location details')."\n" ?>
    │       ├── <?= Yii::t('app','Efies & maintenance record')."\n" ?>
    │
    ├── <?= Yii::t('app','Request/Registration Module')."\n" ?>
    ├── <?= Yii::t('app','Records/List Management Module')."\n" ?>
    ├── <?= Yii::t('app','Approval Module')."\n" ?>
    ├── <?= Yii::t('app','Data export Module')."\n" ?>
    ├── <?= Yii::t('app','Reporting Module')."\n" ?>
    ├── <?= Yii::t('app','System Maintenance Module')."\n" ?>

</code></pre>
</section>


<!-- ============================================================= -->

<section id="akses">
  <h2 class="page-header"><a href="#akses"><?= Yii::t('app','Role Based Access Control Management (RBAC)') ?></a></h2>
  <h3><?= Yii::t('app','User Role Access')."\n" ?></h3>
  <p class="lead"><?= Yii::t('app','Role Base Access can be dynamically configure to control user access for each of every link(urls), datas, permissions, roles, rule, and special rule (Custom Logic Rule). Currently role are divide into 5')."\n" ?> :-</p>
  <ul class="bring-up">
    <li><b><?= Html::a(Yii::t('app','Administrator')."\n",'#') ?></b></li>
    <li><b><?= Html::a('Company Admin','#') ?></b></li>
    <li><b><?= Html::a('Company User','#') ?></b></li>
    <li><b><?= Html::a('Branch Admin','#') ?></b></li>
    <li><b><?= Html::a('Branch User','#') ?></b></li>
</ul>
<p>
    <b>Nota:</b> <?= Yii::t('app','Customization on RBAC module configuration must be through developer advice')."\n" ?>.
</p>
</section>

<section id="kod-warna">
  <h3><?= Yii::t('app','Color Code Legend')."\n" ?></h3>
  <p class="lead"><?= Yii::t('app','Color code legend are being use minimally in the system to ease user recognize and increase effieciency and awareness to the system/process notification or data labelling')."\n" ?>. <?= Yii::t('app','Below are the color code legend used')."\n" ?>:</p>
  <div class="box box-solid" style="max-width: 300px;">
    <div class="box-body no-padding">
      <table id="layout-skins-list" class="table table-striped bring-up nth-2-center">
        <thead  class="bg-purple">
          <tr>
            <th style="width: 210px;"><?= Yii::t('app','Description')."\n" ?></th>
            <th><?= Yii::t('app','Color Code')."\n" ?>.</th>
        </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="2" class="bg-info"><?= Yii::t('app','Notification')."\n" ?>.</td>
    </tr>
    <tr>
        <td><b><?= Yii::t('app','Success Status')."\n" ?>.</b></td>
        <td><a class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a></td>
    </tr>
    <tr>
        <td><b><?= Yii::t('app','Fail Status')."\n" ?>.</b></td>
        <td><a class="btn btn-danger btn-xs"><i class="fa fa-eye"></i></a></td>
    </tr>
<!--               <tr>
                <td colspan="2" class="bg-info"><?= Yii::t('app','Data Status')."\n" ?>.</td>
              </tr>
              <tr>
                <td><b><?= Yii::t('app','Status')."\n" ?>.</b> data sedang diproses di peringkat <b>Jabatan</b></td>
                <td><a class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a></td>
              </tr>
              <tr>
                <td><b><?= Yii::t('app','Status')."\n" ?>.</b> data sedang diproses di peringkat <b>Jabatan Undang-undang</b></td>
                <td><a class="btn btn-warning btn-xs"><i class="fa fa-eye"></i></a></td>
              </tr>
              <tr>
                <td><b><?= Yii::t('app','Status')."\n" ?>.</b> data telah <b>Selesai</b></td>
                <td><a class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a></td>
              </tr>
          -->
      </tbody>
  </table>
</div><!-- /.box-body -->
</div><!-- /.box -->
</section>


<section id="user-registration">
  <h3><?= Yii::t('app','User Registration')."\n" ?>.</h3>
  <p class="lead"><?= Yii::t('app','User may personally register an account in the system however it requires '. Yii::t('app','Administrator')."\n" . 'approval to activate their account')."\n" ?>. <code><?= Yii::t('app','Access Management')."\n" ?> > <?= Yii::t('app','User')."\n" ?></code>. </p>
</section>

<section id="user">
  <h3><?= Yii::t('app','Activated & Assigned User System')."\n" ?>.</h3>
  <p class="lead"><?= Yii::t('app','Administrator may configure user account access through Assignment')."\n" ?> <code><?= Yii::t('app','Access Management')."\n" ?> > <?= Yii::t('app','Assignment','')."\n" ?></code> <?= Yii::t('app','or through the <i class="glyphicon glyphicon-lock"></i> logo in '."\n") ?> <code><?= Yii::t('app','Access Management')."\n" ?> > <?= Yii::t('app','User')."\n" ?></code>.</p>
</section>

<section id="configuration">
  <h2><?= Yii::t('app','System Configuration')."\n" ?><b>(<?= Yii::t('app','Administrator')."\n" ?>)</b></h2>
      <?php /*
      <div>
        <h3>Setting</h3>
        <p class="lead">Pengguna <b><?= Yii::t('app','Administrator')."\n" ?>.</b> boleh mengubah konfigurasi sistem melalui pautan <code>Konfigurasi Sistem > Setting</code>.</p>
        <div class="box box-solid" style="max-width: 800px;">
          <div class="box-body no-padding">
            <table class="table table-striped bring-up nth-2-center">
              <thead  class="bg-purple">
                <tr>
                  <th style="width: 210px;">Setting</th>
                  <th>Keterangan</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><b>simplifiedVersion</b></td>
                  <td><b>Mengaktifkan versi <i>light version</i></b>. Versi ini mengurangkan transaksi data diantara pelayan (<i>Server</i>) dan pengguna (<i>Client</i>)</td>
                </tr>
                <tr>
                  <td><b>templateDirectory</b></td>
                  <td><b>Lokasi</b> penyimpanan <b>templat draf</b>. Contoh: <code>/upload/template/</code></td>
                </tr>
                <tr>
                  <td><b>minPasswordLength</b></td>
                  <td>Hadkan <b>panjang katalaluan</b> minimum</td>
                </tr>
                <tr>
                  <td><b>minStaffNoLength</b></td>
                  <td>Hadkan <b>panjang no pekerja</b> minimum</td>
                </tr>
                <tr>
                  <td><b>lateDraftWarningInDays</b></td>
                  <td>Had <b>jumlah hari kelewatan</b> draf perjanjian dari tarikh SST (Surat Setuju Terima)</td>
                </tr>
                <tr>
                  <td><b>departmentAlterLimit</b></td>
                  <td><b>Had maksima status draf</b> yang dibenarkan pengubahsuaian oleh pengguna jabatan</td>
                </tr>
                <tr>
                  <td><b>draftParamRequired</b></td>
                  <td>Konfigurasi mewajibkan pengguna mengisi parameter draf sebelum dihantar.
                    <br><code>1</code> : pengguna <b>diwajibkan</b> isi.
                    <br><code>0</code> : pengguna <b>tidak diwajibkan</b> isi.
                  </td>
                </tr>
                <tr>
                  <td><b>defaultJuuStaffAssigned</b></td>
                  <td><b>No Pekerja</b> staf JUU yang ditauliah menerima semua permohonan draf</td>
                </tr>

              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <p><b>Nota:</b> Sebarang perubahan perlu mendapat nasihat pembangun.</p>
      </div>
    */ ?>
    <div>
        <h3><?= Yii::t('app','List Management')."\n" ?>.</h3>
        <p class="lead"><?= Yii::t('app','Admin may configure available Fire Extinguisher type and service type througth this module')."\n" ?> <code><?= Yii::t('app','List Management')."\n" ?> > <?= Yii::t('app','Fe Type')."\n" ?></code> or <code><?= Yii::t('app','List Management')."\n" ?> > <?= Yii::t('app','Fe Service Type')."\n" ?></code>.</p>
    </div>

</section>



<section id="user-manual">
    <h2><?= Yii::t('app','System Usage')."\n" ?></h2>

    <div>
        <h3><?= Yii::t('app','Registration and Records Management')."\n" ?>.</h3>
        <p class="lead"> <?= Yii::t('app','User may add new company, branch, efies records, maintenance records or upload attachment')."\n" ?>.</p>
        <ol>
            <li><?= Yii::t('app','Fill up form through')."\n" ?> <code><?= Yii::t('app','Registration')."\n" ?> > </code>.
                <ul>
                    <li><code>Company</code>.</li>
                    <li><code>Branch</code>.</li>
                    <li><code>Efies profile</code>.</li>
                    <li><code>Maintenance</code>.</li>
                    <li><code>Attachment</code>.</li>
                </ul>
            </li>
            <li><?= Yii::t('app','Make sure all the field are filled up as per requested and click <button class="btn btn-xs btn-success">Create</button> to confirm')."\n" ?>.</li>
            <li><?= Yii::t('app','Click <button class="btn btn-xs btn-primary">Update</button> if there is any mistake during add new data')."\n" ?>.</li>
            <li><?= Yii::t('app','Click <button class="btn btn-xs btn-danger">Delete</button> to remove data')."\n" ?>.</li>
            <li><?= Yii::t('app','Click <button class="btn btn-xs btn-success">Download</button> to download attachment')."\n" ?>.</li>
        </ol>
        <p class="lead"> <?= Yii::t('app','For attachment, user may upload any document or image file up to designated file size determine by Administrator')."\n" ?>.</p>

    </div>

    <div>
        <h3><?= Yii::t('app','Records Viewer Filtering')."\n" ?>.</h3>
        <p class="lead"> <?= Yii::t('app','User may add new search and filter every data available by using advance search & filter features')."\n" ?>. <?= Yii::t('app','This features generally available on every table/record existed in the system')."\n" ?></p>
        <ol>
            <li><?= Yii::t('app','Click <button class="btn btn-xs btn-info">Advance Search</button> to draw out Advance Search & Filter Form')."\n" ?>.</li>
            <li><?= Yii::t('app','Fill up form for any specific or known data/filter')."\n" ?>.</li>
            <li><?= Yii::t('app','Click <button class="btn btn-xs btn-primary">Search</button> to start filtering and Searching')."\n" ?>.</li>
            <li><?= Yii::t('app','Click <button class="btn btn-xs btn-default">Reset</button> on mistaken input to reset the form')."\n" ?>.</li>
        </ol>
        or You may also..<br><br>
        <ol>
            <li><?= Yii::t('app','Fill up the filter form row (the top row) in the table records for any column and press/hit <code>ENTER</code> on your keyboard')."\n" ?> <code><?= Yii::t('app','Registration')."\n" ?> > <?= Yii::t('app','Company')."\n" ?></code>.
                <li><?= Yii::t('app','Fill more column to filter more specific and press/hit <code>ENTER</code> on your keyboard')."\n" ?> <code><?= Yii::t('app','Registration')."\n" ?> > <?= Yii::t('app','Company')."\n" ?></code>.
                </li>
            </ol>
        </div>

        <div>
            <h3><?= Yii::t('app','Data Export')."\n" ?>.</h3>
            <p class="lead"> <?= Yii::t('app','User may export data on various format')."\n" ?>.</p>
            <ol>
                <li><?= Yii::t('app','Click <button class="btn btn-xs btn-default" title="Export"><i class="glyphicon glyphicon-export"></i>  <span class="caret"></span></button> to draw export document format option')."\n" ?>. Supported format :
                    <ul>
                        <li><code><?= Yii::t('app','HTML')."\n" ?></code> <?= Yii::t('app','usual use case are to embed into another web services view or for easier data scrapping through frontend to support old technology')."\n" ?>.</li>
                        <li><code><?= Yii::t('app','CSV')."\n" ?></code> <?= Yii::t('app','usual use case are to migrate data to another database platform or SAAS platform')."\n" ?>.</li>
                        <li><code><?= Yii::t('app','Text')."\n" ?></code> <?= Yii::t('app','usual use case are to use as lightwieght plain text data storage')."\n" ?>.</li>
                        <li><code><?= Yii::t('app','Excel')."\n" ?></code> <?= Yii::t('app','usual use case are to merge with current documented soft copy')."\n" ?>.</li>
                        <li><code><?= Yii::t('app','PDF')."\n" ?></code> <?= Yii::t('app','usual use case are to export as protected document or persistent data presentation accros multiple host OS/Platform/Applications')."\n" ?>.</li>
                        <li><code><?= Yii::t('app','JSON')."\n" ?></code> <?= Yii::t('app','usual use case are to share data through API or data sharing with another services that can consume JSON data type')."\n" ?>.</li>
                    </ul>
                </li>
                <li><?= Yii::t('app','Select format and click <button class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-ok">OK</i></button> to confirm export') ?>.</li>
            </ol>
        </div>

        <div>
            <h3><?= Yii::t('app','Report Generation')."\n" ?>.</h3>
            <p class="lead"> <?= Yii::t('app','User may add new company')."\n" ?>.</p>
            <ol>
                <li><?= Yii::t('app','Fill up form through')."\n" ?> <code><?= Yii::t('app','Registration')."\n" ?> > <?= Yii::t('app','Company')."\n" ?></code>.
                    <ul>
                        <li><code>Company Name</code> <?= Yii::t('app','is required')."\n" ?>.</li>
                        <li><code>Email</code> <?= Yii::t('app','is required')."\n" ?>.</li>
                    </ul>
                </li>
                <li><?= Yii::t('app','Make sure all the field are filled up as per requested and click <button class="btn btn-xs btn-success">Create</button> to confirm')."\n" ?>.</li>
                <li><?= Yii::t('app','Click <button class="btn btn-xs btn-primary">Update</button> if there is any mistake during add new data')."\n" ?>.</li>
                <li><?= Yii::t('app','Click <button class="btn btn-xs btn-danger">Delete</button> to remove data')."\n" ?>.</li>
            </ol>
            <p class="lead"> <?= Yii::t('app','User may do the same to all the data under Registration')."\n" ?>.</p>

        </div>

    <!-- <div>
        <h3>Isi Draf perjanjian</h3>
        <p class="lead"> Modul ini boleh diakses oleh Pengguna Jabatan</p>
        <ol>
            <li>Pilih kontrak melalui pautan <code>Permohonan > Draf Pejanjian</code>.</li>
            <li>Klik ikon  <i class="glyphicon glyphicon-eye-open"></i> untuk melihat perincian kontrak.</li>
            <li>Klik ikon  <i class="fa fa-paste"></i> atau butang <button class="btn btn-xs btn-primary">Isi Draf</button> untuk mengisi draf perjanjian.</li>
            <li>Muat turun template draf sebagai panduan mengisi draf perjanjian.</li>
            <ul>
                <li><code>Nama Draf Perjanjian</code> wajib diisi bagi memudahkan carian sistem.</li>
                <li><code>Tarikh SST</code> digalakkan diisi untuk tujuan notifikasi.</li>
                <li>Digalakkan melengkapkan draf sebelum menghantar.</li>
            </ul>
            <li>Kemaskini jika terdapat kesalahan.</li>
        </ol>

    </div> -->

    <!-- <div>
        <h3>Isi Senarai Semak</h3>
        <p class="lead"> Modul ini boleh diakses oleh Pengguna Jabatan</p>
        <ol>
            <li>Pilih kontrak melalui pautan <code>Permohonan > Draf Pejanjian</code>.</li>
            <li>Klik ikon  <i class="glyphicon glyphicon-eye-open"></i> untuk melihat perincian kontrak.</li>
            <li>Klik ikon  <i class="fa fa-file"></i> atau butang <button class="btn btn-xs btn-primary">Senarai Semak</button> untuk mengisi senarai semak.</li>
            <li>Kemaskini jika terdapat kesalahan.</li>
        </ol>

    </div> -->

<!--     <div>
        <h3>Hantar Draf Perjanjian untuk semakan JUU</h3>
        <p class="lead"> Modul ini boleh diakses oleh Pengguna Jabatan</p>
        <ol>
            <li>Pilih kontrak melalui pautan <code>Permohonan > Draf Pejanjian</code>.</li>
            <li>Klik ikon  <i class="glyphicon glyphicon-eye-open"></i> untuk melihat perincian kontrak.</li>
            <li>Kemaskini jika terdapat kesalahan.</li>
            <li>Klik ikon  <i class="glyphicon glyphicon-export"></i> atau butang <button class="btn btn-xs btn-primary">Hantar</button> untuk menghantar draf.</li>
            <li>Pastikan Status Draf berubah. Pilihan kontrak anda melalui pautan <code>Permohonan > Draf Pejanjian</code> akan berubah kod warna dari <span class="btn btn-warning btn-xs">Kuning</i></span> kepada <span class="btn btn-info btn-xs">Biru</i></span> kecuali bagi kontrak dalam status lewat akan ditanda kod warna <span class="btn btn-danger btn-xs">Merah</i></span>.</li>
            <li>Log status draf boleh didapati dari pautan <code>Rekod > Draf Dokumen > Status Draf</code> atau <code>Permohonan > Draf Perjanjian > Log Status Draf</code> </li>
            <li>Jika draf dikembalikan kepada anda (ditanda dengan kod warna <span class="btn btn-warning btn-xs">Kuning</i></span>) selepas dihantar, sila lihat nota yang dilampirkan melalui pautan <code>Permohonan > Draf Perjanjian > Nota</code> untuk tindakan selanjutnya.</li>
        </ol>
        <p><b>Nota:</b> Berdasarkan konfigurasi asal sistem, Pengguna jabatan tidak lagi boleh mengubal sebarang parameter sistem.</p>
    </div> -->

<!--     <div>
        <h3>Kemaskini Status Draf</h3>
        <p class="lead"> Modul ini boleh diakses oleh Pengguna JUU Sahaja</p>
        <ol>
            <li>Pilih kontrak melalui pautan <code>Permohonan > Draf Pejanjian</code>.</li>
            <li>Klik ikon  <i class="glyphicon glyphicon-eye-open"></i> untuk melihat perincian kontrak.</li>
            <li>Klik ikon  <i class="fa fa-pencil"></i> atau butang <button class="btn btn-xs btn-primary">Kemaskini Status</button> untuk mengubah atau merekod log status baru.</li>
        </ol>
        <p><b>Nota:</b> Setiap perubahan status akan direkodkan sebagai log status. Sila pastikan borang kemaskini status diisi dengan betul sebelum dihantar.</p>

    </div>
-->
<!--     <div>
        <h3>Panduan Mem</h3>
        <p class="lead"> Modul ini boleh diakses oleh Pengguna JUU Sahaja</p>
        <ol>
            <li>Muat naik templat draf melalui pautan <code>Permohonan > Muat Naik Template</code>.</li>
            <li>Klik ikon  <i class="glyphicon glyphicon-eye-open"></i> untuk melihat perincian kontrak.</li>
            <li>Klik ikon  <i class="fa fa-pencil"></i> atau butang <button class="btn btn-xs btn-primary">Kemaskini Status</button> untuk mengubah atau merekod log status baru.</li>
        </ol>
        <p><b>Nota:</b> Setiap perubahan status akan direkodkan sebagai log status. Sila pastikan borang kemaskini status diisi dengan betul sebelum dihantar.</p>

    </div>
-->
<!--     <div>
        <h3>Muat Naik Templat Draf</h3>
        <p class="lead"> Modul ini boleh diakses oleh Pengguna JUU Sahaja</p>
        <ol>
            <li>Sila pastikan anda mematuhi .</li>
            <li>Muat naik templat draf melalui pautan <code>Permohonan > Muat Naik Template</code>.</li>
            <li>Klik ikon  <i class="glyphicon glyphicon-eye-open"></i> untuk melihat perincian kontrak.</li>
            <li>Klik ikon  <i class="fa fa-pencil"></i> atau butang <button class="btn btn-xs btn-primary">Kemaskini Status</button> untuk mengubah atau merekod log status baru.</li>
        </ol>
        <p><b>Notes:</b> Setiap perubahan status akan direkodkan sebagai log status. Sila pastikan borang kemaskini status diisi dengan betul sebelum dihantar.</p>

    </div> -->
</section>

<section>
    <h3>FAQ</h3>
    <p class="lead"> For further inquiries, please do contact our support at <a href="http://www.sarraglobal.com" target="_blank">sarraglobal.com</a> or you may directly mail to us at <a href="mailto:support@sarraglobal.com?Subject=ESTORV3%20:%20" target="_top">support@sarraglobal.com</a></p>

</section>

</div><!-- /.content -->
