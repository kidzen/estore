<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\DisposeDetailValueSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-dispose-detail-value-search">
    <div class="row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]);
        ?>

        <div class="col-md-4">
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'dispose_id')->textInput(['placeholder' => 'Dispose']) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'key_id')->textInput(['placeholder' => 'Key']) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'value')->textInput(['maxlength' => true, 'placeholder' => 'Value']) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status')->widget(\kartik\widgets\Select2::classname(), [
            'data' => [1 => 'Active', 0 => 'Inactive'],
            'options' => ['placeholder' => 'Select status ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
