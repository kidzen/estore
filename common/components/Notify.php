<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use Yii;

class Notify {

    public $config;
    public $key;
    public $message;
    public $duration;
    public $title;

    public function fail($config = null) {

        $this->message = isset($config['message']) ? $config['message'] : Yii::t('app','Error processing request');
        $this->duration = isset($config['duration']) ? $config['duration'] : 8000;
        $this->title = isset($config['title']) ? $config['title'] : Yii::t('app',' Fail');
        $this->key = isset($config['key']) ? $config['key'] : 0;
        $this->duration = 10000;
        $this->duration = 1000000;
        return Yii::$app->session->setFlash($this->key, [
                    'type' => 'danger',
                    'duration' => $this->duration,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => $this->title,
                    'message' => $this->message,
        ]);
    }

    public function success($config = null) {
        $this->message = isset($config['message']) ? $config['message'] : Yii::t('app','Error processing request');
        $this->duration = isset($config['duration']) ? $config['duration'] : 8000;
        $this->title = isset($config['title']) ? $config['title'] : Yii::t('app',' Fail');
        $this->key = isset($config['key']) ? $config['key'] : 0;

        return Yii::$app->session->setFlash($this->key, [
                    'type' => 'success',
                    'duration' => $this->duration,
                    'icon' => 'glyphicon glyphicon-check-sign',
                    'title' => $this->title,
                    'message' => $this->message,
        ]);
    }

    public function info($message, $duration = 3000) {
        return Yii::$app->session->setFlash('info', [
                    'type' => 'info',
                    'duration' => $duration,
                    'icon' => 'glyphicon glyphicon-info-sign',
                    'title' => Yii::t('app',' Info'),
                    'message' => $message
        ]);
    }

    public function warning($message, $duration = 3000) {
        return Yii::$app->session->setFlash('warning', [
                    'type' => 'warning',
                    'duration' => $duration,
                    'icon' => 'glyphicon glyphicon-exclamation-mark',
                    'title' => Yii::t('app',' Warning'),
                    'message' => $message
        ]);
    }

}
