<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            // 'class' => 'yii\rbac\DbManager',
            'class' => 'yii\rbac\PhpManager',
            'itemFile' => '@common/rbac/items.php',
            'assignmentFile' => '@common/rbac/assignments.php',
            'ruleFile' => '@common/rbac/rules.php',
            // 'cache' => 'cache',
            // 'defaultRoles' => ['guest'],
        ],
        // 'log' => ['class' => 'yii\log\Dispatcher'],
        'formatter' => [
            // 'class' => 'yii\i18n\Formatter'
            'dateFormat' => 'php:d M Y',
            'datetimeFormat' => 'php:d M Y H:i:s',
            // 'timeZone' => 'Asia/KualaLumpur'
        ],
        'i18n' => ['class' => 'yii\i18n\I18N'],
        // 'mailer' => ['class' => 'yii\swiftmailer\Mailer'],
        'urlManager' => ['class' => 'yii\web\UrlManager'],
        'assetManager' => ['class' => 'yii\web\AssetManager'],
        // 'security' => ['class' => 'yii\base\Security'],
    ],
    'modules' => [
        'store' => [
            'class' => 'frontend\modules\store\StoreModule',
        ],
    ],
];
