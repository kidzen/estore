<?php

namespace common\models;

use Yii;
use \common\models\base\DirectIssue as BaseDirectIssue;

/**
 * This is the model class for table "direct_issue".
 */
class DirectIssue extends BaseDirectIssue
{
    /**
     * @inheritdoc
     */
    // public function rules()
    // {
    //     return array_replace_recursive(parent::rules(),
	   //  [
    //         [['transaction_id','type', 'amount', 'supplier_id', 'section_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
    //         [['particulars'], 'string'],
    //         [['total'], 'number'],
    //         [['date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
    //         [['serial_no', 'refference_no', 'ivr', 'officer_name', 'receipt_no'], 'string', 'max' => 255]
    //     ]
    //     );
    // }


    public function generateBaucer()
    {
        $bilPrefix = Yii::$app->params['settings']['directIssueBaucerPrefix'] ?? "SB/DI/".date('Y/');
        $length = strlen($bilPrefix)+1;
        $col = 'ivr';
        $tableName = self::tableName();
        switch (Yii::$app->db->driverName) {
            case 'oci':
            $bilStock = self::find()
            // ->where(['EXTRACT(MONTH FROM "'.$tableName.'"."created_at")' => date('m')])
            ->where(['EXTRACT(YEAR FROM "'.$tableName.'"."created_at")' => date('Y')])
            ->joinWith('transaction')
            ->andWhere(['transaction.type_id' => \common\models\Transaction::TYPE_DIRECT_ISSUE])
            ->max('CAST(SUBSTR("'.$tableName.'"."'.$col.'",'.$length.') AS INT)');
            break;
            case 'mysql':
            $bilStock = self::find()
            // ->where(['EXTRACT(MONTH FROM '.$tableName.'.created_at)' => date('m')])
            ->where(['EXTRACT(YEAR FROM '.$tableName.'.created_at)' => date('Y')])
            ->joinWith('transaction')
            ->andWhere(['transaction.type_id' => \common\models\Transaction::TYPE_DIRECT_ISSUE])
            ->max('CAST(SUBSTR('.$tableName.'.'.$col.','.$length.') AS INT)');
            break;

            default:
                # code...
            break;
        }
        if($this->isNewRecord){
            $bilStock = $bilStock + 1;
        }

        return $bilPrefix.$bilStock;

        // $lastBillQuery = self::find();
        // $lastBillQuery->where = null;
        // $lastBill = $lastBillQuery->max($col);
        // $baucer = $bilPrefix.intval($lastBill);
        // // var_dump($baucer);die;
        // return $baucer;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // custom code here
        if($insert) {
            $transaction = \common\models\Transaction::add(\common\models\Transaction::TYPE_DIRECT_ISSUE);
            $this->transaction_id = $transaction->id;
            $this->ivr = $this->generateBaucer();
            // $this->section_id = Yii::$app->user->activeSection['id'];
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
    /* public function beforeSave($insert)
    {
        // custom code here
        return parent::beforeSave($insert);
    } */
}
