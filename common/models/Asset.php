<?php

namespace common\models;

use Yii;
use \common\models\base\Asset as BaseAsset;

/**
 * This is the model class for table "asset".
 */
class Asset extends BaseAsset
{
    /**
     * @inheritdoc
     */
/*    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['category_id', 'brand_id', 'model_id', 'quantity', 'min_stock', 'approved', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['card_no', 'code_no', 'description'], 'string', 'max' => 255],
            [['card_no'], 'unique'],
            [['code_no'], 'unique'],
            [['description'], 'unique']
        ]
        );
    }*/

    public static function arrayList($callback = false)
    {
        $callback = is_array($callback) ? $callback : self::find()->orderBy('asset.id')->asArray()->all();
        return \yii\helpers\ArrayHelper::map($callback, 'id', function ($model) {
            return $model['code_no'].' - '.$model['description'];
        } );
    }


    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
     public function beforeSave($insert)
    {
        // custom code here
        if($insert) {
            $transaction = \common\models\Transaction::add(\common\models\Transaction::TYPE_ASSET_REGISTRATION);
            $this->transaction_id = $transaction->id;
            // $this->receipt_no = $this->generateBaucer();
            // $this->section_id = Yii::$app->user->activeSection['id'];
        }
        return parent::beforeSave($insert);
    }
}
