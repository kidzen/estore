<?php

namespace common\models;

use Yii;
use \common\models\base\DirectIssueItem as BaseDirectIssueItem;

/**
 * This is the model class for table "direct_issue_item".
 */
class DirectIssueItem extends BaseDirectIssueItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['direct_issue_id', 'dispose_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['unit_price'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['serial_no'], 'string', 'max' => 255]
        ]
        );
    }


    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
    /* public function beforeSave($insert)
    {
        // custom code here
        return parent::beforeSave($insert);
    } */
}
