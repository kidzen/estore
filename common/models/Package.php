<?php

namespace common\models;

use Yii;
use \common\models\base\Package as BasePackage;

/**
 * This is the model class for table "package".
 */
class Package extends BasePackage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['order_id', 'package_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['package_date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['detail', 'delivery'], 'string', 'max' => 255]
        ]
        );
    }


    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
    /* public function beforeSave($insert)
    {
        // custom code here
        return parent::beforeSave($insert);
    } */
}
