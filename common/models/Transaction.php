<?php

namespace common\models;

use Yii;
use \common\models\base\Transaction as BaseTransaction;
use \yii\base\Exception;

/**
 * This is the model class for table "transaction".
 */
class Transaction extends BaseTransaction
{
    const TYPE_ASSET_REGISTRATION = 1;
    const TYPE_CHECKIN = 2;
    const TYPE_DIRECT_ISSUE = 3;
    const TYPE_CHECKOUT = 4;
    const TYPE_MOVEMENT = 5;
    const TYPE_DISPOSE = 6;
    /**
     * @inheritdoc
     */
    // public function rules()
    // {
    //     return array_replace_recursive(parent::rules(),
    //         [
    //             [['type', 'check_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
    //             [['check_date', 'deleted_at', 'created_at', 'updated_at'], 'safe']
    //         ]
    //     );
    // }

    public static function add($type)
    {
        $transaction = new self();
        $transaction->section_id = Yii::$app->user->activeSection['id'];
        $transaction->type_id = $type;
        $transaction->check_date = date('Y-m-d');
        if(!$transaction->save()) {
            throw new Exception("Fail to save transaction", 500);
        }
        return $transaction;
    }

    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
/*    public function beforeSave($insert)
    {
        // custom code here
        if($insert) {
            $this->section_id = Yii::$app->user->activeSection['id'];
        }

        return parent::beforeSave($insert);
    }*/
}
