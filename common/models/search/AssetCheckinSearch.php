<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AssetCheckin;

/**
 * common\models\search\AssetCheckinSearch represents the model behind the search form about `common\models\AssetCheckin`.
 */
 class AssetCheckinSearch extends AssetCheckin
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'transaction_id', 'budget_id', 'asset_id', 'supplier_id', 'check_by', 'approved_id', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['rnvr', 'check_date', 'purchased_date', 'refference_file', 'detail', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetCheckin::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = [
            'id' => SORT_DESC,
            // 'approved_id' => SORT_DESC,
        ];
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'budget_id' => $this->budget_id,
            'asset_id' => $this->asset_id,
            'supplier_id' => $this->supplier_id,
            'check_date' => $this->check_date,
            'check_by' => $this->check_by,
            'purchased_date' => $this->purchased_date,
            'approved_id' => $this->approved_id,
            'approved_at' => $this->approved_at,
            'approved_by' => $this->approved_by,
            'status' => $this->status,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'rnvr', $this->rnvr])
            ->andFilterWhere(['like', 'refference_file', $this->refference_file])
            ->andFilterWhere(['like', 'detail', $this->detail]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
