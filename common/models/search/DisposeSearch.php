<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Dispose;

/**
 * common\models\search\DisposeSearch represents the model behind the search form about `common\models\Dispose`.
 */
 class DisposeSearch extends Dispose
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'transaction_id', 'category_id', 'method_id', 'reason_id', 'quantity', 'requested_by', 'disposed_by', 'approved_id', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['refference_no', 'description', 'requested_date', 'disposed_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['current_revenue', 'cost', 'returns'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dispose::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'category_id' => $this->category_id,
            'method_id' => $this->method_id,
            'reason_id' => $this->reason_id,
            'quantity' => $this->quantity,
            'current_revenue' => $this->current_revenue,
            'cost' => $this->cost,
            'returns' => $this->returns,
            'requested_date' => $this->requested_date,
            'requested_by' => $this->requested_by,
            'disposed_date' => $this->disposed_date,
            'disposed_by' => $this->disposed_by,
            'approved_id' => $this->approved_id,
            'approved_at' => $this->approved_at,
            'approved_by' => $this->approved_by,
            'status' => $this->status,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'refference_no', $this->refference_no])
            ->andFilterWhere(['like', 'description', $this->description]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
