<?php

namespace common\models;

use Yii;
use \common\models\base\OrderItem as BaseOrderItem;

/**
 * This is the model class for table "order_item".
 */
class OrderItem extends BaseOrderItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
           [
            [['asset_id', 'order_id', 'rq_quantity', 'app_quantity', 'current_balance', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['total_price'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe']
        ]
    );
    }


    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
    /* public function beforeSave($insert)
    {
        // custom code here
        return parent::beforeSave($insert);
    } */

    /**
     * @inheritdoc
     */
    public function updateQuantity($id)
    {
        // custom code here
        $model = self::findOne($id);
        $model->app_quantity = \common\models\AssetItem::find()->andWhere(['order_item_id' => $model->id])->count();
        return $model->save();
    }
}
