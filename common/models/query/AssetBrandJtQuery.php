<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\AssetBrandJt]].
 *
 * @see \common\models\query\AssetBrandJt
 */
class AssetBrandJtQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        if(!\Yii::$app->user->can('Administrator')) {
            $this->andWhere('[[status]]=1');
        }
        return $this;
    }*/

    public function mine()
    {
        $this->andWhere(['asset_brand_jt.created_by' => \Yii::$app->user->id]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AssetBrandJt[]|array
     */
    public function all($db = null, $bypass = false)
    {
        // uncomment and edit permission rule to view all
        /*if(!\Yii::$app->user->can('Administrator')) {
            $this->mine();
        }*/
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AssetBrandJt|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
