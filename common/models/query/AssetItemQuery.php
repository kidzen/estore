<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\AssetItem]].
 *
 * @see \common\models\query\AssetItem
 */
class AssetItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        if(!\Yii::$app->user->can('Administrator')) {
            $this->andWhere('[[status]]=1');
        }
        return $this;
    }*/

    public function mine()
    {
        // $this->andWhere(['asset_item.created_by' => \Yii::$app->user->id]);
        // $this->andWhere(['asset_item.order_item_id' => null]);
        return $this;
    }

    public function instore()
    {
        // $this->andWhere(['asset_item.created_by' => \Yii::$app->user->id]);
        $this->andWhere(['asset_item.order_item_id' => null]);
        $this->andWhere(['asset_item.dispose_id' => null]);
        return $this;
    }

    public function checkouted()
    {
        // $this->andWhere(['asset_item.created_by' => \Yii::$app->user->id]);
        $this->andWhere(['not',['asset_item.order_item_id' => null]]);
        $this->andWhere(['asset_item.dispose_id' => null]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AssetItem[]|array
     */
    public function all($db = null, $bypass = false)
    {
        // uncomment and edit permission rule to view all
        /*if(!\Yii::$app->user->can('Administrator')) {
            $this->mine();
        }*/
        
        $this->andWhere(['asset_item.section_id' => \Yii::$app->user->activeSection['id']]);
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AssetItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
