<?php

namespace common\models;

use Yii;
use \common\models\base\ApproveLevel as BaseApproveLevel;

/**
 * This is the model class for table "approve_level".
 */
class ApproveLevel extends BaseApproveLevel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
        [
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ]
        );
    }

    public function getLabel()
    {
        switch ($this->name) {
            case 'Approved':
                $label = \kartik\helpers\Html::bsLabel($this->name,'success');
                break;
            case 'Reject':
                $label = \kartik\helpers\Html::bsLabel($this->name,'danger');
                break;

            default:
                # code...
                break;
        }
        return $label;
    }


    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
    /* public function beforeSave($insert)
    {
        // custom code here
        return parent::beforeSave($insert);
    } */
}
