<?php

namespace common\models;

use Yii;
use \common\models\base\AssetItem as BaseAssetItem;
use \yii\base\Exception;

/**
 * This is the model class for table "asset_item".
 */
class AssetItem extends BaseAssetItem
{
    /**
     * @inheritdoc
     */
/*    public function rules()
    {
        return array_replace_recursive(parent::rules(),
            [
                [['section_id', 'checkin_id', 'order_item_id', 'dispose_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
                [['unit_price'], 'number'],
                [['deleted_at', 'created_at', 'updated_at'], 'safe'],
                [['serial_no', 'stock_no'], 'string', 'max' => 255],
                [['serial_no'], 'unique'],
                [['stock_no'], 'unique']
            ]
        );
    }*/

/*    public static function arrayList()
    {
        $result = \common\models\AssetItem::find()->joinWith('asset')->orderBy('id')->asArray()->all();
        return \yii\helpers\ArrayHelper::map($result, 'id', function ($model) {
            return $model['stock_no'].' - '.$model['asset']['description'];
        });
    }
*/
    public function getStatusLabel()
    {
        if($this->order_item_id){
            $result = \kartik\helpers\Html::bsLabel('Checkouted','warning');
        } elseif ($this->dispose_id) {
            $result = \kartik\helpers\Html::bsLabel('Dispose','danger');
        } else {
            $result = \kartik\helpers\Html::bsLabel('Checkin','success');
        }
        return $result;
    }

    public function getAsset()
    {
        return $this->hasOne(\common\models\Asset::className(), ['id' => 'asset_id'])->via('checkin');
    }

    public function getSection()
    {
        return $this->hasOne(\common\models\Section::className(), ['id' => 'section_id']);
    }

    public function getPost2()
    {
        if($this->itemMovement) {
            return $this->getItemMovement()->orderBy('id desc')->one()->section->name;
        } else {
            return $this->checkin->section->name;
        }
    }

    public function getMovements()
    {
        $this->hasMany(\common\models\ItemMovement::className(), ['order_id' => 'order_id'])->via('orderItem');
    }

    public function getPost()
    {
        if(!empty($this->movements)) {
            return $this->getMovements()->orderBy('item_movement.id desc')->one()->section;
        } else {
            return $this->checkin->transaction->section;
        }
    }

    public function approve($id, $orderId)
    {
        $item = self::findOne($id);
        // $orderItem = \common\models\OrderItem::find()
        // ->joinWith('order')
        // ->andWhere(['order.id' => $orderId])
        // ->andWhere(['asset_id' => $item->asset->id])
        // ->asArray()
        // ->one();
        $orderItem = \common\models\OrderItem::findOne(['order_id' => $orderId, 'asset_id' => $item->asset->id]);
        $item->order_item_id = $orderItem['id'];
        $item->save();
        \common\models\OrderItem::updateQuantity($orderItem['id']);

    }

    public function cancel($id)
    {
        $item = self::findOne($id);
        $orderItemId = $item->order_item_id;
        $item->order_item_id = null;
        $item->save();
        \common\models\OrderItem::updateQuantity($orderItemId);
    }


    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }
        $this->section_id = $this->post->id;

        $this->save(false);

/*        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }*/
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->deleted_by != 0 && $this->order_item_id){
            throw new Exception("Item is still in used", 1);
        }
        return parent::beforeSave($insert);
    }
}
