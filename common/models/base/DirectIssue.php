<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "direct_issue".
 *
 * @property integer $id
 * @property string $ivr
 * @property integer $transaction_id
 * @property integer $type_id
 * @property string $refference_no
 * @property string $particulars
 * @property integer $amount
 * @property double $total
 * @property integer $supplier_id
 * @property string $officer_name
 * @property string $date
 * @property string $receipt_no
 * @property integer $status
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\DirectIssueType $type
 * @property \common\models\Supplier $supplier
 * @property \common\models\Transaction $transaction
 * @property \common\models\User $createdBy
 * @property \common\models\User $updatedBy
 * @property \common\models\DirectIssueItem[] $directIssueItems
 */
class DirectIssue extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            'type',
            'supplier',
            'transaction',
            'createdBy',
            'updatedBy',
            'directIssueItems'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ivr'], 'required'],
            [['transaction_id', 'type_id', 'amount', 'supplier_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['particulars'], 'string'],
            [['total'], 'number'],
            [['date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['ivr', 'refference_no', 'officer_name', 'receipt_no'], 'string', 'max' => 255],
            [['ivr'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'direct_issue';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ivr' => Yii::t('app', 'Ivr'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'refference_no' => Yii::t('app', 'Refference No'),
            'particulars' => Yii::t('app', 'Particulars'),
            'amount' => Yii::t('app', 'Amount'),
            'total' => Yii::t('app', 'Total'),
            'supplier_id' => Yii::t('app', 'Supplier ID'),
            'officer_name' => Yii::t('app', 'Officer Name'),
            'date' => Yii::t('app', 'Date'),
            'receipt_no' => Yii::t('app', 'Receipt No'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(\common\models\DirectIssueType::className(), ['id' => 'type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(\common\models\Supplier::className(), ['id' => 'supplier_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(\common\models\Transaction::className(), ['id' => 'transaction_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectIssueItems()
    {
        return $this->hasMany(\common\models\DirectIssueItem::className(), ['direct_issue_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    public static function arrayList($callback = false)
    {
        $callback = is_array($callback) ? $callback : self::find()->orderBy('direct_issue.id')->asArray()->all();
        return \yii\helpers\ArrayHelper::map($callback, 'id', 'name');
    }
    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \common\models\query\DirectIssueQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \common\models\query\DirectIssueQuery(get_called_class());
        // uncomment and edit permission rule to view own items only
        /*if(!\Yii::$app->user->can('permission')){
           $query->mine();
        } */
        // uncomment and edit permission rule to view deleted items
        /*if(\Yii::$app->user->can('see_deleted')){
           return $query;
        } */
        return $query->andWhere(['direct_issue.deleted_by' => 0]);
    }
}
