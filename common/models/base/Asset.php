<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "asset".
 *
 * @property integer $id
 * @property integer $transaction_id
 * @property integer $category_id
 * @property integer $brand_id
 * @property integer $model_id
 * @property string $card_no
 * @property string $code_no
 * @property string $description
 * @property integer $quantity
 * @property integer $min_stock
 * @property integer $approved_id
 * @property string $approved_at
 * @property integer $approved_by
 * @property integer $status
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\AssetBrand $brand
 * @property \common\models\ApproveLevel $approved
 * @property \common\models\AssetModel $model
 * @property \common\models\User $approvedBy
 * @property \common\models\User $createdBy
 * @property \common\models\User $updatedBy
 * @property \common\models\Transaction $transaction
 * @property \common\models\AssetCategory $category
 * @property \common\models\AssetCheckin[] $assetCheckins
 * @property \common\models\OrderItem[] $orderItems
 */
class Asset extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            'brand',
            'approved',
            'model',
            'approvedBy',
            'createdBy',
            'updatedBy',
            'transaction',
            'category',
            'assetCheckins',
            'orderItems'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transaction_id', 'category_id', 'brand_id', 'model_id', 'quantity', 'min_stock', 'approved_id', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'required'],
            [['approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['card_no', 'code_no', 'description'], 'string', 'max' => 255],
            [['description'], 'unique'],
            [['card_no'], 'unique'],
            [['code_no'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asset';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'model_id' => Yii::t('app', 'Model ID'),
            'card_no' => Yii::t('app', 'Card No'),
            'code_no' => Yii::t('app', 'Code No'),
            'description' => Yii::t('app', 'Description'),
            'quantity' => Yii::t('app', 'Quantity'),
            'min_stock' => Yii::t('app', 'Min Stock'),
            'approved_id' => Yii::t('app', 'Approved ID'),
            'approved_at' => Yii::t('app', 'Approved At'),
            'approved_by' => Yii::t('app', 'Approved By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(\common\models\AssetBrand::className(), ['id' => 'brand_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApproved()
    {
        return $this->hasOne(\common\models\ApproveLevel::className(), ['id' => 'approved_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(\common\models\AssetModel::className(), ['id' => 'model_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'approved_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(\common\models\Transaction::className(), ['id' => 'transaction_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(\common\models\AssetCategory::className(), ['id' => 'category_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckins()
    {
        return $this->hasMany(\common\models\AssetCheckin::className(), ['asset_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(\common\models\OrderItem::className(), ['asset_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    public static function arrayList($callback = false)
    {
        $callback = is_array($callback) ? $callback : self::find()->orderBy('asset.id')->asArray()->all();
        return \yii\helpers\ArrayHelper::map($callback, 'id', 'name');
    }
    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \common\models\query\AssetQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \common\models\query\AssetQuery(get_called_class());
        // uncomment and edit permission rule to view own items only
        /*if(!\Yii::$app->user->can('permission')){
           $query->mine();
        } */
        // uncomment and edit permission rule to view deleted items
        /*if(\Yii::$app->user->can('see_deleted')){
           return $query;
        } */
        return $query->andWhere(['asset.deleted_by' => 0]);
    }
}
