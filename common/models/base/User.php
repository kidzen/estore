<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 * @property \common\models\ApproveLevel[] $approveLevels
 * @property \common\models\Asset[] $assets
 * @property \common\models\AssetBrand[] $assetBrands
 * @property \common\models\AssetBrandJt[] $assetBrandJts
 * @property \common\models\AssetCategory[] $assetCategories
 * @property \common\models\AssetCheckin[] $assetCheckins
 * @property \common\models\AssetItem[] $assetItems
 * @property \common\models\AssetModel[] $assetModels
 * @property \common\models\Budget[] $budgets
 * @property \common\models\DirectIssue[] $directIssues
 * @property \common\models\DirectIssueItem[] $directIssueItems
 * @property \common\models\DirectIssueType[] $directIssueTypes
 * @property \common\models\Dispose[] $disposes
 * @property \common\models\DisposeCategory[] $disposeCategories
 * @property \common\models\DisposeDetailKey[] $disposeDetailKeys
 * @property \common\models\DisposeDetailValue[] $disposeDetailValues
 * @property \common\models\DisposeMethod[] $disposeMethods
 * @property \common\models\DisposeReason[] $disposeReasons
 * @property \common\models\ItemMovement[] $itemMovements
 * @property \common\models\ItemMovementType[] $itemMovementTypes
 * @property \common\models\Order[] $orders
 * @property \common\models\OrderItem[] $orderItems
 * @property \common\models\Package[] $packages
 * @property \common\models\Profile $profile
 * @property \common\models\Profile[] $profiles
 * @property \common\models\Section[] $sections
 * @property \common\models\SectionAssignment[] $sectionAssignments
 * @property \common\models\SectionCategory[] $sectionCategories
 * @property \common\models\Setting[] $settings
 * @property \common\models\Supplier[] $suppliers
 * @property \common\models\Transaction[] $transactions
 * @property \common\models\TransactionType[] $transactionTypes
 * @property \common\models\Usage[] $usages
 * @property \common\models\UsageCategory[] $usageCategories
 * @property \common\models\UsageDetailKey[] $usageDetailKeys
 * @property \common\models\UsageDetailValue[] $usageDetailValues
 * @property \common\models\User $createdBy
 * @property \common\models\User[] $users
 * @property \common\models\User $updatedBy
 */
class User extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            'approveLevels',
            'assets',
            'assetBrands',
            'assetBrandJts',
            'assetCategories',
            'assetCheckins',
            'assetItems',
            'assetModels',
            'budgets',
            'directIssues',
            'directIssueItems',
            'directIssueTypes',
            'disposes',
            'disposeCategories',
            'disposeDetailKeys',
            'disposeDetailValues',
            'disposeMethods',
            'disposeReasons',
            'itemMovements',
            'itemMovementTypes',
            'orders',
            'orderItems',
            'packages',
            'profile',
            'profiles',
            'sections',
            'sectionAssignments',
            'sectionCategories',
            'settings',
            'suppliers',
            'transactions',
            'transactionTypes',
            'usages',
            'usageCategories',
            'usageDetailKeys',
            'usageDetailValues',
            'createdBy',
            'users',
            'updatedBy'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email'], 'required'],
            [['status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApproveLevels()
    {
        return $this->hasMany(\common\models\ApproveLevel::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssets()
    {
        return $this->hasMany(\common\models\Asset::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetBrands()
    {
        return $this->hasMany(\common\models\AssetBrand::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetBrandJts()
    {
        return $this->hasMany(\common\models\AssetBrandJt::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCategories()
    {
        return $this->hasMany(\common\models\AssetCategory::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckins()
    {
        return $this->hasMany(\common\models\AssetCheckin::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetItems()
    {
        return $this->hasMany(\common\models\AssetItem::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetModels()
    {
        return $this->hasMany(\common\models\AssetModel::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBudgets()
    {
        return $this->hasMany(\common\models\Budget::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectIssues()
    {
        return $this->hasMany(\common\models\DirectIssue::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectIssueItems()
    {
        return $this->hasMany(\common\models\DirectIssueItem::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectIssueTypes()
    {
        return $this->hasMany(\common\models\DirectIssueType::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposes()
    {
        return $this->hasMany(\common\models\Dispose::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposeCategories()
    {
        return $this->hasMany(\common\models\DisposeCategory::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposeDetailKeys()
    {
        return $this->hasMany(\common\models\DisposeDetailKey::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposeDetailValues()
    {
        return $this->hasMany(\common\models\DisposeDetailValue::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposeMethods()
    {
        return $this->hasMany(\common\models\DisposeMethod::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposeReasons()
    {
        return $this->hasMany(\common\models\DisposeReason::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemMovements()
    {
        return $this->hasMany(\common\models\ItemMovement::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemMovementTypes()
    {
        return $this->hasMany(\common\models\ItemMovementType::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(\common\models\Order::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(\common\models\OrderItem::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackages()
    {
        return $this->hasMany(\common\models\Package::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(\common\models\Profile::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(\common\models\Profile::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(\common\models\Section::className(), ['id' => 'section_id'])->viaTable('section_assignment', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionAssignments()
    {
        return $this->hasMany(\common\models\SectionAssignment::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionCategories()
    {
        return $this->hasMany(\common\models\SectionCategory::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettings()
    {
        return $this->hasMany(\common\models\Setting::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasMany(\common\models\Supplier::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(\common\models\Transaction::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionTypes()
    {
        return $this->hasMany(\common\models\TransactionType::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsages()
    {
        return $this->hasMany(\common\models\Usage::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsageCategories()
    {
        return $this->hasMany(\common\models\UsageCategory::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsageDetailKeys()
    {
        return $this->hasMany(\common\models\UsageDetailKey::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsageDetailValues()
    {
        return $this->hasMany(\common\models\UsageDetailValue::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(\common\models\User::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    public static function arrayList($callback = false)
    {
        $callback = is_array($callback) ? $callback : self::find()->joinWith('profile')->orderBy('user.id')->asArray()->all();
        return \yii\helpers\ArrayHelper::map($callback, 'id', 'profile.name');
    }
    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \common\models\query\UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \common\models\query\UserQuery(get_called_class());
        // uncomment and edit permission rule to view own items only
        /*if(!\Yii::$app->user->can('permission')){
           $query->mine();
        } */
        // uncomment and edit permission rule to view deleted items
        /*if(\Yii::$app->user->can('see_deleted')){
           return $query;
        } */
        return $query->andWhere(['user.deleted_by' => 0]);
    }
}
