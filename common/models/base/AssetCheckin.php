<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "asset_checkin".
 *
 * @property integer $id
 * @property string $rnvr
 * @property integer $transaction_id
 * @property integer $budget_id
 * @property integer $asset_id
 * @property integer $supplier_id
 * @property string $check_date
 * @property integer $check_by
 * @property string $purchased_date
 * @property string $refference_file
 * @property string $detail
 * @property integer $approved_id
 * @property string $approved_at
 * @property integer $approved_by
 * @property integer $status
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\Asset $asset
 * @property \common\models\Supplier $supplier
 * @property \common\models\ApproveLevel $approved
 * @property \common\models\User $checkBy
 * @property \common\models\User $approvedBy
 * @property \common\models\User $createdBy
 * @property \common\models\User $updatedBy
 * @property \common\models\Transaction $transaction
 * @property \common\models\Budget $budget
 * @property \common\models\AssetItem[] $assetItems
 */
class AssetCheckin extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            'asset',
            'supplier',
            'approved',
            'checkBy',
            'approvedBy',
            'createdBy',
            'updatedBy',
            'transaction',
            'budget',
            'assetItems'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rnvr'], 'required'],
            [['transaction_id', 'budget_id', 'asset_id', 'supplier_id', 'check_by', 'approved_id', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['check_date', 'purchased_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['detail'], 'string'],
            [['rnvr', 'refference_file'], 'string', 'max' => 255],
            [['rnvr'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asset_checkin';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rnvr' => Yii::t('app', 'Rnvr'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'budget_id' => Yii::t('app', 'Budget ID'),
            'asset_id' => Yii::t('app', 'Asset ID'),
            'supplier_id' => Yii::t('app', 'Supplier ID'),
            'check_date' => Yii::t('app', 'Check Date'),
            'check_by' => Yii::t('app', 'Check By'),
            'purchased_date' => Yii::t('app', 'Purchased Date'),
            'refference_file' => Yii::t('app', 'Refference File'),
            'detail' => Yii::t('app', 'Detail'),
            'approved_id' => Yii::t('app', 'Approved ID'),
            'approved_at' => Yii::t('app', 'Approved At'),
            'approved_by' => Yii::t('app', 'Approved By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(\common\models\Asset::className(), ['id' => 'asset_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(\common\models\Supplier::className(), ['id' => 'supplier_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApproved()
    {
        return $this->hasOne(\common\models\ApproveLevel::className(), ['id' => 'approved_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'check_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'approved_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(\common\models\Transaction::className(), ['id' => 'transaction_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBudget()
    {
        return $this->hasOne(\common\models\Budget::className(), ['id' => 'budget_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetItems()
    {
        return $this->hasMany(\common\models\AssetItem::className(), ['checkin_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    public static function arrayList($callback = false)
    {
        $callback = is_array($callback) ? $callback : self::find()->orderBy('asset_checkin.id')->asArray()->all();
        return \yii\helpers\ArrayHelper::map($callback, 'id', 'name');
    }
    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \common\models\query\AssetCheckinQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \common\models\query\AssetCheckinQuery(get_called_class());
        // uncomment and edit permission rule to view own items only
        /*if(!\Yii::$app->user->can('permission')){
           $query->mine();
        } */
        // uncomment and edit permission rule to view deleted items
        /*if(\Yii::$app->user->can('see_deleted')){
           return $query;
        } */
        return $query->andWhere(['asset_checkin.deleted_by' => 0]);
    }
}
