<?php

namespace common\models;

use Yii;
use \common\models\base\AssetCheckin as BaseAssetCheckin;

/**
 * This is the model class for table "asset_checkin".
 */
class AssetCheckin extends BaseAssetCheckin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(),
           [
            [['rnvr', 'asset_id', 'check_date'], 'required'],
            // [['transaction_id', 'budget_id', 'asset_id', 'supplier_id', 'check_by', 'approved_id', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            // [['check_date', 'purchased_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            // [['detail'], 'string'],
            // [['rnvr', 'refference_file'], 'string', 'max' => 255],
            // [['rnvr'], 'unique']
        ]
    );
    }

    public function getSection()
    {
        // var_dump($this->transaction->section->attributes);die;
        $this->hasOne(\common\models\Section::className(), ['id' => 'section_id'])->via('transaction');
    }
    public function generateBaucer()
    {
        $transType = \common\models\Transaction::TYPE_CHECKIN;
        $bilPrefix = Yii::$app->params['settings']['checkinBaucerPrefix'] ?? "SB/IN/".date('Y/');

        $length = strlen($bilPrefix)+1;
        $col = 'rnvr';
        $tableName = self::tableName();
        switch (Yii::$app->db->driverName) {
            case 'oci':
            $bilStock = self::find()
            // ->where(['EXTRACT(MONTH FROM "'.$tableName.'"."created_at")' => date('m')])
            ->where(['EXTRACT(YEAR FROM "'.$tableName.'"."created_at")' => date('Y')])
            ->joinWith('transaction')
            ->andWhere(['transaction.type_id' => $transType])
            ->max('CAST(SUBSTR("'.$tableName.'"."'.$col.'",'.$length.') AS INT)');
            break;
            case 'mysql':
            $bilStock = self::find()
            // ->where(['EXTRACT(MONTH FROM '.$tableName.'.created_at)' => date('m')])
            ->where(['EXTRACT(YEAR FROM '.$tableName.'.created_at)' => date('Y')])
            ->joinWith('transaction')
            ->andWhere(['transaction.type_id' => $transType])
            ->max('CAST(SUBSTR('.$tableName.'.'.$col.','.$length.') AS INT)');
            break;

            default:
                # code...
            break;
        }
        $bilStock = $bilStock + 1;
        return $bilPrefix.$bilStock;
    }

    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // custom code here
        if($insert) {
            $transaction = \common\models\Transaction::add(\common\models\Transaction::TYPE_CHECKIN);
            $this->transaction_id = $transaction->id;
            $this->rnvr = $this->generateBaucer();
            $this->approved_id = 3;
            // $this->section_id = Yii::$app->user->activeSection['id'];
        }
        return parent::beforeSave($insert);
    }
}
