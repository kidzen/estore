<?php
namespace common\models\form;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\base\Exception;

/**
 * Login form
 */
class AssetMovementForm extends Model
{
    public $movement_type;
    public $asset_item_id;
    public $section_id;

    public $instruction_id;
    public $order_no;
    public $order_date;
    public $ordered_by;
    public $required_date;
    public $checkout_date;
    public $checkout_by;

    public $app_quantity;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['asset_item_id', 'movement_type','order_no','section_id'], 'required'],
            [['asset_item_id','section_id','movement_type'], 'integer'],
            [['instruction_id','order_no','checkout_date','checkout_by','order_date','ordered_by','required_date',], 'string'],
            // rememberMe must be a boolean value
        ];
    }

    public function generateBaucer()
    {
        $bilPrefix = Yii::$app->params['settings']['orderBaucerPrefix'] ?? "SB/MV/".date('Y/');
        $length = strlen($bilPrefix)+1;
        $col = 'order_no';
        $tableName = \common\models\Order::tableName();
        switch (Yii::$app->db->driverName) {
            case 'oci':
            $bilStock = \common\models\Order::find()
            // ->where(['EXTRACT(MONTH FROM "'.$tableName.'"."created_at")' => date('m')])
            ->where(['EXTRACT(YEAR FROM "'.$tableName.'"."created_at")' => date('Y')])
            ->joinWith('transaction')
            ->andWhere(['transaction.type_id' => 2])
            ->max('CAST(SUBSTR("'.$tableName.'"."'.$col.'",'.$length.') AS INT)');
            break;
            case 'mysql':
            $bilStock = \common\models\Order::find()
            // ->where(['EXTRACT(MONTH FROM '.$tableName.'.created_at)' => date('m')])
            ->where(['EXTRACT(YEAR FROM '.$tableName.'.created_at)' => date('Y')])
            ->joinWith('transaction')
            ->andWhere(['transaction.type_id' => 2])
            ->max('CAST(SUBSTR('.$tableName.'.'.$col.','.$length.') AS INT)');
            break;

            default:
                # code...
            break;
        }
        $bilStock = $bilStock + 1;
        return $bilPrefix.$bilStock;

        // $lastBillQuery = \common\models\Order::find();
        // $lastBillQuery->where = null;
        // $lastBill = $lastBillQuery->max($col);
        // $baucer = $bilPrefix.intval($lastBill);
        // // var_dump($baucer);die;
        // return $baucer;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function save()
    {
        $movement = new \common\models\ItemMovement();
        $order = new \common\models\Order();
        $orderItem = new \common\models\OrderItem();
        $item = \common\models\AssetItem::findOne($this->asset_item_id);


        $order->instruction_id = $this->instruction_id;
        $order->order_no = $this->generateBaucer();
        $order->order_date = $this->order_date;
        $order->ordered_by = $this->ordered_by;
        $order->required_date = $this->required_date;
        $order->checkout_date = $this->checkout_date;
        $order->checkout_by = $this->checkout_by;
        // var_dump($order->attributes);die;

        if(!$order->save()) {
            throw new Exception("Error Processing Request", 1);
        }

        $orderItem->section_id = $this->section_id;
        $orderItem->order_id = $order->id;
        $orderItem->asset_id = \common\models\AssetItem::findOne($this->asset_item_id)->asset->id;

        if(!$orderItem->save()) {
            throw new Exception("Error Processing Request", 1);
        }

        $movement->type = $this->movement_type;
        $movement->section_id = $this->section_id;
        $movement->order_item_id = $orderItem->id;
        $movement->asset_item_id = $this->asset_item_id;
        if(!$movement->save()) {
            throw new Exception("Error Processing Request", 1);
        }
        $item->section_id = $this->section_id;
        if(!$item->save()) {
            throw new Exception("Error Processing Request", 1);
        }

        return true;
    }

}
