<?php

namespace common\models;

use Yii;
use \common\models\base\Dispose as BaseDispose;

/**
 * This is the model class for table "dispose".
 */
class Dispose extends BaseDispose
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['transaction_id', 'category_id', 'method_id', 'reason_id', 'quantity', 'requested_by', 'disposed_by', 'approved_id', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['current_revenue', 'cost', 'returns'], 'number'],
            [['requested_date', 'disposed_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['refference_no', 'description'], 'string', 'max' => 255]
        ]
        );
    }


    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
    /* public function beforeSave($insert)
    {
        // custom code here
        return parent::beforeSave($insert);
    } */
}
