<?php

namespace common\models;

use Yii;
use \common\models\base\Order as BaseOrder;

/**
 * This is the model class for table "order".
 */
class Order extends BaseOrder
{
    public $transactionType;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
           [
            [['transaction_id', 'instruction_id', 'ordered_by', 'usage_id', 'checkout_by', 'approved', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['order_date', 'required_date', 'checkout_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['order_no'], 'string', 'max' => 255]
        ]
    );
    }


    public function generateBaucer()
    {
        if($this->transactionType == \common\models\Transaction::TYPE_MOVEMENT) {
            $bilCode = 'MV';
            $transType = \common\models\Transaction::TYPE_MOVEMENT;
            $bilPrefix = Yii::$app->params['settings']['movementBaucerPrefix'] ?? "SB/$bilCode/".date('Y/');
        } else if($this->transactionType == \common\models\Transaction::TYPE_CHECKOUT) {
            $bilCode = 'OUT';
            $transType = \common\models\Transaction::TYPE_CHECKOUT;
            $bilPrefix = Yii::$app->params['settings']['orderBaucerPrefix'] ?? "SB/$bilCode/".date('Y/');
        }

        $length = strlen($bilPrefix)+1;
        $col = 'order_no';
        $tableName = self::tableName();
        switch (Yii::$app->db->driverName) {
            case 'oci':
            $bilStock = self::find()
            // ->where(['EXTRACT(MONTH FROM "'.$tableName.'"."created_at")' => date('m')])
            ->where(['EXTRACT(YEAR FROM "'.$tableName.'"."created_at")' => date('Y')])
            ->joinWith('transaction')
            ->andWhere(['transaction.type_id' => $transType])
            ->max('CAST(SUBSTR("'.$tableName.'"."'.$col.'",'.$length.') AS INT)');
            break;
            case 'mysql':
            $bilStock = self::find()
            // ->where(['EXTRACT(MONTH FROM '.$tableName.'.created_at)' => date('m')])
            ->where(['EXTRACT(YEAR FROM '.$tableName.'.created_at)' => date('Y')])
            ->joinWith('transaction')
            ->andWhere(['transaction.type_id' => $transType])
            ->max('CAST(SUBSTR('.$tableName.'.'.$col.','.$length.') AS INT)');
            break;

            default:
                # code...
            break;
        }
        $bilStock = $bilStock + 1;
        return $bilPrefix.$bilStock;

        // $lastBillQuery = self::find();
        // $lastBillQuery->where = null;
        // $lastBill = $lastBillQuery->max($col);
        // $baucer = $bilPrefix.intval($lastBill);
        // // var_dump($baucer);die;
        // return $baucer;
    }

    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // custom code here
        if($insert) {
            $transaction = \common\models\Transaction::add($this->transactionType);
            $this->transaction_id = $transaction->id;
            $this->order_no = $this->generateBaucer();
            // $this->section_id = Yii::$app->user->activeSection['id'];
        }
        return parent::beforeSave($insert);
    }

    public function approveTransaction($id)
    {
        $model = self::findOne($id);
        $model->approved_id = 3;
        return $model->save(false);
    }

}
