<?php
return [
    'superadmin' => [
        'type' => 2,
        'description' => 'User can Superadmin',
    ],
    'admin' => [
        'type' => 2,
        'description' => 'User can Admin',
    ],
    'purchase' => [
        'type' => 2,
        'description' => 'User can Purchase',
    ],
    'manage-asset' => [
        'type' => 2,
        'description' => 'User can Manage Asset',
    ],
    'manage-asset-checkin' => [
        'type' => 2,
        'description' => 'User can Manage Asset Checkin',
    ],
    'manage-asset-disposal' => [
        'type' => 2,
        'description' => 'User can Manage Asset Disposal',
    ],
    'manage-asset-movement' => [
        'type' => 2,
        'description' => 'User can Manage Asset Movement',
    ],
    'manage-inventory' => [
        'type' => 2,
        'description' => 'User can Manage Inventory',
    ],
    'manage-inventory-checkin' => [
        'type' => 2,
        'description' => 'User can Manage Inventory Checkin',
    ],
    'manage-inventory-checkout' => [
        'type' => 2,
        'description' => 'User can Manage Inventory Checkout',
    ],
    'manage-direct-issue' => [
        'type' => 2,
        'description' => 'User can Manage Direct Issue',
    ],
    'Administrator' => [
        'type' => 1,
        'description' => 'Administrator',
        'children' => [
            'superadmin',
            'admin',
            'purchase',
            'manage-asset',
            'manage-asset-checkin',
            'manage-asset-disposal',
            'manage-asset-movement',
            'manage-inventory',
            'manage-inventory-checkin',
            'manage-inventory-checkout',
            'manage-direct-issue',
        ],
    ],
    'Asset Admin' => [
        'type' => 1,
        'description' => 'Asset Admin',
    ],
    'Asset User' => [
        'type' => 1,
        'description' => 'Asset User',
    ],
    'Store Admin' => [
        'type' => 1,
        'description' => 'Store Admin',
    ],
    'Store User' => [
        'type' => 1,
        'description' => 'Store User',
    ],
    'Supplier' => [
        'type' => 1,
        'description' => 'Supplier',
    ],
    'Vendor' => [
        'type' => 1,
        'description' => 'Vendor',
    ],
    'Client' => [
        'type' => 1,
        'description' => 'Client',
    ],
];
