<?php
return [
	// system params
    'adminEmail' => 'support@muzzam-tek.com',
	'version'=>'1.0',

	// company params
    'appNameShort' => 'eStore', //size 132x132
    'appNameFull' => 'eStore PDRM', //size 132x132
    'companyNameShort' => 'PDRM',
    'companyNameFull' => 'eStore PDRM System',
    'companyWebsite' => 'http://www.muzzam-tek.com.my/',

	// assets param

    // 'iconPath' => '/img/FEMS_logo/fems_icon_HD.ico', //size 132x132
    // 'companyLogoIcon' => '/img/FEMS_logo/FEMS-white-logo-250x100.png', //size 32x32
    // 'companyLogo' => '/img/FEMS_logo/FEMS-white-logo-250x100.png', //size 32x32
    // 'companyLogoAlt' => '/img/FEMS_logo/FEMS-black-logo-250x100.png', //size 32x32

    'iconPath' => '/img/logo_sb_pdrm.jpeg', //size 132x132
    'companyLogoIcon' => '/img/logo_sb_pdrm.jpeg', //size 32x32
    'companyLogo' => '/img/logo_sb_pdrm.jpeg', //size 32x32
    'companyLogoAlt' => '/img/logo_sb_pdrm.jpeg', //size 32x32
];
