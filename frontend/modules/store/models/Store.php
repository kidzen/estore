<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Store as BaseStore;

/**
 * This is the model class for table "store".
 */
class Store extends BaseStore
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
            [
                [['category_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
                [['deleted_at', 'created_at', 'updated_at'], 'safe'],
                [['name', 'address', 'contact_no'], 'string', 'max' => 255]
            ]
        );
    }

    public static function arrayList()
    {
        $return = \yii\helpers\ArrayHelper::map(
            \frontend\modules\store\models\Store::find()
            ->where(['deleted_by' => 0])
            ->andWhere(['not in', 'id', Yii::$app->cache->get('active-store')])
            ->orderBy('id')->asArray()->all()
            , 'id', 'name');
        return $return;
    }
}
