<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\StoreAssignment as BaseStoreAssignment;

/**
 * This is the model class for table "store_assignment".
 */
class StoreAssignment extends BaseStoreAssignment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['store_id', 'user_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }

}
