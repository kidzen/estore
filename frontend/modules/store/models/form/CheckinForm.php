<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\modules\store\models\InventoryCheckin;
use frontend\modules\store\models\ActivityLog;

/**
 * ContactForm is the model behind the contact form.
 */
class CheckinForm extends Model
{
    public $id;
    public $inventory_id;
    public $vendor_id;
    public $store_id;
    public $quantity;
    public $unit_price;
    public $total_price;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory_id', 'vendor_id', 'store_id','quantity','unit_price'], 'required'],
            [['inventory_id', 'vendor_id', 'store_id','quantity'], 'integer'],
            [['unit_price','total_price'] 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inventory_id' => Yii::t('app','Inventory'),
            'vendor_id' => Yii::t('app','Vendor'),
            'store_id' => Yii::t('app','Store'),
            'quantity' => Yii::t('app','Quantity'),
            'unit_price' => Yii::t('app','Unit Price'),
        ];
    }

    /**
     * Submit checkin items request.
     *
     * @param object $model object of checkin model
     * @return bool whether the transaction was succes
     */
    public function submit()
    {
        $this->total_price = $this->unit_price * $this->quantity;

        $inventoryCheckin = new InventoryCheckin();
        $inventoryCheckin->inventory_id = $this->inventory_id;
        $inventoryCheckin->store_id = $this->store_id;
        $inventoryCheckin->vendor_id = $this->vendor_id;
        $inventoryCheckin->items_unit_price = $this->total_price;
        $inventoryCheckin->items_quantity = $this->quantity;

        //log checkin transaction
        if($inventoryCheckin->save()) {
            //add new item into inventory item
            for($i = 0,$i < $this->quantity,$i++;) {
                $valid = InventoryItem::add($this->inventory_id,$inventoryCheckin->id);
            }
            //log transaction into activity log
            $valid = $valid && ActivityLog::success();
            if($valid) {
                ActivityLog::success();
                return true;
            }
        }
        return throw new Exception(Yii::t('app',"Error Processing Request"), 1);
    }
}
