<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Approval as BaseApproval;

/**
 * This is the model class for table "approval".
 */
class Approval extends BaseApproval
{

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
         [
            [['id', 'total_price', 'unit_price'], 'number'],
            [['id_inventory_item', 'id_inventory', 'id_category', 'id_inventory_checkin', 'id_order_item', 'id_order', 'rq_quantity', 'app_quantity', 'approved', 'current_balance'], 'integer'],
            [['sku', 'order_no', 'items_category_name', 'items_inventory_name', 'order_date', 'required_date'], 'string', 'max' => 255],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryItem()
    {
        return $this->hasOne(\frontend\modules\store\models\InventoryItem::className(), ['id' => 'id_inventory_item']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemInventoryName()
    {
        return $this->inventory->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryCheckin()
    {
        return $this->hasOne(\frontend\modules\store\models\InventoryCheckin::className(), ['id' => 'id_inventory_checkin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(\frontend\modules\store\models\Order::className(), ['id' => 'id_order']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(\frontend\modules\store\models\Inventory::className(), ['id' => 'id_inventory']);
    }


}
