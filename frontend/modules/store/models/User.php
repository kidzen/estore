<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\identity\User as BaseUser;

/**
 * This is the model class for table "user".
 */
class User extends BaseUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        // return array_replace_recursive(parent::rules(),
        return
        [
            [['role_id'], 'required'],
            [['role_id', 'status', 'deleted_by'], 'integer'],
            [['username', 'email', 'profile_pic', 'password', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
        // ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(\frontend\modules\store\models\Profile::className(), ['staff_no' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function arrayList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->joinWith('profile')->orderBy('user.id')->asArray()->all(), 'id', function($model) {return $model['profile']['staff_no'].' : '.$model['profile']['name'];});
    }

}
