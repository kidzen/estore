<?php

namespace frontend\modules\store\models;

use Yii;
use kartik\helpers\Html;
use \frontend\modules\store\models\base\OrderItem as BaseOrderItem;

/**
 * This is the model class for table "order_item".
 */
class OrderItem extends BaseOrderItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory_id', 'order_id', 'rq_quantity', 'app_quantity', 'current_balance', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['unit_price'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['rq_quantity', 'inventory_id','order_id'], 'required'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($insert){
            $this->current_balance = Inventory::findOne($this->inventory_id)->quantity;
        }
        return true;
    }


    /**
     * @return \yii\db\ActiveQuery
     */

    public function getInventoriesDetail()
    {
        $cardNo = Html::tag('div',Yii::t('app','Card No').' : '.$this->inventory->card_no);
        $cardNo = isset($this->inventory->card_no) ? $cardNo : null;
        $codeNo = Html::tag('div',Yii::t('app','Code No').' : '.$this->inventory->code_no);
        $codeNo = isset($this->inventory->code_no) ? $codeNo : null;
        $description = Html::tag('div',Yii::t('app','Description').' : '.$this->inventory->description);
        $description = isset($this->inventory->description) ? $description : null;
        $quantity = Html::tag('div',Yii::t('app','Balance').' : '.$this->inventory->quantity);
        $quantity = isset($this->inventory->quantity) ? $quantity : null;
        $result = $cardNo . $codeNo . $description . $quantity;
        return $result;
    }

    public function getRequestQuantitiesDetail()
    {
        $requested = Html::tag('div',Yii::t('app','Requested').' : '.$this->rq_quantity);
        $requested = isset($this->rq_quantity) ? $requested : null;
        $approved = Html::tag('div',Yii::t('app','Approved').' : '.$this->app_quantity);
        $approved = isset($this->app_quantity) ? $approved : null;
        $result = $requested . $approved;

        // $result = '
        // <div style="min-width:110px;">
        //     <span class="pull-left">
        //         <div class="pull-right">'.Yii::t('app','Requested : ').'</div><br>
        //         <div class="pull-right">'.Yii::t('app','Approved : ').'</div>
        //     </span>
        //     <span class="pull-right">
        //         <div class="pull-right">
        //             <span class="pull-right">'.$this->rq_quantity.'</span>
        //             <br>
        //             <span class="pull-right">'.$this->app_quantity.'</span>
        //         </div>
        //     </span>
        // </div>
        // ';

        return $result;
    }

    public function getUsage()
    {
        return $this->hasOne(\frontend\modules\store\models\Usage::className(), ['id'=>'id']);
    }

    public static function updateApproveQuantity($id)
    {
        $model = self::findOne($id);
        if($model) {
            // var_dump($model->getInventoryItems()->asArray()->all());die;
            $model->app_quantity = $model->getInventoryItems()
            // ->andWhere(['order_item.deleted_by'=>0])
            ->count();
            // $model->app_quantity = 1;
            return $model->save();
        } else {
            throw new Exception("No Data found", 1);
            return false;
        }
    }

    public function getApprovedLabel()
    {
        switch ($this->order->approved) {
            case 1:
            return Html::BsLabel('Approved','success');
            break;
            case 2:
            return Html::BsLabel('Pending','warning');
            break;
            case 8:
            return Html::BsLabel('Rejected','danger');
            break;

            default:
            return null;
            break;
        }
        return $this->hasOne(\frontend\modules\store\models\Usage::className(), ['id'=>'id']);
    }

    public function getUsageDetails()
    {
        $requested = Html::tag('div',Yii::t('app','Card No : ').$this->rq_quantity);
        $requested = isset($this->rq_quantity) ? $requested : null;
        $approved = Html::tag('div',Yii::t('app','Code No : ').$this->app_quantity);
        $approved = isset($this->app_quantity) ? $approved : null;
        $result = $requested . $approved;
        return $result;
    }

}
