<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\VehicleList as BaseVehicleList;

/**
 * This is the model class for table "vehicle_list".
 */
class VehicleList extends BaseVehicleList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['reg_no', 'model', 'type'], 'string', 'max' => 255]
        ]);
    }

}
