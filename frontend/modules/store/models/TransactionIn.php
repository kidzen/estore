<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\TransactionIn as BaseTransactionIn;

/**
 * This is the model class for table "transaction_in".
 */
class TransactionIn extends BaseTransactionIn
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'type', 'check_by', 'refference_id', 'approved_by', 'inventory_id'], 'integer'],
            [['approved_at'], 'safe'],
            [['unit_price', 'count', 'total_price'], 'number'],
            [['check_date'], 'string', 'max' => 7],
            [['refference'], 'string', 'max' => 255]
        ]);
    }

}
