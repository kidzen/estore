<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\ReportAll as BaseReportAll;

/**
 * This is the model class for table "report_all".
 */
class ReportAll extends BaseReportAll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'year', 'count_in', 'price_in', 'COUNT_OUT', 'PRICE_OUT', 'count_current', 'price_current'], 'number'],
            [['quarter'], 'string', 'max' => 1]
        ]);
    }

}
