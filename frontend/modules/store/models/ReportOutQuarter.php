<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\ReportOutQuarter as BaseReportOutQuarter;

/**
 * This is the model class for table "report_out_quarter".
 */
class ReportOutQuarter extends BaseReportOutQuarter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['year', 'count', 'total_price'], 'number'],
            [['quarter'], 'string', 'max' => 1]
        ]);
    }

}
