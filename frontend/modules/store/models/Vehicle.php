<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Vehicle as BaseVehicle;

/**
 * This is the model class for table "vehicle".
 */
class Vehicle extends BaseVehicle
{
    /**
     * @inheritdoc
     */

    public function getUsageList()
    {
        return $this->hasOne(\frontend\modules\store\models\UsageList::className(), ['ref_id' => 'id'])
        ->andWhere(['ref_cat' => 'vehicle']);
    }

}
