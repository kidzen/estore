<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\DisposeCategory as BaseDisposeCategory;

/**
 * This is the model class for table "dispose_category".
 */
class DisposeCategory extends BaseDisposeCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ]);
    }

}
