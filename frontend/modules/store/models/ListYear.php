<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\ListYear as BaseListYear;

/**
 * This is the model class for table "list_year".
 */
class ListYear extends BaseListYear
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['year'], 'number'],
            [['quarter'], 'string', 'max' => 1]
        ]);
    }

}
