<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Profile as BaseProfile;

/**
 * This is the model class for table "profile".
 */
class Profile extends BaseProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_no', 'name', 'position', 'department'], 'string', 'max' => 255]
        ]);
    }

    public static function primaryKey()
    {
        return ['staff_no'];
    }

    public function getStoreAssignments()
    {
        return $this->hasMany(\frontend\modules\store\models\StoreAssignment::className(),['user_id'=>'staff_no']);
    }

}
