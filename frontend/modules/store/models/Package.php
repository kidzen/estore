<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Package as BasePackage;

/**
 * This is the model class for table "package".
 */
class Package extends BasePackage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['order_id', 'package_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['package_date', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['detail', 'delivery'], 'string', 'max' => 255]
        ]);
    }

}
