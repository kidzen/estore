<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\InventoryItem as BaseInventoryItem;
use yii\base\Exception;
use yii\db\Expression;

/**
 * This is the model class for table "inventory_item".
 */
class InventoryItem extends BaseInventoryItem
{
    /**
     * @inheritdoc
     */
    public static function SkuGenerator($options)
    {
        switch (Yii::$app->dbStore->driverName) {
            case 'oci':
            // generate sku
            $skuCounter = self::find()
            //  xxxx-xxxx-{SKU}
            ->select("MAX(SUBSTR(\"sku\", -6)) AS \"sku\"")
            ->joinWith('inventory')
            // //  xxxx-{MMYY}-xxxx
            ->where(["SUBSTR(\"sku\", -11,4)" => date('my')])
            // //  xxxx-{MMYY}-xxxx
            ->andWhere(['"inventory"."code_no"' => $options['inventory']['code_no']])
            ->one();
            $skuCounter = isset($skuCounter->sku) ? substr($skuCounter->sku+1, -6) : 0;

            $skuId = str_pad($skuCounter, 6, STR_PAD_LEFT, 0);
            // $skuId = str_pad($skuCounter, 6, STR_PAD_LEFT, 0);
            // $sku = $options['inventory']['code_no'].'-'.date('my').'-'.$skuId;
            break;
            case 'mysql':
            // generate sku
            $skuCounter = self::find()
            //  xxxx-xxxx-{SKU}
            ->select(["MAX(SUBSTR([[sku]], -6)) AS \"sku\""])
            ->joinWith('inventory')
            // //  xxxx-{MMYY}-xxxx
            ->where(["SUBSTR([[sku]], -11,4)" => date('my')])
            // //  xxxx-{MMYY}-xxxx
            ->andWhere(['inventory.code_no' => $options['inventory']['code_no']])
            ->one();

            $skuCounter = ($skuCounter->sku != null) ? substr($skuCounter->sku+1, -6) : 0;
            $skuId = str_pad($skuCounter, 6, STR_PAD_LEFT, 0);
            // $skuId = str_pad($skuCounter, 6, STR_PAD_LEFT, 0);
            // $sku = $options['inventory']['code_no'].'-'.date('my').'-'.$skuId;
            break;

            default:
                # code...
            break;
        }
        return $skuCounter;

    }

    public static function checkin($options)
    {
        $valid = true;
        $defaultMaxExecutionTime = ini_get('max_execution_time');
        $defaultMemoryLimit = ini_get('memory_limit');
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '-1');
        for ($i=0; $i < $options['items_quantity']; $i++) {
            # code...
            $item = new self();
            $item->inventory_id = $options['inventory']['id'];
            $item->unit_price = $options['unit_price'];
            $item->checkin_id = $options['checkin_id'];
            $skuId = str_pad(self::skuGenerator($options), 6, STR_PAD_LEFT, 0);
            $sku = $options['inventory']['code_no'].'-'.date('my').'-'.$skuId;
            $item->sku = $sku;
            // $item->deleted_by = 0;
            // $item->deleted_at = new Expression('CURRENT_TIMESTAMP');
            // $item->created_at = new Expression('CURRENT_TIMESTAMP');
            // $item->updated_at = new Expression('CURRENT_TIMESTAMP');
            // $item->created_by = Yii::$app->user->id;
            // $item->updated_by = Yii::$app->user->id;
                // var_dump($item->sku);
            if (!$valid = $valid && $item->validate()) {
                // At least one model has invalid data
                throw new Exception(serialize($item->errors));

                break;
            }
            // $valid = $valid && $item->validate();
            $valid = $valid && $item->save(false);
            // $rows[0] = $item->attributes;

        }
        // var_dump($item->attributes);die;
        // $command = Yii::$app->dbStore->createCommand()
        // ->batchInsert(self::tableName(), (new self)->attributes(), $rows)
        // ->execute();
        // ini_set('max_execution_time', $defaultMaxExecutionTime);
        // ini_set('memory_limit', $defaultMemoryLimit);
        if($valid){
            $valid = $valid && Inventory::updateQuantity($options['inventory']['id']);
        } else {
            throw new Exception("Fail to add new items", 1);
        }
        return $valid;
    }

    public static function Approve($id, $orderItemId)
    {
        $inventoryItem = InventoryItem::findOne($id);
        $inventory = Inventory::findOne($inventoryItem->checkin->inventory_id);
        $orderItem = OrderItem::findOne($orderItemId);
        $order = Order::findOne($orderItem->order_id);

        $dbtransac = \Yii::$app->dbStore->beginTransaction();
        try {
            if ($order && $orderItem && $inventory && $inventoryItem) {
                //assign checkout id to items(link)
                $inventoryItem->checkout_id = $orderItemId;
                //remove 1 items from inventory
                $inventory->quantity = $inventory->quantity - 1;
                //add 1 item to approved quantity
                $orderItem->app_quantity = $orderItem->app_quantity + 1;
                //update total price of checkout items
                $orderItem->unit_price = $orderItem->unit_price + $inventoryItem->unit_price;
                //save all model
                if (!$inventoryItem->save(false))
                    Throw new Exception(' Data item inventori tidak dapat dikemaskini.');
                if (!$inventory->save(false))
                    Throw new Exception(' Data inventori tidak dapat dikemaskini.');
                if (!$orderItem->save(false))
                    Throw new Exception(' Data pesanan tidak dapat dikemaskini.');
                $dbtransac->commit();
                \Yii::$app->notify->success(['message' => 'Proses kemaskini data berjaya']);
            } else {
                throw new Exception(' Data tidak wujud.');
            }
        } catch (Exception $e) {
            \Yii::$app->notify->fail(['message' => $e->getMessage()]);
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail(['message' => $e->getMessage()]);
            $dbtransac->rollBack();
        }

    }
    public static function ApprovalCancel($id,$orderItemId)
    {
        $inventoryItem = InventoryItem::findOne($id);
        $inventory = Inventory::findOne($inventoryItem->checkin->inventory_id);
        $orderItem = OrderItem::findOne($orderItemId);
        $order = Order::findOne($orderItem->order_id);

        $dbtransac = \Yii::$app->dbStore->beginTransaction();
        try {
            if ($order && $orderItem && $inventory && $inventoryItem) {
                //remove checkout id to item(link)
                $inventoryItem->checkout_id = null;
                //add 1 item from inventory #enhance:use inventories update quantity function
                $inventory->quantity = $inventory->quantity + 1;
                //remove 1 item to approved quantity
                $orderItem->app_quantity = $orderItem->app_quantity - 1;
                //update total price of checkout item
                $orderItem->unit_price = $orderItem->unit_price - $inventoryItem->unit_price;
                //save all model
                if (!$inventoryItem->save(false))
                    Throw new Exception(' Data item inventori tidak dapat dikemaskini.');
                if (!$inventory->save(false))
                    Throw new Exception(' Data inventori tidak dapat dikemaskini.');
                if (!$orderItem->save(false))
                    Throw new Exception(' Data pesanan tidak dapat dikemaskini.');
                $dbtransac->commit();
                \Yii::$app->notify->success(['message' => 'Proses kemaskini data berjaya']);
            } else {
                throw new Exception(' Data tidak wujud.');
            }
        } catch (Exception $e) {
            \Yii::$app->notify->fail(['message' => $e->getMessage()]);
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail(['message' => $e->getMessage()]);
            $dbtransac->rollBack();
        }
    }
    public static function dispose($ids,$disposeId)
    {
        $valid = true;
        $defaultMaxExecutionTime = ini_get('max_execution_time');
        $defaultMemoryLimit = ini_get('memory_limit');
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '-1');
        foreach ($ids as $key => $id) {
            $item = self::findOne($id);
            $item->dispose_id = $disposeId;
            $item->save(false);
        }
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(\frontend\modules\store\models\Order::className(), ['id' => 'order_id'])->via('checkout');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(\frontend\modules\store\models\Store::className(), ['id' => 'store_id'])->via('checkin');
    }

}

