<?php

namespace frontend\modules\store\models;

use Yii;

use \frontend\modules\store\models\base\Credential as BaseCredential;

/**
 * This is the model class for table "credential".
 */
class Credential extends BaseCredential
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_no', 'password'], 'required'],
            [['staff_no'], 'integer'],
            [['password'], 'string', 'max' => 100],
            [['staff_no'], 'unique']
        ]);
    }

    public function afterFind()
    {
        $this->password = Yii::$app->security->generatePasswordHash($this->password);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByStaffNo($staffNo)
    {
        return static::findOne(['staff_no' => $staffNo]);
    }

}
