<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\UsageCategory as BaseUsageCategory;

/**
 * This is the model class for table "usage_category".
 */
class UsageCategory extends BaseUsageCategory
{
    /**
     * @inheritdoc
     */

    public function getVehicles()
    {
        return $this->hasMany(\frontend\modules\store\models\Vehicle::className(), ['id'=>'item_id'])->via('usages');
    }

    public function getStores()
    {
        return $this->hasMany(\frontend\modules\store\models\Store::className(), ['id'=>'item_id'])->via('usages');
    }

    public function getWorkshops()
    {
        return $this->hasMany(\frontend\modules\store\models\Workshop::className(), ['id' => 'item_id'])->via('usages');
    }

}
