<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\TransactionAll as BaseTransactionAll;

/**
 * This is the model class for table "transaction_all".
 */
class TransactionAll extends BaseTransactionAll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'type', 'check_by', 'inventory_id'], 'integer'],
            [['unit_price', 'count_in', 'price_in', 'count_out', 'price_out', 'count_current', 'price_current'], 'number'],
            [['check_date'], 'string', 'max' => 7],
            [['approved_date'], 'string', 'max' => 8],
            [['refference'], 'string', 'max' => 255]
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'check_by']);
    }

}
