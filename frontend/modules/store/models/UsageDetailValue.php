<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\UsageDetailValue as BaseUsageDetailValue;

/**
 * This is the model class for table "usage_detail_value".
 */
class UsageDetailValue extends BaseUsageDetailValue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['key_id', 'usage_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['value', 'refference'], 'string', 'max' => 255]
        ]);
    }

}
