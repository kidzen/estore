<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Order as BaseOrder;
use \yii\base\Exception;

/**
 * This is the model class for table "order".
 */
class Order extends BaseOrder
{
    /**
     * @inheritdoc
     */
    const STATUS_REJECTED = 8;
    const STATUS_APPROVED = 1;
    const STATUS_PENDING = 2;

    const SCENARIO_UPDATE = 'update';


    /**
     * @inheritdoc
     */
    public static function arrayList()
    {
        $callback = function($data) {
            return $data['order_no'];
        };
        $orderArray = \yii\helpers\ArrayHelper::map(self::find()->orderBy('id')->asArray()->all(), 'id', $callback);
        return $orderArray;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transaction_id', 'instruction_id', 'ordered_by', 'usage_id', 'checkout_by', 'approved', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['order_date', 'required_date', 'checkout_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['ordered_by_temp', 'order_no'], 'string', 'max' => 255],
            ['required_date', 'validateRequiredDate'],
            [['required_date', 'order_date'],'required'],
/*            [
                'required_date', 'compare', 'compareAttribute' => 'order_date', 'operator' => '>',
                'message' => '"Tarikh Diperlukan" tidak boleh mendahului "Tarikh Pesanan"',
                'enableClientValidation' => true,
            ],
            [
                'required_date', 'compare', 'compareAttribute' => 'order_date', 'operator' => '>=',
                'enableClientValidation' => true, 'message' => '"Tarikh Diperlukan" tidak boleh mendahului "Tarikh Pesanan"'
            ],
*/
/*            // [['order_date', 'required_date'], 'date','format'=>'php:U'],
            [['order_date', 'required_date'], 'required'],

            // [['order_date', 'required_date'], 'date', 'timestampAttributeFormat ' => 'php:d-M-Y'],
            ['order_date', 'date', 'timestampAttribute' => 'fromDate', 'timestampAttributeFormat ' => 'php:d-M-Y'],
            ['required_date', 'date', 'timestampAttribute' => 'toDate'],
            // ['order_date', 'compare', 'compareAttribute' => 'required_date', 'operator' => '<=', 'enableClientValidation' => true],
            [
                'required_date', 'compare', 'compareAttribute' => 'order_date',
                'operator' => '>=', 'enableClientValidation' => true,
                'message' => '{attribute} tidak boleh mendahului {compareAttribute}'
            ],

*/            // ['fromDate', 'date', 'timestampAttribute' => 'fromDate'],
            // ['toDate', 'date', 'timestampAttribute' => 'toDate'],
            // ['fromDate', 'compare', 'compareAttribute' => 'toDate', 'operator' => '<', 'enableClientValidation' => false],
        ];
    }

    // public function scenarios()
    // {
    //     $scenarios = parent::scenarios();
    //     $scenarios[self::SCENARIO_UPDATE] = ['email', 'password'];

    //     return $scenarios;
    // }

    public function validateRequiredDate() {
        if(strtotime($this->required_date) < strtotime($this->order_date)){
            $this->addError('order_date','"Tarikh Pesanan" tidak boleh melebihi "Tarikh Diperlukan"');
            $this->addError('required_date','"Tarikh Diperlukan" tidak boleh mendahului "Tarikh Pesanan"');
        }
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_APPROVED => 'Approved',
            self::STATUS_PENDING => 'Pending',
            self::STATUS_REJECTED => 'Rejected',
        ];
    }
    public function getOrderPrefix()
    {
        switch (Yii::$app->dbStore->driverName) {
            case 'oci':
            $bilStock = self::find()
            ->andWhere(['EXTRACT(MONTH FROM "created_at")' => date('m')])
            ->andWhere(['EXTRACT(YEAR FROM "created_at")' => date('Y')])
            ->max('CAST(SUBSTR("order_no",9) AS INT)');
            break;
            case 'mysql':
            $bilStock = self::find()
            ->andWhere(['EXTRACT(MONTH FROM created_at)' => date('m')])
            ->andWhere(['EXTRACT(YEAR FROM created_at)' => date('Y')])
            ->max('CAST(SUBSTR(order_no,9) AS INT)');
            break;

            default:
                # code...
            break;
        }
        $bilStock = $bilStock + 1;
        return date('Y/m-').$bilStock;
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($insert){
            if(!$transactionId = Transaction::add(Transaction::CHECKOUT)) {
                throw new Exception("Fail to initialize transaction", 1);
            }
            $this->transaction_id = $this->orderPrefix;
            $this->transaction_id = $transactionId;
            $this->approved = self::STATUS_PENDING;
            $this->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
            $this->approved_by = Yii::$app->user->id;
        }
        return true;
    }

    public function updateOrder()
    {
        foreach ($this->orderItems as $key => $orderItem) {
            // var_dump($orderItem['rq_quantity']);
            // var_dump($orderItem['rq_quantity'] < $orderItem['app_quantity'] ?? 0);
            if($orderItem['rq_quantity'] < isset($orderItem['app_quantity']) ? $orderItem['app_quantity'] : 0) {
                throw new Exception("Approve quantity error", 1);
            }
            $orderItem->link('order',$this);
            if(!$orderItem->save()) {
                throw new Exception("order item save fail", 1);
            }
        }
        if(!$this->save()) {
            throw new Exception("Error Processing Request", 1);
        }
        return true;
    }

    public static function fifoApproval($id)
    {
        $model = self::findOne($id);
        foreach ($model->orderItems as $i => $orderItem) {
            $items = InventoryItem::find()
            ->select('inventory_item.id,checkin_id')
            ->andWhere(['inventory_item.inventory_id'=>$orderItem->inventory->id])
            ->andWhere(['inventory_item.checkout_id'=>null])
            ->limit($orderItem['rq_quantity'] - $orderItem['app_quantity'])
            ->orderBy(['inventory_item.created_at'=>SORT_ASC])
            // ->distinct()
            ->asArray()->all();
            foreach ($items as $key => $item) {
                InventoryItem::approve($item['id'],$orderItem['id']);
            }
            OrderItem::updateApproveQuantity($orderItem->id);
        }
        // $model->approved = self::STATUS_APPROVED;
        // $model->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
        // $model->approved_by = Yii::$app->user->id;
        return $model->save() ? true : false;
    }

    public static function approveTransaction($id)
    {
        $model = self::findOne($id);
        $model->approved = self::STATUS_APPROVED;
        $model->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
        $model->approved_by = Yii::$app->user->id;
        return $model->save() ? true : false;
    }

    public static function usageList()
    {
        $usageList = \frontend\modules\store\models\UsageList::find()->orderBy('id')->andWhere(['not',['ref_cat' => 'store']])->asArray()->all();
        $arrayList = \yii\helpers\ArrayHelper::map($usageList, 'id', 'name','ref_cat');
        // $arrayList = \yii\helpers\ArrayHelper::map($usageList, 'id', 'NAME');
        // var_dump($usageList);
        // var_dump($arrayList);
        // die;
        return $arrayList;
    }

    public function getInventoryItems()
    {
        return $this->hasMany(\frontend\modules\store\models\InventoryItem::className(), ['checkout_id' => 'id'])->via('orderItems');
    }

    public function voidOrder()
    {
        if($this->approved !== 2 && !Yii::$app->user->can('Administrator')) {
            throw new Exception("Only admin can overide", 1);
        }
        if($this->inventoryItems) {
            foreach ($this->inventoryItems as $key => $value) {
                $value->checkout_id = null;
                $value->save(false);
            }
        }
    }
    public static function rejectTransaction($id)
    {
        $model = self::findOne($id);
        $model->voidOrder();
        $model->approved = self::STATUS_REJECTED;
        $model->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
        $model->approved_by = Yii::$app->user->id;
        return $model->save() ? true : false;
    }
}
