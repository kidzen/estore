<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\UsageList as BaseUsageList;

/**
 * This is the model class for table "usage_list".
 */
class UsageList extends BaseUsageList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
        [
            [['id', 'ref_id'], 'integer'],
            [['ref_cat'], 'string', 'max' => 8],
            [['name'], 'string', 'max' => 255]
        ]);
    }

    public static function primaryKey()
    {
        return ['id','ref_cat','ref_id'];
    }

    public function getVehicle()
    {
        return $this->hasMany(\frontend\modules\store\models\Vehicle::className(), ['id' => 'ref_id'])
        ->andWhere(['ref_cat' => 'vehicle']);
    }

    public function getStore()
    {
        return $this->hasMany(\frontend\modules\store\models\Store::className(), ['id' => 'ref_id'])
        ->andWhere(['ref_cat' => 'store']);
    }

    public function getWorkshop()
    {
        return $this->hasOne(\frontend\modules\store\models\Workshop::className(), ['id' => 'ref_id'])
        ->andWhere(['ref_cat' => 'workshop']);
    }

}
