<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Sisken as BaseSisken;

/**
 * This is the model class for table "sisken".
 */
class Sisken extends BaseSisken
{
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['id'];
    }
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id'], 'required'],
            [['id'], 'number'],
            [['bengkel_panel_id', 'selenggara_id', 'alasan_id', 'id_kenderaan'], 'integer'],
            [['tahun', 'bulan', 'no_kerja', 'arahan_kerja', 'jk_jg', 'nama_panel', 'category_panel', 'tempoh_selenggara', 'alasan', 'tarikh_arahan', 'no_sebutharga', 'tarikh_kenderaan_tiba', 'tarikh_jangka_siap', 'tarikh_sebut_harga', 'status_sebut_harga', 'no_do', 'tarikh_siap', 'tempoh_jaminan', 'tarikh_do', 'tarikh_cetakan', 'status', 'catatan_sebutharga', 'catatan', 'nama_pic', 'tarikh_permohonan', 'kategori_kerosakan_id', 'kategori_kerosakan', 'kategori', 'isservice', 'ispancit', 'odometer_terkini', 'no_plat', 'model', 'kod_jabatan', 'nama_jabatan', 'jumlah_kos'], 'string', 'max' => 255]
        ]);
    }

}
