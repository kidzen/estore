<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Inventory as BaseInventory;
use \yii\base\Exception;

/**
 * This is the model class for table "inventory".
 */
class Inventory extends BaseInventory
{
    /**
     * @inheritdoc
     */
    public static function arrayList()
    {
        $callback = function($data) {
            return $data['code_no'].' - '.$data['description'];
            return $data['code_no'].' - '.$data['description'].' {'.(isset($data['storeQuantity']) ? $data['storeQuantity'] : 0 ) .'}';
        };
        $inventoryArray = \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Inventory::find()->orderBy('id')->asArray()->all(), 'id', $callback);
        return $inventoryArray;
    }

    public function afterSave($insert,$changedAttributes)
    {
        $valid = true;
        if ($insert){
            $valid = $valid && self::updateQuantity($this->id);

        }
        return $valid;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function afterFind()
    {
        $this->quantity = $this->getInventoryItems()
        ->joinWith('checkin')
        // ->andOnCondition(['and',['checkout_id' => null],['dispose_id' => null]])
        ->andOnCondition(['inventory_item.checkout_id' => null])
        ->andOnCondition(['inventory_item.dispose_id' => null])
        ->andOnCondition(['inventory_checkin.store_id' => Yii::$app->cache->get('active-store')])
        ->count();
    }

    public static function updateQuantity($id)
    {
        $inventory = self::findOne($id);
        $inventory->quantity = InventoryItem::find()
        ->andWhere(['inventory_item.inventory_id'=>$id])
        ->count();
        // var_dump($inventory->quantity);
        $valid = $inventory->save();
        if($valid){
            return $valid;
        } else {
            throw new Exception("Fail to update inventory card quantity", 1);

        }
    }
    public function getInventoryItems()
    {
        return $this->hasMany(\frontend\modules\store\models\InventoryItem::className(), ['inventory_id' => 'id'])
        ->andWhere(['inventory_item.checkout_id'=>null,'inventory_item.dispose_id'=>null])
        ;
    }

}
