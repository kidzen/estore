<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Kewps10 as BaseKewps10;

/**
 * This is the model class for table "kewps10".
 */
class Kewps10 extends BaseKewps10
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['order_id', 'order_item_id', 'ordered_by', 'transaction_id', 'rq_quantity', 'current_balance', 'created_by', 'approved_by'], 'integer'],
            [['app_quantity', 'unit_price', 'average_unit_price', 'batch_total_price'], 'number'],
            [['order_required_date', 'order_date'], 'string', 'max' => 7],
            [['order_no', 'card_no', 'code_no', 'description'], 'string', 'max' => 255],
            [['created_date', 'approved_date'], 'string', 'max' => 8]
        ]
        );
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'ordered_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'approved_by']);
    }


    /**
     * @inheritdoc
     */
    /* public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add code here. given sample code
        if ($insert === false) {
            return; // only work with newly created payments
        }

        if ($this->credit->save(false) === false) {
            throw new Exception("credit couldn't be update");
        }
    } */

    /**
     * @inheritdoc
     */
    /* public function beforeSave($insert)
    {
        // custom code here
        return parent::beforeSave($insert);
    } */
}
