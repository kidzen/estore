<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\ActivityLog as BaseActivityLog;

/**
 * This is the model class for table "activity_log".
 */
class ActivityLog extends BaseActivityLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['remote_ip', 'action', 'controller', 'params', 'route', 'status', 'messages'], 'string', 'max' => 255]
        ]);
    }

}
