<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "kewps10".
 *
 * @property integer $order_id
 * @property string $order_required_date
 * @property integer $order_item_id
 * @property string $order_no
 * @property integer $ordered_by
 * @property string $order_date
 * @property integer $transaction_id
 * @property string $card_no
 * @property string $code_no
 * @property string $description
 * @property integer $rq_quantity
 * @property string $app_quantity
 * @property integer $current_balance
 * @property string $unit_price
 * @property string $average_unit_price
 * @property string $batch_total_price
 * @property string $created_date
 * @property integer $created_by
 * @property integer $approved_by
 * @property string $approved_date
 */
class Kewps10 extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'order_item_id', 'ordered_by', 'transaction_id', 'rq_quantity', 'current_balance', 'created_by', 'approved_by'], 'integer'],
            [['app_quantity', 'unit_price', 'average_unit_price', 'batch_total_price'], 'number'],
            [['order_required_date', 'order_date'], 'string', 'max' => 7],
            [['order_no', 'card_no', 'code_no', 'description'], 'string', 'max' => 255],
            [['created_date', 'approved_date'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kewps10';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('app', 'Order ID'),
            'order_required_date' => Yii::t('app', 'Order Required Date'),
            'order_item_id' => Yii::t('app', 'Order Item ID'),
            'order_no' => Yii::t('app', 'Order No'),
            'ordered_by' => Yii::t('app', 'Ordered By'),
            'order_date' => Yii::t('app', 'Order Date'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'card_no' => Yii::t('app', 'Card No'),
            'code_no' => Yii::t('app', 'Code No'),
            'description' => Yii::t('app', 'Description'),
            'rq_quantity' => Yii::t('app', 'Rq Quantity'),
            'app_quantity' => Yii::t('app', 'App Quantity'),
            'current_balance' => Yii::t('app', 'Current Balance'),
            'unit_price' => Yii::t('app', 'Unit Price'),
            'average_unit_price' => Yii::t('app', 'Average Unit Price'),
            'batch_total_price' => Yii::t('app', 'Batch Total Price'),
            'created_date' => Yii::t('app', 'Created Date'),
            'approved_by' => Yii::t('app', 'Approved By'),
            'approved_date' => Yii::t('app', 'Approved Date'),
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\Kewps10Query the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\store\models\query\Kewps10Query(get_called_class());
    }
}
