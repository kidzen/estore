<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "TRANSACTION_OUT_SISKEN".
 *
 * @property string $ID
 * @property integer $ARAHAN_KERJA_ID
 * @property string $ORDER_NO
 * @property string $CHECK_DATE
 * @property integer $STATUS
 * @property integer $ORDER_ITEMS_ID
 * @property string $CATEGORY_NAME
 * @property string $CARD_NO
 * @property string $CODE_NO
 * @property string $INVENTORY_DESCRIPTION
 * @property string $ITEMS
 * @property string $UNIT_PRICE
 * @property string $BATCH_QUANTITY
 * @property string $BATCH_TOTAL_PRICE
 * @property string $TOTAL_QUANTITY
 * @property string $TOTAL_PRICE
 */
class TransactionOutSisken extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'UNIT_PRICE', 'BATCH_QUANTITY', 'BATCH_TOTAL_PRICE', 'TOTAL_QUANTITY', 'TOTAL_PRICE'], 'number'],
            [['ARAHAN_KERJA_ID', 'STATUS', 'ORDER_ITEMS_ID'], 'integer'],
            [['ORDER_NO', 'CATEGORY_NAME', 'CARD_NO', 'CODE_NO', 'INVENTORY_DESCRIPTION'], 'string', 'max' => 255],
            [['CHECK_DATE'], 'string', 'max' => 9],
            [['ITEMS'], 'string', 'max' => 4000]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TRANSACTION_OUT_SISKEN';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ARAHAN_KERJA_ID' => Yii::t('app', 'Arahan  Kerja  ID'),
            'ORDER_NO' => Yii::t('app', 'Order  No'),
            'CHECK_DATE' => Yii::t('app', 'Check  Date'),
            'STATUS' => Yii::t('app', 'Status'),
            'ORDER_ITEMS_ID' => Yii::t('app', 'Order  Items  ID'),
            'CATEGORY_NAME' => Yii::t('app', 'Category  Name'),
            'CARD_NO' => Yii::t('app', 'Card  No'),
            'CODE_NO' => Yii::t('app', 'Code  No'),
            'INVENTORY_DESCRIPTION' => Yii::t('app', 'Inventory  Description'),
            'ITEMS' => Yii::t('app', 'Items'),
            'UNIT_PRICE' => Yii::t('app', 'Unit  Price'),
            'BATCH_QUANTITY' => Yii::t('app', 'Batch  Quantity'),
            'BATCH_TOTAL_PRICE' => Yii::t('app', 'Batch  Total  Price'),
            'TOTAL_QUANTITY' => Yii::t('app', 'Total  Quantity'),
            'TOTAL_PRICE' => Yii::t('app', 'Total  Price'),
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\TransactionOutSiskenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\store\models\query\TransactionOutSiskenQuery(get_called_class());
    }
}
