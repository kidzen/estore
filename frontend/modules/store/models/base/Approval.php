<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "approval".
 *
 * @property integer $id
 * @property integer $id_inventory_item
 * @property integer $id_inventory
 * @property integer $id_category
 * @property integer $id_inventory_checkin
 * @property integer $id_order_item
 * @property integer $id_order
 * @property string $sku
 * @property string $unit_price
 * @property string $order_no
 * @property string $items_category_name
 * @property string $item_inventory_name
 * @property integer $rq_quantity
 * @property integer $app_quantity
 * @property integer $approved
 * @property integer $current_balance
 * @property double $total_price
 * @property string $order_date
 * @property string $required_date
 */
class Approval extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_inventory_item', 'id_inventory', 'id_category', 'id_inventory_checkin', 'id_order_item', 'id_order', 'rq_quantity', 'app_quantity', 'approved', 'current_balance'], 'integer'],
            [['unit_price', 'total_price'], 'number'],
            [['order_date', 'required_date'], 'safe'],
            [['sku', 'order_no', 'items_category_name', 'item_inventory_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'approval';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_inventory_item' => Yii::t('app', 'Id Inventory Item'),
            'id_inventory' => Yii::t('app', 'Id Inventory'),
            'id_category' => Yii::t('app', 'Id Category'),
            'id_inventory_checkin' => Yii::t('app', 'Id Inventory Checkin'),
            'id_order_item' => Yii::t('app', 'Id Order Item'),
            'id_order' => Yii::t('app', 'Id Order'),
            'sku' => Yii::t('app', 'Sku'),
            'unit_price' => Yii::t('app', 'Unit Price'),
            'order_no' => Yii::t('app', 'Order No'),
            'items_category_name' => Yii::t('app', 'Item Category Name'),
            'item_inventory_name' => Yii::t('app', 'Item Inventory Name'),
            'rq_quantity' => Yii::t('app', 'Rq Quantity'),
            'app_quantity' => Yii::t('app', 'App Quantity'),
            'approved' => Yii::t('app', 'Approved'),
            'current_balance' => Yii::t('app', 'Current Balance'),
            'total_price' => Yii::t('app', 'Total Price'),
            'order_date' => Yii::t('app', 'Order Date'),
            'required_date' => Yii::t('app', 'Required Date'),
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\ApprovalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\store\models\query\ApprovalQuery(get_called_class());
    }
}
