<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vendor_usage".
 *
 * @property string $id
 * @property string $vendor
 * @property string $year
 * @property string $month
 * @property string $date
 * @property string $created_at
 * @property integer $inventory_id
 * @property string $items_total_price
 * @property string $by_year
 * @property string $by_vendor
 * @property string $by_year_vendor
 * @property string $by_month_vendor
 * @property string $by_date_vendor
 */
class VendorUsage extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'month', 'items_total_price', 'by_year', 'by_vendor', 'by_year_vendor', 'by_month_vendor', 'by_date_vendor'], 'number'],
            [['created_at'], 'safe'],
            [['inventory_id'], 'integer'],
            [['vendor'], 'string', 'max' => 255],
            [['date'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor_usage';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vendor' => Yii::t('app', 'Vendor'),
            'year' => Yii::t('app', 'Year'),
            'month' => Yii::t('app', 'Month'),
            'date' => Yii::t('app', 'Date'),
            'inventory_id' => Yii::t('app', 'Inventory ID'),
            'items_total_price' => Yii::t('app', 'Items Total Price'),
            'by_year' => Yii::t('app', 'By Year'),
            'by_vendor' => Yii::t('app', 'By Vendor'),
            'by_year_vendor' => Yii::t('app', 'By Year Vendor'),
            'by_month_vendor' => Yii::t('app', 'By Month Vendor'),
            'by_date_vendor' => Yii::t('app', 'By Date Vendor'),
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\VendorUsageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\store\models\query\VendorUsageQuery(get_called_class());
    }
}
