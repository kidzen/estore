<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "sisken".
 *
 * @property string $tahun
 * @property string $bulan
 * @property string $id
 * @property string $no_kerja
 * @property string $arahan_kerja
 * @property string $jk_jg
 * @property integer $bengkel_panel_id
 * @property string $nama_panel
 * @property string $category_panel
 * @property integer $selenggara_id
 * @property string $tempoh_selenggara
 * @property integer $alasan_id
 * @property string $alasan
 * @property string $tarikh_arahan
 * @property string $no_sebutharga
 * @property string $tarikh_kenderaan_tiba
 * @property string $tarikh_jangka_siap
 * @property string $tarikh_sebut_harga
 * @property string $status_sebut_harga
 * @property string $no_do
 * @property string $tarikh_siap
 * @property string $tempoh_jaminan
 * @property string $tarikh_do
 * @property string $tarikh_cetakan
 * @property string $status
 * @property string $catatan_sebutharga
 * @property string $catatan
 * @property string $nama_pic
 * @property string $tarikh_permohonan
 * @property integer $id_kenderaan
 * @property string $kategori_kerosakan_id
 * @property string $kategori_kerosakan
 * @property string $kategori
 * @property string $isservice
 * @property string $ispancit
 * @property string $odometer_terkini
 * @property string $no_plat
 * @property string $model
 * @property string $kod_jabatan
 * @property string $nama_jabatan
 * @property string $jumlah_kos
 */
class Sisken extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'number'],
            [['bengkel_panel_id', 'selenggara_id', 'alasan_id', 'id_kenderaan'], 'integer'],
            [['tahun', 'bulan', 'no_kerja', 'arahan_kerja', 'jk_jg', 'nama_panel', 'category_panel', 'tempoh_selenggara', 'alasan', 'tarikh_arahan', 'no_sebutharga', 'tarikh_kenderaan_tiba', 'tarikh_jangka_siap', 'tarikh_sebut_harga', 'status_sebut_harga', 'no_do', 'tarikh_siap', 'tempoh_jaminan', 'tarikh_do', 'tarikh_cetakan', 'status', 'catatan_sebutharga', 'catatan', 'nama_pic', 'tarikh_permohonan', 'kategori_kerosakan_id', 'kategori_kerosakan', 'kategori', 'isservice', 'ispancit', 'odometer_terkini', 'no_plat', 'model', 'kod_jabatan', 'nama_jabatan', 'jumlah_kos'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sisken';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tahun' => Yii::t('app', 'Tahun'),
            'bulan' => Yii::t('app', 'Bulan'),
            'id' => Yii::t('app', 'ID'),
            'no_kerja' => Yii::t('app', 'No Kerja'),
            'arahan_kerja' => Yii::t('app', 'Arahan Kerja'),
            'jk_jg' => Yii::t('app', 'Jk Jg'),
            'bengkel_panel_id' => Yii::t('app', 'Bengkel Panel ID'),
            'nama_panel' => Yii::t('app', 'Nama Panel'),
            'category_panel' => Yii::t('app', 'Category Panel'),
            'selenggara_id' => Yii::t('app', 'Selenggara ID'),
            'tempoh_selenggara' => Yii::t('app', 'Tempoh Selenggara'),
            'alasan_id' => Yii::t('app', 'Alasan ID'),
            'alasan' => Yii::t('app', 'Alasan'),
            'tarikh_arahan' => Yii::t('app', 'Tarikh Arahan'),
            'no_sebutharga' => Yii::t('app', 'No Sebutharga'),
            'tarikh_kenderaan_tiba' => Yii::t('app', 'Tarikh Kenderaan Tiba'),
            'tarikh_jangka_siap' => Yii::t('app', 'Tarikh Jangka Siap'),
            'tarikh_sebut_harga' => Yii::t('app', 'Tarikh Sebut Harga'),
            'status_sebut_harga' => Yii::t('app', 'Status Sebut Harga'),
            'no_do' => Yii::t('app', 'No Do'),
            'tarikh_siap' => Yii::t('app', 'Tarikh Siap'),
            'tempoh_jaminan' => Yii::t('app', 'Tempoh Jaminan'),
            'tarikh_do' => Yii::t('app', 'Tarikh Do'),
            'tarikh_cetakan' => Yii::t('app', 'Tarikh Cetakan'),
            'status' => Yii::t('app', 'Status'),
            'catatan_sebutharga' => Yii::t('app', 'Catatan Sebutharga'),
            'catatan' => Yii::t('app', 'Catatan'),
            'nama_pic' => Yii::t('app', 'Nama Pic'),
            'tarikh_permohonan' => Yii::t('app', 'Tarikh Permohonan'),
            'id_kenderaan' => Yii::t('app', 'Id Kenderaan'),
            'kategori_kerosakan_id' => Yii::t('app', 'Kategori Kerosakan ID'),
            'kategori_kerosakan' => Yii::t('app', 'Kategori Kerosakan'),
            'kategori' => Yii::t('app', 'Kategori'),
            'isservice' => Yii::t('app', 'Isservice'),
            'ispancit' => Yii::t('app', 'Ispancit'),
            'odometer_terkini' => Yii::t('app', 'Odometer Terkini'),
            'no_plat' => Yii::t('app', 'No Plat'),
            'model' => Yii::t('app', 'Model'),
            'kod_jabatan' => Yii::t('app', 'Kod Jabatan'),
            'nama_jabatan' => Yii::t('app', 'Nama Jabatan'),
            'jumlah_kos' => Yii::t('app', 'Jumlah Kos'),
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\SiskenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\store\models\query\SiskenQuery(get_called_class());
    }
}
