<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inventory_item".
 *
 * @property integer $id
 * @property integer $inventory_id
 * @property integer $checkin_id
 * @property integer $checkout_id
 * @property integer $dispose_id
 * @property string $sku
 * @property string $unit_price
 * @property integer $status
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\User $updatedBy
 * @property \common\models\User $createdBy
 * @property \frontend\modules\store\models\Inventory $inventory
 * @property \frontend\modules\store\models\InventoryCheckin $checkin
 * @property \frontend\modules\store\models\OrderItem $checkout
 * @property \frontend\modules\store\models\Dispose $dispose
 */
class InventoryItem extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            // 'updatedBy',
            // 'createdBy',
            'inventory',
            'checkin',
            'checkout',
            'dispose'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory_id', 'checkin_id', 'checkout_id', 'dispose_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['unit_price'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['sku'], 'string', 'max' => 255],
            [['sku'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory_item';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'inventory_id' => Yii::t('app', 'Inventory ID'),
            'checkin_id' => Yii::t('app', 'Checkin ID'),
            'checkout_id' => Yii::t('app', 'Checkout ID'),
            'dispose_id' => Yii::t('app', 'Dispose ID'),
            'sku' => Yii::t('app', 'Sku'),
            'unit_price' => Yii::t('app', 'Unit Price'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(\frontend\modules\store\models\Inventory::className(), ['id' => 'inventory_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckin()
    {
        return $this->hasOne(\frontend\modules\store\models\InventoryCheckin::className(), ['id' => 'checkin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckout()
    {
        return $this->hasOne(\frontend\modules\store\models\OrderItem::className(), ['id' => 'checkout_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispose()
    {
        return $this->hasOne(\frontend\modules\store\models\Dispose::className(), ['id' => 'dispose_id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\InventoryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \frontend\modules\store\models\query\InventoryItemQuery(get_called_class());
        // uncomment and edit permission rule to view own items only
        if(!\Yii::$app->user->can('permission')){
           $query->mine();
        }
        // uncomment and edit permission rule to view deleted items
        /*if(\Yii::$app->user->can('see_deleted')){
           return $query;
        } */
        return $query->andWhere(['inventory_item.deleted_by' => 0]);
    }
}
