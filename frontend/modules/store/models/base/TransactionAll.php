<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "transaction_all".
 *
 * @property integer $id
 * @property integer $type
 * @property string $check_date
 * @property string $approved_date
 * @property integer $check_by
 * @property string $refference
 * @property integer $inventory_id
 * @property string $unit_price
 * @property string $count_in
 * @property string $price_in
 * @property string $count_out
 * @property string $price_out
 * @property string $count_current
 * @property string $price_current
 */
class TransactionAll extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'check_by', 'inventory_id'], 'integer'],
            [['unit_price', 'count_in', 'price_in', 'count_out', 'price_out', 'count_current', 'price_current'], 'number'],
            [['check_date'], 'string', 'max' => 7],
            [['approved_date'], 'string', 'max' => 8],
            [['refference'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction_all';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'check_date' => Yii::t('app', 'Check Date'),
            'approved_date' => Yii::t('app', 'Approved Date'),
            'check_by' => Yii::t('app', 'Check By'),
            'refference' => Yii::t('app', 'Refference'),
            'inventory_id' => Yii::t('app', 'Inventory ID'),
            'unit_price' => Yii::t('app', 'Unit Price'),
            'count_in' => Yii::t('app', 'Count In'),
            'price_in' => Yii::t('app', 'Price In'),
            'count_out' => Yii::t('app', 'Count Out'),
            'price_out' => Yii::t('app', 'Price Out'),
            'count_current' => Yii::t('app', 'Count Current'),
            'price_current' => Yii::t('app', 'Price Current'),
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\TransactionAllQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\store\models\query\TransactionAllQuery(get_called_class());
    }
}
