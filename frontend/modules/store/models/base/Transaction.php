<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "transaction".
 *
 * @property integer $id
 * @property integer $store_id
 * @property integer $type
 * @property string $check_date
 * @property integer $check_by
 * @property integer $status
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \frontend\modules\store\models\Dispose[] $disposes
 * @property \frontend\modules\store\models\InventoryCheckin[] $inventoryCheckins
 * @property \frontend\modules\store\models\Order[] $orders
 * @property \common\models\User $updatedBy
 * @property \common\models\User $createdBy
 * @property \common\models\User $checkBy
 * @property \frontend\modules\store\models\Store $store
 */
class Transaction extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            'disposes',
            'inventoryCheckins',
            'orders',
            'updatedBy',
            'createdBy',
            'checkBy',
            'store'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'type', 'check_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['check_date', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'store_id' => Yii::t('app', 'Store ID'),
            'type' => Yii::t('app', 'Type'),
            'check_date' => Yii::t('app', 'Check Date'),
            'check_by' => Yii::t('app', 'Check By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposes()
    {
        return $this->hasMany(\frontend\modules\store\models\Dispose::className(), ['transaction_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryCheckins()
    {
        return $this->hasMany(\frontend\modules\store\models\InventoryCheckin::className(), ['transaction_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(\frontend\modules\store\models\Order::className(), ['transaction_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'check_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(\frontend\modules\store\models\Store::className(), ['id' => 'store_id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\TransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \frontend\modules\store\models\query\TransactionQuery(get_called_class());
        // uncomment and edit permission rule to view own items only
        if(!\Yii::$app->user->can('administrator')){
           $query->mine();
        }
        // uncomment and edit permission rule to view deleted items
        /*if(\Yii::$app->user->can('see_deleted')){
           return $query;
        } */
        return $query->andWhere(['transaction.deleted_by' => 0]);
    }
}
