<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "dispose".
 *
 * @property integer $id
 * @property string $refference_no
 * @property integer $transaction_id
 * @property integer $category_id
 * @property integer $method_id
 * @property integer $reason_id
 * @property integer $quantity
 * @property double $current_revenue
 * @property double $cost
 * @property double $returns
 * @property string $description
 * @property string $requested_at
 * @property integer $requested_by
 * @property string $disposed_at
 * @property integer $disposed_by
 * @property integer $approved
 * @property string $approved_at
 * @property integer $approved_by
 * @property integer $status
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \frontend\modules\store\models\Transaction $transaction
 * @property \frontend\modules\store\models\DisposeCategory $category
 * @property \frontend\modules\store\models\DisposeMethod $method
 * @property \frontend\modules\store\models\DisposeReason $reason
 * @property \common\models\User $updatedBy
 * @property \common\models\User $createdBy
 * @property \common\models\User $requestedBy
 * @property \common\models\User $disposedBy
 * @property \common\models\User $approvedBy
 * @property \frontend\modules\store\models\InventoryItem[] $inventoryItems
 */
class Dispose extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            'transaction',
            'category',
            'method',
            'reason',
            'updatedBy',
            'createdBy',
            'requestedBy',
            'disposedBy',
            'approvedBy',
            'inventoryItems'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transaction_id', 'category_id', 'method_id', 'reason_id', 'quantity', 'requested_by', 'disposed_by', 'approved', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['current_revenue', 'cost', 'returns'], 'number'],
            [['requested_at', 'disposed_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['refference_no', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dispose';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'refference_no' => Yii::t('app', 'Refference No'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'method_id' => Yii::t('app', 'Method ID'),
            'reason_id' => Yii::t('app', 'Reason ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'current_revenue' => Yii::t('app', 'Current Revenue'),
            'cost' => Yii::t('app', 'Cost'),
            'returns' => Yii::t('app', 'Returns'),
            'description' => Yii::t('app', 'Description'),
            'requested_at' => Yii::t('app', 'Requested At'),
            'requested_by' => Yii::t('app', 'Requested By'),
            'disposed_at' => Yii::t('app', 'Disposed At'),
            'disposed_by' => Yii::t('app', 'Disposed By'),
            'approved' => Yii::t('app', 'Approved'),
            'approved_at' => Yii::t('app', 'Approved At'),
            'approved_by' => Yii::t('app', 'Approved By'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(\frontend\modules\store\models\Transaction::className(), ['id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(\frontend\modules\store\models\DisposeCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethod()
    {
        return $this->hasOne(\frontend\modules\store\models\DisposeMethod::className(), ['id' => 'method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReason()
    {
        return $this->hasOne(\frontend\modules\store\models\DisposeReason::className(), ['id' => 'reason_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'requested_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'disposed_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'approved_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryItems()
    {
        return $this->hasMany(\frontend\modules\store\models\InventoryItem::className(), ['dispose_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\DisposeQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \frontend\modules\store\models\query\DisposeQuery(get_called_class());
        // uncomment and edit permission rule to view own items only
        if(!\Yii::$app->user->can('administrator')){
           $query->mine();
        }
        // uncomment and edit permission rule to view deleted items
        /*if(\Yii::$app->user->can('see_deleted')){
           return $query;
        } */
        return $query->andWhere(['dispose.deleted_by' => 0]);
    }
}
