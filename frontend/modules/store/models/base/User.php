<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $profile_pic
 * @property string $password
 * @property integer $role_id
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $status
 * @property integer $deleted_by
 * @property integer $deleted_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \frontend\modules\store\models\ActivityLog[] $activityLogs
 * @property \frontend\modules\store\models\Dispose[] $disposes
 * @property \frontend\modules\store\models\DisposeCategory[] $disposeCategories
 * @property \frontend\modules\store\models\DisposeMethod[] $disposeMethods
 * @property \frontend\modules\store\models\DisposeReason[] $disposeReasons
 * @property \frontend\modules\store\models\Inventory[] $inventories
 * @property \frontend\modules\store\models\InventoryCategory[] $inventoryCategories
 * @property \frontend\modules\store\models\InventoryCheckin[] $inventoryCheckins
 * @property \frontend\modules\store\models\InventoryItem[] $inventoryItems
 * @property \frontend\modules\store\models\Order[] $orders
 * @property \frontend\modules\store\models\OrderItem[] $orderItems
 * @property \frontend\modules\store\models\Package[] $packages
 * @property \frontend\modules\store\models\Role[] $roles
 * @property \frontend\modules\store\models\Store[] $stores
 * @property \frontend\modules\store\models\StoreAssignment[] $storeAssignments
 * @property \frontend\modules\store\models\Transaction[] $transactions
 * @property \frontend\modules\store\models\Usage[] $usages
 * @property \frontend\modules\store\models\UsageCategory[] $usageCategories
 * @property \frontend\modules\store\models\UsageDetailKey[] $usageDetailKeys
 * @property \frontend\modules\store\models\UsageDetailValue[] $usageDetailValues
 * @property \frontend\modules\store\models\Role $role
 * @property \frontend\modules\store\models\Vehicle[] $vehicles
 * @property \frontend\modules\store\models\Vendor[] $vendors
 * @property \frontend\modules\store\models\Workshop[] $workshops
 */
class User extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            'activityLogs',
            'disposes',
            'disposeCategories',
            'disposeMethods',
            'disposeReasons',
            'inventories',
            'inventoryCategories',
            'inventoryCheckins',
            'inventoryItems',
            'orders',
            'orderItems',
            'packages',
            'roles',
            'stores',
            'storeAssignments',
            'transactions',
            'usages',
            'usageCategories',
            'usageDetailKeys',
            'usageDetailValues',
            'role',
            'vehicles',
            'vendors',
            'workshops'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id'], 'required'],
            [['role_id', 'status', 'deleted_by'], 'integer'],
            [['username', 'email', 'profile_pic', 'password', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'profile_pic' => Yii::t('app', 'Profile Pic'),
            'password' => Yii::t('app', 'Password'),
            'role_id' => Yii::t('app', 'Role ID'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityLogs()
    {
        return $this->hasMany(\frontend\modules\store\models\ActivityLog::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposes()
    {
        return $this->hasMany(\frontend\modules\store\models\Dispose::className(), ['approved_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposeCategories()
    {
        return $this->hasMany(\frontend\modules\store\models\DisposeCategory::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposeMethods()
    {
        return $this->hasMany(\frontend\modules\store\models\DisposeMethod::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisposeReasons()
    {
        return $this->hasMany(\frontend\modules\store\models\DisposeReason::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventories()
    {
        return $this->hasMany(\frontend\modules\store\models\Inventory::className(), ['approved_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryCategories()
    {
        return $this->hasMany(\frontend\modules\store\models\InventoryCategory::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryCheckins()
    {
        return $this->hasMany(\frontend\modules\store\models\InventoryCheckin::className(), ['approved_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventoryItems()
    {
        return $this->hasMany(\frontend\modules\store\models\InventoryItem::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(\frontend\modules\store\models\Order::className(), ['approved_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(\frontend\modules\store\models\OrderItem::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackages()
    {
        return $this->hasMany(\frontend\modules\store\models\Package::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(\frontend\modules\store\models\Role::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStores()
    {
        return $this->hasMany(\frontend\modules\store\models\Store::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreAssignments()
    {
        return $this->hasMany(\frontend\modules\store\models\StoreAssignment::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(\frontend\modules\store\models\Transaction::className(), ['check_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsages()
    {
        return $this->hasMany(\frontend\modules\store\models\Usage::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsageCategories()
    {
        return $this->hasMany(\frontend\modules\store\models\UsageCategory::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsageDetailKeys()
    {
        return $this->hasMany(\frontend\modules\store\models\UsageDetailKey::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsageDetailValues()
    {
        return $this->hasMany(\frontend\modules\store\models\UsageDetailValue::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(\frontend\modules\store\models\Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicles()
    {
        return $this->hasMany(\frontend\modules\store\models\Vehicle::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors()
    {
        return $this->hasMany(\frontend\modules\store\models\Vendor::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkshops()
    {
        return $this->hasMany(\frontend\modules\store\models\Workshop::className(), ['created_by' => 'id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            // 'blameable' => [
            //     'class' => BlameableBehavior::className(),
            //     'createdByAttribute' => 'created_by',
            //     'updatedByAttribute' => 'updated_by',
            // ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \frontend\modules\store\models\query\UserQuery(get_called_class());
        // uncomment and edit permission rule to view own items only
        /*if(\Yii::$app->user->can('permission')){
           $query->mine();
        } */
        // uncomment and edit permission rule to view deleted items
        /*if(\Yii::$app->user->can('see_deleted')){
           return $query;
        } */
        return $query->andWhere(['user.deleted_by' => 0]);
    }
}
