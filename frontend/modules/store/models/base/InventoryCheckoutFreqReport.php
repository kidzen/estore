<?php

namespace frontend\modules\store\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inventory_checkout_freq_report".
 *
 * @property integer $inventory_id
 * @property string $year
 * @property string $month
 * @property string $date
 * @property string $by_inventory
 * @property string $by_year
 * @property string $by_month
 * @property string $by_date
 * @property string $rank_inventory
 * @property string $rank_year
 * @property string $rank_month
 * @property string $rank_date
 */
class InventoryCheckoutFreqReport extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
        ];
    }

    public static function getDb() {
        return Yii::$app->dbStore;
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public static function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory_id'], 'integer'],
            [['year', 'month', 'by_inventory', 'by_year', 'by_month', 'by_date', 'rank_inventory', 'rank_year', 'rank_month', 'rank_date'], 'number'],
            [['date'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory_checkout_freq_report';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inventory_id' => Yii::t('app', 'Inventory ID'),
            'year' => Yii::t('app', 'Year'),
            'month' => Yii::t('app', 'Month'),
            'date' => Yii::t('app', 'Date'),
            'by_inventory' => Yii::t('app', 'By Inventory'),
            'by_year' => Yii::t('app', 'By Year'),
            'by_month' => Yii::t('app', 'By Month'),
            'by_date' => Yii::t('app', 'By Date'),
            'rank_inventory' => Yii::t('app', 'Rank Inventory'),
            'rank_year' => Yii::t('app', 'Rank Year'),
            'rank_month' => Yii::t('app', 'Rank Month'),
            'rank_date' => Yii::t('app', 'Rank Date'),
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->andWhere(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\InventoryCheckoutFreqReportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\modules\store\models\query\InventoryCheckoutFreqReportQuery(get_called_class());
    }
}
