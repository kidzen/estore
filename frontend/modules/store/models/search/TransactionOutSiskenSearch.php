<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\TransactionOutSisken;

/**
 * frontend\modules\store\models\search\TransactionOutSiskenSearch represents the model behind the search form about `frontend\modules\store\models\TransactionOutSisken`.
 */
 class TransactionOutSiskenSearch extends TransactionOutSisken
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'UNIT_PRICE', 'BATCH_QUANTITY', 'BATCH_TOTAL_PRICE', 'TOTAL_QUANTITY', 'TOTAL_PRICE'], 'number'],
            [['ARAHAN_KERJA_ID', 'STATUS', 'ORDER_ITEMS_ID'], 'integer'],
            [['ORDER_NO', 'CHECK_DATE', 'CATEGORY_NAME', 'CARD_NO', 'CODE_NO', 'INVENTORY_DESCRIPTION', 'ITEMS'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransactionOutSisken::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ARAHAN_KERJA_ID' => $this->ARAHAN_KERJA_ID,
            'STATUS' => $this->STATUS,
            'ORDER_ITEMS_ID' => $this->ORDER_ITEMS_ID,
            'UNIT_PRICE' => $this->UNIT_PRICE,
            'BATCH_QUANTITY' => $this->BATCH_QUANTITY,
            'BATCH_TOTAL_PRICE' => $this->BATCH_TOTAL_PRICE,
            'TOTAL_QUANTITY' => $this->TOTAL_QUANTITY,
            'TOTAL_PRICE' => $this->TOTAL_PRICE,
        ]);

        $query->andFilterWhere(['like', 'ORDER_NO', $this->ORDER_NO])
            ->andFilterWhere(['like', 'CHECK_DATE', $this->CHECK_DATE])
            ->andFilterWhere(['like', 'CATEGORY_NAME', $this->CATEGORY_NAME])
            ->andFilterWhere(['like', 'CARD_NO', $this->CARD_NO])
            ->andFilterWhere(['like', 'CODE_NO', $this->CODE_NO])
            ->andFilterWhere(['like', 'INVENTORY_DESCRIPTION', $this->INVENTORY_DESCRIPTION])
            ->andFilterWhere(['like', 'ITEMS', $this->ITEMS]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
