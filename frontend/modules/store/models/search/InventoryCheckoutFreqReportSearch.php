<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\InventoryCheckoutFreqReport;

/**
 * frontend\modules\store\models\search\InventoryCheckoutFreqReportSearch represents the model behind the search form about `frontend\modules\store\models\InventoryCheckoutFreqReport`.
 */
 class InventoryCheckoutFreqReportSearch extends InventoryCheckoutFreqReport
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inventory_id'], 'integer'],
            [['year', 'month', 'by_inventory', 'by_year', 'by_month', 'by_date', 'rank_inventory', 'rank_year', 'rank_month', 'rank_date'], 'number'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InventoryCheckoutFreqReport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'inventory_id' => $this->inventory_id,
            'year' => $this->year,
            'month' => $this->month,
            'by_inventory' => $this->by_inventory,
            'by_year' => $this->by_year,
            'by_month' => $this->by_month,
            'by_date' => $this->by_date,
            'rank_inventory' => $this->rank_inventory,
            'rank_year' => $this->rank_year,
            'rank_month' => $this->rank_month,
            'rank_date' => $this->rank_date,
        ]);

        $query->andFilterWhere(['like', 'date', $this->date]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
