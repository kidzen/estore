<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\Approval;

/**
 * frontend\modules\store\models\search\ApprovalSearch represents the model behind the search form about `frontend\modules\store\models\Approval`.
 */
 class ApprovalSearch extends Approval
{
    // use \common\components\RelationSFTrait;
    public $items_category_name;
    public $items_inventory_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'total_price', 'unit_price'], 'number'],
            [['id_inventory_item', 'id_inventory', 'id_category', 'id_inventory_checkin', 'id_order_item', 'id_order', 'rq_quantity', 'app_quantity', 'approved', 'current_balance'], 'integer'],
            [['sku', 'order_no', 'items_category_name', 'items_inventory_name', 'order_date', 'required_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Approval::find();
        $query->joinWith('inventoryItem');
        $query->joinWith('inventory');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_inventory_item' => $this->id_inventory_item,
            'id_inventory' => $this->id_inventory,
            'id_category' => $this->id_category,
            'id_inventory_checkin' => $this->id_inventory_checkin,
            'id_order_item' => $this->id_order_item,
            'id_order' => $this->id_order,
            'rq_quantity' => $this->rq_quantity,
            'app_quantity' => $this->app_quantity,
            'approved' => $this->approved,
            'current_balance' => $this->current_balance,
            'unit_price' => $this->unit_price,
            'total_price' => $this->total_price,
        ]);

        $query->andFilterWhere(['like', 'sku', $this->sku])
            ->andFilterWhere(['like', 'order_no', $this->order_no])
            ->andFilterWhere(['like', 'items_category_name', $this->items_category_name])
            ->andFilterWhere(['like', 'items_inventory_name', $this->items_inventory_name])
            ->andFilterWhere(['like', 'order_date', $this->order_date])
            ->andFilterWhere(['like', 'required_date', $this->required_date]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
