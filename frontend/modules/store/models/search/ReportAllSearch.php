<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\ReportAll;

/**
 * frontend\modules\store\models\search\ReportAllSearch represents the model behind the search form about `frontend\modules\store\models\ReportAll`.
 */
 class ReportAllSearch extends ReportAll
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'count_in', 'price_in', 'COUNT_OUT', 'PRICE_OUT', 'count_current', 'price_current'], 'number'],
            [['quarter'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportAll::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'year' => $this->year,
            'count_in' => $this->count_in,
            'price_in' => $this->price_in,
            'COUNT_OUT' => $this->COUNT_OUT,
            'PRICE_OUT' => $this->PRICE_OUT,
            'count_current' => $this->count_current,
            'price_current' => $this->price_current,
        ]);

        $query->andFilterWhere(['like', 'quarter', $this->quarter]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
