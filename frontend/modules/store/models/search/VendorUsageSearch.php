<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\VendorUsage;

/**
 * frontend\modules\store\models\search\VendorUsageSearch represents the model behind the search form about `frontend\modules\store\models\VendorUsage`.
 */
 class VendorUsageSearch extends VendorUsage
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'month', 'items_total_price', 'by_year', 'by_vendor', 'by_year_vendor', 'by_month_vendor', 'by_date_vendor'], 'number'],
            [['vendor', 'date', 'created_at'], 'safe'],
            [['inventory_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VendorUsage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'year' => $this->year,
            'month' => $this->month,
            'created_at' => $this->created_at,
            'inventory_id' => $this->inventory_id,
            'items_total_price' => $this->items_total_price,
            'by_year' => $this->by_year,
            'by_vendor' => $this->by_vendor,
            'by_year_vendor' => $this->by_year_vendor,
            'by_month_vendor' => $this->by_month_vendor,
            'by_date_vendor' => $this->by_date_vendor,
        ]);

        $query->andFilterWhere(['like', 'vendor', $this->vendor])
            ->andFilterWhere(['like', 'date', $this->date]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
