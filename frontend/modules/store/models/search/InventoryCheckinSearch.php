<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\InventoryCheckin;

/**
 * frontend\modules\store\models\search\InventoryCheckinSearch represents the model behind the search form about `frontend\modules\store\models\InventoryCheckin`.
 */
 class InventoryCheckinSearch extends InventoryCheckin
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'transaction_id', 'budget_id', 'store_id', 'inventory_id', 'vendor_id', 'items_quantity', 'check_by', 'approved', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['items_total_price'], 'number'],
            [['check_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InventoryCheckin::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'budget_id' => $this->budget_id,
            'store_id' => $this->store_id,
            'inventory_id' => $this->inventory_id,
            'vendor_id' => $this->vendor_id,
            'items_quantity' => $this->items_quantity,
            'items_total_price' => $this->items_total_price,
            'check_date' => $this->check_date,
            'check_by' => $this->check_by,
            'approved' => $this->approved,
            'approved_at' => $this->approved_at,
            'approved_by' => $this->approved_by,
            'status' => $this->status,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
