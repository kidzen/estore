<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\TransactionAll;

/**
 * frontend\modules\store\models\search\TransactionAllSearch represents the model behind the search form about `frontend\modules\store\models\TransactionAll`.
 */
 class TransactionAllSearch extends TransactionAll
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'check_by', 'inventory_id'], 'integer'],
            [['check_date', 'approved_date', 'refference'], 'safe'],
            [['unit_price', 'count_in', 'price_in', 'count_out', 'price_out', 'count_current', 'price_current'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransactionAll::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'check_by' => $this->check_by,
            'inventory_id' => $this->inventory_id,
            'unit_price' => $this->unit_price,
            'count_in' => $this->count_in,
            'price_in' => $this->price_in,
            'count_out' => $this->count_out,
            'price_out' => $this->price_out,
            'count_current' => $this->count_current,
            'price_current' => $this->price_current,
        ]);

        $query->andFilterWhere(['like', 'check_date', $this->check_date])
            ->andFilterWhere(['like', 'approved_date', $this->approved_date])
            ->andFilterWhere(['like', 'refference', $this->refference]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
