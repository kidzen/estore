<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\InventoryItem;

/**
 * frontend\modules\store\models\search\InventoryItemSearch represents the model behind the search form about `frontend\modules\store\models\InventoryItem`.
 */
 class InventoryItemSearch extends InventoryItem
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inventory_id', 'checkin_id', 'checkout_id', 'dispose_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['sku', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['unit_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InventoryItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'inventory_item.id' => $this->id,
            'inventory_item.inventory_id' => $this->inventory_id,
            'inventory_item.checkin_id' => $this->checkin_id,
            'inventory_item.checkout_id' => $this->checkout_id,
            'inventory_item.dispose_id' => $this->dispose_id,
            'inventory_item.unit_price' => $this->unit_price,
            'inventory_item.status' => $this->status,
            'inventory_item.deleted_by' => $this->deleted_by,
            'inventory_item.deleted_at' => $this->deleted_at,
            'inventory_item.created_at' => $this->created_at,
            'inventory_item.updated_at' => $this->updated_at,
            'inventory_item.created_by' => $this->created_by,
            'inventory_item.updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'sku', $this->sku]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
