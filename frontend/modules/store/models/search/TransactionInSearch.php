<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\TransactionIn;

/**
 * frontend\modules\store\models\search\TransactionInSearch represents the model behind the search form about `frontend\modules\store\models\TransactionIn`.
 */
 class TransactionInSearch extends TransactionIn
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'check_by', 'refference_id', 'approved_by', 'inventory_id'], 'integer'],
            [['check_date', 'refference', 'approved_at'], 'safe'],
            [['unit_price', 'count', 'total_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransactionIn::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'check_by' => $this->check_by,
            'refference_id' => $this->refference_id,
            'approved_at' => $this->approved_at,
            'approved_by' => $this->approved_by,
            'inventory_id' => $this->inventory_id,
            'unit_price' => $this->unit_price,
            'count' => $this->count,
            'total_price' => $this->total_price,
        ]);

        $query->andFilterWhere(['like', 'check_date', $this->check_date])
            ->andFilterWhere(['like', 'refference', $this->refference]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
