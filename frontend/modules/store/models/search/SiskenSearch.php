<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\Sisken;

/**
 * frontend\modules\store\models\search\SiskenSearch represents the model behind the search form about `frontend\modules\store\models\Sisken`.
 */
 class SiskenSearch extends Sisken
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun', 'bulan', 'no_kerja', 'arahan_kerja', 'jk_jg', 'nama_panel', 'category_panel', 'tempoh_selenggara', 'alasan', 'tarikh_arahan', 'no_sebutharga', 'tarikh_kenderaan_tiba', 'tarikh_jangka_siap', 'tarikh_sebut_harga', 'status_sebut_harga', 'no_do', 'tarikh_siap', 'tempoh_jaminan', 'tarikh_do', 'tarikh_cetakan', 'status', 'catatan_sebutharga', 'catatan', 'nama_pic', 'tarikh_permohonan', 'kategori_kerosakan_id', 'kategori_kerosakan', 'kategori', 'isservice', 'ispancit', 'odometer_terkini', 'no_plat', 'model', 'kod_jabatan', 'nama_jabatan', 'jumlah_kos'], 'safe'],
            [['id'], 'number'],
            [['bengkel_panel_id', 'selenggara_id', 'alasan_id', 'id_kenderaan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sisken::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'bengkel_panel_id' => $this->bengkel_panel_id,
            'selenggara_id' => $this->selenggara_id,
            'alasan_id' => $this->alasan_id,
            'id_kenderaan' => $this->id_kenderaan,
        ]);

        $query->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'bulan', $this->bulan])
            ->andFilterWhere(['like', 'no_kerja', $this->no_kerja])
            ->andFilterWhere(['like', 'arahan_kerja', $this->arahan_kerja])
            ->andFilterWhere(['like', 'jk_jg', $this->jk_jg])
            ->andFilterWhere(['like', 'nama_panel', $this->nama_panel])
            ->andFilterWhere(['like', 'category_panel', $this->category_panel])
            ->andFilterWhere(['like', 'tempoh_selenggara', $this->tempoh_selenggara])
            ->andFilterWhere(['like', 'alasan', $this->alasan])
            ->andFilterWhere(['like', 'tarikh_arahan', $this->tarikh_arahan])
            ->andFilterWhere(['like', 'no_sebutharga', $this->no_sebutharga])
            ->andFilterWhere(['like', 'tarikh_kenderaan_tiba', $this->tarikh_kenderaan_tiba])
            ->andFilterWhere(['like', 'tarikh_jangka_siap', $this->tarikh_jangka_siap])
            ->andFilterWhere(['like', 'tarikh_sebut_harga', $this->tarikh_sebut_harga])
            ->andFilterWhere(['like', 'status_sebut_harga', $this->status_sebut_harga])
            ->andFilterWhere(['like', 'no_do', $this->no_do])
            ->andFilterWhere(['like', 'tarikh_siap', $this->tarikh_siap])
            ->andFilterWhere(['like', 'tempoh_jaminan', $this->tempoh_jaminan])
            ->andFilterWhere(['like', 'tarikh_do', $this->tarikh_do])
            ->andFilterWhere(['like', 'tarikh_cetakan', $this->tarikh_cetakan])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'catatan_sebutharga', $this->catatan_sebutharga])
            ->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'nama_pic', $this->nama_pic])
            ->andFilterWhere(['like', 'tarikh_permohonan', $this->tarikh_permohonan])
            ->andFilterWhere(['like', 'kategori_kerosakan_id', $this->kategori_kerosakan_id])
            ->andFilterWhere(['like', 'kategori_kerosakan', $this->kategori_kerosakan])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'isservice', $this->isservice])
            ->andFilterWhere(['like', 'ispancit', $this->ispancit])
            ->andFilterWhere(['like', 'odometer_terkini', $this->odometer_terkini])
            ->andFilterWhere(['like', 'no_plat', $this->no_plat])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'kod_jabatan', $this->kod_jabatan])
            ->andFilterWhere(['like', 'nama_jabatan', $this->nama_jabatan])
            ->andFilterWhere(['like', 'jumlah_kos', $this->jumlah_kos]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
