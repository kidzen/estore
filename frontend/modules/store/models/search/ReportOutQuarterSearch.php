<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\ReportOutQuarter;

/**
 * frontend\modules\store\models\search\ReportOutQuarterSearch represents the model behind the search form about `frontend\modules\store\models\ReportOutQuarter`.
 */
 class ReportOutQuarterSearch extends ReportOutQuarter
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'count', 'total_price'], 'number'],
            [['quarter'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportOutQuarter::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'year' => $this->year,
            'count' => $this->count,
            'total_price' => $this->total_price,
        ]);

        $query->andFilterWhere(['like', 'quarter', $this->quarter]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
