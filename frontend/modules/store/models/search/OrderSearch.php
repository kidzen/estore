<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\Order;

/**
 * frontend\modules\store\models\search\OrderSearch represents the model behind the search form about `frontend\modules\store\models\Order`.
 */
 class OrderSearch extends Order
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'transaction_id', 'instruction_id', 'ordered_by', 'usage_id', 'checkout_by', 'approved', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['order_date', 'ordered_by_temp', 'order_no', 'required_date', 'checkout_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'instruction_id' => $this->instruction_id,
            'order_date' => $this->order_date,
            'ordered_by' => $this->ordered_by,
            'usage_id' => $this->usage_id,
            'required_date' => $this->required_date,
            'checkout_date' => $this->checkout_date,
            'checkout_by' => $this->checkout_by,
            'approved' => $this->approved,
            'approved_at' => $this->approved_at,
            'approved_by' => $this->approved_by,
            'status' => $this->status,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'ordered_by_temp', $this->ordered_by_temp])
            ->andFilterWhere(['like', 'order_no', $this->order_no]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
