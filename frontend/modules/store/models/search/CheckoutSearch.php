<?php

namespace frontend\modules\store\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\store\models\Checkout;

/**
 * frontend\modules\store\models\search\CheckoutSearch represents the model behind the search form about `frontend\modules\store\models\Checkout`.
 */
 class CheckoutSearch extends Checkout
{
    // use \common\components\RelationSFTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'order_item_id', 'ordered_by', 'transaction_id', 'rq_quantity', 'current_balance', 'created_by', 'approved_by'], 'integer'],
            [['order_required_date', 'order_no', 'order_date', 'card_no', 'code_no', 'description', 'created_date', 'approved_date'], 'safe'],
            [['app_quantity', 'unit_price', 'average_unit_price', 'batch_total_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Checkout::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $dataProvider->sort->attributes[$property] = [
        //     'asc' => [$attribute => SORT_ASC],
        //     'desc' => [$attribute => SORT_DESC],
        // ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->andWhere('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_item_id' => $this->order_item_id,
            'ordered_by' => $this->ordered_by,
            'transaction_id' => $this->transaction_id,
            'rq_quantity' => $this->rq_quantity,
            'app_quantity' => $this->app_quantity,
            'current_balance' => $this->current_balance,
            'unit_price' => $this->unit_price,
            'average_unit_price' => $this->average_unit_price,
            'batch_total_price' => $this->batch_total_price,
            'created_by' => $this->created_by,
            'approved_by' => $this->approved_by,
        ]);

        $query->andFilterWhere(['like', 'order_required_date', $this->order_required_date])
            ->andFilterWhere(['like', 'order_no', $this->order_no])
            ->andFilterWhere(['like', 'order_date', $this->order_date])
            ->andFilterWhere(['like', 'card_no', $this->card_no])
            ->andFilterWhere(['like', 'code_no', $this->code_no])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_date', $this->created_date])
            ->andFilterWhere(['like', 'approved_date', $this->approved_date]);

        // $query->andFilterWhere(['like', 'attribute', $this->$property]);

        return $dataProvider;
    }
}
