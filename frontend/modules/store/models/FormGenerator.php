<?php

namespace frontend\modules\store\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "ACTIVITY_LOGS".
 *
 * @property string $ID
 * @property integer $USER_ID
 * @property string $REMOTE_IP
 * @property string $ACTION
 * @property string $CONTROLLER
 * @property string $PARAMS
 * @property string $ROUTE
 * @property string $STATUS
 * @property string $MESSAGES
 * @property string $CREATED_AT
 */
class FormGenerator extends Model {

    const LOG_STATUS_SUCCESS = 'success';
    const LOG_STATUS_INFO = 'info';
    const LOG_STATUS_WARNING = 'warning';
    const LOG_STATUS_ERROR = 'error';
    const LOG_STATUS_FAIL = 'fail';

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public $id;
    public $form_id;
    public $form_name;
    public $params;
    public $inventory_id;
    public $inventory_list;
    public $order_id;
    public $year;
    public $month;
    public $print_all;



    /**
     * @inheritdoc
     */
//    public static function tableName() {
//        return 'ACTIVITY_LOGS';
//    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['form_id'], 'required'],
            [['form_id','id'], 'number'],
            [['inventory_id','inventory_list','order_id','year','month','print_all'], 'integer'],
            [[ 'form_name', 'params'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'year' => 'Tahun',
            'month' => 'Bulan',
            'order_id' => 'No Pesanan',
            'inventory_id' => 'Nama Inventori',
            'inventory_list' => 'Senarai inventori',
            'form_id' => 'ID Borang',
            'form_name' => 'Nama Borang',
            'params' => 'Maklumat Borang',
            'print_all' => 'Cetak Semua Transaksi',
        ];
    }

    /**
     * Adds a message to ActionLog model
     *
     * @param string $status The log status information
     * @param mixed $message The log message
     * @param int $uID The user id
     */
//    public static function add($status = null, $message = null) {
//        $model = Yii::createObject(__CLASS__);
//        $model->USER_ID = (int)Yii::$app->user->id;
//        $model->REMOTE_IP = $_SERVER['REMOTE_ADDR'];
//        $model->ACTION = Yii::$app->requestedAction->id;
//        $model->CONTROLLER = Yii::$app->requestedAction->controller->id;
//        $model->PARAMS = serialize(Yii::$app->requestedAction->controller->actionParams);
//        $model->ROUTE = Yii::$app->requestedRoute;
//        $model->STATUS = $status;
//        $model->MESSAGES = ($message !== null) ? serialize($message) : null;
//
//        return $model->save();
//    }
}
