<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\InventoryCheckoutFreqReport as BaseInventoryCheckoutFreqReport;

/**
 * This is the model class for table "inventory_checkout_freq_report".
 */
class InventoryCheckoutFreqReport extends BaseInventoryCheckoutFreqReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['inventory_id'], 'integer'],
            [['year', 'month', 'by_inventory', 'by_year', 'by_month', 'by_date', 'rank_inventory', 'rank_year', 'rank_month', 'rank_date'], 'number'],
            [['date'], 'string', 'max' => 11]
        ]);
    }

}
