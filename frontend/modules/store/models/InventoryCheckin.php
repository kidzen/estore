<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\InventoryCheckin as BaseInventoryCheckin;

/**
 * This is the model class for table "inventory_checkin".
 */
class InventoryCheckin extends BaseInventoryCheckin
{
    const STATUS_REJECTED = 8;
    const STATUS_APPROVED = 1;
    const STATUS_PENDING = 2;
    /**
     * @inheritdoc
     */
    // public function rules()
    // {
    //     return [
    //         [['transaction_id', 'budget_id', 'store_id', 'inventory_id', 'vendor_id', 'items_quantity', 'check_by', 'approved', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
    //         [['items_total_price'], 'number'],
    //         [['check_date', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe']
    //     ];
    // }
    public function rules()
    {
        return array_merge(parent::rules(),[
            [['inventory_id', 'items_quantity', 'items_total_price','check_date'], 'required'],
        ]);
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($insert){
            if(!$transactionId = Transaction::add(Transaction::CHECKIN)) {
                throw new Exception("Error Processing Transaction", 1);
            }
            $this->store_id = Yii::$app->cache->get('active-store');
            $this->items_total_price = $this->items_total_price * $this->items_quantity;
            $this->transaction_id = $transactionId;
            switch ((Yii::$app->dbStore->driverName)) {
                case 'oci':
                    # code...
                $this->check_date = date('d-M-y');
                break;
                case 'mysql':
                    # code...
                $this->check_date = date('Y-m-d');
                break;

                default:
                    # code...
                break;
            }
            $this->approved = self::STATUS_APPROVED;
            $this->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
            $this->approved_by = Yii::$app->user->id;
        }
        return true;
    }

    public function afterSave($insert,$changedAttributes)
    {
        if($this->items_quantity == 0) {
            throw new Exception("Error Processing Request", 1);
        }
        if ($insert){

            $valid = InventoryItem::checkin([
                'unit_price' => $this->items_total_price / $this->items_quantity,
                'inventory' => $this->inventory,
                'items_quantity' => $this->items_quantity,
                'checkin_id' => $this->id,
            ]);
            // $valid = true;
        } else {
            $valid = true;
        }
        return $valid;
    }

    public function getApprovedLabel()
    {
        switch ($this->approved) {
            case 1:
                return \kartik\helpers\Html::bsLabel(Yii::t('app','approved'),'success');
                break;
            case 2:
                return \kartik\helpers\Html::bsLabel(Yii::t('app','pending'),'warning');
                break;
            case 8:
                return \kartik\helpers\Html::bsLabel(Yii::t('app','rejected'),'danger');
                break;

            default:
                return \kartik\helpers\Html::bsLabel(Yii::t('app','inactive'),'default');
                break;
        }
    }

    public function getStatusLabel()
    {
        switch ($this->status) {
            case 1:
                return \kartik\helpers\Html::bsLabel(Yii::t('app','active'),'success');
                break;

            default:
                return \kartik\helpers\Html::bsLabel(Yii::t('app','inactive'),'danger');
                break;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function reject()
    {
        if($this->inventoryItems) {
            foreach ($this->inventoryItems as $key => $value) {
                $value->delete();
            }
        }
        $transaction = $this->transaction;
        $this->delete();
        $transaction->delete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByName()
    {
        return $this->updatedBy->profile->staff_no . ' : ' . $this->updatedBy->profile->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedByName()
    {
        return $this->createdBy->profile->staff_no . ' : ' . $this->createdBy->profile->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedByName()
    {
        return $this->approvedBy->profile->staff_no . ' : ' . $this->approvedBy->profile->name;
    }

}
