<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\UsageItem as BaseUsageItem;

/**
 * This is the model class for table "usage_item".
 */
class UsageItem extends BaseUsageItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['identity_key', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['identity_no'], 'string', 'max' => 255],
            [['identity_no'], 'unique']
        ]);
    }

}
