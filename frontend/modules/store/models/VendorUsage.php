<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\VendorUsage as BaseVendorUsage;

/**
 * This is the model class for table "vendor_usage".
 */
class VendorUsage extends BaseVendorUsage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'year', 'month', 'items_total_price', 'by_year', 'by_vendor', 'by_year_vendor', 'by_month_vendor', 'by_date_vendor'], 'number'],
            [['created_at'], 'safe'],
            [['inventory_id'], 'integer'],
            [['vendor'], 'string', 'max' => 255],
            [['date'], 'string', 'max' => 11]
        ]);
    }

}
