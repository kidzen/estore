<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\TransactionOutSisken as BaseTransactionOutSisken;

/**
 * This is the model class for table "TRANSACTION_OUT_SISKEN".
 */
class TransactionOutSisken extends BaseTransactionOutSisken
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ID', 'UNIT_PRICE', 'BATCH_QUANTITY', 'BATCH_TOTAL_PRICE', 'TOTAL_QUANTITY', 'TOTAL_PRICE'], 'number'],
            [['ARAHAN_KERJA_ID', 'STATUS', 'ORDER_ITEMS_ID'], 'integer'],
            [['ORDER_NO', 'CATEGORY_NAME', 'CARD_NO', 'CODE_NO', 'INVENTORY_DESCRIPTION'], 'string', 'max' => 255],
            [['CHECK_DATE'], 'string', 'max' => 9],
            [['ITEMS'], 'string', 'max' => 4000]
        ]);
    }

}
