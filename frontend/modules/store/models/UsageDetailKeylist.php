<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\UsageDetailKeylist as BaseUsageDetailKeylist;

/**
 * This is the model class for table "usage_detail_keylist".
 */
class UsageDetailKeylist extends BaseUsageDetailKeylist
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['key'], 'string', 'max' => 255]
        ]);
    }

}
