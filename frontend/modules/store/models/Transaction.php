<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Transaction as BaseTransaction;

/**
 * This is the model class for table "transaction".
 */
class Transaction extends BaseTransaction
{
    const CHECKIN = 1;
    const CHECKOUT = 2;
    const DISPOSE = 3;
    /**
     * @inheritdoc
     */

    public static function add($type)
    {
        $model = new self();
        $model->type = $type;
        $model->store_id = \Yii::$app->cache->get('active-store');
        $model->check_date = new \yii\db\Expression('CURRENT_TIMESTAMP');
        $model->check_by = Yii::$app->user->id;
        return $model->save() ? $model->id : false;
    }

    public function getTypeLabel2()
    {
        switch ($this->type) {
            case 1:
            $this->type = 'IN';
            break;
            case 2:
            $this->type = 'OUT';
            break;
            case 3:
            $this->type = 'DISPOSE';
            break;

            default:
            $this->type = 'UNDEFINED';
            break;
        }
        return $this->type;
    }

    public function getTypeLabel()
    {
        switch($this->type) {
            case 1:
            return \kartik\helpers\Html::bsLabel('Checkin','success');
            break;
            case 2:
            return \kartik\helpers\Html::bsLabel('Checkout','primary');
            break;
            case 3:
            return \kartik\helpers\Html::bsLabel('Dispose','danger');
            break;
            default:
            return \kartik\helpers\Html::bsLabel('Undefined','default');
            break;
        }
    }

    public function getRefference()
    {
        switch($this->type) {
            case 1:
            $ref = \frontend\modules\store\models\InventoryCheckin::find()->andWhere(['inventory_checkin.transaction_id' => $this->id])->asArray()->one();
            return \kartik\helpers\Html::a($ref['id'],['/inventory-checkin/view','id'=>$ref['id']]);
            break;
            case 2:
            $ref = \frontend\modules\store\models\Order::find()->andWhere(['order.transaction_id' => $this->id])->asArray()->one();
            return \kartik\helpers\Html::a($ref['order_no'],['/order/view','id'=>$ref['id']]);
            break;
            case 3:
            $ref = \frontend\modules\store\models\Dispose::find()->andWhere(['dispose.transaction_id' => $this->id])->asArray()->one();
            return \kartik\helpers\Html::a($ref['id'],['/dispose/view','id'=>$ref['id']]);
            break;
            default:
            return \kartik\helpers\Html::bsLabel('Undefined','default');
            break;
        }
    }

}
