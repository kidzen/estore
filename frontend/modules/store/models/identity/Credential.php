<?php
namespace frontend\modules\store\models\identity;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use frontend\modules\store\models\base\Credential as BaseCredential;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class Credential extends BaseCredential implements IdentityInterface
{


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['staff_no' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getAvatar()
    {
        return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(\frontend\modules\store\models\Profile::className(), ['staff_no' => 'staff_no']);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreList2()
    {
        $this->hasMany(\frontend\modules\store\models\Store::className(), ['id' => 'store_id'])->via('storeAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreAssigned()
    {
        return $this->hasMany(\frontend\modules\store\models\StoreAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreList()
    {
        $storeList = [];
        foreach (\frontend\modules\store\models\Store::findAll(['store.deleted_by' => 0]) as $key => $value) {
            $storeList[$value['id']] = $value;
        }
/*        foreach ($this->getStoreAssigned()->with('store')->asArray()->all() as $key => $value) {
            $storeList[$value['store_id']] = $value;
        }*/
        return $storeList;
    }

}
