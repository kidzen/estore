<?php
namespace frontend\modules\store\models\identity;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use frontend\modules\store\models\Credential as BaseUser;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends BaseUser implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['staff_no' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['staff_no' => $username]);
    }


    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->staff_no;
    }

    public function getAvatar()
    {
        return '';
    }

    public function getProfile()
    {
        return $this->hasOne(\frontend\modules\store\models\Profile::className(), ['staff_no' => 'staff_no']);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     * ? $this->credential
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        // return Yii::$app->security->validatePassword($password, isset($this->credential) ? $this->credential->password : null);
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreList2()
    {
        $this->hasMany(\frontend\modules\store\models\Store::className(), ['id' => 'store_id'])->via('storeAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreAssigned()
    {
        return $this->hasMany(\frontend\modules\store\models\StoreAssignment::className(), ['user_id' => 'staff_no']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreList()
    {
        $storeList = [];

        foreach ($this->getStoreAssigned()->with('store')->asArray()->all() as $key => $value) {
            $storeList[$value['store_id']] = $value;
        }
        return $storeList;
    }
}
