<?php
namespace frontend\modules\store\models\identity;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use frontend\modules\store\models\base\User as BaseUser;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends BaseUser implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['id' => $username, 'status' => self::STATUS_ACTIVE]);
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getAvatar()
    {
        return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCredential()
    {
        return $this->hasOne(\frontend\modules\store\models\Credential::className(), ['staff_no' => 'id']);
        // return $this->hasOne(\frontend\modules\store\models\Credential::className(), ['staff_no' => 'username']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getUsername0()
    // {
    //     return $this->hasOne(\frontend\modules\store\models\Profile::className(), ['staff_no' => 'username']);
    // }

    public function getProfile()
    {
        return $this->hasOne(\frontend\modules\store\models\Profile::className(), ['staff_no' => 'id']);
        // return $this->hasOne(\frontend\modules\store\models\Profile::className(), ['staff_no' => 'username']);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     * ? $this->credential
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        // return $this->credential->password;
        // return Yii::$app->security->validatePassword($password, isset($this->credential) ? $this->credential->password : null);
        return Yii::$app->security->validatePassword($password, $this->credential->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreList2()
    {
        $this->hasMany(\frontend\modules\store\models\Store::className(), ['id' => 'store_id'])->via('storeAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreAssigned()
    {
        return $this->hasMany(\frontend\modules\store\models\StoreAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreList()
    {
        $storeList = [];
        foreach (\frontend\modules\store\models\Store::findAll(['store.deleted_by' => 0]) as $key => $value) {
            $storeList[$value['id']] = $value;
        }
/*        foreach ($this->getStoreAssigned()->with('store')->asArray()->all() as $key => $value) {
            $storeList[$value['store_id']] = $value;
        }*/
        return $storeList;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function registerNewUser($username)
    {
        $model = new self();
        $model->id = $username;
        $model->username = $username;
        $model->role_id = 2;
        if($model->save()){
            return true;
        }
        return false;
    }
}
