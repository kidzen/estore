<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\DisposeDetailKey as BaseDisposeDetailKey;

/**
 * This is the model class for table "dispose_detail_key".
 */
class DisposeDetailKey extends BaseDisposeDetailKey
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['key', 'description'], 'string', 'max' => 255]
        ]);
    }

}
