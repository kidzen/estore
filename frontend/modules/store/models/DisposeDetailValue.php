<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\DisposeDetailValue as BaseDisposeDetailValue;

/**
 * This is the model class for table "dispose_detail_value".
 */
class DisposeDetailValue extends BaseDisposeDetailValue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['dispose_id', 'key_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['value'], 'string', 'max' => 255]
        ]);
    }

}
