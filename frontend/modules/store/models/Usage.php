<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Usage as BaseUsage;

/**
 * This is the model class for table "usage".
 */
class Usage extends BaseUsage
{
    /**
     * @inheritdoc
     */

    public function getCategory()
    {
        if(isset($this->vehicle)){
            return 'vehicle';
        } else if(isset($this->store)) {
            return 'store';
        } else if(isset($this->workshop)) {
            return 'workshop';
        } else {
            return null;
        }
        return $this->hasOne(\frontend\modules\store\models\Vehicle::className(), ['id'=>'item_id']);
    }

    public function getDetail()
    {
        switch ($this->category) {
            case 'vehicle':
                return $this->vehicle;
                break;
            case 'store':
                return $this->store;
                break;
            case 'workshop':
                return $this->workshop;
                break;

            default:
                return null;
                break;
        }
    }

    public function getVehicle()
    {
        return $this->hasOne(\frontend\modules\store\models\Vehicle::className(), ['id'=>'item_id']);
    }

    public function getStore()
    {
        return $this->hasOne(\frontend\modules\store\models\Store::className(), ['id'=>'item_id']);
    }

    public function getWorkshop()
    {
        return $this->hasOne(\frontend\modules\store\models\Workshop::className(), ['id' => 'item_id']);
    }
}
