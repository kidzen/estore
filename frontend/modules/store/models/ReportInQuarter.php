<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\ReportInQuarter as BaseReportInQuarter;

/**
 * This is the model class for table "report_in_quarter".
 */
class ReportInQuarter extends BaseReportInQuarter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['year', 'count', 'total_price'], 'number'],
            [['quarter'], 'string', 'max' => 1]
        ]);
    }

}
