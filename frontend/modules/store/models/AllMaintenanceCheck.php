<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\AllMaintenanceCheck as BaseAllMaintenanceCheck;

/**
 * This is the model class for table "all_maintenance_check".
 */
class AllMaintenanceCheck extends BaseAllMaintenanceCheck
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id'], 'number'],
            [['data_id'], 'integer'],
            [['type'], 'string', 'max' => 3],
            [['table_name', 'col_name'], 'string', 'max' => 17],
            [['fault_value', 'true_value'], 'string', 'max' => 200]
        ]);
    }

}
