<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Dispose as BaseDispose;

/**
 * This is the model class for table "dispose".
 */
class Dispose extends BaseDispose
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
        [
            [['transaction_id', 'category_id', 'method_id', 'reason_id', 'quantity', 'requested_by', 'disposed_by', 'approved', 'approved_by', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['current_revenue', 'cost', 'returns'], 'number'],
            [['requested_at', 'disposed_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['refference_no', 'description'], 'string', 'max' => 255]
        ]);
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($insert){
            if(!$transactionId = Transaction::add(Transaction::DISPOSE)) {
                throw new Exception("Fail to initialize transaction", 1);
            }
            $this->transaction_id = $transactionId;
        }
        return true;
    }

    public function getItemToDispose2()
    {
        $searchModel = new \frontend\modules\store\models\search\InventoryItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['dispose_id'=>$id]);
        return [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
    }

    public function getItemToDispose()
    {
        $searchModel = new \frontend\modules\store\models\search\InventoryItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['inventory_item.checkout_id'=>null,'inventory_item.dispose_id'=>null]);
        return [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
    }

    public function evaluateCurrentRevenue()
    {
        $totalDepreciatedValue = 0;
        foreach ($this->inventoryItems as $key => $value) {
            $lifeSpan = date('Y') - date('Y', strtotime($value['checkin']['check_date']));
            $depreciatedValue = $value['unit_price'];
            for ($i=0; $i < $lifeSpan; $i++) {
                $depreciatedValue -= (0.2 * $depreciatedValue);
            }
            $totalDepreciatedValue = $totalDepreciatedValue + $depreciatedValue;
        }
        return $totalDepreciatedValue;
    }
}
