<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\InventoryCategory as BaseInventoryCategory;

/**
 * This is the model class for table "inventory_category".
 */
class InventoryCategory extends BaseInventoryCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'filter', 'filter' => 'trim'],
            [['name'], 'unique'],

        ]);
    }

}
