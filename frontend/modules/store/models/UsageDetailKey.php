<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\UsageDetailKey as BaseUsageDetailKey;

/**
 * This is the model class for table "usage_detail_key".
 */
class UsageDetailKey extends BaseUsageDetailKey
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['category_id', 'status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ]);
    }

}
