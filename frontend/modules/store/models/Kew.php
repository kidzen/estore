<?php

namespace frontend\modules\store\models\kew;

use Yii;
use frontend\modules\store\models\kew\Kewps;
use frontend\modules\store\models\kew\Kewpa;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use frontend\modules\store\models\People;
use frontend\modules\store\models\Orders;
use frontend\modules\store\models\OrderItems;
use yii\data\ArrayDataProvider;

class Kew {

    public static function print($kew,$param) {
        switch ($kew) {
            // kewps
            case 'kewps1':
                Kewps::pdfKewps1($param);
                break;
            case 'kewps2':
                Kewps::pdfKewps2($param);
                break;
            case 'kewps3':
                Kewps::pdfKewps3($param);
                break;
            case 'kewps4':
                Kewps::pdfKewps4($param);
                break;
            case 'kewps5':
                Kewps::pdfKewps5($param);
                break;
            case 'kewps6':
                Kewps::pdfKewps6($param);
                break;
            case 'kewps7':
                Kewps::pdfKewps7($param);
                break;
            case 'kewps8':
                Kewps::pdfKewps8($param);
                break;
            case 'kewps9':
                Kewps::pdfKewps9($param);
                break;
            case 'kewps10':
                Kewps::pdfKewps10($param);
                break;
            case 'kewps11':
                Kewps::pdfKewps11($param);
                break;
            case 'kewps12':
                Kewps::pdfKewps12($param);
                break;
            case 'kewps13':
                Kewps::pdfKewps13($param);
                break;
            case 'kewps14':
                Kewps::pdfKewps14($param);
                break;
            case 'kewps15':
                Kewps::pdfKewps15($param);
                break;
            case 'kewps16':
                Kewps::pdfKewps16($param);
                break;
            case 'kewps17':
                Kewps::pdfKewps17($param);
                break;
            case 'kewps18':
                Kewps::pdfKewps18($param);
                break;
            case 'kewps19':
                Kewps::pdfKewps19($param);
                break;
            case 'kewps20':
                Kewps::pdfKewps20($param);
                break;
            case 'kewps21':
                Kewps::pdfKewps21($param);
                break;
            case 'kewps22':
                Kewps::pdfKewps22($param);
                break;
            // kewpa
            case 'kewpa1':
                Kewpa::pdfKewpa1($param);
                break;
            case 'kewpa2':
                Kewpa::pdfKewpa2($param);
                break;
            case 'kewpa3':
                Kewpa::pdfKewpa3($param);
                break;
            case 'kewpa4':
                Kewpa::pdfKewpa4($param);
                break;
            case 'kewpa5':
                Kewpa::pdfKewpa5($param);
                break;
            case 'kewpa6':
                Kewpa::pdfKewpa6($param);
                break;
            case 'kewpa7':
                Kewpa::pdfKewpa7($param);
                break;
            case 'kewpa8':
                Kewpa::pdfKewpa8($param);
                break;
            case 'kewpa9':
                Kewpa::pdfKewpa9($param);
                break;
            case 'kewpa10':
                Kewpa::pdfKewpa10($param);
                break;
            case 'kewpa11':
                Kewpa::pdfKewpa11($param);
                break;
            case 'kewpa12':
                Kewpa::pdfKewpa12($param);
                break;
            case 'kewpa13':
                Kewpa::pdfKewpa13($param);
                break;
            case 'kewpa14':
                Kewpa::pdfKewpa14($param);
                break;
            case 'kewpa15':
                Kewpa::pdfKewpa15($param);
                break;
            case 'kewpa16':
                Kewpa::pdfKewpa16($param);
                break;
            case 'kewpa17':
                Kewpa::pdfKewpa17($param);
                break;
            case 'kewpa18':
                Kewpa::pdfKewpa18($param);
                break;
            case 'kewpa19':
                Kewpa::pdfKewpa19($param);
                break;
            case 'kewpa20':
                Kewpa::pdfKewpa20($param);
                break;
            case 'kewpa21':
                Kewpa::pdfKewpa21($param);
                break;
            case 'kewpa22':
                Kewpa::pdfKewpa22($param);
                break;

            default:
                # code...
                break;
        }
    }

}
