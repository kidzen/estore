<?php

namespace frontend\modules\store\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\store\models\query\InventoryCheckoutFreqReport]].
 *
 * @see \frontend\modules\store\models\query\InventoryCheckoutFreqReport
 */
class InventoryCheckoutFreqReportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\InventoryCheckoutFreqReport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\InventoryCheckoutFreqReport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
