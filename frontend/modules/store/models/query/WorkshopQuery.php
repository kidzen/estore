<?php

namespace frontend\modules\store\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\store\models\query\Workshop]].
 *
 * @see \frontend\modules\store\models\query\Workshop
 */
class WorkshopQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        if(!\Yii::$app->user->can('Administrator')) {
            $this->andWhere('[[status]]=1');
        }
        return $this;
    }*/

    public function mine()
    {
            $this->andWhere('[[created_by]]='.\Yii::$app->user->id);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\Workshop[]|array
     */
    public function all($db = null, $bypass = false)
    {
        // uncomment and edit permission rule to view all
        /*if(!\Yii::$app->user->can('Administrator')) {
            $this->mine();
        }*/
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\Workshop|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
