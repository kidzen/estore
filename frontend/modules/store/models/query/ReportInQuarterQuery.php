<?php

namespace frontend\modules\store\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\store\models\query\ReportInQuarter]].
 *
 * @see \frontend\modules\store\models\query\ReportInQuarter
 */
class ReportInQuarterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\ReportInQuarter[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\ReportInQuarter|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
