<?php

namespace frontend\modules\store\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\store\models\query\Order]].
 *
 * @see \frontend\modules\store\models\query\Order
 */
class OrderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        if(!\Yii::$app->user->can('Administrator')) {
            $this->andWhere('[[status]]=1');
        }
        return $this;
    }*/

    public function mine()
    {
            // $this->andWhere('[[created_by]]='.\Yii::$app->user->id);
            $this->joinWith('transaction');
            $this->andFilterWhere(['transaction.store_id' => \Yii::$app->cache->get('active-store')]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\Order[]|array
     */
    public function all($db = null, $bypass = false)
    {
        // uncomment and edit permission rule to view all
        /*if(!\Yii::$app->user->can('Administrator')) {
            $this->mine();
        }*/
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\Order|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
