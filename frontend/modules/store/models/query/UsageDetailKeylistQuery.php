<?php

namespace frontend\modules\store\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\store\models\query\UsageDetailKeylist]].
 *
 * @see \frontend\modules\store\models\query\UsageDetailKeylist
 */
class UsageDetailKeylistQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\UsageDetailKeylist[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\UsageDetailKeylist|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
