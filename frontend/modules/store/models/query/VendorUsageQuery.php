<?php

namespace frontend\modules\store\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\store\models\query\VendorUsage]].
 *
 * @see \frontend\modules\store\models\query\VendorUsage
 */
class VendorUsageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\VendorUsage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\VendorUsage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
