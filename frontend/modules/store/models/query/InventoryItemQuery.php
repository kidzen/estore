<?php

namespace frontend\modules\store\models\query;

/**
 * This is the ActiveQuery class for [[\frontend\modules\store\models\query\InventoryItem]].
 *
 * @see \frontend\modules\store\models\query\InventoryItem
 */
class InventoryItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        if(!\Yii::$app->user->can('Administrator')) {
            $this->andWhere('[[status]]=1');
        }
        return $this;
    }*/

    public function mine()
    {
        if(!empty(\Yii::$app->user->storeList)) {
            $this->joinWith('checkin');
            // $this->andFilterWhere(['in', 'inventory_checkin.store_id', \Yii::$app->user->storeList]);
            $this->andFilterWhere(['inventory_checkin.store_id' => \Yii::$app->cache->get('active-store')]);
        } else {
            $this->andWhere(['inventory_item.created_by' => \Yii::$app->user->id]);
        }
        return $this;
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\InventoryItem[]|array
     */
    public function all($db = null, $bypass = false)
    {
        // uncomment and edit permission rule to view all
        /*if(!\Yii::$app->user->can('Administrator')) {
            $this->mine();
        }*/
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\store\models\query\InventoryItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
