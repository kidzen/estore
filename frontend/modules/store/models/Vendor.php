<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Vendor as BaseVendor;

/**
 * This is the model class for table "vendor".
 */
class Vendor extends BaseVendor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'address', 'contact_no', 'email'], 'string', 'max' => 255]
        ]);
    }

}
