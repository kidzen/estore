<?php

namespace frontend\modules\store\models\kew;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use frontend\modules\store\models\People;
use frontend\modules\store\models\Orders;
use frontend\modules\store\models\OrderItems;
use yii\data\ArrayDataProvider;
// refference for mpdf
        // mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right ,  $margin_top , $margin_bottom , $margin_header ,  $margin_footer , $orientation)
        // $mpdf = new \mPDF('c', 'A4-P','','',$margin_left, $margin_right ,  $margin_top , $margin_bottom , $margin_header ,  $margin_footer );
        // $mpdf = new \mPDF('c', 'A4-P','9','aria',1            ,2              ,3              ,4              ,5              ,6);
// need attention
// form 3
// form 14

/**
 * This is the model class for table KEWPS FORM.
 *
 * @property string $ID
 * @property integer $TRANS_ID
 * @property string $APPROVED_AT
 */
class Kewps {

    public static function pdfKewps10($id) {
        $searchModel = new \frontend\modules\store\models\FormKewps10Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->with(['ORDER_ID' => $id]);
        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
        $items = $dataProvider->models;
        // var_dump($items);die();

        $arraylist = array_chunk($items, 3);
        // var_dump(sizeof($arraylist[2]));die();


        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps102_1', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        $contentBody = [];
        foreach ($arraylist as $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps102_2', [
                'items' => $list,
                'dataProvider' => $dataProvider,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps102_3', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        //set mpdf properties
        $mpdf = new \mPDF('c', 'A4-L','','',13,13,10,6,5,5);
        $mpdf->SetTitle('KEW-PS 10 (Borang Pesanan Dan Pengurusan Stok)');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        //define stylesheet
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 11px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 4 ('.$inventory['DESCRIPTION'].')_'.date('Y-m-d').'.pdf','I');
        exit;
    }
    public static function pdfKewps103($id) {
        // $id = 983;
        // $id = 348;
        // Orders::findOne($id);
        $orders = Orders::findOne();
        $orderItems = $order->items;
        // var_dump($items);die();
        $arraylist = array_chunk($items, 3);
        // var_dump(sizeof($arraylist[2]));die();

        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps102_1', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        $contentBody = [];
        foreach ($arraylist as $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps102_2', [
                'items' => $list,
                'dataProvider' => $dataProvider,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps102_3', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        $mpdf = new \mPDF('c', 'A4-L','','',13,13,10,6,5,5);
        $mpdf->SetTitle('KEW-PS 10 (Borang Pesanan Dan Pengurusan Stok)');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 11px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 4 ('.$inventory['DESCRIPTION'].')_'.date('Y-m-d').'.pdf','I');
        exit;
    }
    public static function PdfKewps4($id,$year = '', $month= '',$printAll = 0) {
        // var_dump($printAll);die();
        $pageSize = 27;
        // $page = 1;
        $inventory = Inventories::find()->andWhere(['ID'=>$id])->with('category')->asArray()->one();
        if($printAll){
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => \frontend\modules\store\models\TransactionsAll::find()
                ->with('checkBy.mpspProfile')
                ->andWhere(['INVENTORY_ID' => $id])
                ->andWhere(['extract(year from APPROVED_DATE)'=> $year])
                ->andWhere(['extract(month from APPROVED_DATE)'=> $month])
                ->orderBy(['INVENTORY_ID'=>SORT_DESC,'ID'=>SORT_ASC])
                ->asArray()->all(),
                'pagination' => [
                'pageSize' => false,
            // 'page' => 1,
                ],
                ]);

        } else {
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => \frontend\modules\store\models\TransactionsAll::find()
                ->with('checkBy.mpspProfile')
                ->andWhere(['INVENTORY_ID' => $id])
                ->orderBy(['INVENTORY_ID'=>SORT_DESC,'ID'=>SORT_ASC])
                ->asArray()->all(),
                'pagination' => [
                'pageSize' => false,
            // 'page' => 1,
                ],
                ]);

        }
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            // $dataProvider->pagination->page = $page;
        }
        $transactions = $dataProvider->models;
        // var_dump($transactions);die();
        // var_dump(sizeof($transactions));die();
        $arraylist = array_chunk($transactions, $pageSize);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps4_1', [
            'transactions' => $transactions,
            'inventory' => $inventory,
            ]);
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps4_2', [
                'transactions' => $list,
                'inventory' => $inventory,
                'pageSize' => $pageSize,
                ]);
        }

        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps4_3', [
            'transactions' => $transactions,
            'inventory' => $inventory,
            ]);

        // mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right ,  $margin_top , $margin_bottom , $margin_header ,  $margin_footer , $orientation)
        $mpdf = new \mPDF('c', 'A4-P','','',15,15,16,16,9,9);
        $mpdf->SetTitle('KEW-PS 4 ('.$inventory['DESCRIPTION'].')');
        // $mpdf->default_font_size = 1;
        // $mpdf->default_font = 1;
        // $mpdf->margin_left = 15;
        // $mpdf->margin_right = 15;
        // $mpdf->margin_top = 16;
        // $mpdf->margin_bottom = 16;
        // $mpdf->margin_header = 9;
        // $mpdf->margin_footer = 9;
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 12px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
                // $mpdf->AddPage('P','SINGLE-SIDED');
                // AddPage (string $orientation,string $type, string $resetpagenum, string $pagenumstyle, string $suppress, float $margin-left, float $margin-right, float $$margin-top, float $$margin-bottom, float $$margin-header, float $margin-footer, string $odd-header-name, string $even-header-name, string $$odd-footer-name, string $$even-footer-name, mixed $$odd-header-value, mixed $even-header-value, mixed $odd-footer-value, mixed $$even-footer-value, string $pageselector, mixed $sheet-size)
            }
        }
        $mpdf->Output('KEW-PS 4 ('.$inventory['DESCRIPTION'].')_'.date('Y-m-d').'.pdf','I');
        exit;
        // $pdf->render();
        // exit;
    }
    public static function PdfKewps5($id) {
        // $categoryId = 1;
        // $month = 11;
        // $id= 11;
        $pageSize = 10;
        $page = 1;

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \frontend\modules\store\models\Inventories::find()
            // ->andWhere(['CATEGORY_ID'=>$categoryId])
            ->andFilterWhere(['in','ID', $id])
            ->andFilterWhere(['DELETED'=> 0])
            ->orderBy(['ID'=>SORT_ASC])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            $dataProvider->pagination->page = $page;
        }


        $items = $dataProvider->models;
        // var_dump($items);die();
        $arraylist = array_chunk($items, 35);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps5_1');
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps5_2', [
                'items' => $list,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps5_3');

        // mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right ,  $margin_top , $margin_bottom , $margin_header ,  $margin_footer , $orientation)
        $mpdf = new \mPDF('c', 'A4-P','','',15,15,16,16,9,9);
        $mpdf->SetTitle('KEW-PS 5 (Senarai Daftar Kad Kawalan Stok)');
        // $mpdf->default_font_size = 1;
        // $mpdf->default_font = 1;
        // $mpdf->margin_left = 15;
        // $mpdf->margin_right = 15;
        // $mpdf->margin_top = 16;
        // $mpdf->margin_bottom = 16;
        // $mpdf->margin_header = 9;
        // $mpdf->margin_footer = 9;
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 11px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 5 (Senarai Daftar Kad Kawalan Stok)_'.date('Y-m-d').'.pdf','I');
        exit;

    }

    public static function PdfKewps7($id = [141,142],$year) {
        // $id = [142,143,144,145,146,147,148,149,150,151,152,153,154,651];
        // $month = 11;
        $year2 = $year;
        // $year2 = 2013;
        $year1 = $year2 - 1;
        $pageSize = 10;
        // $page = 1;

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \frontend\modules\store\models\Inventories::find()
            ->andFilterWhere(['in', 'ID', $id])
            // ->andFilterWhere(['in', 'extract(year from APPROVED_AT)', $year])
            ->andFilterWhere(['DELETED'=> 0])
            ->orderBy(['ID'=>SORT_ASC])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            // $dataProvider->pagination->page = $page;
        }


        $items = $dataProvider->models;
        $arraylist = array_chunk($items, 5);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps7_1',[
            'year1' => $year1,
            'year2' => $year2,
            ]);
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps7_2', [
                'items' => $list,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps7_3');

        $mpdf = new \mPDF('c', 'A4-L','','',13,13,10,6,5,5);
        $mpdf->SetTitle('KEW-PS 7 (Penentuan Kumpulan Stok)');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 11px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 7 (Penentuan Kumpulan Stok)_'.date('Y-m-d').'.pdf','I');
        exit;
    }

    public static function PdfKewps8($id = [420, 421,423]) {
        // $id = [142,143,144,145,146,147,148,149,150,151,152,153,154,651];
        $searchModel = new InventoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['DELETED' => 0]);
        $dataProvider->query->andFilterWhere(['in', 'ID', $id]);

        $items = $dataProvider->models;
        $contentBody = [];
        foreach ($items as $index => $item) {
            $contentBody[] = Yii::$app->controller->renderPartial('_kewps8', [
                'item' => $item,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                ]);
        }
        $mpdf = new \mPDF('c', 'A4-L','','',19,17,35,6,5,5);
        $cssName = '@frontend/assets/dist/css/Pdf.css';
        $cssFile = Yii::getAlias($cssName);
        $stylesheet = file_get_contents($cssFile);
        $mpdf->SetImportUse();
        $mpdf->SetTitle('KEW-PS 8 (LABEL FIFO)');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai||');
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->WriteHTML($body, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 8 (LABEL FIFO)_'.date('Y-m-d').'.pdf','I');
        exit;
    }

    public static function PdfKewps9($id = 2) {
        // $id = [142,143,144,145,146,147,148,149,150,151,152,153,154,651];
        // $month = 11;
        // $year = [2016,2017];
        $pageSize = 10;
        // $page = 1;

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \frontend\modules\store\models\Inventories::find()
            ->andFilterWhere(['in', 'ID', $id])
            ->andFilterWhere(['DELETED'=> 0])
            // ->andFilterWhere(['in', 'extract(year from APPROVED_AT)', $year])
            ->orderBy(['ID'=>SORT_ASC])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            // $dataProvider->pagination->page = $page;
        }


        $items = $dataProvider->models;
        // var_dump($items);die();
        $arraylist = array_chunk($items, 5);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps9_1');
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps9_2', [
                'items' => $list,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps9_3');

        $mpdf = new \mPDF('c', 'A4-L','','',13,13,10,6,5,5);
        $mpdf->SetTitle('KEW-PS 9 (Senarai Stok Bertarikh Luput)');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 11px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 9 (Senarai Stok Bertarikh Luput)_'.date('Y-m-d').'.pdf','I');
        exit;
    }

    public static function PdfKewps11($id = 86) {
        // $id = 983;
        // $id = 348;
        $month = 11;
        $pageSize = 10;
        // $page = 1;

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \frontend\modules\store\models\OrderItems::find()
            ->with('inventory', 'order.approvedBy.mpspProfile')
            ->andWhere(['ORDER_ID' => $id])
            // ->andFilterWhere(['in', 'ID', $id])
            ->andFilterWhere(['DELETED'=> 0])
            // ->andFilterWhere(['in', 'extract(year from APPROVED_AT)', $year])
            ->orderBy(['ORDER_ID'=>SORT_ASC])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            // $dataProvider->pagination->page = $page;
        }


        $items = $dataProvider->models;
        // var_dump($items);die();
        $arraylist = array_chunk($items, 5);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps11_1',[
            'items' => $items,
            ]);
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps11_2', [
                'items' => $list,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps11_3',[
            'items' => $items,
            ]);

        $mpdf = new \mPDF('c', 'A4-P','','',15,15,16,16,9,9);
        $mpdf->SetTitle('KEW-PS 11 (Borang Pemesanan Stok)');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.5 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 11px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 11 (Borang Pemesanan Stok)_'.date('Y-m-d').'.pdf','I');
        exit;
    }

    public static function PdfKewps13($year = 2016) {
        // $year = 2016;
        $pageSize = 10;
        // $page = 1;

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \frontend\modules\store\models\ReportAll::find()
            // ->with('inventory', 'order.approvedBy.mpspProfile')
            // ->andWhere(['YEAR' => $year])
            ->andFilterWhere(['in', 'YEAR', $year])
            // ->andFilterWhere(['in', 'extract(year from APPROVED_AT)', $year])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            // $dataProvider->pagination->page = $page;
        }


        $items = $dataProvider->models;
        $items = \yii\helpers\ArrayHelper::index($items, 'QUARTER');
        // var_dump($items);die();
        setlocale(LC_TIME, 'ms');
        $content = Yii::$app->controller->renderPartial('_kewps13', [
            'year' => $year,
            'items' => $items,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
            'marginLeft' => 13,
            'marginRight' => 13,
            'marginTop' => 10,
            'marginBottom' => 12,
            'marginHeader' => 5,
            'marginFooter' => 5,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran C'],
            'SetFooter' => ['Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S")],
            ]
            ]);
        // $mpdf = new \mPDF('utf-8', 'A4-L');
        // $cssName = '@frontend/assets/dist/css/Pdf.css';
        // $cssFile = Yii::getAlias($cssName);
        // $stylesheet = file_get_contents($cssFile);
        // $mpdf->WriteHTML($stylesheet, 1);
        // $mpdf->WriteHTML($content, 2);

        $pdf->render();
        exit;
    }

    public static function PdfKewps14($id = 2,$year) {
//             'marginLeft' => 15,
//             'marginRight' => 15,
//             'marginTop' => 16,
//             'marginBottom' => 16,
//             'marginHeader' => 9,
//             'marginFooter' => 9,
//             'cssFile' => '@frontend/assets/dist/css/Pdf.css',
//             'format' => 'A4-L',
//             'filename' => 'KEW PS 10.pdf',
//             'options' => [
//             'title' => 'Borang Pemesanan Dan Pengeluaran',
// //                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
//             ],
//             'methods' => [
//             'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
//             'SetFooter' => ['Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S")],

//         // $pdf->render();
//         // exit;
        // var_dump($id);die();
        $pageSize = 10;
        $page = 1;

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \frontend\modules\store\models\Inventories::find()
            // ->andWhere(['CATEGORY_ID'=>$categoryId])
            ->andFilterWhere(['in','ID', $id])
            ->andFilterWhere(['DELETED'=> 0])
            // ->andFilterWhere(['extract(year from APPROVED_AT)'=> $year])
            ->orderBy(['ID'=>SORT_ASC])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            $dataProvider->pagination->page = $page;
        }


        $items = $dataProvider->models;
        // var_dump($items);die();
        $arraylist = array_chunk($items, 35);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps14_1',['year'=>$year]);
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps14_2', [
                'items' => $list,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps14_3');

        // mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right ,  $margin_top , $margin_bottom , $margin_header ,  $margin_footer , $orientation)
        $mpdf = new \mPDF('c', 'A4-L','','',13,13,10,6,5,5);
        $mpdf->SetTitle('KEW-PS 14 (Laporan Pemeriksaan / Verifikasi Stok) Tahun '.$year);
        // $mpdf->default_font_size = 1;
        // $mpdf->default_font = 1;
        // $mpdf->margin_left = 15;
        // $mpdf->margin_right = 15;
        // $mpdf->margin_top = 16;
        // $mpdf->margin_bottom = 16;
        // $mpdf->margin_header = 9;
        // $mpdf->margin_footer = 9;
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.6 Lampiran A');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 11px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 14 (Laporan Pemeriksaan / Verifikasi Stok) Tahun '.$year.'_'.date('Y-m-d').'.pdf','I');
        exit;
    }

    public static function PdfKewps17($id = 2) {
        // $id = [142,143,144,145,146,147,148,149,150,151,152,153,154,651];
        // $month = 11;
        // $year = [2016,2017];
        $pageSize = 10;
        // $page = 1;

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \frontend\modules\store\models\Inventories::find()
            ->andFilterWhere(['in', 'ID', $id])
            ->andFilterWhere(['DELETED'=> 0])
            // ->andFilterWhere(['in', 'extract(year from APPROVED_AT)', $year])
            ->orderBy(['ID'=>SORT_ASC])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            // $dataProvider->pagination->page = $page;
        }


        $items = $dataProvider->models;
        // var_dump($items);die();
        $arraylist = array_chunk($items, 5);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps17_1');
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps17_2', [
                'items' => $list,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps17_3');

        $mpdf = new \mPDF('c', 'A4-L','','',13,13,10,6,5,5);
        $mpdf->SetTitle('KEW-PS 17 (Penyata Pelarasan Stok)');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.5 Lampiran D');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 11px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 17 (Penyata Pelarasan Stok)_'.date('Y-m-d').'.pdf','I');
        exit;

    }

    public static function PdfKewps18($id = 2) {


        // $id = [142,143,144,145,146,147,148,149,150,151,152,153,154,651];
        // $month = 11;
        // $year = [2016,2017];
        $pageSize = 10;
        // $page = 1;

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \frontend\modules\store\models\Inventories::find()
            ->andFilterWhere(['in', 'ID', $id])
            ->andFilterWhere(['DELETED'=> 0])
            // ->andFilterWhere(['in', 'extract(year from APPROVED_AT)', $year])
            ->orderBy(['ID'=>SORT_ASC])
            ->asArray()->all(),
            'pagination' => [
            'pageSize' => false,
            // 'page' => 1,
            ],
            ]);
        if(isset($pageSize)){
            $dataProvider->pagination->defaultPageSize = $pageSize;
            // $dataProvider->pagination->page = $page;
        }


        $items = $dataProvider->models;
        // var_dump($items);die();
        $batchSize = 6;
        $arraylist = array_chunk($items, $batchSize);
        $contentHead = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps18_1');
        $contentBody = [];
        foreach ($arraylist as $listIndex => $list) {
            $contentBody[] = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps18_2', [
                'items' => $list,
                'batchSize' => $batchSize,
                ]);
        }
        $contentFoot = Yii::$app->controller->renderPartial('@frontend/views/official-form/_kewps18_3');

        $mpdf = new \mPDF('c', 'A4-L','','',13,13,10,6,5,5);
        $mpdf->SetTitle('KEW-PS 18 (perakuan Ambil Alih)');
        $mpdf->SetHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.6 Lampiran E');
        setlocale(LC_TIME, 'ms');
        $mpdf->SetFooter('Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S"));
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $overideCss = '
        .table thead tr th,.table thead tr td,.table tbody tr th,.table tbody tr td,.table tfoot tr th,.table tfoot tr td {
            font-size: 11px;
        }
        ';
        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($overideCss, 1);
        foreach ($contentBody as $i => $body) {
            $mpdf->WriteHTML($contentHead, 2);
            $mpdf->WriteHTML($body, 2);
            $mpdf->WriteHTML($contentFoot, 2);
            if ($i != sizeof($contentBody) - 1) {
                $mpdf->AddPage();
            }
        }
        $mpdf->Output('KEW-PS 18 (perakuan Ambil Alih)_'.date('Y-m-d').'.pdf','I');
        exit;


        $model = Orders::findOne($id);
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
        $items = $dataProvider->models;
        $content = Yii::$app->controller->renderPartial('_kewps18', [
            'items' => $items,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
//            'marginLeft' => 13,
//            'marginRight' => 13,
//            'marginTop' => 10,
//            'marginBottom' => 6,
//            'marginHeader' => 5,
//            'marginFooter' => 5,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'marginHeader' => 9,
            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
            'SetFooter' => ['Copyright © Majlis Perbandaran Seberang Perai|{PAGENO}|Dicetak pada ' . strftime("%A, %d %B %Y %H:%M:%S")],
            ]
            ]);
        $mpdf = new \mPDF('utf-8', 'A4-L');
        $cssName = '@frontend/assets/dist/css/Pdf.css';
        $cssFile = Yii::getAlias($cssName);
        $stylesheet = file_get_contents($cssFile);
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($content, 2);

        $pdf->render();
        exit;
    }



}
