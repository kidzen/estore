<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Checkout as BaseCheckout;

/**
 * This is the model class for table "checkout".
 */
class Checkout extends BaseCheckout
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['order_id', 'order_item_id', 'ordered_by', 'transaction_id', 'rq_quantity', 'current_balance', 'created_by', 'approved_by'], 'integer'],
            [['app_quantity', 'unit_price', 'average_unit_price', 'batch_total_price'], 'number'],
            [['order_required_date', 'order_date'], 'string', 'max' => 7],
            [['order_no', 'card_no', 'code_no', 'description'], 'string', 'max' => 255],
            [['created_date', 'approved_date'], 'string', 'max' => 8]
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItem()
    {
        return $this->hasOne(\frontend\modules\store\models\OrderItem::className(), ['id' => 'order_item_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(\frontend\modules\store\models\Order::className(), ['id' => 'order_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'ordered_by']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(\frontend\modules\store\models\Transaction::className(), ['id' => 'transaction_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventory()
    {
        return $this->hasOne(\frontend\modules\store\models\Inventory::className(), ['id' => 'inventory_id'])->via('orderItem');
    }

}
