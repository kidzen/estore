<?php

namespace frontend\modules\store\models;

use Yii;
use \frontend\modules\store\models\base\Budget as BaseBudget;

/**
 * This is the model class for table "budget".
 */
class Budget extends BaseBudget
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['revenue'], 'number'],
            [['status', 'deleted_by', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ]);
    }

}
