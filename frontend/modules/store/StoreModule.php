<?php

namespace frontend\modules\store;

/**
 * store module definition class
 */
class StoreModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\store\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->defaultRoute = 'site';
        $this->layout = 'main';
        // $this->layout = 'main2';
        // var_dump($this->layout);die;
        parent::init();

        // custom initialization code goes here
    }
}
