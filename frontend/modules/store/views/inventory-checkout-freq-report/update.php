<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\InventoryCheckoutFreqReport */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Inventory Checkout Freq Report',
]) . ' ' . $model->inventory_id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Checkout Freq Report'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->inventory_id, 'url' => ['view', ]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="inventory-checkout-freq-report-update">
	<div class="panel panel-primary">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
