<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\search\InventoryCheckoutFreqReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inventory-checkout-freq-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'inventory_id')->textInput(['placeholder' => 'Inventory']) ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true, 'placeholder' => 'Year']) ?>

    <?= $form->field($model, 'month')->textInput(['maxlength' => true, 'placeholder' => 'Month']) ?>

    <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'placeholder' => 'Date']) ?>

    <?= $form->field($model, 'by_inventory')->textInput(['maxlength' => true, 'placeholder' => 'By Inventory']) ?>

    <?php /* echo $form->field($model, 'by_year')->textInput(['maxlength' => true, 'placeholder' => 'By Year']) */ ?>

    <?php /* echo $form->field($model, 'by_month')->textInput(['maxlength' => true, 'placeholder' => 'By Month']) */ ?>

    <?php /* echo $form->field($model, 'by_date')->textInput(['maxlength' => true, 'placeholder' => 'By Date']) */ ?>

    <?php /* echo $form->field($model, 'rank_inventory')->textInput(['maxlength' => true, 'placeholder' => 'Rank Inventory']) */ ?>

    <?php /* echo $form->field($model, 'rank_year')->textInput(['maxlength' => true, 'placeholder' => 'Rank Year']) */ ?>

    <?php /* echo $form->field($model, 'rank_month')->textInput(['maxlength' => true, 'placeholder' => 'Rank Month']) */ ?>

    <?php /* echo $form->field($model, 'rank_date')->textInput(['maxlength' => true, 'placeholder' => 'Rank Date']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
