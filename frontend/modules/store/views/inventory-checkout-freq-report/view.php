<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\InventoryCheckoutFreqReport */

$this->title = $model->inventory_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Checkout Freq Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-checkout-freq-report-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Inventory Checkout Freq Report').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">

            <?= Html::a(Yii::t('app', 'Update'), ['update', ], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', ], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php
    $gridColumn = [
        'inventory_id',
        'year',
        'month',
        'date',
        'by_inventory',
        'by_year',
        'by_month',
        'by_date',
        'rank_inventory',
        'rank_year',
        'rank_month',
        'rank_date',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    </div>
    </div>
    </div>
    </div>
</div>
