<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\AllMaintenanceCheck */

$this->title = Yii::t('app', 'Create All Maintenance Check');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'All Maintenance Check'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="all-maintenance-check-create">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
