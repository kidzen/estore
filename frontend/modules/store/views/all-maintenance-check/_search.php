<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\search\AllMaintenanceCheckSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-all-maintenance-check-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true, 'placeholder' => 'Type']) ?>

    <?= $form->field($model, 'table_name')->textInput(['maxlength' => true, 'placeholder' => 'Table Name']) ?>

    <?= $form->field($model, 'col_name')->textInput(['maxlength' => true, 'placeholder' => 'Col Name']) ?>

    <?= $form->field($model, 'data_id')->textInput(['placeholder' => 'Data']) ?>

    <?php /* echo $form->field($model, 'fault_value')->textInput(['maxlength' => true, 'placeholder' => 'Fault Value']) */ ?>

    <?php /* echo $form->field($model, 'true_value')->textInput(['maxlength' => true, 'placeholder' => 'True Value']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
