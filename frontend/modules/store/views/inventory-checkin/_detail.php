<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\InventoryCheckin */

?>
<div class="inventory-checkin-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'transaction.id',
            'label' => Yii::t('app', 'Transaction'),
        ],
        [
            'attribute' => 'budget.name',
            'label' => Yii::t('app', 'Budget'),
        ],
        [
            'attribute' => 'store.name',
            'label' => Yii::t('app', 'Store'),
        ],
        [
            'attribute' => 'inventory.description',
            'label' => Yii::t('app', 'Inventory'),
        ],
        [
            'attribute' => 'vendor.name',
            'label' => Yii::t('app', 'Vendor'),
        ],
        'items_quantity',
        'items_total_price',
        'check_date',
        [
                'attribute' => 'approved',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['approved']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
