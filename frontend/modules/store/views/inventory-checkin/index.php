<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\store\models\search\InventoryCheckinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\helpers\Html;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Inventory Checkin');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="inventory-checkin-index">

    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true,
            'visible' => true,
        ],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'transaction_id',
            'visible' => false,
            'label' => Yii::t('app', 'Transaction'),
            'value' => function($model){
                if ($model->transaction)
                    {return $model->transaction->id;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Transaction::find()->asArray()->all(), 'id', 'id'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Transaction', 'id' => 'grid-inventory-checkin-search-transaction_id']
        ],
        [
            'attribute' => 'budget_id',
            'visible' => false,
            'label' => Yii::t('app', 'Budget'),
            'value' => function($model){
                if ($model->budget)
                    {return $model->budget->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Budget::find()->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Budget', 'id' => 'grid-inventory-checkin-search-budget_id']
        ],
        [
            'attribute' => 'store_id',
            'visible' => sizeof(Yii::$app->user->storeList) > 1,
            'label' => Yii::t('app', 'Store'),
            'value' => function($model){
                if ($model->store)
                    {return $model->store->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Store::find()->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Store', 'id' => 'grid-inventory-checkin-search-store_id']
        ],
        [
            'attribute' => 'inventory_id',
            'label' => Yii::t('app', 'Inventory'),
            'value' => function($model){
                if ($model->inventory)
                    {return $model->inventory->description;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Inventory::find()->asArray()->all(), 'id', 'description'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Inventory', 'id' => 'grid-inventory-checkin-search-inventory_id']
        ],
        [
            'attribute' => 'vendor_id',
            'label' => Yii::t('app', 'Vendor'),
            'value' => function($model){
                if ($model->vendor)
                    {return $model->vendor->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Vendor::find()->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Vendor', 'id' => 'grid-inventory-checkin-search-vendor_id']
        ],
        'items_quantity',
        'items_total_price',
        'check_date',
        [
            'attribute' => 'approved',
            'visible' => false,
            'format' => 'raw',
            'value' => function($model) {
                switch ($model['approved']) {
                    case 1:
                    return \kartik\helpers\Html::bsLabel('Active','success');
                    break;

                    default:
                    return \kartik\helpers\Html::bsLabel('Inactive','danger');
                    break;
                }
            }
        ],
        [
            'attribute' => 'status',
            'visible' => false,
            'format' => 'raw',
            'value' => function($model) {
                switch ($model['status']) {
                    case 1:
                    return \kartik\helpers\Html::bsLabel('Active','success');
                    break;

                    default:
                    return \kartik\helpers\Html::bsLabel('Inactive','danger');
                    break;
                }
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {delete-permanent}',
            'buttons' => [
                'delete-permanent' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-trash" style="color:red"></span>', $url, ['title' => 'Delete Permanent']);
                },
            ],
            'visibleButtons' => [
                'delete-permanent' => \Yii::$app->user->can('admin'),
            ]
        ],
    ];
    ?>
    <?= DynaGrid::widget([
        'columns'=>$gridColumn,
        'storage'=>DynaGrid::TYPE_COOKIE,
        'theme'=>'panel-danger',
        'showPersonalize'=>true,
        'gridOptions'=>[
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            // 'showPageSummary'=>true,
            //'floatHeader'=>true,
            //'responsiveWrap'=>false,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inventory-checkin']],
            'panel' => [
                'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
                'before' =>  '<div style="padding-top: 7px;"><em>* The table header sticks to the top in this demo as you scroll</em></div>',
                'after' => false,
            ],
            // 'export' => false,
            // your toolbar can include the additional full export menu
            'toolbar' => [
                ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title'=>Yii::t('app', 'Create Inventory Checkin')]) . ' '.
                Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button'])
            ],
            ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
            '{export}',
            '{toggleData}',
            'exportConfig' => [
                    // ExportMenu::FORMAT_PDF => false
            ]
        ],
    ],
        'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
        ]); ?>

    </div>
