<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\search\ListYearSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-list-year-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true, 'placeholder' => 'Year']) ?>

    <?= $form->field($model, 'quarter')->textInput(['maxlength' => true, 'placeholder' => 'Quarter']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
