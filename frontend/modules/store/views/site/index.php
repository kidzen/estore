<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\widgets\Growl;


$actions = [
    'kewps10' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span> '.Yii::t('yii', 'KEW-PS-10'), ['/request/print-kewps10','id'=>$model['order']['id']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip','data-pjax'=>0,'target'=>'_blank']),['class'=>'text-center']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'kew2' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-file" style="color:green;"> </span> '.Yii::t('yii', 'KEW-PS-10'), $url, ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'view' => function ($url, $model) {
        if ($model['order']['approved'] === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-eye-open"> </span> '.Yii::t('yii', 'View KEW-PS-10'), ['/request/view', 'id' => $model['order']['id']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
        }
    },
    'smart-approve' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-check" style="color:maroon;"> </span> '.Yii::t('yii', 'FIFO Approval'), ['/request/smart-approve', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'FIFO Approval'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'approval' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-check" style="color:green;"> </span> '.Yii::t('yii', 'Approval'), ['/request/approval', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'Approval'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'reject' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model['order']['approved'] === 2) {
//                                    return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->order->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"> </span> '.Yii::t('yii', 'Reject'), ['/request/reject', 'id' => $model->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'item-list' => function ($url, $model) {
        return Html::tag('div',Html::a('<span class="glyphicon glyphicon-list"> </span> '.Yii::t('yii', 'Item List'), ['/request/item-list', 'orderId'=>$model->order->id], ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
    },
//                                    'item-list' => function ($url, $model) {
//                                return Html::a('<span class="glyphicon glyphicon-list"></span>', ['item-list', 'orderId' => $model->order->id], ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
//                            },
    'update' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model->order->approved === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-pencil"> </span> '.Yii::t('yii', 'Update'), ['/request/update', 'orderId'=>$model->order->id], ['title' => Yii::t('yii', 'Update'), 'data-toggle' => 'tooltip']),['class'=>'text-center']);;
        }
    },
    'recover' => function ($url, $model) {
        if ($model->deleted_by === 1) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-refresh"> </span> '.Yii::t('yii', 'Recover'), ['/request/recover', 'orderId'=>$model->order->id], ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip', 'data-method' => 'POST']),['class'=>'text-center']);;
        }
    },
    'delete' => function ($url, $model) {
        if ($model->deleted_by === 0 && $model->order->approved === 2) {
            return Html::tag('div',Html::a('<span class="glyphicon glyphicon-trash"> </span> '.Yii::t('yii', 'Delete'), ['/request/delete', 'orderId'=>$model->order->id], ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                'data-method' => 'post']),['class'=>'text-center']);;
        }
    },
];
?>
<div class="site-index">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <!--        <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua-gradient">
                            <div class="inner">
                                <h3><?= $quantityStockIn ?></h3>

                                <p>Kemasukan Stok Baru Bulan Ini</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green-gradient">
                            <div class="inner">
                                <h3><?= $quantityStockOut ?><!--<sup style="font-size: 20px">%</sup>--></h3>
                                <p>Pengeluaran Stok Bulan Ini</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow-gradient">
                            <div class="inner">
                                <h3><?= $quantityInventoriesCardRegistered ?></h3>

                                <p>Pendaftaran Kad Inventori Bulan Ini</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red-gradient">
                            <div class="inner">
                                <h3><?= $quantityStockRequest ?></h3>
                                <p>Pengesahan Pengeluaran Stok Bulan Ini</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>

                <!-- /.row -->



                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <div class="col-md-8">
                        <!-- All Left item must be content below -->
                        <!-- /.row -->
                        <!-- TABLE: LATEST ORDERS -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Permohonan Terbaru</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div>
                                    <?php if (isset($latestOrder)) { ?>
                                    <?=
                                    \kartik\grid\GridView::widget([
                                        'dataProvider' => $latestOrder,
                                        'responsiveWrap' => false,
                                        'tableOptions' => ['class' => 'table no-margin'],
                                        'layout' => '{items}',
//                                'summary'=>'',
//                                'summary' => "{begin} - {end} {count} {totalCount} {page} {pageCount}",
                                        'columns' => [
//                                        ['class' => 'yii\grid\SerialColumn'],
                                            [
                                                'label' => 'No Order',
                                                'attribute' => 'order.order_no',
                                            ],
                                        // [
                                        //     'label' => 'Kegunaan',
                                        //     'attribute' => 'usage.vehicle.reg_no',
                                        // ],
                                            [
                                                'label' => 'Tarikh Permohonan',
                                                'attribute' => 'order.required_date',
                                            ],
                                            [
                                                'label' => 'Pemohon',
                                                'attribute' => 'order.ordered_by',
                                            ],
                                            [
                                                'label' => 'Status',
                                                'attribute' => 'order.approved',
                                                'format' => 'raw',
                                                'value' => function ($model) {
                                                    if ($model->order->approved == '1') {
                                                        return Html::tag('span', 'Approved', ['class' => 'label label-success']);
                                                    } else if ($model->order->approved == '2') {
                                                        return Html::tag('span', 'Pending', ['class' => 'label label-warning']);
                                                    } else if ($model->order->approved == '8') {
                                                        return Html::tag('span', 'Rejected', ['class' => 'label label-danger']);
                                                    } else {
                                                        return Html::tag('span', 'Undefined', ['class' => 'label label-default']);
                                                    }
                                                }
                                            ],
                                            [
                                                'label' => 'Tarikh Pengesahan',
                                                'attribute' => 'order.approved_at',
                                            ],
                                        ]
                                    ]);
                                    ?>

                                    <?php } ?>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.box-body -->
                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                        <!-- /.row -->

                        <!-- TABLE: LATEST ORDERS -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Menungggu Pengesahan</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div>
                                    <?php if (isset($pendingApproval)) { ?>
                                    <?=
                                    \kartik\grid\GridView::widget([
                                        'dataProvider' => $pendingApproval,
                                        'responsiveWrap' => false,
                                        'tableOptions' => ['class' => 'table no-margin'],
                                        'layout' => '{items}',
//                                'summary'=>'',
//                                'summary' => "{begin} - {end} {count} {totalCount} {page} {pageCount}",
                                        'columns' => [
//                                        ['class' => 'yii\grid\SerialColumn'],
                                            [
                                                'label' => 'No Order',
                                                'attribute' => 'order.order_no',
                                            ],
                                            [
                                                'label' => 'Kegunaan',
                                                'attribute' => 'usage.vehicle.reg_no',
                                            ],
                                            [
                                                'label' => 'Tarikh Permohonan',
                                                'attribute' => 'order.required_date',
                                            ],
                                            [
                                                'label' => 'Pemohon',
                                                'attribute' => 'order.ordered_by',
                                            ],
                                            [
                                                'label' => 'Kuantiti Permohonan',
                                                'attribute' => 'quantity',
                                                'format' => 'raw',
                                                'value' => function ($model) {
                                                    if ($model->app_quantity) {
                                                        return Html::tag('span', 'Requested : ' . $model->rq_quantity . '<br>' . 'Approved : ' . $model->app_quantity);
                                                    } else {
                                                        return Html::tag('span', 'Requested : ' . $model->rq_quantity . '<br>' . 'Approved : ' . 0);
                                                    }
                                                },
                                                'hAlign' => 'center', 'vAlign' => 'middle',
                                            ],
                                            [
                                                'label' => 'Status',
                                                'attribute' => 'order.approved',
                                                'format' => 'raw',
                                                'value' => function ($model) {
                                                    if ($model->order->approved == '1') {
                                                        return Html::tag('span', 'Approved', ['class' => 'label label-success']);
                                                    } else if ($model->order->approved == '2') {
                                                        return Html::tag('span', 'Pending', ['class' => 'label label-warning']);
                                                    } else if ($model->order->approved == '8') {
                                                        return Html::tag('span', 'Rejected', ['class' => 'label label-danger']);
                                                    } else {
                                                        return Html::tag('span', 'Undefined', ['class' => 'label label-default']);
                                                    }
                                                }
                                            ],
                                            [
                                                'label' => 'Tarikh Pengesahan',
                                                'attribute' => 'order.approved_at',
                                            ],
                                            [
                                                'class' => 'kartik\grid\ActionColumn',
                                            // 'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('role_officer_store'),
                                        //                        'group' => true,
                                        //                        'subGroupOf' => 2,
                                                'template' => '{kewps10} {smart-approve} {approval} {reject} {item-list} {view} {update} {delete} {recover}',
                                                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                                                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
                                        //                        'group' => true, 'subGroupOf' => 2,
                                                'dropdown' => true,
                                                'buttons' => $actions
                                            ],
                                        ]
                                    ]);
                                    ?>

                                    <?php } ?>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <?= Html::a('Place New Order', ['request/create'], ['class' => 'btn btn-sm btn-info btn-flat pull-left']) ?>
                                <?= Html::a('View All Orders', ['request/index'], ['class' => 'btn btn-sm btn-default btn-flat pull-right']) ?>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <!-- All Left item must be content above -->

                    </div>
                    <!-- /.col -->

                    <div class="col-md-4">
                        <!-- All right item must be content below -->

                        <!-- PRODUCT LIST -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Kemasukan Stok Baru</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <ul class="products-list product-list-in-box">
                                    <!-- /.item -->
                                    <?php if (isset($latestStockIn) && !empty($StocksInItems->inventoryItems)) : ?>
                                        <?php foreach ($latestStockIn->models as $StocksInItems) : ?>
                                            <li class="item">
                                                <div class="product-info">
                                                    <?= Html::a($StocksInItems->inventory->category->name . Html::tag('span', 'RM ' . $StocksInItems->inventoryItems[0]->unit_price, ['class' => 'label label-info pull-right']), ['inventories/view','id'=>$StocksInItems->inventory->id], ['class' => 'product-title']) ?>
                                                    <span class="product-description">
                                                        <?= $StocksInItems->inventory->description ?>
                                                    </span>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                    <!-- /.item -->
                                </ul>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-center">
                                <?= Html::a('View All Products', ['inventories/index']) ?>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                        <!-- All right item must be content below -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>

            <!-- Main content -->


            <!-- Content Header (Page header) -->


    <!--    <div class="jumbotron">
            <br>
            <h1><b>MPSP </b> e-Store </h1>
            <h1 style="color: #cc99ff;
                -webkit-text-fill-color: #cc99ff; /* Will override color (regardless of order) */
                -webkit-text-stroke-width: 2px;
                -webkit-text-stroke-color: #000099;">
                <strong>MPSP e-Store</strong>
                <strong>SISTEM PENGURUSAN KONTRAKTORISASI AGSE / AGSV</strong>
            </h1>

            <h1 style="color: black;
                letter-spacing: -3px;
                -webkit-text-fill-color: white; /* Will override color (regardless of order) */
                -webkit-text-stroke-width: 2px;
                -webkit-text-stroke-color: blue;">
                <strong>Pangkalan Udara Gong Kedak</strong></h1>
                <strong>MAJLIS PERBANDARAN SEBERANG PERAI</strong></h1>
            <p class="lead">MAJLIS PERBANDARAN SEBERANG PERAI</p>
            <p><a class="btn btn-lg btn-success" href="site/index">Pengenalan Sistem</a></p>
        </div>-->



    <!--    <div class="body-content">

            <div class="row">
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
                </div>
            </div>

        </div>-->
    </div>
