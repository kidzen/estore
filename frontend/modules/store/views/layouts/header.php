<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
$data = [
    [
        'icon' => 'users',
        'url' => '#',
        'colorCode' => 'aqua',
        'content' => '1 new user registered',
    ],
    [
        'icon' => 'warning',
        'url' => '#',
        'colorCode' => 'yellow',
        'content' => '1 unapproved checkin',
    ],
    [
        'icon' => 'warning',
        'url' => '#',
        'colorCode' => 'yellow',
        'content' => '1 unapproved transfer',
    ],
];
$notificationCount = sizeof($data);
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">'.Yii::$app->params['companyNameShort'].'</span><span class="logo-lg">'.Yii::$app->params['companyNameShort'].'</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <?php if (isset(Yii::$app->user->id)) { ?>
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning"><?= $notificationCount ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have <?= $notificationCount ?> notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <?php if($notificationCount > 0): ?>
                                    <ul class="menu">
                                        <?php foreach ($data as $key => $value) : ?>
                                            <li>
                                                <?= Html::a('<i class="fa fa-'.$value['icon'].' text-'.$value['colorCode'].'"></i>'.$value['content'], $value['url']) ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                            <li class="footer"><?= Html::a('View all', '#') ?></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs">e-Store</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <?= Html::a('e-Asset',['/'])  ?>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= \Yii::$app->user->avatar ?>" class="user-image" alt="User Image"/>
                            <span class="hidden-xs"><?= Yii::$app->user->profile->name ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?= \Yii::$app->user->avatar ?>" class="img-circle"
                                alt="User Image"/>

                                <p>
                                    <?= Yii::$app->user->profile->name ?>
                                    <small><?= Yii::$app->user->profile->position ?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?=
                                    Html::a(
                                        'Profil', ['user/view', 'id' => Yii::$app->user->id], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                    ?>
                                </div>

                                <div class="pull-right">
                                    <?=
                                    Html::a(
                                        'Log Keluar', ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                    ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <?php } ?>

                    <!-- User Account: style can be found in dropdown.less -->
<!--                 <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            -->            </ul>
        </div>
    </nav>
</header>
