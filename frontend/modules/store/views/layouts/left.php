<?php
$dbName = \common\components\Migration::getDbName(Yii::$app->dbStore);
switch (Yii::$app->dbStore->driverName) {
    case 'mysql':
    $script = "SELECT table_name as TABLE_NAME
    FROM information_schema.tables
    WHERE table_schema = '$dbName'";
    break;
    case 'oci':
    $script = "
    SELECT
    TABLE_NAME
    FROM USER_TABLES
    UNION ALL
    SELECT
    VIEW_NAME AS TABLE_NAME
    FROM USER_VIEWS
    UNION ALL
    SELECT
    MVIEW_NAME AS TABLE_NAME
    FROM USER_MVIEWS
    ORDER BY TABLE_NAME
    ";

    break;
    default:
    $script = false;
    break;
}
if($script){
    $query = Yii::$app->dbStore->createCommand($script)->queryAll();
    foreach ($query as $key => $value) {
        $url = '/'.strtr($value['TABLE_NAME'], '_', '-');
        $label = strtr($value['TABLE_NAME'], '_', ' ');
        $items[] = ['label' => $label, 'url' => [$url]];
    }
}
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div style="text-align: center">
                <!-- width 200px -->
                <img src="<?= Yii::$app->params['leftMenuImg'] ?>" style="width: 90%" alt="logo-sb-pdrm"/>
            </div>
        </div>

        <!-- search form -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
    -->        <!-- /.search form -->
    <?= dmstr\widgets\Menu::widget(
        [
            'options' => ['class' => 'sidebar-menu tree delay', 'data-widget'=> 'tree'],
            'items' => [
                ['label' => Yii::t('app','Request'), 'options' => ['class' => 'header']],
                [
                    'label' => Yii::t('app','Login'), 'url' => ['/site/login'],
                    'visible' => Yii::$app->user->isGuest
                ],
                [
                    'label' => Yii::t('app','Stock In'),
                    'items' => [
                        ['label' => Yii::t('app','Stock Registration'), 'icon' => 'file-code-o', 'url' => ['/store/inventory-checkin/create'], 'visible' => Yii::$app->user->can('manage-inventory-checkin')],
                        ['label' => Yii::t('app','Card Registration'), 'icon' => 'file-code-o', 'url' => ['/store/inventory/create'], 'visible' => Yii::$app->user->can('manage-inventory-checkin')],
                        ['label' => Yii::t('app','Transaction In'), 'icon' => 'file-code-o', 'url' => ['/store/inventory-checkin/index'], 'visible' => Yii::$app->user->can('manage-inventory-checkin')],
                            // ['label' => Yii::t('app','Transaction In'), 'icon' => 'file-code-o', 'url' => ['/store/transaction/checkin']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Stock Out'),
                    'items' => [
                        ['label' => Yii::t('app','Checkout'), 'icon' => 'file-code-o', 'url' => ['/store/request/checkout'], 'visible' => Yii::$app->user->can('manage-inventory-checkout')],
                        // ['label' => Yii::t('app','Checkout'), 'icon' => 'file-code-o', 'url' => ['/store/request/checkout'], 'visible' => empty(Yii::$app->user->storeList)],
                        // ['label' => Yii::t('app','Instruction Order'), 'icon' => 'file-code-o', 'url' => ['/store/request/instruction']],
                        ['label' => Yii::t('app','Transaction Out'), 'icon' => 'file-code-o', 'url' => ['/store/order/index'], 'visible' => Yii::$app->user->can('manage-inventory-checkout')],

                        // ['label' => Yii::t('app','Checkout'), 'icon' => 'file-code-o', 'url' => ['/store/request/checkout']],
                        // ['label' => Yii::t('app','Instruction Order'), 'icon' => 'file-code-o', 'url' => ['/store/request/instruction']],
                        // ['label' => Yii::t('app','Transaction Out'), 'icon' => 'file-code-o', 'url' => ['/store/request/checkout-list']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Disposal'),
                    'items' => [
                        ['label' => Yii::t('app','Dispose'), 'icon' => 'file-code-o', 'url' => ['/store/request/dispose'], 'visible' => Yii::$app->user->can('store_dispose_create')],
                        ['label' => Yii::t('app','Disposal List'), 'icon' => 'file-code-o', 'url' => ['/store/request/dispose-index'], 'visible' => Yii::$app->user->can('store_dispose_view')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Budget'),
                    'items' => [
                        ['label' => Yii::t('app','Budget Registration'), 'icon' => 'file-code-o', 'url' => ['/store/budget/create'], 'visible' => Yii::$app->user->can('store_budget_create')],
                        // ['label' => Yii::t('app','Budget Grant'), 'icon' => 'file-code-o', 'url' => ['/store/budget/grant'], 'visible' => Yii::$app->user->can('store_budget_view')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                ['label' => Yii::t('app','Admin Section'), 'options' => ['class' => 'header']],
                ['label' => Yii::t('app','My Profile'), 'url' => ['/store/profile/view', 'id' => Yii::$app->user->id]],
                [
                    'label' => Yii::t('app','Store Management'),
                    'items' => [
                        ['label' => Yii::t('app','Category'), 'icon' => 'file-code-o', 'url' => ['/store/inventory-category/index'], 'visible' => Yii::$app->user->can('manage-inventory-checkin')],
                        ['label' => Yii::t('app','Inventory Card'), 'icon' => 'file-code-o', 'url' => ['/store/inventory/index'], 'visible' => Yii::$app->user->can('manage-inventory-checkin')],
                        ['label' => Yii::t('app','Stock'), 'icon' => 'file-code-o', 'url' => ['/store/inventory-item/index'], 'visible' => Yii::$app->user->can('manage-inventory-checkin')],
                        ['label' => Yii::t('app','Store'), 'icon' => 'file-code-o', 'url' => ['/store/store/index'], 'visible' => Yii::$app->user->can('store_store_view')],
                    ],
                    'visible' => Yii::$app->user->can('manage-inventory-checkin'),
                ],
                [
                    'label' => Yii::t('app','List Management'),
                    'items' => [
                        ['label' => Yii::t('app','Vendor'), 'icon' => 'file-code-o', 'url' => ['/store/vendor/index'], 'visible' => Yii::$app->user->can('store_vendor_view')],
                        ['label' => Yii::t('app','Vehicle'), 'icon' => 'file-code-o', 'url' => ['/store/vehicle/index'], 'visible' => Yii::$app->user->can('store_vehicle_view')],
                        ['label' => Yii::t('app','Workshop'), 'icon' => 'file-code-o', 'url' => ['/store/workshop/index'], 'visible' => Yii::$app->user->can('store_workshop_view')],
                        ['label' => Yii::t('app','Usage Category'), 'icon' => 'file-code-o', 'url' => ['/store/usage-category/index'], 'visible' => Yii::$app->user->can('store_usage_view')],
                        ['label' => Yii::t('app','Usage List'), 'icon' => 'file-code-o', 'url' => ['/store/usage-list/index'], 'visible' => Yii::$app->user->can('store_usage_view')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','System Management'),
                    'items' => [
                        ['label' => Yii::t('app','MPSP Profile'), 'icon' => 'file-code-o', 'url' => ['/store/profile/index'], 'visible'=>Yii::$app->user->can('store_admin_user')],
                        ['label' => Yii::t('app','My Profile'), 'icon' => 'file-code-o', 'url' => ['/store/profile/view','id'=>Yii::$app->user->profile->id], 'visible' => Yii::$app->user->can('store_viewOwn_profile')],
                        ['label' => Yii::t('app','Activity Log'), 'icon' => 'file-code-o', 'url' => ['/store/activity-log/index'], 'visible' => Yii::$app->user->can('store_activity_log_view')],
                        ['label' => Yii::t('app','System Maintenance'), 'icon' => 'file-code-o', 'url' => ['/store/site/maintenance']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Access Management'), 'items' => [
                        ['label' => Yii::t('app','User'), 'icon' => 'fa', 'url' => ['/store/user/index'],'visible'=>Yii::$app->user->can('store_admin_user')],
                        ['label' => Yii::t('app','User Profile'), 'icon' => 'fa', 'url' => ['/store/profile/index'],'visible'=>Yii::$app->user->can('store_admin_user')],
                        ['label' => Yii::t('app','Assignment'), 'icon' => 'fa', 'url' => ['/store/admin/assignment'],'visible'=>Yii::$app->user->can('store_admin_user')],
                        ['label' => Yii::t('app','Permission'), 'icon' => 'fa', 'url' => ['/store/admin/permission'],'visible'=>Yii::$app->user->can('store_admin_user')],
                        ['label' => Yii::t('app','Route'), 'icon' => 'fa', 'url' => ['/store/admin/route'],'visible'=>Yii::$app->user->can('store_admin_user')],
                        ['label' => Yii::t('app','Role'), 'icon' => 'fa', 'url' => ['/store/admin/role'],'visible'=>Yii::$app->user->can('store_admin_user')],
                        ['label' => Yii::t('app','Rules'), 'icon' => 'fa', 'url' => ['/store/auth-rule/index'],'visible'=>Yii::$app->user->can('store_admin_user')],
                        // ['label' => Yii::t('app','Auth Link'), 'icon' => 'fa', 'url' => ['/store/auth-item/index'],'visible'=>Yii::$app->user->can('store_admin_user')],
                        ['label' => Yii::t('app','Store Assignment'), 'icon' => 'file-code-o', 'url' => ['/store/store-assignment/index'], 'visible' => Yii::$app->user->can('store_store_assignment_view')],
                    ]
                ],
                ['label' => Yii::t('app','Reporting'), 'options' => ['class' => 'header'],'visible' => Yii::$app->user->can('store_generate_report'),],
                [
                    'label' => Yii::t('app','Form'),
                    'visible' => Yii::$app->user->can('store_generate_report'),
                    'items' => [
                        ['label' => Yii::t('app','KEWPS Form'), 'icon' => 'file-code-o', 'url' => ['/store/official-form/form-generator'],
                        'visible' => Yii::$app->user->can('store_generate_report'),],
                        // ['label' => Yii::t('app','KEWPA Form'), 'icon' => 'file-code-o', 'url' => ['/store/request/checkin']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Report'),
                    'visible' => Yii::$app->user->can('store_generate_report'),
                    'items' => [
                        ['label' => Yii::t('app','All Report'), 'icon' => 'file-code-o', 'url' => ['/store/report/index']],
                        ['label' => Yii::t('app','KEWPS Report'), 'icon' => 'file-code-o', 'url' => ['/store/report/kewps']],
                        ['label' => Yii::t('app','Yearly Report'), 'icon' => 'file-code-o', 'url' => ['/store/report/yearly']],
                        ['label' => Yii::t('app','Supplier Report'), 'icon' => 'file-code-o', 'url' => ['/store/vendor-usage/index']],
                        ['label' => Yii::t('app','Vehicle Report'), 'icon' => 'file-code-o', 'url' => ['/store/vehicle-usage/index']],
                        ['label' => Yii::t('app','Budget Report'), 'icon' => 'file-code-o', 'url' => ['/store/budget-usage/index']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                ['label' => Yii::t('app','Module Developer'), 'options' => ['class' => 'header'],'visible' => false,],
                [
                    'label' => Yii::t('app','Developer'), 'icon' => 'file-code-o',
                    'visible' => false,
                    'items' => [
                        ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/store/gii']],
                        ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/store/debug']],
                        [
                            'label' => 'Under Development', 'icon' => 'file-code-o',
                            'items' => [
                                ['label' => Yii::t('app','Workshop Report'), 'icon' => 'file-code-o', 'url' => ['/store/workshop-usage/index']],
                                ['label' => Yii::t('app','Store Report'), 'icon' => 'file-code-o', 'url' => ['/store/store-usage/index']],
                            ]
                        ],
                        [
                            'label' => 'Database', 'icon' => 'database',
                            'items' => $items
                        ],
                    ],
                    // 'visible' => Yii::$app->user->can('store_admin'),
                ],
            ],
        ]
        ) ?>

    </section>

</aside>
