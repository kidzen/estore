<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\search\ApprovalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-approval-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'id_inventory_items')->textInput(['placeholder' => 'Id Inventory Items']) ?>

    <?= $form->field($model, 'id_inventory')->textInput(['placeholder' => 'Id Inventory']) ?>

    <?= $form->field($model, 'id_category')->textInput(['placeholder' => 'Id Category']) ?>

    <?= $form->field($model, 'id_inventory_checkin')->textInput(['placeholder' => 'Id Inventory Checkin']) ?>

    <?php /* echo $form->field($model, 'id_order_item')->textInput(['placeholder' => 'Id Order Item']) */ ?>

    <?php /* echo $form->field($model, 'id_order')->textInput(['placeholder' => 'Id Order']) */ ?>

    <?php /* echo $form->field($model, 'sku')->textInput(['maxlength' => true, 'placeholder' => 'Sku']) */ ?>

    <?php /* echo $form->field($model, 'unit_price')->textInput(['maxlength' => true, 'placeholder' => 'Unit Price']) */ ?>

    <?php /* echo $form->field($model, 'order_no')->textInput(['maxlength' => true, 'placeholder' => 'Order No']) */ ?>

    <?php /* echo $form->field($model, 'items_category_name')->textInput(['maxlength' => true, 'placeholder' => 'Item Category Name']) */ ?>

    <?php /* echo $form->field($model, 'item_inventory_name')->textInput(['maxlength' => true, 'placeholder' => 'Item Inventory Name']) */ ?>

    <?php /* echo $form->field($model, 'rq_quantity')->textInput(['placeholder' => 'Rq Quantity']) */ ?>

    <?php /* echo $form->field($model, 'app_quantity')->textInput(['placeholder' => 'App Quantity']) */ ?>

    <?php /* echo $form->field($model, 'approved')->textInput(['placeholder' => 'Approved']) */ ?>

    <?php /* echo $form->field($model, 'current_balance')->textInput(['placeholder' => 'Current Balance']) */ ?>

    <?php /* echo $form->field($model, 'total_price')->textInput(['placeholder' => 'Total Price']) */ ?>

    <?php /* echo $form->field($model, 'order_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => Yii::t('app', 'Choose Order Date'),
                        'autoclose' => true
                    ]
                ],
            ]);
 */ ?>

    <?php /* echo $form->field($model, 'required_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => Yii::t('app', 'Choose Required Date'),
                        'autoclose' => true
                    ]
                ],
            ]);
 */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
