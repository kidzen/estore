<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\Approval */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="approval-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'id_inventory_items')->textInput(['placeholder' => 'Id Inventory Items']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'id_inventory')->textInput(['placeholder' => 'Id Inventory']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'id_category')->textInput(['placeholder' => 'Id Category']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'id_inventory_checkin')->textInput(['placeholder' => 'Id Inventory Checkin']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'id_order_item')->textInput(['placeholder' => 'Id Order Item']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'id_order')->textInput(['placeholder' => 'Id Order']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'sku')->textInput(['maxlength' => true, 'placeholder' => 'Sku']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'unit_price')->textInput(['maxlength' => true, 'placeholder' => 'Unit Price']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'order_no')->textInput(['maxlength' => true, 'placeholder' => 'Order No']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'items_category_name')->textInput(['maxlength' => true, 'placeholder' => 'Item Category Name']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'item_inventory_name')->textInput(['maxlength' => true, 'placeholder' => 'Item Inventory Name']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'rq_quantity')->textInput(['placeholder' => 'Rq Quantity']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'app_quantity')->textInput(['placeholder' => 'App Quantity']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'approved')->textInput(['placeholder' => 'Approved']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'current_balance')->textInput(['placeholder' => 'Current Balance']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'total_price')->textInput(['placeholder' => 'Total Price']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'order_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => Yii::t('app', 'Choose Order Date'),
                        'autoclose' => true
                    ]
                ],
            ]);
 ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'required_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => Yii::t('app', 'Choose Required Date'),
                        'autoclose' => true
                    ]
                ],
            ]);
 ?>
</div>

    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
