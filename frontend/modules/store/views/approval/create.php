<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\Approval */

$this->title = Yii::t('app', 'Create Approval');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Approval'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="approval-create">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
