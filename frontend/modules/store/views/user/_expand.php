<?php
use kartik\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'User')),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
/*    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Activity Log')),
        'content' => $this->render('_dataActivityLog', [
            'model' => $model,
            'row' => $model->activityLogs,
        ]),
    ],*/
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Store Assignment')),
        'content' => $this->render('_dataStoreAssignment', [
            'model' => $model,
            'row' => $model->storeAssignments,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Transaction')),
        'content' => $this->render('_dataTransaction', [
            'model' => $model,
            'row' => $model->transactions,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Inventory Checkin')),
        'content' => $this->render('_dataInventoryCheckin', [
            'model' => $model,
            'row' => $model->inventoryCheckins,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Order')),
        'content' => $this->render('_dataOrder', [
            'model' => $model,
            'row' => $model->orders,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Dispose')),
        'content' => $this->render('_dataDispose', [
            'model' => $model,
            'row' => $model->disposes,
        ]),
    ],
];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
