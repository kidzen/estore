<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title"><?= Yii::t('app', 'User').' '. Html::encode($this->title) ?></h2>
                </div>
                <div class="box-body">
                    <div class="col-sm-4">

                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>

                    <div class="col-sm-12">
                        <?php
                        $gridColumn = [
                            ['attribute' => 'id', 'visible' => false],
                            'username',
                            'email:email',
                            'profile_pic',
                            [
                                'attribute' => 'role.name',
                                'label' => Yii::t('app', 'Role'),
                            ],
                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function($model) {
                                    switch($model['status']) {
                                        case 1:
                                        return \kartik\helpers\Html::bsLabel('Active','success');
                                        break;
                                        default:
                                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                                        break;
                                    }
                                },
                                'visible' => true,
                            ],
                        ];
                        echo DetailView::widget([
                            'model' => $model,
                            'attributes' => $gridColumn
                        ]);
                        ?>
                    </div>

                    <div class="col-sm-12">
                        <?php
                        if($providerActivityLog->totalCount){
                            $gridColumnActivityLog = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                'remote_ip',
                                'action',
                                'controller',
                                'params',
                                'route',
                                [
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        switch($model['status']) {
                                            case 1:
                                            return \kartik\helpers\Html::bsLabel('Active','success');
                                            break;
                                            default:
                                            return \kartik\helpers\Html::bsLabel('Inactive','danger');
                                            break;
                                        }
                                    },
                                    'visible' => true,
                                ],
                                'messages',
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerActivityLog,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-activity-log']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Activity Log')),
                                ],
                                'export' => false,
                                'columns' => $gridColumnActivityLog
                            ]);
                        }
                        ?>

                    </div>



                    <div class="col-sm-12">
                        <?php
                        if($providerRole->totalCount){
                            $gridColumnRole = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                'name',
                                [
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        switch($model['status']) {
                                            case 1:
                                            return \kartik\helpers\Html::bsLabel('Active','success');
                                            break;
                                            default:
                                            return \kartik\helpers\Html::bsLabel('Inactive','danger');
                                            break;
                                        }
                                    },
                                    'visible' => true,
                                ],
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerRole,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-role']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Role')),
                                ],
                                'export' => false,
                                'columns' => $gridColumnRole
                            ]);
                        }
                        ?>

                    </div>

                    <div class="col-sm-12">
                        <?php
                        if($providerStore->totalCount){
                            $gridColumnStore = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                [
                                    'attribute' => 'category.name',
                                    'label' => Yii::t('app', 'Category')
                                ],
                                'name',
                                'address',
                                'contact_no',
                                [
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        switch($model['status']) {
                                            case 1:
                                            return \kartik\helpers\Html::bsLabel('Active','success');
                                            break;
                                            default:
                                            return \kartik\helpers\Html::bsLabel('Inactive','danger');
                                            break;
                                        }
                                    },
                                    'visible' => true,
                                ],
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerStore,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-store']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Store')),
                                ],
                                'export' => false,
                                'columns' => $gridColumnStore
                            ]);
                        }
                        ?>

                    </div>

                    <div class="col-sm-12">
                        <?php
                        if($providerStoreAssignment->totalCount){
                            $gridColumnStoreAssignment = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                [
                                    'attribute' => 'store.name',
                                    'label' => Yii::t('app', 'Store')
                                ],
                                [
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        switch($model['status']) {
                                            case 1:
                                            return \kartik\helpers\Html::bsLabel('Active','success');
                                            break;
                                            default:
                                            return \kartik\helpers\Html::bsLabel('Inactive','danger');
                                            break;
                                        }
                                    },
                                    'visible' => true,
                                ],
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerStoreAssignment,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-store-assignment']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Store Assignment')),
                                ],
                                'export' => false,
                                'columns' => $gridColumnStoreAssignment
                            ]);
                        }
                        ?>

                    </div>

                    <div class="col-sm-12">
                        <?php
                        if($providerTransaction->totalCount){
                            $gridColumnTransaction = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                [
                                    'attribute' => 'store.name',
                                    'label' => Yii::t('app', 'Store')
                                ],
                                'type',
                                'check_date',
                                [
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function($model) {
                                        switch($model['status']) {
                                            case 1:
                                            return \kartik\helpers\Html::bsLabel('Active','success');
                                            break;
                                            default:
                                            return \kartik\helpers\Html::bsLabel('Inactive','danger');
                                            break;
                                        }
                                    },
                                    'visible' => true,
                                ],
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerTransaction,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-transaction']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Transaction')),
                                ],
                                'export' => false,
                                'columns' => $gridColumnTransaction
                            ]);
                        }
                        ?>

                    </div>
                    <?php if ($model->role) : ?>
                        <div class="col-sm-12">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h4 class="title"><?= Yii::t('app', 'Role') ?></h4>
                                    <?php
                                    $gridColumnRole = [
                                        ['attribute' => 'id', 'visible' => false],
                                        'name',
                                        [
                                            'attribute' => 'status',
                                            'format' => 'raw',
                                            'value' => function($model) {
                                                switch($model['status']) {
                                                    case 1:
                                                    return \kartik\helpers\Html::bsLabel('Active','success');
                                                    break;
                                                    default:
                                                    return \kartik\helpers\Html::bsLabel('Inactive','danger');
                                                    break;
                                                }
                                            },
                                            'visible' => true,
                                        ],
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model->role,
                                        'attributes' => $gridColumnRole    ]
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>
