<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

$dataProvider = new ArrayDataProvider([
    'allModels' => $model->stores,
    'key' => 'id'
]);
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    ['attribute' => 'id', 'visible' => false],
    [
        'attribute' => 'category.name',
        'label' => Yii::t('app', 'Category')
    ],
    'name',
    'address',
    'contact_no',
    [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function($model) {
            switch($model['status']) {
                case 1:
                return \kartik\helpers\Html::bsLabel('Active','success');
                break;
                default:
                return \kartik\helpers\Html::bsLabel('Inactive','danger');
                break;
            }
        },
        'visible' => true,
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'controller' => 'store',
        'visibleButtons' => [
            // 'view' => false,
            'update' => Yii::$app->user->can('update_store'),
            'delete' => Yii::$app->user->can('delete_store'),
        ],
    ],
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'],
    'pjax' => true,
    'beforeHeader' => [
        [
            'options' => ['class' => 'skip-export']
        ]
    ],
    'export' => [
        'fontAwesome' => true
    ],
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'showPageSummary' => false,
    'persistResize' => false,
]);
