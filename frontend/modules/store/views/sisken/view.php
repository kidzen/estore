<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\Sisken */

$this->title = $model->tahun;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sisken'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sisken-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Sisken').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">

            <?= Html::a(Yii::t('app', 'Update'), ['update', ], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', ], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php
    $gridColumn = [
        'tahun',
        'bulan',
        ['attribute' => 'id', 'visible' => false],
        'no_kerja',
        'arahan_kerja',
        'jk_jg',
        'bengkel_panel_id',
        'nama_panel',
        'category_panel',
        'selenggara_id',
        'tempoh_selenggara',
        'alasan_id',
        'alasan',
        'tarikh_arahan',
        'no_sebutharga',
        'tarikh_kenderaan_tiba',
        'tarikh_jangka_siap',
        'tarikh_sebut_harga',
        'status_sebut_harga',
        'no_do',
        'tarikh_siap',
        'tempoh_jaminan',
        'tarikh_do',
        'tarikh_cetakan',
        'status',
        'catatan_sebutharga',
        'catatan',
        'nama_pic',
        'tarikh_permohonan',
        'id_kenderaan',
        'kategori_kerosakan_id',
        'kategori_kerosakan',
        'kategori',
        'isservice',
        'ispancit',
        'odometer_terkini',
        'no_plat',
        'model',
        'kod_jabatan',
        'nama_jabatan',
        'jumlah_kos',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    </div>
    </div>
    </div>
    </div>
</div>
