<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\store\models\search\SiskenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Sisken');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="sisken-index">

    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        'tahun',
        'bulan',
        ['attribute' => 'id', 'visible' => false],
        'no_kerja',
        'arahan_kerja',
        'jk_jg',
        'bengkel_panel_id',
        'nama_panel',
        'category_panel',
        'selenggara_id',
        'tempoh_selenggara',
        'alasan_id',
        'alasan',
        'tarikh_arahan',
        'no_sebutharga',
        'tarikh_kenderaan_tiba',
        'tarikh_jangka_siap',
        'tarikh_sebut_harga',
        'status_sebut_harga',
        'no_do',
        'tarikh_siap',
        'tempoh_jaminan',
        'tarikh_do',
        'tarikh_cetakan',
        'status',
        'catatan_sebutharga',
        'catatan',
        'nama_pic',
        'tarikh_permohonan',
        'id_kenderaan',
        'kategori_kerosakan_id',
        'kategori_kerosakan',
        'kategori',
        'isservice',
        'ispancit',
        'odometer_terkini',
        'no_plat',
        'model',
        'kod_jabatan',
        'nama_jabatan',
        'jumlah_kos',
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?= DynaGrid::widget([
        'columns'=>$gridColumn,
        'storage'=>DynaGrid::TYPE_COOKIE,
        'theme'=>'panel-primary',
        'showPersonalize'=>true,
        'gridOptions'=>[
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            // 'showPageSummary'=>true,
            //'floatHeader'=>true,
            'responsiveWrap'=>false,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-sisken']],
            'panel' => [
                'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
                'before' =>  '<div style="padding-top: 7px;"><em>* The table header sticks to the top in this demo as you scroll</em></div>',
                'after' => false,
            ],
            // 'export' => false,
            // your toolbar can include the additional full export menu
            'toolbar' => [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title'=>Yii::t('app', 'Create Sisken')]) . ' '.
                    Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button'])
                ],
                ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                '{export}',
                '{toggleData}',
                'exportConfig' => [
                    // ExportMenu::FORMAT_PDF => false
                ]
            ],
        ],
        'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
    ]); ?>

</div>
