<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\Sisken */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="sisken-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'tahun')->textInput(['maxlength' => true, 'placeholder' => 'Tahun']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'bulan')->textInput(['maxlength' => true, 'placeholder' => 'Bulan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'no_kerja')->textInput(['maxlength' => true, 'placeholder' => 'No Kerja']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'arahan_kerja')->textInput(['maxlength' => true, 'placeholder' => 'Arahan Kerja']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'jk_jg')->textInput(['maxlength' => true, 'placeholder' => 'Jk Jg']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'bengkel_panel_id')->textInput(['placeholder' => 'Bengkel Panel']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'nama_panel')->textInput(['maxlength' => true, 'placeholder' => 'Nama Panel']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'category_panel')->textInput(['maxlength' => true, 'placeholder' => 'Category Panel']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'selenggara_id')->textInput(['placeholder' => 'Selenggara']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tempoh_selenggara')->textInput(['maxlength' => true, 'placeholder' => 'Tempoh Selenggara']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'alasan_id')->textInput(['placeholder' => 'Alasan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'alasan')->textInput(['maxlength' => true, 'placeholder' => 'Alasan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tarikh_arahan')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Arahan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'no_sebutharga')->textInput(['maxlength' => true, 'placeholder' => 'No Sebutharga']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tarikh_kenderaan_tiba')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Kenderaan Tiba']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tarikh_jangka_siap')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Jangka Siap']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tarikh_sebut_harga')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Sebut Harga']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'status_sebut_harga')->textInput(['maxlength' => true, 'placeholder' => 'Status Sebut Harga']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'no_do')->textInput(['maxlength' => true, 'placeholder' => 'No Do']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tarikh_siap')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Siap']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tempoh_jaminan')->textInput(['maxlength' => true, 'placeholder' => 'Tempoh Jaminan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tarikh_do')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Do']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tarikh_cetakan')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Cetakan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'status')->textInput(['maxlength' => true, 'placeholder' => 'Status']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'catatan_sebutharga')->textInput(['maxlength' => true, 'placeholder' => 'Catatan Sebutharga']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'catatan')->textInput(['maxlength' => true, 'placeholder' => 'Catatan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'nama_pic')->textInput(['maxlength' => true, 'placeholder' => 'Nama Pic']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'tarikh_permohonan')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Permohonan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'id_kenderaan')->textInput(['placeholder' => 'Id Kenderaan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'kategori_kerosakan_id')->textInput(['maxlength' => true, 'placeholder' => 'Kategori Kerosakan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'kategori_kerosakan')->textInput(['maxlength' => true, 'placeholder' => 'Kategori Kerosakan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'kategori')->textInput(['maxlength' => true, 'placeholder' => 'Kategori']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'isservice')->textInput(['maxlength' => true, 'placeholder' => 'Isservice']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'ispancit')->textInput(['maxlength' => true, 'placeholder' => 'Ispancit']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'odometer_terkini')->textInput(['maxlength' => true, 'placeholder' => 'Odometer Terkini']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'no_plat')->textInput(['maxlength' => true, 'placeholder' => 'No Plat']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'model')->textInput(['maxlength' => true, 'placeholder' => 'Model']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'kod_jabatan')->textInput(['maxlength' => true, 'placeholder' => 'Kod Jabatan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'nama_jabatan')->textInput(['maxlength' => true, 'placeholder' => 'Nama Jabatan']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'jumlah_kos')->textInput(['maxlength' => true, 'placeholder' => 'Jumlah Kos']) ?>
</div>

    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
