<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\search\SiskenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-sisken-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tahun')->textInput(['maxlength' => true, 'placeholder' => 'Tahun']) ?>

    <?= $form->field($model, 'bulan')->textInput(['maxlength' => true, 'placeholder' => 'Bulan']) ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'no_kerja')->textInput(['maxlength' => true, 'placeholder' => 'No Kerja']) ?>

    <?= $form->field($model, 'arahan_kerja')->textInput(['maxlength' => true, 'placeholder' => 'Arahan Kerja']) ?>

    <?php /* echo $form->field($model, 'jk_jg')->textInput(['maxlength' => true, 'placeholder' => 'Jk Jg']) */ ?>

    <?php /* echo $form->field($model, 'bengkel_panel_id')->textInput(['placeholder' => 'Bengkel Panel']) */ ?>

    <?php /* echo $form->field($model, 'nama_panel')->textInput(['maxlength' => true, 'placeholder' => 'Nama Panel']) */ ?>

    <?php /* echo $form->field($model, 'category_panel')->textInput(['maxlength' => true, 'placeholder' => 'Category Panel']) */ ?>

    <?php /* echo $form->field($model, 'selenggara_id')->textInput(['placeholder' => 'Selenggara']) */ ?>

    <?php /* echo $form->field($model, 'tempoh_selenggara')->textInput(['maxlength' => true, 'placeholder' => 'Tempoh Selenggara']) */ ?>

    <?php /* echo $form->field($model, 'alasan_id')->textInput(['placeholder' => 'Alasan']) */ ?>

    <?php /* echo $form->field($model, 'alasan')->textInput(['maxlength' => true, 'placeholder' => 'Alasan']) */ ?>

    <?php /* echo $form->field($model, 'tarikh_arahan')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Arahan']) */ ?>

    <?php /* echo $form->field($model, 'no_sebutharga')->textInput(['maxlength' => true, 'placeholder' => 'No Sebutharga']) */ ?>

    <?php /* echo $form->field($model, 'tarikh_kenderaan_tiba')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Kenderaan Tiba']) */ ?>

    <?php /* echo $form->field($model, 'tarikh_jangka_siap')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Jangka Siap']) */ ?>

    <?php /* echo $form->field($model, 'tarikh_sebut_harga')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Sebut Harga']) */ ?>

    <?php /* echo $form->field($model, 'status_sebut_harga')->textInput(['maxlength' => true, 'placeholder' => 'Status Sebut Harga']) */ ?>

    <?php /* echo $form->field($model, 'no_do')->textInput(['maxlength' => true, 'placeholder' => 'No Do']) */ ?>

    <?php /* echo $form->field($model, 'tarikh_siap')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Siap']) */ ?>

    <?php /* echo $form->field($model, 'tempoh_jaminan')->textInput(['maxlength' => true, 'placeholder' => 'Tempoh Jaminan']) */ ?>

    <?php /* echo $form->field($model, 'tarikh_do')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Do']) */ ?>

    <?php /* echo $form->field($model, 'tarikh_cetakan')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Cetakan']) */ ?>

    <?php /* echo $form->field($model, 'status')->textInput(['maxlength' => true, 'placeholder' => 'Status']) */ ?>

    <?php /* echo $form->field($model, 'catatan_sebutharga')->textInput(['maxlength' => true, 'placeholder' => 'Catatan Sebutharga']) */ ?>

    <?php /* echo $form->field($model, 'catatan')->textInput(['maxlength' => true, 'placeholder' => 'Catatan']) */ ?>

    <?php /* echo $form->field($model, 'nama_pic')->textInput(['maxlength' => true, 'placeholder' => 'Nama Pic']) */ ?>

    <?php /* echo $form->field($model, 'tarikh_permohonan')->textInput(['maxlength' => true, 'placeholder' => 'Tarikh Permohonan']) */ ?>

    <?php /* echo $form->field($model, 'id_kenderaan')->textInput(['placeholder' => 'Id Kenderaan']) */ ?>

    <?php /* echo $form->field($model, 'kategori_kerosakan_id')->textInput(['maxlength' => true, 'placeholder' => 'Kategori Kerosakan']) */ ?>

    <?php /* echo $form->field($model, 'kategori_kerosakan')->textInput(['maxlength' => true, 'placeholder' => 'Kategori Kerosakan']) */ ?>

    <?php /* echo $form->field($model, 'kategori')->textInput(['maxlength' => true, 'placeholder' => 'Kategori']) */ ?>

    <?php /* echo $form->field($model, 'isservice')->textInput(['maxlength' => true, 'placeholder' => 'Isservice']) */ ?>

    <?php /* echo $form->field($model, 'ispancit')->textInput(['maxlength' => true, 'placeholder' => 'Ispancit']) */ ?>

    <?php /* echo $form->field($model, 'odometer_terkini')->textInput(['maxlength' => true, 'placeholder' => 'Odometer Terkini']) */ ?>

    <?php /* echo $form->field($model, 'no_plat')->textInput(['maxlength' => true, 'placeholder' => 'No Plat']) */ ?>

    <?php /* echo $form->field($model, 'model')->textInput(['maxlength' => true, 'placeholder' => 'Model']) */ ?>

    <?php /* echo $form->field($model, 'kod_jabatan')->textInput(['maxlength' => true, 'placeholder' => 'Kod Jabatan']) */ ?>

    <?php /* echo $form->field($model, 'nama_jabatan')->textInput(['maxlength' => true, 'placeholder' => 'Nama Jabatan']) */ ?>

    <?php /* echo $form->field($model, 'jumlah_kos')->textInput(['maxlength' => true, 'placeholder' => 'Jumlah Kos']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
