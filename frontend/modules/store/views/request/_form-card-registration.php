<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\Inventory */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="inventory-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <div class="col-md-4">
        <?= $form->field($model, 'category_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\InventoryCategory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Inventory category')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'card_no')->textInput(['maxlength' => true, 'placeholder' => 'Card No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'code_no')->textInput(['maxlength' => true, 'placeholder' => 'Code No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'quantity')->textInput(['placeholder' => 'Quantity']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'location')->textInput(['maxlength' => true, 'placeholder' => 'Location']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'min_stock')->textInput(['placeholder' => 'Min Stock']) ?>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-4">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
