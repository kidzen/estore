<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\InventoryCheckin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventory-checkin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <div class="col-md-4">
        <?= $form->field($model, 'inventory_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \frontend\modules\store\models\Inventory::arrayList(),
            'options' => ['placeholder' => Yii::t('app', 'Choose Inventory')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <?php if(Yii::$app->user->can('store_budget_view')) : ?>
        <div class="col-md-4">
            <?= $form->field($model, 'budget_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Budget::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Budget')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    <?php endif; ?>

    <div class="col-md-4">
        <?= $form->field($model, 'vendor_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Vendor::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Vendor')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-4">
        <?= $form->field($model, 'items_quantity')->textInput(['placeholder' => 'Items Quantity']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'items_total_price')->textInput(['placeholder' => 'Items Total Price']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'check_date')->widget(\kartik\datecontrol\DateControl::classname());
        ?>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-4">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
