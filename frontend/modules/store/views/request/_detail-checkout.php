<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\Order */
?>
<div class="order-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
        <?php
        $gridColumn = [
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'transaction.id',
                'label' => Yii::t('app', 'Transaction'),
            ],
            'instruction_id',
            'order_date',
            [
                'attribute' => 'orderedBy.username',
                'label' => Yii::t('app', 'Ordered By'),
            ],
            'order_no',
            [
                'attribute' => 'usage.id',
                'label' => Yii::t('app', 'Usage'),
            ],
            'required_date',
            'checkout_date',
            [
                'attribute' => 'checkoutBy.username',
                'label' => Yii::t('app', 'Checkout By'),
            ],
            'approved',
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn
        ]);
        ?>
    </div>
</div>
