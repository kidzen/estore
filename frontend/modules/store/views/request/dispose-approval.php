<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Approval');
$this->params['breadcrumbs'][] = $this->title;

$title = empty($this->title) ? Yii::t('app', 'Report') : $this->title . ' Report';
// die;
/*
<div class="modal fade modalApprovalSearch" tabindex="-1" role="dialog" aria-labelledby="searchModal">
    <?php $this->render('_search-approval', ['searchModel' => $searchModel, 'orderId' => $dispose['id']]); ?>
</div>
<div class="modal fade modalMultipleSearch" tabindex="-1" role="dialog" aria-labelledby="searchModal">
    <?php $this->render('_multiple-approval', ['searchModel' => $searchModel, 'orderId' => $dispose['id']]); ?>
</div>
*/
// var_dump($dispose['category']['name']);die;
// var_dump($dispose->attributes);die;
$quo = "
<h3>#{$dispose['refference_no']}</h3>
<table class=\"table table-condensed\">
<tr>
<td width=\"10px\">{$disposeLabel['label']}</td>
<td width=\"10px\">:</td>
<td min-width=\"30px\">{$dispose['category']['name']}</td>
<td style=\"padding: 0px 10px\"></td>
<td width=\"10px\">{$disposeLabel['label']}</td>
<td width=\"10px\">:</td>
<td style=\"min-width:100px\">{$dispose['method']['name']}</td>
</tr>
<tr>
<td>{$disposeLabel['label']}</td>
<td width=\"10px\">:</td>
<td>{$dispose['refference_no']}</td>
</tr>
<tr>
<td>{$disposeLabel['label']}</td>
</tr>
<tr>
<td colspan=\"5\">
<div  style=\"padding-left:20px\">
<table class=\"table table-condensed\">
<tr>
<td class=\"text-center\">#</td>
<td class=\"text-center\">Description</td>
<td class=\"text-center\">Sku</td>
<td class=\"text-center\">Unit Price</td>
</tr>";
$total = 0;
foreach ($dispose->inventoryItems as $key => $value) {
    $i = $key+1;
    $total += $value['unit_price'];

    $quo .= "<tr>
    <td class=\"text-center\">{$i}</td>
    <td>{$value['inventory']['code_no']} - {$value['inventory']['description']}</td>
    <td class=\"text-center\">{$value['sku']}</td>
    <td class=\"text-center\">{$value['unit_price']}</td>
    </tr>";
}
$quo .= "
<tr>
<td colspan=\"3\">Total</td>
<td class=\"text-center\">{$total}</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
";
?>
<div class="approval-index">

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider2,
        // 'filterModel' => $searchModel2,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        // 'toolbar' => false,
        'toolbar' => [
            ['content' =>
                Html::a('Back', Yii::$app->request->referrer, ['class' => 'btn btn-default', 'title' => Yii::t('app', 'Create inventory-items'),]). ' ' .
                Html::a('Update', ['/request/dispose-update','id' => $dispose['id']], ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Update'),]). ' ' .
                // Html::a('<i class="glyphicon glyphicon-check"></i> <span>Pengesahan <i>FIFO</i></span>', ['smart-approve', 'orderId' => $dispose['id']], ['id' => 'approve-transaction', 'class' => 'btn bg-maroon-gradient', 'title' => Yii::t('app', 'FIFO Approval'),]) . ' ' .
                Html::a('<i class="glyphicon glyphicon-check"></i> <span>Sahkan Transaksi</span>', ['approve-dispose', 'disposeId' => $dispose['id']], ['id' => 'approve-transaction', 'class' => 'btn bg-green-gradient', 'title' => Yii::t('app', 'Approve Transaction'),]) //. ' ' .
            //                Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) //. ' ' .
            //                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => Yii::t('app', 'Add Book'), 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' ' .
            ],
            // '{export}',
            // '{toggleData}',
        ],
        // set export properties
/*        'exportConfig' => [
            'pdf' => $pdf,
            'csv' => '{csv}',
            'xls' => '{xls}',
        ],
        'export' => [
            'fontAwesome' => true,
            'target' => '_self',
        ],*/
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'panel' => [
            'heading' => false,
            'footer' => false,
            'before' => $quo,
        ],
        'responsiveWrap' => false,
        'persistResize' => false,
        'showPageSummary'=> true,
        //        'exportConfig' => $exportConfig,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            // [
            //     'attribute' => 'id',
            //     'hAlign' => 'center', 'vAlign' => 'middle', 'width'=> '50px',
            // ],
            // [
            //     // 'label' => 'Name',
            //     'attribute' => 'inventory.description',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            [
                'label' => 'Inventory Details',
                'attribute' => 'INVENTORY_DETAILS',
                'format' => 'raw',
                'value' => function($model) {
                    $card = 'Card No : ' . $model->inventory->card_no;
                    $code = 'Code No : ' . $model->inventory->code_no;
                    $description = 'Description : ' . $model->inventory->description;
                    return Html::tag('span', $card . '<br>' . $code . '<br>' . $description);
                },
                'hAlign' => 'left', 'vAlign' => 'middle',
                'pageSummary' => 'Total',
            ],
            [
                'attribute' => 'sku',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            // [
            //     'label' => 'Store Balance',
            //     'attribute' => 'inventory.quantity',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            [
                'attribute' => 'checkin.transaction.check_date',
                'label' => 'Check In Date',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            // [
            //     'attribute' => 'checkout.order.transaction.check_date',
            //     'label' => 'Check Out Date',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            [
                'label' => 'Status',
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->deleted_by == '0') {
                        if (!($model->checkout_id || $model->dispose_id)) {
                            return Html::label('In Store', null, ['class' => 'label label-success']);
                        } else if ($model->dispose_id) {
                            return Html::label('Diposed', null, ['class' => 'label label-danger']);
                        } else {
                            return Html::label('Checkout', null, ['class' => 'label label-danger']);
                        }
                    } else {
                        return Html::label('deleted', null, ['class' => 'label label-danger']);
                    }
                },
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'unit_price',
                'label' => 'UNIT PRICE (RM)',
                'format' => ['decimal', 2],
                // 'format' => 'currency',
                'width' => '60px', 'hAlign' => 'center', 'vAlign' => 'middle',
                'pageSummary' => true,
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                // 'visible' => Yii::$app->user->can('admin_store'),
                'template' => '{cancel}',
                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
                'buttons' => [
                    'cancel' => function ($url, $model) use ($dispose){
                        return Html::a('<span class="glyphicon glyphicon-remove danger" style="color:red;"></span>', ['cancel-dispose', 'itemId' => $model['id'],'disposeId'=>$dispose['id']], ['title' => Yii::t('yii', 'Cancel'), 'data-toggle' => 'tooltip','data-method' => 'POST',]);
                    },
                ],
            ],
        ],
    ]);
    ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'id' => 'approval-grid',
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //        'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => $this->title,
        ],
        'responsiveWrap' => false,
        'persistResize' => false,
        //        'exportConfig' => $exportConfig,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            // [
            //     'attribute' => 'id',
            //     'hAlign' => 'center', 'vAlign' => 'middle', 'width'=> '50px',
            // ],
            // [
            //     // 'label' => 'Name',
            //     'attribute' => 'inventory.description',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            [
                'label' => 'Inventory Details',
                'attribute' => 'INVENTORY_DETAILS',
                'format' => 'raw',
                'value' => function($model) {
                    $card = 'Card No : ' . $model->inventory->card_no;
                    $code = 'Code No : ' . $model->inventory->code_no;
                    $description = 'Description : ' . $model->inventory->description;
                    return Html::tag('span', $card . '<br>' . $code . '<br>' . $description);
                },
                'hAlign' => 'left', 'vAlign' => 'middle',
                'pageSummary' => 'Total',
            ],
            [
                'attribute' => 'sku',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            // [
            //     'label' => 'Store Balance',
            //     'attribute' => 'inventory.quantity',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            [
                'attribute' => 'checkin.transaction.check_date',
                'label' => 'Check In Date',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            // [
            //     'attribute' => 'checkout.order.transaction.check_date',
            //     'label' => 'Check Out Date',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
            [
                'label' => 'Status',
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->deleted_by == '0') {
                        if (!$model->checkout_id) {
                            return Html::label('In Store', null, ['class' => 'label label-success']);
                        } else {
                            return Html::label('Checkout', null, ['class' => 'label label-danger']);
                        }
                    } else {
                        return Html::label('deleted', null, ['class' => 'label label-danger']);
                    }
                },
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'unit_price',
                'label' => 'UNIT PRICE (RM)',
                'format' => ['decimal', 2],
                // 'format' => 'currency',
                'width' => '60px', 'hAlign' => 'center', 'vAlign' => 'middle',
                'pageSummary' => true,
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                // 'visible' => Yii::$app->user->can('admin_store'),
                'template' => '{cancel}',
                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
                'buttons' => [
                    'cancel' => function ($url, $model) use ($dispose) {
                        return Html::a('<span class="btn btn-sm btn-danger">DISPOSE</span>', ['add-dispose', 'itemId' => $model['id'], 'disposeId' => $dispose['id']]
                            , ['title' => Yii::t('yii', 'Dispose'), 'data-toggle' => 'tooltip','data-method' => 'POST']);
                        // return Html::a('<span class="glyphicon glyphicon-remove danger" style="color:red;"></span>', ['cancel', 'id' => $model->id,'orderItemId'=>$model->checkout_id], ['title' => Yii::t('yii', 'Cancel'), 'data-toggle' => 'tooltip','data-method' => 'POST',]);
                    },
                ],
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>

</div>
