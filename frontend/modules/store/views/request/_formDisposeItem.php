<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\store\models\search\InventoryItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
// use kartik\export\ExportMenu;
// use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Inventory Item');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
    $('.search-button').toggle(200);
    return false;
});";
$this->registerJs($search);
?>
<div class="inventory-item-index">
    <p>
        <?= Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search-disposeItem', ['model' => $searchModel]); ?>
    </div>

    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'inventory_id',
            'label' => Yii::t('app', 'Inventory'),
            'value' => function($model){
                if ($model->inventory)
                    {return $model->inventory->description;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Inventory::find()->asArray()->all(), 'id', 'description'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Inventory', 'id' => 'grid-inventory-item-search-inventory_id']
        ],
        'sku',
        'unit_price',
        [
            'class' => 'yii\grid\CheckboxColumn',
        ],
    ];
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        // // 'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
    ?>

</div>
