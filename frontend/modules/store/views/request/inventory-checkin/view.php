<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\InventoryCheckin */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Checkin'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-checkin-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Inventory Checkin').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">

            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'transaction.id',
                'label' => Yii::t('app', 'Transaction'),
            ],
        [
                'attribute' => 'budget.name',
                'label' => Yii::t('app', 'Budget'),
            ],
        [
                'attribute' => 'store.name',
                'label' => Yii::t('app', 'Store'),
            ],
        'inventory_id',
        [
                'attribute' => 'vendor.name',
                'label' => Yii::t('app', 'Vendor'),
            ],
        'items_quantity',
        'items_total_price',
        'check_date',
        'approved',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <?php if ($model->budget) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">Budget<?= ' '. Html::encode($this->title) ?></h4>
    <?php
    $gridColumnBudget = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'revenue',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->budget,
        'attributes' => $gridColumnBudget    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->updatedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->createdBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->transaction) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">Transaction<?= ' '. Html::encode($this->title) ?></h4>
    <?php
    $gridColumnTransaction = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'store.name',
                'label' => Yii::t('app', 'Store'),
            ],
        'type',
        'check_date',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->transaction,
        'attributes' => $gridColumnTransaction    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->store) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">Store<?= ' '. Html::encode($this->title) ?></h4>
    <?php
    $gridColumnStore = [
        ['attribute' => 'id', 'visible' => false],
        'category_id',
        'name',
        'address',
        'contact_no',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->store,
        'attributes' => $gridColumnStore    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->vendor) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">Vendor<?= ' '. Html::encode($this->title) ?></h4>
    <?php
    $gridColumnVendor = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'address',
        'contact_no',
        'email',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->vendor,
        'attributes' => $gridColumnVendor    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->checkBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->checkBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->approvedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4 class="title">User<?= ' '. Html::encode($this->title) ?></h4>
    <?php
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'staff_no',
        'profile_pic',
                'role_id',
        'password_reset_token',
        'auth_key',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->approvedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>

        <div class="col-sm-12">
<?php
if($providerInventoryItem->totalCount){
    $gridColumnInventoryItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'inventory.description',
                'label' => Yii::t('app', 'Inventory')
            ],
                        [
                'attribute' => 'checkout.id',
                'label' => Yii::t('app', 'Checkout')
            ],
            'sku',
            'unit_price',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInventoryItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inventory-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Inventory Item')),
        ],
        'export' => false,
        'columns' => $gridColumnInventoryItem
    ]);
}
?>

        </div>
    </div>
    </div>
    </div>
    </div>
</div>
