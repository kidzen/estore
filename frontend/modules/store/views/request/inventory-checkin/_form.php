<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\InventoryCheckin */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'InventoryItem',
        'relID' => 'inventory-item',
        'value' => \yii\helpers\Json::encode($model->inventoryItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="inventory-checkin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'transaction_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Transaction::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Transaction')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'budget_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Budget::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Budget')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'store_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Store::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Store')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'inventory_id')->textInput(['placeholder' => 'Inventory']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'vendor_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Vendor::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Vendor')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
 ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'items_quantity')->textInput(['placeholder' => 'Items Quantity']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'items_total_price')->textInput(['placeholder' => 'Items Total Price']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'check_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => Yii::t('app', 'Choose Check Date'),
                        'autoclose' => true
                    ]
                ],
            ]);
 ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'approved')->textInput(['placeholder' => 'Approved']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'status')->textInput(['placeholder' => 'Status']) ?>
</div>

    <div class="clearfix"></div>
<div class="col-md-12">
    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'InventoryItem')),
            'content' => $this->render('_formInventoryItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->inventoryItems),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
</div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
