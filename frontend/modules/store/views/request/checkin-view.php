<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\InventoryCheckin */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Checkin'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-checkin-view">

    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title"><?= Yii::t('app', 'Inventory Checkin').' '. Html::encode($this->title) ?></h2>
                </div>
                <div class="box-body">
                    <div class="col-sm-4">

                        <?php Html::a(Yii::t('app', 'Update'), ['checkin-update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['checkin-delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                // 'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>

                    <div class="col-sm-12">
                        <?php
                        $gridColumn = [
                            ['attribute' => 'id', 'visible' => false],
                            [
                                'attribute' => 'transaction.id',
                                'label' => Yii::t('app', 'Transaction'),
                            ],
                            [
                                'attribute' => 'store.name',
                                'label' => Yii::t('app', 'Store'),
                            ],
                            [
                                'attribute' => 'inventory.description',
                                'label' => Yii::t('app', 'Inventory'),
                            ],
                            [
                                'attribute' => 'vendor.name',
                                'label' => Yii::t('app', 'Vendor'),
                            ],
                            'items_quantity',
                            'items_total_price:currency',
                            'check_date',
                            'approvedLabel:raw',
                            [
                                'attribute' => 'createdByName',
                                'label' => Yii::t('app', 'Created By'),
                            ],
                            [
                                'attribute' => 'updatedByName',
                                'label' => Yii::t('app', 'Updated By'),
                            ],
                            [
                                'attribute' => 'approvedByName',
                                'label' => Yii::t('app', 'Approved By'),
                            ],
                        ];
                        echo DetailView::widget([
                            'model' => $model,
                            'attributes' => $gridColumn
                        ]);
                        ?>
                    </div>
                    <?php if ($model->inventory) : ?>
                        <div class="col-sm-12">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h4 class="title">Inventory : <?= ' '. Html::encode($model->inventory->description) ?></h4>
                                    <?php
                                    $gridColumnInventory = [
                                        ['attribute' => 'id', 'visible' => false],
                                        'category.name',
                                        'card_no',
                                        'code_no',
                                        'description',
                                        'quantity',
                                        'min_stock',
                                        'location',
                                        'approved',
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model->inventory,
                                        'attributes' => $gridColumnInventory
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->vendor) : ?>
                        <div class="col-sm-12">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h4 class="title">Vendor : <?= ' '. Html::encode($model->vendor->name) ?></h4>
                                    <?php
                                    $gridColumnVendor = [
                                        ['attribute' => 'id', 'visible' => false],
                                        'name',
                                        'address',
                                        'contact_no',
                                        'email',
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model->vendor,
                                        'attributes' => $gridColumnVendor
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="col-sm-12">
                        <?php
                        if($providerInventoryItem->totalCount){
                            $gridColumnInventoryItem = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                [
                                    'attribute' => 'inventory.description',
                                    'label' => Yii::t('app', 'Inventory')
                                ],
                                [
                                    'attribute' => 'checkout.id',
                                    'label' => Yii::t('app', 'Checkout Transaction')
                                ],
                                'sku',
                                'unit_price:currency',
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerInventoryItem,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inventory-item']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Inventory Item')),
                                ],
                                'export' => false,
                                'columns' => $gridColumnInventoryItem
                            ]);
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
