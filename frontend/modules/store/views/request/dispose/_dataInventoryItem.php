<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->inventoryItems,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'inventory.description',
                'label' => Yii::t('app', 'Inventory')
            ],
        [
                'attribute' => 'checkin.id',
                'label' => Yii::t('app', 'Checkin')
            ],
        [
                'attribute' => 'checkout.id',
                'label' => Yii::t('app', 'Checkout')
            ],
        'sku',
        'unit_price',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'inventory-item'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
