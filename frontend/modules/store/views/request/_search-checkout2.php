<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\search\OrderItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-order-item-search">
    <div class="row">
        <?php $form = ActiveForm::begin([
                    // 'action' => ['index'],
            'method' => 'get',
        ]);
        ?>
        <div class="col-md-12">
            <div class="box">


                <div class="col-md-4">
                    <?= $form->field($model, 'inventory_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Inventory::find()->orderBy('id')->asArray()->all(), 'id', function($model) { return $model['code_no'].' - '.$model['description']; }),
                        'options' => ['placeholder' => Yii::t('app', 'Choose Inventory')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-4">

                    <?= $form->field($model, 'order_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Order::find()->orderBy('id')->asArray()->all(), 'id', 'order_no'),
                        'options' => ['placeholder' => Yii::t('app', 'Choose Order')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">

                    <?= $form->field($model, 'rq_quantity')->textInput(['placeholder' => 'Rq Quantity']) ?>
                </div>
                <div class="col-md-4">

                    <?= $form->field($model, 'app_quantity')->textInput(['placeholder' => 'App Quantity']) ?>
                </div>
                <div class="col-md-4">

                    <?= $form->field($model, 'current_balance')->textInput(['placeholder' => 'Current Balance']) ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <?= $form->field($model, 'unit_price')->textInput(['placeholder' => 'Unit Price']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'status')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \frontend\modules\store\models\Order::getStatusList(),
                        'options' => ['placeholder' => Yii::t('app', 'Choose Status')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                    <!-- ->dropdownList(['prompt' => 'Status']) ?> -->
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 form-group">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
                </div>
                <div class="clearfix"></div>


            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
