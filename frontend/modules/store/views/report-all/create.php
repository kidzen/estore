<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\ReportAll */

$this->title = Yii::t('app', 'Create Report All');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Report All'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-all-create">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
