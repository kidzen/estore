<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\search\ReportAllSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-report-all-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true, 'placeholder' => 'Year']) ?>

    <?= $form->field($model, 'quarter')->textInput(['maxlength' => true, 'placeholder' => 'Quarter']) ?>

    <?= $form->field($model, 'count_in')->textInput(['maxlength' => true, 'placeholder' => 'Count In']) ?>

    <?= $form->field($model, 'price_in')->textInput(['maxlength' => true, 'placeholder' => 'Price In']) ?>

    <?php /* echo $form->field($model, 'COUNT_OUT')->textInput(['maxlength' => true, 'placeholder' => 'COUNT OUT']) */ ?>

    <?php /* echo $form->field($model, 'PRICE_OUT')->textInput(['maxlength' => true, 'placeholder' => 'PRICE OUT']) */ ?>

    <?php /* echo $form->field($model, 'count_current')->textInput(['maxlength' => true, 'placeholder' => 'Count Current']) */ ?>

    <?php /* echo $form->field($model, 'price_current')->textInput(['maxlength' => true, 'placeholder' => 'Price Current']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
