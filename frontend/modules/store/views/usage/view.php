<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\Usage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usage'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usage-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Usage').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">

            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'category.name',
            'label' => Yii::t('app', 'Category'),
        ],
        'item_id',
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

        <div class="col-sm-12">
<?php
if($providerOrder->totalCount){
    $gridColumnOrder = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'transaction.id',
                'label' => Yii::t('app', 'Transaction')
            ],
            'instruction_id',
            'order_date',
            [
                'attribute' => 'orderedBy.username',
                'label' => Yii::t('app', 'Ordered By')
            ],
            'ordered_by_temp',
            'order_no',
                        'required_date',
            'checkout_date',
            [
                'attribute' => 'checkoutBy.username',
                'label' => Yii::t('app', 'Checkout By')
            ],
            [
                'attribute' => 'approved',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['approved']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerOrder,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-order']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Order')),
        ],
        'export' => false,
        'columns' => $gridColumnOrder
    ]);
}
?>

        </div>
    <?php if ($model->category) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title"><?= Yii::t('app', 'Category') ?></h4>
    <?php
    $gridColumnUsageCategory = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo DetailView::widget([
        'model' => $model->category,
        'attributes' => $gridColumnUsageCategory    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>

        <div class="col-sm-12">
<?php
if($providerUsageDetailValue->totalCount){
    $gridColumnUsageDetailValue = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'key.name',
                'label' => Yii::t('app', 'Key')
            ],
                        'value',
            'refference',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerUsageDetailValue,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-usage-detail-value']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Usage Detail Value')),
        ],
        'export' => false,
        'columns' => $gridColumnUsageDetailValue
    ]);
}
?>

        </div>
    </div>
    </div>
    </div>
    </div>
</div>
