<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\ReportOutQuarter */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="report-out-quarter-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'year')->textInput(['maxlength' => true, 'placeholder' => 'Year']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'quarter')->textInput(['maxlength' => true, 'placeholder' => 'Quarter']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'count')->textInput(['maxlength' => true, 'placeholder' => 'Count']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'total_price')->textInput(['maxlength' => true, 'placeholder' => 'Total Price']) ?>
</div>

    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
