<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'UsageCategory')),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Usage')),
        'content' => $this->render('_dataUsage', [
            'model' => $model,
            'row' => $model->usages,
        ]),
    ],
    // [
    //     'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Usage Detail Key')),
    //     'content' => $this->render('_dataUsageDetailKey', [
    //         'model' => $model,
    //         'row' => $model->usageDetailKeys,
    //     ]),
    // ],
];
switch ($model->id) {
    case '1':
    $items[] = [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Workshop')),
        'content' => $this->render('_dataWorkshop', [
            'model' => $model,
            'row' => $model->workshops,
        ]),
    ];
    break;
    case '2':
    $items[] = [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Vehicle')),
        'content' => $this->render('_dataVehicle', [
            'model' => $model,
            'row' => $model->vehicles,
        ]),
    ];
    break;
    case '3':
    $items[] = [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Store')),
        'content' => $this->render('_dataStore', [
            'model' => $model,
            'row' => $model->stores,
        ]),
    ];
    break;

    default:
        # code...
    break;
}
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
