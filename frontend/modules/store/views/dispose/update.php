<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\Dispose */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dispose',
]) . ' ' . $model->description;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispose'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->description, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dispose-update">
	<div class="panel panel-danger">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
