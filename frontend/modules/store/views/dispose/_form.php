<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\Dispose */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'InventoryItem',
        'relID' => 'inventory-item',
        'value' => \yii\helpers\Json::encode($model->inventoryItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="dispose-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'refference_no')->textInput(['maxlength' => true, 'placeholder' => 'Refference No']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'category_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\DisposeCategory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose category')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'method_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\DisposeMethod::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose method')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'reason_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\DisposeReason::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose reason')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<!-- <div class="col-md-4">
    <?= $form->field($model, 'current_revenue')->textInput(['placeholder' => 'Current Revenue']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'cost')->textInput(['placeholder' => 'Cost']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'returns')->textInput(['placeholder' => 'Returns']) ?>
</div>
 -->
<div class="col-md-4">
    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>
</div>

<!-- <div class="col-md-4">
    <?= $form->field($model, 'disposed_at')->textInput(['placeholder' => 'Disposed At']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'disposed_by')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>
 -->
<?php if(Yii::$app->user->can('admin')) : ?>
<div class="col-md-4">
    <?= $form->field($model, 'approved')->widget(\kartik\widgets\Select2::classname(), [
            'data' => [1 => 'Approved', 0 => 'Inactive', 2 => 'Pending', 8 => 'Rejected', ],
            'options' => ['placeholder' => 'Select approved ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>
</div>
<?php endif; ?>

<!-- <div class="col-md-4">
    <?= $form->field($model, 'status')->widget(\kartik\widgets\Select2::classname(), [
            'data' => [1 => 'Active', 0 => 'Inactive'],
            'options' => ['placeholder' => 'Select status ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>
</div>
 -->
    <div class="clearfix"></div>
<div class="col-md-12">
    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'InventoryItem')),
            'content' => $this->render('_formInventoryItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->inventoryItems),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
</div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
