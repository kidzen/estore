<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\StoreAssignment */

$this->title = Yii::t('app', 'Create Store Assignment');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Store Assignment'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-assignment-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
