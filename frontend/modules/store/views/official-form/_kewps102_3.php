

<?php
// var_dump($items[0]->attributes);die;
?>
<tfoot>
    <tr>
        <td class="foot-head-col-1 foot-head-row-1" style="white-space:nowrap;" colspan="2">Pegawai Pemesan :</td>
        <td class="foot-head-col-2 foot-head-row-1 border-right-bold" style="white-space:nowrap;" colspan="2">Pegawai Penerima :</td>
        <td class="foot-head-col-3 foot-head-row-1" style="white-space:nowrap;" colspan="5">Tarikh Diluluskan dan Direkodkan Oleh :</td>
        <td class="foot-head-col-4 foot-head-row-1" style="white-space:nowrap;" colspan="2">Dikeluarkan dan Direkodkan Oleh :</td>
    </tr>
    <tr>
        <td class="no-border"><strong>Nama :</strong></td>
        <td class="no-border pull-right border-right" style="font-size:12;"><?= mb_strimwidth(strtoupper($items[0]['orderedBy']['profile']['name']),0,20,"...",'UTF-8') ?></td>
        <td class="no-border"><strong>Nama :</strong></td>
        <td class="no-border pull-right border-right-bold"> </td>
        <td class="no-border"><strong>Nama :</strong></td>
        <td class="no-border pull-right border-right" style="font-size:12;" colspan="4"><?= mb_strimwidth($items[0]['approvedBy']['profile']['name'],0,40,"...",'UTF-8') ?></td>
        <td class="no-border"><strong>Nama :</strong></td>
        <td class="no-border pull-right border-right"> </td>
    </tr>
    <tr>
        <td class="no-border"><strong>Jawatan :</strong></td>
        <td class="no-border pull-right border-right"> </td>
        <td class="no-border"><strong>Jawatan :</strong></td>
        <td class="no-border pull-right border-right-bold"> </td>
        <td class="no-border"><strong>Jawatan :</strong></td>
        <td class="no-border pull-right border-right" style="font-size:12;" colspan="4"><?= mb_strimwidth($items[0]['approvedBy']['profile']['position'],0,40,"...",'UTF-8') ?></td>
        <td class="no-border"><strong>Jawatan :</strong></td>
        <td class="no-border pull-right border-right"> </td>
    </tr>
    <tr>
        <td class="no-border"><strong>Jabatan :</strong></td>
        <td class="no-border pull-right border-right"> </td>
        <td class="no-border"><strong>Jabatan :</strong></td>
        <td class="no-border pull-right border-right-bold"> </td>
        <td class="no-border"><strong>Jabatan :</strong></td>
        <td class="no-border pull-right border-right" style="font-size:12;" colspan="4"><?= mb_strimwidth($items[0]['approvedBy']['profile']['department'],0,40,"...",'UTF-8') ?></td>
        <td class="no-border"><strong>Jabatan :</strong></td>
        <td class="no-border pull-right border-right"> </td>
    </tr>
    <tr>
        <td class="no-border"><strong>Tarikh :</strong></td>
        <td class="no-border pull-right border-right" style="font-size:12;"><?= mb_strimwidth($items[0]['order_date'],0,40,"...",'UTF-8') ?></td>
        <td class="no-border"><strong>Tarikh :</strong></td>
        <td class="no-border pull-right border-right-bold"> </td>
        <td class="no-border"><strong>Tarikh :</strong></td>
        <td class="no-border pull-right border-right" style="font-size:12;" colspan="4"><?= mb_strimwidth($items[0]['approved_date'],0,40,"...",'UTF-8') ?></td>
        <td class="no-border"><strong>Tarikh :</strong></td>
        <td class="no-border pull-right border-right"> </td>
    </tr>
    <tr>
        <td class="foot-note-col-1 foot-note-vertical-align-top border-right-bold" colspan="4" rowspan="3">
            <br><br>
            <p>Nota</p>
            <p>Salinan 1 - Disimpan oleh pemesan</p>
            <p>Salinan 2 - Bahagian Bekalan, Kawalan Dan Akaun</p>
            <p>Salinan 3 - Bahagian Simpanan</p>
            <p>Salinan 4 - Bahagian Bungkusan Dan Penghantaran</p>
            <p>Salinan 5 – Disimpan oleh  pemesan setelah stok diterima</p>
        </td>
        <td class="text-center foot-vertical-align-top text-center foot-deliver-head text-center info" colspan="7"><strong>BAHAGIAN BUNGKUSAN DAN,PENGHANTARAN</strong></td>
    </tr>
    <tr>
        <td class="vertical-align-top" colspan="2" rowspan="2"><strong>Butir-Butir Bungkusan</strong></td>
        <td class="vertical-align-top" colspan="3" rowspan="2"><strong>Butir-Butir Penghantaran</strong></td>
        <td class="vertical-align-top no-border" colspan="2">Telah dibungkus dan dihantar oleh :<br>..........................................................</td>
    </tr>
    <tr>
        <td class="vertical-align-top no-border" >
            <strong>Nama : </strong><br>
            <strong>Jawatan : </strong><br>
            <strong>Jabatan : </strong><br>
            <strong>Tarikh : </strong><br>
        </td>
        <td class="vertical-align-top no-border"> </td>
    </tr>

</tfoot>
</table>

</div>
</div>
