<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\EstorInventories */

//$this->title = $inventory->ID;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estor Inventories'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//var_dump($inventory);
//var_dump($listItem[0]);die();
//die();
?>


<div class="box-body table-responsive kewps4">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN B</strong><br><strong>KEW.PS-4</strong></p>
        <p class="form-detail-right"><strong>No. Kad : <?= isset($inventory['card_no']) ? $inventory['card_no'] : '...............' ?></strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>KAD PETAK</strong></p>
        <div>
            <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
                <tbody>
                    <tr>
                        <td colspan="2" >Rujuk Kawalan Stok :</th>
                        <td colspan="5" ><?= $inventory['card_no'] ?></th>
                        <td colspan="2" >Nombor Kod:</th>
                        <td colspan="4" ><?= $inventory['code_no'] ?></th>
                    </tr>
                    <tr>
                        <td colspan="2">Perihal Stok:</th>
                        <td colspan="11"><?= $inventory['description'] ?></th>
                    </tr>
                    <tr>
                        <td colspan="2">Kumpulan Stok:</th>
                        <td colspan="5" ><?= $inventory['category']['name'] ?></th>
                        <td colspan="2" >Lokasi Stok:</th>
                        <td colspan="4"><?= $inventory['location'] ?></th>
                    </tr>
                </tbody>
            </table>
        </div>
        <br>
        <!--/.row-->
    </div>
    <div class="box-body table-responsive">
        <div>
            <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap kewps4">
            <!--<table class="table table-bordered table-hover">-->
                <thead>
                    <!--<tr class="kartik-sheet-style">-->
                    <tr>
                        <th rowspan="2">Bil</th>
                        <th rowspan="2">Tarikh</th>
                        <th rowspan="2">Keterangan</th>
                        <th rowspan="2">No Rujukan</th>
                        <th colspan="4" style="text-align: center">Kuantiti</span></th>
                        <th rowspan="2">Nama Pegawai Stor</th>
                    </tr>
                    <tr>
                        <th>Terima</th>
                        <th>Seunit (RM)</th>
                        <th>Keluar</th>
                        <th>Baki</th>
                    </tr>
                </thead>

