

<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN A</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="pull-right form-id"><strong>KEW.PS-10</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="form-name"><strong>BORANG PESANAN DAN PENGELUARAN STOK</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
        <?php //var_dump($items[0]->orders->vhicle);die(); ?>
            <thead>
                <tr>
                    <td class="text-bold border-right-bold no-border-bottom" colspan="4"><div>Daripada :</div></td>
                    <td class="text-bold no-border-bottom" colspan="7"><div>Kepada :</div></td>
                </tr>
                <tr>
                    <td class="text-bold border-right-bold no-border-top" colspan="4"><div><br></div></td>
                    <td class="text-bold text-center no-border-top" colspan="7"><div><?= isset($items[0]['order']['vehicle']) ? $items[0]['order']['vehicle']['reg_no']: ''; ?></div></td>
                </tr>
                <tr>
                    <td class="text-center info border-right-bold" colspan="4">Dilengkapkan Oleh Stor Pesanan</td>
                    <td class="text-center info" colspan="7">Dilengkapkan Oleh Stor Pengeluar</td>
                </tr>
                <tr>
                    <td class="text-center border-right-bold" colspan="4">No Pemesanan : <?= mb_strimwidth($items[0]['order_no'],0,20,"...",'UTF-8') ?></td>
                    <td class="text-center" colspan="7">No Pengeluaran : <?= mb_strimwidth($items[0]['order_no'],0,20,"...",'UTF-8') ?></td>
                </tr>
                <tr>
                    <td class="text-center border-right-bold" colspan="4">Tarikh Bekalan Dikehendaki : </td>
                    <td class="text-center info" colspan="5">BAHAGIAN BEKALAN,KAWALAN DAN AKAUN</td>
                    <td class="text-center info" colspan="2">BAHAGIAN SIMPANAN</td>
                </tr>
                <tr>
                    <td class="text-center info" rowspan="2">No. Kod</td>
                    <td class="text-center info" colspan="2" rowspan="2">Perihal Stok</td>
                    <td class="text-center info border-right-bold" rowspan="2">Kuantiti</td>
                    <td class="text-center info" colspan="2">Kad Kawalan Stok</td>
                    <td class="text-center info" rowspan="2">Kuantiti Diluluskan</td>
                    <td class="text-center info" colspan="2">Harga (RM)</td>
                    <td class="text-center info" rowspan="2">Kuantiti Dikeluarkan</td>
                    <td class="text-center info" rowspan="2">Catatan</td>
                </tr>
                <tr>
                    <td class="text-center info">No. Kad</td>
                    <td class="text-center info">Baki Sedia Ada</td>
                    <td class="text-center info">Seunit</td>
                    <td class="text-center info">Jumlah</td>
                </tr>
            </thead>
