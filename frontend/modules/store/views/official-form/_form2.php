<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\FormGenerator */
/* @var $form ActiveForm */
?>
<div class="form2">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'ID') ?>
        <?= $form->field($model, 'FORM_ID') ?>
        <?= $form->field($model, 'FORM_NAME') ?>
        <?= $form->field($model, 'PARAMS') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- _form2 -->
