<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\EstorInventories */

//$this->title = $inventory->ID;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estor Inventories'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//var_dump($inventory);
//var_dump($listItem[0]);die();
//die();
?>

<tbody>
    <?php
    // if (sizeof($transactions) >= 1) {
    if ($transactions) {
        foreach ($transactions as $i => $transaction) {
            ?>
            <?php
            if ($i == 0) { ?>
            <!-- if (sizeof($transactions) >= 1) { ?> -->
            <tr>
                <td class="text-center">/</td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="3" class="pull-right ">Baki Dibawa Kehadapan</td>
                <?php if ($transaction) { ?>
                <td class="text-center"><?= $transaction['count_current'] - $transaction['count_in'] + $transaction['count_out'] ?></td>
                <?php } else { ?>
                <td class="text-center">0</td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td class="text-center"><?= $i + 1 ?></td>
                <td class="text-center nowrap"><?= $transaction['check_date']; ?></td>
                <td class="text-center"><?php
                    if ($transaction['type'] == 1) {
                        echo 'IN';
                    } else {
                        echo 'OUT';
                    }
                    ?>
                </td>
                <td class="text-center" style="font-size: 10;width: 15%;white-space: nowrap;"><?= mb_strimwidth($transaction['refference'],0,35,'...'); ?></td>
                <td class="text-center"><?= $transaction['count_in']; ?></td>
                <td class="pull-right"><?php
                    if ($transaction['type'] == 1) {
                        echo $transaction['price_in'] / $transaction['count_in'];
                        ;
                    } else {
                        echo $transaction['price_out'] / $transaction['count_out'];
                        ;
                    }
                    ?>
                </td>
                <td class="text-center"><?= $transaction['count_out']; ?></td>
                <td class="text-center"><?= $transaction['count_current']; ?></td>
                <td style="font-size: 10;width: 30%;" class="text-center"><?= mb_strimwidth($transaction['checkBy']['profile']['name'],0,30,'...') ?></td>
            </tr>
            <?php } ?>
            <?php for ($j = sizeof($transactions); $j < 27; $j++) { ?>
            <tr>
                <td class="text-center"><?= $j + 1 ?></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td></td>
                <td class="text-center"></td>
                <td class="pull-right"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center">&nbsp;</td>
            </tr>
            <?php } ?>
            <!-- avoid errors (untested) -->
            <?php } else { ?>
            <?php for ($j = 0; $j < 27; $j++) { ?>
            <tr>
                <td class="kv-align-center"><?= $j + 1 ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <?php } ?>
            <!-- end avoid errors (untested) -->
            <?php } ?>
        </tbody>
