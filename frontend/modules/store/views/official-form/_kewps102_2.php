
<tbody>

    <?php foreach ($items as $item) { ?>
        <tr>
            <td class="text-center" style="width:10%"><?= $item['code_no'] ?></td>
            <td class="nowrap" style="font-size:12;width:40%" colspan="2"><?= mb_strimwidth($item['description'],0,35,"...",'UTF-8') ?></td>
            <td class="text-center" style="width:7%"><?= $item['rq_quantity'] ?></td>
            <td class="text-center" style="width:7%"><?= $item['card_no'] ?></td>
            <td class="text-center" style="width:7%"><?= $item['current_balance'] ?></td>
            <td class="text-center" style="width:7%"><?= $item['app_quantity'] ?></td>
            <td class="text-center" style="width:7%"><?= number_format($item['unit_price'],2) ?></td>
            <td class="text-center" style="width:7%;text-align: center;"><?= number_format($item['batch_total_price'],2)  ?></td>
            <td class="text-center" style="width:8%"><?= $item['app_quantity'] ?></td>
            <td class="text-center" style="width:15%"></td>

        </tr>
    <?php } ?>
    <?php for ($j = sizeof($items); $j < 3; $j++) { ?>
        <tr>
            <td class="col-1 " style="width:10%"><br></td>
            <td class="col-2 " style="width:40%" colspan="2"> </td>
            <td class="col-3 border-right-bold" style="width:7%"> </td>
            <td class="col-4 " style="width:7%"> </td>
            <td class="col-5 " style="width:7%"> </td>
            <td class="col-5 " style="width:7%"> </td>
            <td class="col-5 " style="width:7%"> </td>
            <td class="no-border-right text-center " style="width:7%;"> </td>
            <td class="col-9 " style="width:8%"> </td>
            <td class="col-10 " style="width:15%"></td>
        </tr>
    <?php } ?>

<tbody>
