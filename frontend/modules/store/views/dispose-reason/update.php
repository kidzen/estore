<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\DisposeReason */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dispose Reason',
]) . ' ' . $model->name;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispose Reason'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dispose-reason-update">
	<div class="panel panel-danger">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
