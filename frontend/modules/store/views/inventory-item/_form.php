<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\InventoryItem */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="inventory-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <div class="col-md-4">
        <?= $form->field($model, 'inventory_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Inventory::find()->orderBy('inventory.id')->asArray()->all(), 'id', 'description'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Inventory')],
            'pluginOptions' => [
                'allowClear' => true,
                'disabled' => true,
            ],
        ]);
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'checkin_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\InventoryCheckin::find()->orderBy('inventory_checkin.id')->asArray()->all(), 'id', 'id'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Inventory checkin')],
            'pluginOptions' => [
                'allowClear' => true,
                'disabled' => true,
            ],
        ]);
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'checkout_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\OrderItem::find()->orderBy('order_item.id')->asArray()->all(), 'id', 'id'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Order item')],
            'pluginOptions' => [
                'allowClear' => true,
                'disabled' => true,
            ],
        ]);
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'dispose_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Dispose::find()->orderBy('dispose.id')->asArray()->all(), 'id', 'description'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Dispose')],
            'pluginOptions' => [
                'allowClear' => true,
                'disabled' => true,
            ],
        ]);
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'sku')->textInput(['maxlength' => true, 'placeholder' => 'Sku']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'unit_price')->textInput(['maxlength' => true, 'placeholder' => 'Unit Price']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status')->widget(\kartik\widgets\Select2::classname(), [
            'data' => [1 => 'Active', 0 => 'Inactive'],
            'options' => ['placeholder' => 'Select status ...'],
            'pluginOptions' => [
                'allowClear' => true,
                'disabled' => true,
            ],
        ]);
        ?>
    </div>

    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="col-md-4">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
