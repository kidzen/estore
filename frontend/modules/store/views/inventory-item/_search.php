<?php

use kartik\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\search\InventoryItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inventory-item-search">
    <div class="row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]);
        ?>

        <div class="col-md-4">
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'inventory_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Inventory::find()->orderBy('inventory.id')->asArray()->all(), 'id', 'description'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Inventory')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'checkin_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\InventoryCheckin::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Inventory checkin')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'checkout_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\OrderItem::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Order item')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'dispose_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\frontend\modules\store\models\Dispose::find()->orderBy('dispose.id')->asArray()->all(), 'id', 'description'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    </div>

    <?php /* echo $form->field($model, 'sku')->textInput(['maxlength' => true, 'placeholder' => 'Sku']) */ ?>

    <?php /* echo $form->field($model, 'unit_price')->textInput(['maxlength' => true, 'placeholder' => 'Unit Price']) */ ?>

    <?php /* echo $form->field($model, 'status')->widget(\kartik\widgets\Select2::classname(), [
            'data' => [1 => 'Active', 0 => 'Inactive'],
            'options' => ['placeholder' => 'Select status ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); */ ?>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
