<?php
use kartik\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Transaction')),
        // 'active' => false,
        'active' => false,
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Dispose')),
        'visible' => $model->disposes,
        'active' => true,
        'content' => $this->render('_dataDispose', [
            'model' => $model,
            'row' => $model->disposes,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Inventory Checkin')),
        'visible' => $model->inventoryCheckins,
        'active' => true,
        'content' => $this->render('_dataInventoryCheckin', [
            'model' => $model,
            'row' => $model->inventoryCheckins,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('app', 'Order')),
        'active' => true,
        'visible' => $model->orders,
        'content' => $this->render('_dataOrder', [
            'model' => $model,
            'row' => $model->orders,
        ]),
    ],
                    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
