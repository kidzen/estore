<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->disposes,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'refference_no',
        [
                'attribute' => 'category.name',
                'label' => Yii::t('app', 'Category')
            ],
        [
                'attribute' => 'method.name',
                'label' => Yii::t('app', 'Method')
            ],
        [
                'attribute' => 'reason.name',
                'label' => Yii::t('app', 'Reason')
            ],
        'quantity',
        'current_revenue',
        'cost',
        'returns',
        'description',
        'requested_at',
        [
                'attribute' => 'requestedBy.username',
                'label' => Yii::t('app', 'Requested By')
            ],
        'disposed_at',
        [
                'attribute' => 'disposedBy.username',
                'label' => Yii::t('app', 'Disposed By')
            ],
        [
                'attribute' => 'approved',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['approved']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
        [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model) {
                    switch($model['status']) {
                        case 1:
                        return \kartik\helpers\Html::bsLabel('Active','success');
                        break;
                        default:
                        return \kartik\helpers\Html::bsLabel('Inactive','danger');
                        break;
                    }
                },
                'visible' => true,
            ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'dispose'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
