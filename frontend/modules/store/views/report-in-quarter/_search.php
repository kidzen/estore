<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\search\ReportInQuarterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-report-in-quarter-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true, 'placeholder' => 'Year']) ?>

    <?= $form->field($model, 'quarter')->textInput(['maxlength' => true, 'placeholder' => 'Quarter']) ?>

    <?= $form->field($model, 'count')->textInput(['maxlength' => true, 'placeholder' => 'Count']) ?>

    <?= $form->field($model, 'total_price')->textInput(['maxlength' => true, 'placeholder' => 'Total Price']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
