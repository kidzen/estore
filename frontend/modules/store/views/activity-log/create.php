<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\store\models\ActivityLog */

$this->title = Yii::t('app', 'Create Activity Log');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activity Log'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-log-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
