<?php

namespace frontend\modules\store\controllers;

use Yii;
use frontend\modules\store\models\KewForm;
use frontend\modules\store\models\Dispose;
use frontend\modules\store\models\search\DisposeSearch;
use frontend\modules\store\models\Order;
use frontend\modules\store\models\search\OrderSearch;
use frontend\modules\store\models\OrderItem;
use frontend\modules\store\models\search\OrderItemSearch;
use frontend\modules\store\models\InventoryCheckin;
use frontend\modules\store\models\search\InventoryCheckinSearch;
use frontend\modules\store\models\Inventory;
use frontend\modules\store\models\search\InventorySearch;
use frontend\modules\store\models\InventoryItem;
use frontend\modules\store\models\search\InventoryItemSearch;
use frontend\modules\store\models\Checkout;
use frontend\modules\store\models\search\CheckoutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\Exception;
use yii\filters\VerbFilter;

/**
 * RequestController implements the CRUD actions for Order model.
 */
class RequestController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Print Kewps 10.
     * @param integer orderId
     * @return mixed
     */
    public function actionKewps10($orderId)
    {
        KewForm::printKew('kewps10', ['id' => $orderId]);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Smart Approval.
     * @param integer orderId
     * @return mixed
     */
    public function actionSmartApprove($orderId)
    {
        Order::fifoApproval($orderId);
        return $this->redirect(['/request/approval', 'orderId' => $orderId]);
    }

    /**
     * Reject.
     * @param integer orderId
     * @return mixed
     */
    public function actionReject($id)
    {
        Order::rejectTransaction($id);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Approval.
     * @param integer orderId
     * @return mixed
     */
    public function actionApproval($orderId)
    {
        $searchModel = new \frontend\modules\store\models\search\ApprovalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $orderId);
        $dataProvider->query->andWhere(['id_order' => $orderId]);
        $dataProvider->query->andWhere(['checkout_id' => null]);
        $dataProvider->query->orderBy('inventory_item.sku');

        $searchModel2 = new \frontend\modules\store\models\search\InventoryItemSearch();
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
        $dataProvider2->query->joinWith('order');
        $dataProvider2->query->andWhere(['order.id' => $orderId]);

        $order = \frontend\modules\store\models\Order::findOne($orderId);
        $orderLabel['order_date'] = Yii::t('app','Tarikh Pesanan');
        $orderLabel['required_date'] = Yii::t('app','Tarikh Diperlukan');
        $orderLabel['orderBy'] = Yii::t('app','Pemesan');
        $orderLabel['detail'] = Yii::t('app','Perincian');

        if(Yii::$app->request->post()) {
            var_dump(Yii::$app->request->post());die;
            InventoryItem::approve($id, $orderItemId);
        }

        // $dataProvider2->query->andWhere('in','INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' , $orderItemsId);
        // var_dump($dataProvider2->models[0]->attributes);die();

        if (\Yii::$app->request->post('smart-approve')) {
            $dbtransac = \Yii::$app->dbStore->beginTransaction();
            try {
                if ($order && $orderItems && $inventory && $inventoryItems) {
                    //assign checkout id to items(link)
                    $inventoryItems->checkout_transaction_id = $orderItemsId;
                    //remove 1 items from inventory
                    $inventory->quantity = $inventory->quantity - 1;
                    //add 1 item to approved quantity
                    $orderItems->app_quantity = $orderItems->app_quantity + 1;
                    //update total price of checkout items
                    $orderItems->unit_price = $orderItems->unit_price + $inventoryItems->unit_price;
                    //save all model
                    if (!$inventoryItems->save(false))
                        Throw new Exception(' Data item inventori tidak dapat dikemaskini.');
                    if (!$inventory->save(false))
                        Throw new Exception(' Data inventori tidak dapat dikemaskini.');
                    if (!$orderItems->save(false))
                        Throw new Exception(' Data pesanan tidak dapat dikemaskini.');
                    $dbtransac->commit();
                    \Yii::$app->notify->success(['message' => 'Proses kemaskini data berjaya']);
                } else {
                    throw new Exception(' Data tidak wujud.');
                }
            } catch (Exception $e) {
                \Yii::$app->notify->fail(['message' => $e->getMessage()]);
                $dbtransac->rollBack();
            } catch (\yii\db\Exception $e) {
                \Yii::$app->notify->fail(['message' => $e->getMessage()]);
                $dbtransac->rollBack();
            }
        }
        return $this->render('approval', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModel2' => $searchModel2,
            'dataProvider2' => $dataProvider2,
            'orderId' => $orderId,
            'order' => $order,
            'orderLabel' => $orderLabel,
        ]);
    }

    public function actionCancel($id, $orderItemId) {

        InventoryItem::approvalCancel($id,$orderItemId);
        return $this->redirect(Yii::$app->request->referrer);
    }
    /**
     * Item List.
     * @param integer orderId
     * @return mixed
     */
    public function actionItemList($id)
    {
        $model  = $this->findCheckoutItem($id);
        $searchModel = new \frontend\modules\store\models\search\InventoryItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['inventory_item.checkout_id' => $id]);
        return $this->render('item-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Checkin Method.
     * @return mixed
     */
    public function actionCheckinList()
    {
        $searchModel = new InventoryCheckinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('checkin-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheckin()
    {
        $model = new InventoryCheckin();
        if ($model->load(Yii::$app->request->post())) {
        // var_dump($model->attributes);die;
            // $dbTransac = Yii::$app->dbStore->beginTransaction();
            try {
                if(!$model->save()) {
                    throw new Exception("Error Processing Request", 1);
                }
                // $dbTransac->commit();
                Yii::$app->notify->success();
                return $this->redirect(['checkin-view', 'id' => $model->id]);
            } catch (Exception $e) {
                // $dbTransac->rollback();
                // Yii::$app->session->addFlash('error', $e->errorInfo[2]);
                // var_dump($e->getMessage());die;
                Yii::$app->notify->fail(['message' => $e->getMessage()]);
                // return $this->redirect(['checkin-list']);
            }
        }
        // $model->check_date = date('d-M-y');
        // var_dump($model->check_date);die;
        // $model->check_date = isset($model->check_date) ? $model->check_date : date('d-M-y');
        return $this->render('checkin', [
            'model' => $model,
        ]);
    }

    public function actionCheckinView($id)
    {
        $model = $this->findCheckin($id);
        $providerInventoryItem = new \yii\data\ArrayDataProvider([
            'allModels' => $model->inventoryItems,
        ]);
        return $this->render('checkin-view', [
            'model' => $model,
            'providerInventoryItem' => $providerInventoryItem,
        ]);
    }

    public function actionUpdateCheckin($id)
    {
        $model = $this->findCheckin($id);
        return $this->redirect(['/inventory-checkin/update', 'id' => $model->id]);

        // if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('checkin-update', [
        //         'model' => $model,
        //     ]);
        // }
    }

    public function actionCheckinDelete($id)
    {
        // fail due to post method is required
        $dbtransac = Yii::$app->dbStore->beginTransaction();
        try {
            $model = $this->findCheckin($id);
            $model->reject();
            $dbtransac->commit();
            Yii::$app->notify->success();
        } catch (Exception $e) {
            Yii::$app->notify->fail();
            return $this->redirect(['/request/checkin-view','id'=>$id]);
        }
        return $this->redirect(['/request/checkin-list']);
    }

    public function actionCardRegistration()
    {
        $model = new Inventory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/inventory/view', 'id' => $model->id]);
        } else {
            return $this->render('card-registration', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Approval Method.
     * @return mixed
     */
    public function actionIndex2()
    {
        $searchModel = new OrderItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['order_id'=>SORT_DESC,'id'=>SORT_ASC];

        return $this->render('checkout-index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('checkout-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprove($id, $orderItemId) {
        if(Yii::$app->request->post()) {
            InventoryItem::approve($id, $orderItemId);
        }
        // return true;
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionApproveTransaction($orderId)
    {
        if(Order::approveTransaction($orderId)) {
            return $this->redirect(['checkout-view', 'id' => $orderId]);
        }
        return $this->redirect(Yii::$app->request->refferer);
    }
    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionKewps10View2($id)
    {
        $model = $this->findCheckout($id);
        $providerOrderItem = new \yii\data\ArrayDataProvider([
            'allModels' => $model->orderItems,
        ]);
        // var_dump($model->orderItems);die;
        $providerPackage = new \yii\data\ArrayDataProvider([
            'allModels' => $model->packages,
        ]);
        return $this->render('kewps10-view', [
            'model' => $this->findCheckout($id),
            'providerOrderItem' => $providerOrderItem,
            'providerPackage' => $providerPackage,
        ]);
    }
    public function actionKewps10View($id)
    {
        $model = $this->findCheckout($id);
//        $model = $this->findModel($id);
        $searchModel = new CheckoutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['order_id' => $id]);
        $items = $dataProvider->models;

        return $this->render('kewps10-view2', [
            'items' => $items,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Checkout Method.
     * @return mixed
     */
    public function actionCheckoutView($id)
    {
        $model = $this->findCheckout($id);
        $providerOrderItem = new \yii\data\ArrayDataProvider([
            'allModels' => $model->orderItems,
        ]);
        $providerPackage = new \yii\data\ArrayDataProvider([
            'allModels' => $model->packages,
        ]);
        return $this->render('checkout-view', [
            'model' => $this->findCheckout($id),
            'providerOrderItem' => $providerOrderItem,
            'providerPackage' => $providerPackage,
        ]);
    }

    public function actionPrintKewps10($id)
    {
        return \frontend\modules\store\models\KewpsForm::pdfKewps10($id);
    }

    // public function actionViewCheckout($id)
    // {
    //     $model = $this->findModel($id);
    //     $providerOrderItem = new \yii\data\ArrayDataProvider([
    //         'allModels' => $model->orderItems,
    //     ]);
    //     $providerPackage = new \yii\data\ArrayDataProvider([
    //         'allModels' => $model->packages,
    //     ]);
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //         'providerOrderItem' => $providerOrderItem,
    //         'providerPackage' => $providerPackage,
    //     ]);
    // }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCheckout()
    {
        $model = new Order();
        $skippedRelations = array_diff(['orderItems'], $model::relationNames());
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll($skippedRelations)) {
            return $this->redirect(['index2']);
            // return $this->redirect(['checkout-view', 'id' => $model->id]);
        } else {
            return $this->render('checkout', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdateCheckout($id)
    {
        $model = \frontend\modules\store\models\Order::findOne($id);
        // $model->scenario = \frontend\modules\store\models\Order::SCENARIO_UPDATE;
        if ($model->loadAll(Yii::$app->request->post())) {
            try {
                $model->updateOrder();
                // return $this->redirect(['index2']);
                Yii::$app->notify->success();
                return $this->redirect(['checkout-view', 'id' => $model->id]);
            } catch (Exception $e) {
                Yii::$app->notify->fail(['message' => $e->getMessage()]);

                return $this->render('checkout', [
                    'model' => $model,
                ]);
            }
        }
        return $this->render('checkout', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionDeleteCheckout($id)
    {
        $dbTransac = Yii::$app->dbStore->beginTransaction();
        try {
            $model = \frontend\modules\store\models\Order::findOne($id);
            $model->voidOrder();
            \frontend\modules\store\models\Order::rejectTransaction($id);
            $dbTransac->commit();
            Yii::$app->notify->success();
        } catch (Exception $e) {
            $dbTransac->rollback();
            Yii::$app->notify->fail(['message' => $e->getMessage()]);

        }
        return $this->redirect(['index2']);
        return $this->redirect(['checkout-view', 'id' => $model->id]);
    }

    public function actionCheckoutList()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('checkout-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Disposal.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionDisposeIndex()
    {
        $searchModel = new DisposeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('dispose-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDispose()
    {
        $model = new Dispose();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->notify->success();
            return $this->redirect(['dispose-approval', 'id' => $model->id]);
        }

        return $this->render('dispose-create', [
            'model' => $model,
        ]);
    }

    public function actionApproveDispose($disposeId)
    {
        if($model = Dispose::findOne($disposeId)) {
            $model->approved = 1;
            $model->approved_by = Yii::$app->user->id;
            $model->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
            $model->disposed_by = Yii::$app->user->id;
            $model->disposed_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
            $model->quantity = sizeof($model->inventoryItems);
            $model->current_revenue = $model->evaluateCurrentRevenue();
            $model->returns = $model->evaluateCurrentRevenue();
            $model->save();
            Yii::$app->notify->success();
            return $this->redirect(['dispose-view', 'id' => $disposeId]);
        }
        Yii::$app->notify->fail();
        return $this->redirect(Yii::$app->request->refferer);
    }

    public function actionAddDispose($itemId,$disposeId)
    {
        $dispose = \frontend\modules\store\models\Dispose::findOne($disposeId);
        $item = \frontend\modules\store\models\InventoryItem::findOne($itemId);
        $item->status = 9;
        $item->link('dispose',$dispose);
        return $this->redirect(['dispose-approval', 'id' => $disposeId]);
    }

    public function actionCancelDispose($itemId,$disposeId)
    {
        $item = \frontend\modules\store\models\InventoryItem::findOne($itemId);
        $item->dispose_id = null;
        $item->status = 1;
        $item->save(false);
        return $this->redirect(['dispose-approval', 'id' => $disposeId]);
    }

    public function actionDisposeApproval($id)
    {
        $dispose = Dispose::findOne($id);
        $disposeLabel['label'] = 'Label';
        $searchModel = new \frontend\modules\store\models\search\InventoryItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['checkout_id'=>null,'dispose_id'=>null]);

        $searchModel2 = new \frontend\modules\store\models\search\InventoryItemSearch();
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
        $dataProvider2->query->andWhere(['dispose_id'=>$id]);

        if ($dispose->load(Yii::$app->request->post())) {
            $dbtransac = \Yii::$app->dbStore->beginTransaction();
            try {
                $dispose->save();
                \frontend\modules\store\models\InventoryItem::dispose(Yii::$app->request->post('selection'),$dispose->id);
                $dbtransac->commit();
                \Yii::$app->notify->success();
                return $this->redirect(['view-dispose', 'id' => $dispose->id]);
            } catch (Exception $e) {
                $dbtransac->rollback();
                Yii::$app->notify->fail(['message' => $e->getMessage()]);

            }

        }

        return $this->render('dispose-approval', [
            'dispose' => $dispose,
            'disposeLabel' => $disposeLabel,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'dataProvider2' => $dataProvider2,
            'searchModel2' => $searchModel2,
        ]);
    }

    public function actionDisposeView($id)
    {
        $model = Dispose::findOne($id);
        $providerInventoryItem = new \yii\data\ArrayDataProvider([
            'allModels' => $model->inventoryItems,
        ]);
        return $this->render('dispose-view', [
            'model' => Dispose::findOne($id),
            'providerInventoryItem' => $providerInventoryItem,
        ]);
    }

    public function actionDisposeDelete($id)
    {
        $model = Dispose::findOne($id);
        $dbTransac = Yii::$app->dbStore->beginTransaction();
        try {
            $model->deleted_by = Yii::$app->user->id;
            $model->deleted_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
            if(!$model->save()) {
                throw new Exception("Error Processing Request", 1);
            }
            foreach ($model->inventoryItems as $key => $value) {
                $model->unlink('inventoryItems',$model->inventoryItems[$key]);
            }
            $dbTransac->commit();
            Yii::$app->notify->success();
        } catch (Exception $e) {
            $dbTransac->rollback();
            Yii::$app->notify->fail(['message' => $e->getMessage()]);
        }
        return $this->redirect(['dispose-index']);
    }

    public function actionDispose2()
    {

        $searchModel = new Search\InventoryItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['checkout_id'=>null,'dispose_id'=>null]);
        return [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
    }
    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCheckoutItem($id)
    {
        if (($model = OrderItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    protected function findCheckout($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    protected function findCheckin($id)
    {
        if (($model = InventoryCheckin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
    * Action to load a tabular form grid
    * for OrderItem
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddOrderItem()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('OrderItem');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
                return $this->renderAjax('_form-checkout-item', ['row' => $row]);
            } else {
                throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
            }
        }
    }
