<?php
namespace frontend\modules\store\controllers;
namespace frontend\modules\store\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\store\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use vova07\console\ConsoleRunner;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['logout', 'signup','index'],
                'rules' => [
                    [
                        'actions' => ['signup','login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        // \Yii::$app->notify->success();
        // \Yii::$app->notify->success();
        // \Yii::$app->notify->fail();
        // $test = new \common\components\Notify();
        // $test = \Yii::$app->notify->fail(['duration' => 2]);
        // var_dump($test);
        // die;
//        if (!\Yii::$app->user->isGuest) {
//            $limit = EstorInventories::find()
//                    ->andWhere('QUANTITY < MIN_STOCK')
//                    ->all();
//            $warning = EstorInventories::find()
//                    ->andWhere('QUANTITY <= MIN_STOCK + 2')
//                    ->andwhere('QUANTITY >= MIN_STOCK')
//                    ->all();
//            foreach ($limit as $alert) {
//                \Yii::$app->session->setFlash('error', [
//                    'type' => Growl::TYPE_DANGER,
//                    'duration' => 3000,
//                    'icon' => 'glyphicon glyphicon-remove-sign',
//                    'title' => $alert->DESCRIPTION,
//                    'message' => ' Stok berada dibawah paras minimum.',
//                    'positonY' => 'top',
//                    'positonX' => 'right'
//                ]);
//            }
//            foreach ($warning as $alert) {
//                \Yii::$app->session->setFlash('warning', [
//                    'type' => Growl::TYPE_WARNING,
//                    'duration' => 3000,
//                    'icon' => 'glyphicon glyphicon-exclamation-sign',
//                    'title' => $alert->DESCRIPTION,
//                    'message' => ' Stok menghampiri paras minimum.',
//                    'positonY' => 'top',
//                    'positonX' => 'right'
//                ]);
//            }
//        }
//        $currentYear = date('Y');
        $currentMonth = date('m-y');
//        var_dump($currentMonth);die();
        if (Yii::$app->dbStore->driverName == 'oci') {
            $quantityStockIn = \frontend\modules\store\models\InventoryItem::find()->andWhere(['TO_CHAR("inventory_item"."created_at",\'mm-YY\')' => $currentMonth])->count();
            $quantityStockOut = \frontend\modules\store\models\InventoryItem::find()->andWhere(['TO_CHAR("inventory_item"."updated_at",\'mm-YY\')' => $currentMonth])->andWhere(['not', ['inventory_item.checkout_id' => null]])->count();
            ;
            $quantityStockRequest = \frontend\modules\store\models\Order::find()->andWhere(['TO_CHAR("order"."created_at",\'mm-YY\')' => $currentMonth])->count();
            $quantityInventoriesCardRegistered = \frontend\modules\store\models\Inventory::find()->andWhere(['TO_CHAR("inventory"."created_at",\'mm-YY\')' => $currentMonth])->count();

            $latestOrder = new \yii\data\ActiveDataProvider([
                'query' => \frontend\modules\store\models\OrderItem::find()->limit(5)->orderBy(['order_item.created_at' => SORT_DESC]),
                'pagination' => false,
            ]);
            $latestStockIn = new \yii\data\ActiveDataProvider([
                'query' => \frontend\modules\store\models\InventoryCheckin::find()->limit(5)->orderBy(['inventory_checkin.created_at' => SORT_DESC]),
                'pagination' => false,
            ]);
            $pendingApproval = new \yii\data\ActiveDataProvider([
                'query' => \frontend\modules\store\models\OrderItem::find()->joinWith('order')->andWhere(['order.approved' => 2,'order.deleted_by'=>0])->limit(5)->orderBy(['order.approved_at' => SORT_DESC]),
                'pagination' => false,
            ]);
            // var_dump($latestStockIn->models[0]->inventoryItems);die();
        } else {
            $quantityStockIn = \frontend\modules\store\models\InventoryItem::find()->andWhere(['date_format("created_at",\'%m-%y\')' => $currentMonth])->andWhere(['inventory_item.deleted_by' => 0])->count();
            $quantityStockOut = \frontend\modules\store\models\InventoryItem::find()->andWhere(['date_format("updated_at",\'%m-%y\')' => $currentMonth])->andWhere(['not', ['inventory_item.checkout_id' => null]])->andWhere(['inventory_item.deleted_by' => 0])->count();
            ;
            $quantityStockRequest = \frontend\modules\store\models\Order::find()->andWhere(['date_format("created_at",\'%m-%y\')' => $currentMonth])->andWhere(['order.deleted_by' => 0])->count();
            $quantityInventoriesCardRegistered = \frontend\modules\store\models\Inventory::find()->andWhere(['date_format("created_at",\'%m-%y\')' => $currentMonth])->andWhere(['inventory.deleted_by' => 0])->count();

            $latestOrder = new \yii\data\ActiveDataProvider([
                'query' => \frontend\modules\store\models\OrderItem::find()->limit(5)
                ->orderBy(['order_item.created_at' => SORT_DESC]),
                'pagination' => false,
            ]);
            $latestStockIn = new \yii\data\ActiveDataProvider([
                'query' => \frontend\modules\store\models\InventoryCheckin::find()->limit(5)
                ->orderBy(['inventory_checkin.created_at' => SORT_DESC]),
                'pagination' => false,
            ]);
            $pendingApproval = new \yii\data\ActiveDataProvider([
                'query' => \frontend\modules\store\models\OrderItem::find()->joinWith('order')->andWhere(['order.approved' => 2])->limit(5)->orderBy(['order_item.created_at' => SORT_DESC]),
                'pagination' => false,
            ]);
        }
//        var_dump($latestStockIn->models);die();
//        var_dump($latestStockIn->models);die();
//        $quantityStockIn = \frontend\modules\store\models\EstorItems::find()
//                        ->andWhere(['extract(month from CHECKIN_DATE)' => date('m')])
//                        ->asArray()->count();
//        $quantityStockOut = \frontend\modules\store\models\EstorItems::find()
//                        ->andWhere(['extract(month from CHECKOUT_DATE)' => date('m')])
//                        ->asArray()->count();
//        $quantityStockRequest = \frontend\modules\store\models\StockItems::find()
//                        ->andWhere(['extract(month from CREATED_DATE)' => date('m')])
//                        ->asArray()->count();
//        $quantityInventoriesCardRegistered = \frontend\modules\store\models\EstorInventories::find()
//                        ->andWhere(['extract(month from APPROVED_DATE)' => date('m')])
//                        ->asArray()->count();
//        $latestOrder = new \yii\data\ActiveDataProvider([
//            'query' => \frontend\modules\store\models\Stocks::find()->limit(5),
//            'pagination' => false,
////            'pagination' => ['pageSize'=>2],
//        ]);
//        $latestStockIn = new \yii\data\ActiveDataProvider([
//            'query' => \frontend\modules\store\models\EstorItems::find()->limit(4),
//            'pagination' => false,
//        ]);
        return $this->render('index', [
            'quantityStockIn' => $quantityStockIn,
            'quantityStockOut' => $quantityStockOut,
            'quantityStockRequest' => $quantityStockRequest,
            'quantityInventoriesCardRegistered' => $quantityInventoriesCardRegistered,
            'latestOrder' => $latestOrder,
            'latestStockIn' => $latestStockIn,
            'pendingApproval' => $pendingApproval,
        ]);
    }
    public function actionIndex2()
    {
        return $this->render('index');
    }
    public function actionInfo()
    {
        return $this->render('info');
    }

    public function actionChangeStore($id)
    {
        $cache = Yii::$app->cache;
        $cache->set('active-store', $id);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'main-login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        \Yii::$app->cache->delete('active-store');
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionMaintenance() {
        // echo '<pre>';
            // Yii::$app->notify->success(['message' => ' Sistem dalam keadaan baik.']);
        $data = \frontend\modules\store\models\AllMaintenanceCheck::find()->asArray()->all();
        if (empty($data)) {
            // Yii::$app->notify->success();
            \Yii::$app->notify->success(['message' => 'Sistem dalam keadaan baik.']);
            return $this->redirect(Yii::$app->request->referrer);
            die('test');
        }
        try {
            $dbtransac = Yii::$app->dbStore->beginTransaction();
            foreach ($data as $i => $dat) {
                // var_dump($dat['attributes']) . '<br>';
                if ($dat['type'] == 'text') {
                    $updateStatement = Yii::$app->dbStore->createCommand('UPDATE "' . $dat['table_name'] . '" SET "' . $dat['col_name'] . '" = \'' . $dat['true_value'] . '\' WHERE "id" = ' . $dat['data_id']);
                } else {
                    $updateStatement = Yii::$app->dbStore->createCommand('UPDATE "' . $dat['table_name'] . '" SET "' . $dat['col_name'] . '" = ' . $dat['true_value'] . ' WHERE "id" = ' . $dat['data_id']);
                }
        // var_dump($updateStatement->getSql());die;
        // var_dump($data);die;
                $success = $updateStatement->execute();
                if (!$success) {
                    throw new Exception(' Sistem gagal dikemaskini.');
                }
//                 Yii::$app->session->setFlash($dat['id'], [
//                     'type' => 'success',
//                     'duration' => 5000,
//                     'icon' => 'glyphicon glyphicon-check-sign',
//                     'title' => ' BERJAYA.',
// //                    'message' => ' Kuantiti permohonan yang disahkan telah diselenggara. <br>' . count($orderItems) . ' data pesanan bermasalah berjaya dikemaskini.',
//                     'message' => $dat['table_name'] . ' berjaya dikemaskini. ' . $dat['col_name'] . ' dikemaskini dari ' . $dat['fault_value'] . ' ke ' . $dat['true_value'],
//                 ]);

                \Yii::$app->notify->success([
                    'index' => $i,
                    'message' => $dat['table_name'] . ' berjaya dikemaskini. ' . $dat['col_name'] . ' dikemaskini dari ' . $dat['fault_value'] . ' ke ' . $dat['true_value']
                ]);
            }
            $dbtransac->commit();
        } catch (\Exception $e) {
            \Yii::$app->notify->fail(['message' => $e->getMessage()]);
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail(['message' => $e->getMessage()]);
            $dbtransac->rollBack();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                \Yii::$app->notify->success(['message' => ' Check your email for further instructions.']);

                return $this->goHome();
            } else {
                \Yii::$app->notify->fail(['message' => ' Sorry, we are unable to reset password for the provided email address.']);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
