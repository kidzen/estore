<?php

namespace frontend\modules\store\controllers;

use Yii;
use frontend\modules\store\models\FormGenerator;
use frontend\modules\store\models\Inventory;
use frontend\modules\store\models\KewpsForm;
use frontend\modules\store\models\Order;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;

class OfficialFormController extends \yii\web\Controller {

	public function actionIndex() {
		return $this->render('index');
	}

	public function actionFormGenerator() {
		$model = new FormGenerator();
		$year = \frontend\modules\store\models\TransactionAll::find()
		->select('EXTRACT(YEAR FROM "check_date") as "year"')
		->distinct()->indexBy('year')->asArray()->all();
		$yearList = ArrayHelper::map($year,'year','year');
		$monthList = [
		1 => 'Januari',
		2 => 'Febuari',
		3 => 'Mac',
		4 => 'April',
		5 => 'Mei',
		6 => 'Jun',
		7 => 'Julai',
		8 => 'Ogos',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Disember',
		];
		$formId = [
		// 3 => 'KEW.PS-3 ()',
		4 => 'KEW.PS-4 (KAD PETAK)',
		5 => 'KEW.PS-5 (SENARAI DAFTAR KAD KAWALAN STOK)',
		7 => 'KEW.PS-7 (PENENTUAN KUMPULAN STOK)',
		8 => 'KEW.PS-8 (LABEL FIFO)',
		9 => 'KEW.PS-9 (SENARAI STOK BERTARIKH LUPUT)',
		10 => 'KEW.PS-10 (BORANG PESANAN DAN PENGELUARAN STOK)',
		11 => 'KEW.PS-11 (BORANG PEMESANAN STOK)',
		13 => 'KEW.PS-13 (LAPORAN KEDUDUKAN STOK)',
		14 => 'KEW.PS-14 (LAPORAN PEMERIKSAAN / VERIFIKASI STOR)',
		17 => 'KEW.PS-17 (PENYATA PELARASAN STOK)',
		18 => 'KEW.PS-18 (PERAKUAN AMBIL ALIH)',
		];
		$inventoryList = Inventory::find()->andFilterWhere(['deleted_by' => 0])->asArray()->all();
		$inventoriesArray = ArrayHelper::map($inventoryList, 'id', function($model) { return $model['code_no'] . ' - ' .$model['description'];});
		$orderList = Order::find()
		->andFilterWhere(['deleted_by' => 0])
		// ->andFilterWhere(['APPROVED' => 1])
		// ->orderBy(['id'=>SORT_ASC])
		->orderBy(['to_number(substr([[order_no]],9))'=>SORT_ASC])
		->asArray()->all();
		$ordersArray = ArrayHelper::map($orderList, 'id', 'order_no');
		$formName = new FormGenerator();
//        var_dump($model);die();
		if ($model->load(Yii::$app->request->post())) {
			// var_dump(Yii::$app->request->post());die();
			if ($model->form_id == 3) {
				// KewpsForm::pdfKewps4(
				//     $id = $model->inventory_id,
				//     $year = $model->year,
				//     $month = $model->month
				//     );
			} else if ($model->form_id == 4) {
				KewpsForm::pdfKewps4(
					$id = $model->inventory_id,
					$year = $model->year,
					$month = $model->month,
					// $printAll = $model->printAll
					$printAll = 0
					);
			} else if ($model->form_id == 5) {
				KewpsForm::pdfKewps5(
					$id = $model->inventory_list
					);
			} else if ($model->form_id == 7) {
				KewpsForm::pdfKewps7(
					$id = $model->inventory_list,
					$year = $model->year
					);
			} else if ($model->form_id == 8) {
				KewpsForm::pdfKewps8(
					$id = $model->inventory_list
					);
			} else if ($model->form_id == 9) {
				KewpsForm::pdfKewps9(
					$id = $model->inventory_list
					);
			} else if ($model->form_id == 10) {
				KewpsForm::pdfKewps10(
					$id = $model->order_id
					);

			} else if ($model->form_id == 11) {
				KewpsForm::pdfKewps11(
					$id = $model->order_id
					);
			} else if ($model->form_id == 13) {
				KewpsForm::pdfKewps13(
					$year = $model->year
					);
			} else if ($model->form_id == 14) {
				KewpsForm::pdfKewps14(
					$id = $model->inventory_list,
					$year = $model->year
					);
			} else if ($model->form_id == 17) {
				KewpsForm::pdfKewps17(
					$id = $model->inventory_list
					);
			} else if ($model->form_id == 18) {
				KewpsForm::pdfKewps18(
					$id = $model->inventory_list
					);
			}
		}
		return $this->render('form-generator', [
			'model' => $model,
			'formId' => $formId,
			'yearList' => $yearList,
			'monthList' => $monthList,
			'ordersArray' => $ordersArray,
			'inventoriesArray' => $inventoriesArray,
			]);
	}
	public function actionFormOptions() {
		$out = [];
		if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
				$form_id = $parents[0];
				if ($form_id == 3) {
					$out = \frontend\modules\store\models\Inventories::find()
					// ->select('id,CODE_NO,CARD_NO,description,QUANTITY,LOCATION')
					->select('id,description as name')
					->asArray()->all();
					// $out  = ArrayHelper::index($out,'id');

				}
				if ($form_id == 4) {
					$out = \frontend\modules\store\models\Order::find()
					->select('id,order_no as name')
					// ->select('ORDERS.id,order_no,ARAHAN_KERJA_ID,APPROVED,VEHICLE_ID')
					// ->with('arahanKerja','vehicle')
					->asArray()->all();
				}
				echo Json::encode(['output' => $out, 'selected' => '']);
				return;
			}
		}
		echo Json::encode(['output' => '', 'selected' => '']);
	}

	public function actionPdfKewps3($id = null) {
		var_dump('note created yet');die();
	}
	public function actionPdfKewps4($id = null) {
		var_dump($id);die();
		KewpsForm::pdfKewps4($id);
	}

	public function actionPdfKewps5($id = null) {
		KewpsForm::pdfKewps5($id);
	}

	public function actionPdfKewps7($id = [145, 144]) {
		KewpsForm::pdfKewps7($id);
	}

	public function actionPdfKewps8($id = null) {
		KewpsForm::pdfKewps8($id);
	}

	public function actionPdfKewps82($id = null) {
		KewpsForm::pdfKewps82($id);
	}
	public function actionPdfKewps9($id = null) {
		KewpsForm::pdfKewps9($id);
	}

	public function actionPdfKewps10($id = null) {
		KewpsForm::pdfKewps10($id);
	}
	public function actionPdfKewps11($id = null) {
		KewpsForm::pdfKewps11($id);
	}

	public function actionPdfKewps13($id = null, $year = null) {
		KewpsForm::pdfKewps13($id);
	}

	public function actionPdfKewps14($id = null) {
		KewpsForm::pdfKewps14($id);
	}

	public function actionPdfKewps17($id = null) {
		KewpsForm::pdfKewps17($id);
	}

	public function actionPdfKewps18($id = null) {
		KewpsForm::pdfKewps18($id);
	}

}
