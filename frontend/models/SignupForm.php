<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use common\models\Profile;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $name;
    public $ic_no;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['ic_no', 'trim'],
            ['ic_no', 'required'],
            ['ic_no', 'unique', 'targetClass' => '\common\models\Profile', 'message' => 'This IC No has already been taken.'],
            ['ic_no', 'string', 'min' => 2, 'max' => 255],

            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if(!$user->save()) {
                return null;
            }

            $profile = new Profile();
            $profile->id = $user->id;
            $profile->name = $this->name;
            $profile->staff_no = $this->username;
            $profile->ic_no = $this->ic_no;
            $profile->email = $this->email;
            if(!$profile->save()) {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception("Error Processing Request", 1);
            return null;
        }

        return $user->save() ? $user : null;
    }
}
