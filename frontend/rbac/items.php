<?php
return [
    'movement_approve' => [
        'type' => 2,
    ],
    'checkin_create' => [
        'type' => 2,
    ],
    'checkin_update' => [
        'type' => 2,
    ],
    'checkin_delete' => [
        'type' => 2,
    ],
    'checkin_delete_permanent' => [
        'type' => 2,
    ],
    'Administrator' => [
        'type' => 1,
    ],
];
