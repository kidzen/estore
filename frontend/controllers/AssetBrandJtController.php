<?php

namespace frontend\controllers;

use Yii;
use common\models\AssetBrandJt;
use common\models\search\AssetBrandJtSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Exception;
use yii\helpers\Url;

/**
 * AssetBrandJtController implements the CRUD actions for AssetBrandJt model.
 */
class AssetBrandJtController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }
    /*
    public function beforeAction($action)
    {
        $toRedir = [
            'update' => 'view',
            'create' => 'view',
            'delete' => 'index',
        ];

        if (isset($toRedir[$action->id])) {
            Yii::$app->response->redirect(Url::to([$toRedir[$action->id]]), 301);
            Yii::$app->end();
        }
        return parent::beforeAction($action);
    }
    */

    /**
     * Lists all AssetBrandJt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AssetBrandJtSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AssetBrandJt model.
     * @param integer $brand_id
     * @param integer $category_id
     * @return mixed
     */
    public function actionView($brand_id, $category_id)
    {
        $model = $this->findModel($brand_id, $category_id);
        return $this->render('view', [
            'model' => $this->findModel($brand_id, $category_id),
        ]);
    }

    /**
     * Creates a new AssetBrandJt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AssetBrandJt();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'brand_id' => $model->brand_id, 'category_id' => $model->category_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AssetBrandJt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $brand_id
     * @param integer $category_id
     * @return mixed
     */
    public function actionUpdate($brand_id, $category_id)
    {
        $model = $this->findModel($brand_id, $category_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'brand_id' => $model->brand_id, 'category_id' => $model->category_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AssetBrandJt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $brand_id
     * @param integer $category_id
     * @return mixed
     */
    public function actionDelete($brand_id, $category_id)
    {
        $this->findModel($brand_id, $category_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
     * Permanently deletes an existing AssetBrandJt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $brand_id
     * @param integer $category_id
     * @return mixed
     */
    public function actionDeletePermanent($brand_id, $category_id)
    {

        $model = $this->findModel($brand_id, $category_id);
        if($model->deleted_by != 0) {
            if($model->delete()) {
                Yii::$app->notify->success();
                return $this->redirect(['index']);
            }
        }
        Yii::$app->notify->fail();
        return $this->redirect(['index']);
    }


    /**
     * Finds the AssetBrandJt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $brand_id
     * @param integer $category_id
     * @return AssetBrandJt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($brand_id, $category_id)
    {
        if (($model = AssetBrandJt::findOne(['brand_id' => $brand_id, 'category_id' => $category_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
