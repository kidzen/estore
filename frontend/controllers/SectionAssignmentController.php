<?php

namespace frontend\controllers;

use Yii;
use common\models\SectionAssignment;
use common\models\search\SectionAssignmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Exception;
use yii\helpers\Url;

/**
 * SectionAssignmentController implements the CRUD actions for SectionAssignment model.
 */
class SectionAssignmentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }
    /*
    public function beforeAction($action)
    {
        $toRedir = [
            'update' => 'view',
            'create' => 'view',
            'delete' => 'index',
        ];

        if (isset($toRedir[$action->id])) {
            Yii::$app->response->redirect(Url::to([$toRedir[$action->id]]), 301);
            Yii::$app->end();
        }
        return parent::beforeAction($action);
    }
    */

    /**
     * Lists all SectionAssignment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SectionAssignmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SectionAssignment model.
     * @param integer $section_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionView($section_id, $user_id)
    {
        $model = $this->findModel($section_id, $user_id);
        return $this->render('view', [
            'model' => $this->findModel($section_id, $user_id),
        ]);
    }

    /**
     * Creates a new SectionAssignment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SectionAssignment();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'section_id' => $model->section_id, 'user_id' => $model->user_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SectionAssignment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $section_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionUpdate($section_id, $user_id)
    {
        $model = $this->findModel($section_id, $user_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'section_id' => $model->section_id, 'user_id' => $model->user_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SectionAssignment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $section_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionDelete($section_id, $user_id)
    {
        $this->findModel($section_id, $user_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
     * Permanently deletes an existing SectionAssignment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $section_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionDeletePermanent($section_id, $user_id)
    {

        $model = $this->findModel($section_id, $user_id);
        if($model->deleted_by != 0) {
            if($model->delete()) {
                Yii::$app->notify->success();
                return $this->redirect(['index']);
            }
        }
        Yii::$app->notify->fail();
        return $this->redirect(['index']);
    }


    /**
     * Finds the SectionAssignment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $section_id
     * @param integer $user_id
     * @return SectionAssignment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($section_id, $user_id)
    {
        if (($model = SectionAssignment::findOne(['section_id' => $section_id, 'user_id' => $user_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
