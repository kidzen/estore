<?php

namespace frontend\controllers;

use Yii;
use common\models\Order;
use common\models\Asset;
use common\models\AssetItem;
use common\models\AssetCheckin;
use common\models\search\AssetSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Exception;
use yii\helpers\Url;

/**
 * AssetController implements the CRUD actions for Asset model.
 */
class RequestController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // 'access' => [
            //     'class' => \yii\filters\AccessControl::className(),
            //     'rules' => [
            //         [
            //             'allow' => true,
            //             'actions' => ['index', 'view', 'create', 'update', 'delete', 'add-asset-checkin', 'add-order-item'],
            //             'roles' => ['@']
            //         ],
            //         [
            //             'allow' => false
            //         ]
            //     ]
            // ]
        ];
    }
    /*
    public function beforeAction($action)
    {
        $toRedir = [
            'update' => 'view',
            'create' => 'view',
            'delete' => 'index',
        ];

        if (isset($toRedir[$action->id])) {
            Yii::$app->response->redirect(Url::to([$toRedir[$action->id]]), 301);
            Yii::$app->end();
        }
        return parent::beforeAction($action);
    }
    */

    /**
     * Creates a new AssetCheckin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCheckin()
    {
        $model = new AssetCheckin();

        if ($model->load(Yii::$app->request->post())) {
            $dbTransac = Yii::$app->db->beginTransaction();
            try {
                $model->save();
                Yii::$app->notify->success();
                $dbTransac->commit();
                return $this->redirect(['/asset-checkin/view', 'id' => $model->id]);
            } catch (Exception $e) {
                Yii::$app->notify->fail($e->getMessage());
                $dbTransac->rollback();
            }
        }

        return $this->render('checkin', [
            'model' => $model,
        ]);
    }
    /**
     * Creates a new AssetCheckin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCheckinSingle()
    {
        $model = new \common\models\AssetItem();
        if ($model->load(Yii::$app->request->post())) {
            $dbTransac = Yii::$app->db->beginTransaction();
            try {
                $model->save();
                Yii::$app->notify->success();
                $dbTransac->commit();
                return $this->redirect(['/asset-checkin/view', 'id' => $model->checkin->id]);
            } catch (Exception $e) {
                Yii::$app->notify->fail($e->getMessage());
                $dbTransac->rollback();
            }
        }

        return $this->render('checkin-single', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new AssetCheckin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionMovement()
    {
        $model = new \common\models\form\MovementForm();

        if ($model->load(Yii::$app->request->post())) {
            if($model->move()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('checkin', [
            'model' => $model,
        ]);
    }

    /**
     * Reject.
     * @param integer orderId
     * @return mixed
     */
    public function actionReject($id)
    {
        Order::rejectTransaction($id);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Approval.
     * @param integer orderId
     * @return mixed
     */
    public function actionApproval($id)
    {
        $order = \common\models\Order::findOne($id);
        $orderLabel['order_date'] = Yii::t('app','Order Date');
        $orderLabel['required_date'] = Yii::t('app','Required Date');
        $orderLabel['orderBy'] = Yii::t('app','Order By');
        $orderLabel['detail'] = Yii::t('app','Detail');
        // foreach ($order->orderItems as $key => $value) {
        //     # code...
        // }
        $assetIds = \yii\helpers\ArrayHelper::map($order->orderItems,'id','asset_id');
        // var_dump($assetIds);die;
        $queryObject = \common\models\AssetItem::find();
        $query1 = clone $queryObject;
        $query1->joinWith('asset');
        $query1->joinWith('orderItem');
        $query1->checkouted();
        // $query1->where(['asset_item.deleted_by' => 0]);
        // $query1->andWhere(['asset_item.dispose_id' => null]);
        $query1->andWhere(['in','asset.id', $assetIds]);
        $query1->andWhere(['in','order_id', $order->id]);

        $query2 = clone $queryObject;
        $query2->joinWith('asset');
        $query2->andWhere(['in','asset.id', $assetIds]);
        $query2->andWhere(['order_item_id' => null]);
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query1,
        ]);
        $dataProvider2 = new \yii\data\ActiveDataProvider([
            'query' => $query2,
        ]);

        if(Yii::$app->request->post()) {
            if(Order::approveTransaction($orderId)) {
                return $this->redirect(['checkout-view', 'id' => $orderId]);
            }
            return $this->redirect(Yii::$app->request->refferer);
        }

        // $dataProvider2->query->andWhere('in','INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' , $orderItemsId);
        // var_dump($dataProvider2->models[0]->attributes);die();

        if (\Yii::$app->request->post('smart-approve')) {
            $dbtransac = \Yii::$app->db->beginTransaction();
            try {
                if ($order && $orderItems && $inventory && $inventoryItems) {
                    //assign checkout id to items(link)
                    $inventoryItems->checkout_transaction_id = $orderItemsId;
                    //remove 1 items from inventory
                    $inventory->quantity = $inventory->quantity - 1;
                    //add 1 item to approved quantity
                    $orderItems->app_quantity = $orderItems->app_quantity + 1;
                    //update total price of checkout items
                    $orderItems->unit_price = $orderItems->unit_price + $inventoryItems->unit_price;
                    //save all model
                    if (!$inventoryItems->save(false))
                        Throw new Exception(' Data item inventori tidak dapat dikemaskini.');
                    if (!$inventory->save(false))
                        Throw new Exception(' Data inventori tidak dapat dikemaskini.');
                    if (!$orderItems->save(false))
                        Throw new Exception(' Data pesanan tidak dapat dikemaskini.');
                    $dbtransac->commit();
                    \Yii::$app->notify->success(['message' => 'Proses kemaskini data berjaya']);
                } else {
                    throw new Exception(' Data tidak wujud.');
                }
            } catch (Exception $e) {
                \Yii::$app->notify->fail(['message' => $e->getMessage()]);
                $dbtransac->rollBack();
            } catch (\yii\db\Exception $e) {
                \Yii::$app->notify->fail(['message' => $e->getMessage()]);
                $dbtransac->rollBack();
            }
        }
        return $this->render('approval', [
            // 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            // 'searchModel2' => $searchModel2,
            'dataProvider2' => $dataProvider2,
            // 'orderId' => $orderId,
            'order' => $order,
            'orderLabel' => $orderLabel,
        ]);
    }

    public function actionCancel($id) {

        AssetItem::cancel($id);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionApprove($id, $orderId) {
        AssetItem::approve($id, $orderId);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionApproveTransaction($orderId)
    {
        if(Order::approveTransaction($orderId)) {
            return $this->redirect(['/order/view', 'id' => $orderId]);
        }
        return $this->redirect(Yii::$app->request->refferer);
    }
}
