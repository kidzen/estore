<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\UsageSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-usage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'category_id')->textInput(['placeholder' => 'Category']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'item_id')->textInput(['placeholder' => 'Item']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'reason')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
