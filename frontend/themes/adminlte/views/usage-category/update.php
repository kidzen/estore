<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UsageCategory */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Usage Category',
]) . ' ' . $model->name;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usage Category'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="usage-category-update">
	<div class="panel panel-danger">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
