<?php

use yii\bootstrap\Modal;
use kartik\widgets\Growl;
use kartik\helpers\Html;
use kartik\grid\GridView;
?>
<div class="site-index">
    <section class="content-header">
        <h1>
            <?= Yii::t('app','Dashboard') ?>
            <small><?= Yii::t('app','Version '. Yii::$app->params['version']) ?></small>
        </h1>
        <!--        <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Info boxes -->
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua-gradient">
                            <div class="inner">
                                <h3><?= $data[0]['value'] ?? 0 ?></h3>

                                <p><?= $data[0]['description'] ?></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <?= Html::a('More info <i class="fa fa-arrow-circle-right"></i>',$data[0]['url'],['class' => 'small-box-footer']) ?>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green-gradient">
                            <div class="inner">
                                <h3><?= $data[1]['value'] ?? 0 ?></h3>
                                <p><?= $data[1]['description'] ?></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <?= Html::a('More info <i class="fa fa-arrow-circle-right"></i>',$data[1]['url'],['class' => 'small-box-footer']) ?>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow-gradient">
                            <div class="inner">
                                <h3>RM <?= $data[2]['value'] ?? 0 ?></h3>

                                <p><?= $data[2]['description'] ?></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <?= Html::a('More info <i class="fa fa-arrow-circle-right"></i>',$data[2]['url'],['class' => 'small-box-footer']) ?>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red-gradient">
                            <div class="inner">
                                <h3><?= $data[3]['value'] ?></h3>
                                <p><?= $data[3]['description'] ?></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <?= Html::a('More info <i class="fa fa-arrow-circle-right"></i>',$data[3]['url'],['class' => 'small-box-footer']) ?>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>

                <!-- /.row -->
                <div class="row">
                <div class="col-md-12">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider[0],
                        'hover' => true,
                        'panel' => [
                            'type'=>'danger',
                            'heading' => '<span class="glyphicon glyphicon-book"></span>  Asset' ,
                            'before' =>  '<div style="padding-top: 7px;"><em>* The table header sticks to the top in this demo as you scroll</em></div>',
                            'after' => false,
                        ],
                        'columns' => [
                            'section.name',
                            [
                                'attribute' => 'serial_no',
                                'format' => 'raw',
                                'value' => function($model) {
                                    $return[0] = $model->stock_no ? 'Stock No : '. $model->stock_no : '';
                                    $return[1] = $model->serial_no ? 'Serial No : '. $model->serial_no : '';
                                    return $return[0]. '<br>' . $return[1];
                                }
                            ],
                            'checkin.check_date',
                            'unit_price',
                        ]
                    ]) ?>
                </div>
                </div>


            </section>

        </div>
