<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
?>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b><?= Yii::$app->params['appNameShort'] ?></b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Please fill out the following fields to signup:</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form->field($model, 'email',$fieldOptions3)
        // ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>


        <?= $form
        ->field($model, 'username', $fieldOptions1)
        // ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
        ->field($model, 'name', $fieldOptions1)
        // ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('name')]) ?>

        <?= $form
        ->field($model, 'ic_no', $fieldOptions1)
        // ->label(false)
        ->textInput(['placeholder' => $model->getAttributeLabel('ic_no')]) ?>

        <?= $form
        ->field($model, 'password', $fieldOptions2)
        // ->label(false)
        ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Sign up', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>

        <?= Html::a('I forgot my password',['/site/request-password-reset']) ?><br>
        <?= Html::a('Already register',['/site/login']) ?>

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->

