<?php
$dbName = \common\components\Migration::getDbName(Yii::$app->db);
switch (Yii::$app->db->driverName) {
    case 'mysql':
    $script = "SELECT table_name as TABLE_NAME
    FROM information_schema.tables
    WHERE table_schema = '$dbName'";
    break;
    case 'oci':
    $script = "
    SELECT
    TABLE_NAME
    FROM USER_TABLES
    UNION ALL
    SELECT
    VIEW_NAME AS TABLE_NAME
    FROM USER_VIEWS
    UNION ALL
    SELECT
    MVIEW_NAME AS TABLE_NAME
    FROM USER_MVIEWS
    ORDER BY TABLE_NAME
    ";

    break;
    default:
    $script = false;
    break;
}
if($script){
    $query = Yii::$app->db->createCommand($script)->queryAll();
    foreach ($query as $key => $value) {
        $url = '/'.strtr($value['TABLE_NAME'], '_', '-');
        $label = strtr($value['TABLE_NAME'], '_', ' ');
        $items[] = ['label' => $label, 'url' => [$url]];
    }
}
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div style="text-align: center">
                <img src="<?= Yii::$app->params['leftMenuImg'] ?>" style="width: 90%" alt="logo-sb-pdrm"/>
            </div>
        </div>

        <!-- search form -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
    -->        <!-- /.search form -->
    <?= dmstr\widgets\Menu::widget(
        [
            'options' => ['class' => 'sidebar-menu tree delay', 'data-widget'=> 'tree'],
            'items' => [
                [
                    'label' => Yii::t('app','Login'), 'url' => ['/site/login'],
                    'visible' => Yii::$app->user->isGuest
                ],
                ['label' => Yii::t('app','Request'), 'options' => ['class' => 'header']],
                [
                    'label' => Yii::t('app','Check In'),
                    // 'visible' => Yii::$app->user->can('manage-asset'),
                    'items' => [
                        ['label' => Yii::t('app','Checkin'), 'icon' => 'file-code-o', 'url' => ['/asset-checkin/create'],'visible' => Yii::$app->user->can('manage-asset-checkin')],
                        ['label' => Yii::t('app','Checkin List'), 'icon' => 'file-code-o', 'url' => ['/asset-checkin/index'],'visible' => Yii::$app->user->can('manage-asset-checkin')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Direct Issue'),
                    'visible' => Yii::$app->user->can('manage-direct-issue'),
                    'items' => [
                        ['label' => Yii::t('app','Direct Issue'), 'icon' => 'file-code-o', 'url' => ['/direct-issue/create'],'visible' => Yii::$app->user->can('manage-direct-issue')],
                        ['label' => Yii::t('app','Direct Issue List'), 'icon' => 'file-code-o', 'url' => ['/direct-issue/index'],'visible' => Yii::$app->user->can('manage-direct-issue')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Movement'),
                    // 'visible' => Yii::$app->user->can('admin'),
                    'items' => [
                        ['label' => Yii::t('app','Placement'), 'icon' => 'file-code-o', 'url' => ['/item-movement/move-asset2'],'visible' => Yii::$app->user->can('manage-asset-movement')],
                        ['label' => Yii::t('app','Movement List'), 'icon' => 'file-code-o', 'url' => ['/order/movement-index'],'visible' => Yii::$app->user->can('manage-asset-movement')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Disposal'),
                    // 'visible' => Yii::$app->user->can('admin'),
                    'items' => [
                        ['label' => Yii::t('app','Dispose'), 'icon' => 'file-code-o', 'url' => ['/dispose/create'],'visible' => Yii::$app->user->can('manage-asset-disposal')],
                        ['label' => Yii::t('app','Dispose List'), 'icon' => 'file-code-o', 'url' => ['/dispose/index'],'visible' => Yii::$app->user->can('manage-asset-disposal')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Request'),
                    'items' => [
                        // ['label' => Yii::t('app','Checkin'), 'icon' => 'file-code-o', 'url' => ['/request/checkin']],
                        ['label' => Yii::t('app','Asset Master'), 'icon' => 'file-code-o', 'url' => ['/asset/create'],'visible' => Yii::$app->user->can('manage-asset')],
                        // ['label' => Yii::t('app','Budget'), 'icon' => 'file-code-o', 'url' => ['/budget/create']],
                        ['label' => Yii::t('app','Checkin'), 'icon' => 'file-code-o', 'url' => ['/asset-checkin/create'],'visible' => Yii::$app->user->can('manage-asset-checkin')],
                        ['label' => Yii::t('app','Direct Issue'), 'icon' => 'file-code-o', 'url' => ['/direct-issue/create'],'visible' => Yii::$app->user->can('manage-direct-issue')],
                        ['label' => Yii::t('app','Movement'), 'icon' => 'file-code-o', 'url' => ['/item-movement/move-asset'],'visible' => Yii::$app->user->can('manage-asset-movement')],
                        ['label' => Yii::t('app','Dispose'), 'icon' => 'file-code-o', 'url' => ['/dispose/create'],'visible' => Yii::$app->user->can('manage-asset-disposal')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Record'),
                    'visible' => Yii::$app->user->can('admin'),
                    'items' => [
                        ['label' => Yii::t('app','Asset'), 'icon' => 'file-code-o', 'url' => ['/asset-item/index'],'visible' => Yii::$app->user->can('manage-asset-checkin')],
                        // ['label' => Yii::t('app','Budget'), 'icon' => 'file-code-o', 'url' => ['/budget/index']],
                        ['label' => Yii::t('app','Direct Issue'), 'icon' => 'file-code-o', 'url' => ['/direct-issue/index'],'visible' => Yii::$app->user->can('manage-direct-issue')],
                        ['label' => Yii::t('app','Movement'), 'icon' => 'file-code-o', 'url' => ['/item-movement/index'],'visible' => Yii::$app->user->can('manage-asset-movement')],
                        ['label' => Yii::t('app','Dispose'), 'icon' => 'file-code-o', 'url' => ['/dispose/index'],'visible' => Yii::$app->user->can('manage-asset-disposal')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                ['label' => Yii::t('app','Admin Section'), 'options' => ['class' => 'header']],
                [
                    'label' => Yii::t('app','Asset Management'),
                    // 'visible' => Yii::$app->user->can('admin'),
                    'items' => [
                        ['label' => Yii::t('app','Asset Master'), 'icon' => 'file-code-o', 'url' => ['/asset/index'],'visible' => Yii::$app->user->can('manage-asset')],
                        ['label' => Yii::t('app','Asset'), 'icon' => 'file-code-o', 'url' => ['/asset-item/index'],'visible' => Yii::$app->user->can('manage-asset-checkin')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','List Management'),
                    'visible' => Yii::$app->user->can('manage-asset'),
                    'items' => [
                        ['label' => Yii::t('app','Brand'), 'icon' => 'file-code-o', 'url' => ['/asset-brand/index']],
                        ['label' => Yii::t('app','Model'), 'icon' => 'file-code-o', 'url' => ['/asset-model/index']],
                        ['label' => Yii::t('app','Asset Category'), 'icon' => 'file-code-o', 'url' => ['/asset-category/index']],
                        ['label' => Yii::t('app','Direct Issue Type'), 'icon' => 'file-code-o', 'url' => ['/direct-issue-type/index']],
                        ['label' => Yii::t('app','Dispose Category'), 'icon' => 'file-code-o', 'url' => ['/dispose-category/index']],
                        ['label' => Yii::t('app','Dispose Method'), 'icon' => 'file-code-o', 'url' => ['/dispose-method/index']],
                        ['label' => Yii::t('app','Dispose Reason'), 'icon' => 'file-code-o', 'url' => ['/dispose-reason/index']],
                        ['label' => Yii::t('app','Movement Type'), 'icon' => 'file-code-o', 'url' => ['/item-movement-type/index']],
                        ['label' => Yii::t('app','Supplier'), 'icon' => 'file-code-o', 'url' => ['/supplier/index']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','System Management'),
                    'visible' => Yii::$app->user->can('admin'),
                    'items' => [
                        ['label' => Yii::t('app','Users'), 'icon' => 'file-code-o', 'url' => ['/user/index']],
                        ['label' => Yii::t('app','Section Assignment'), 'icon' => 'file-code-o', 'url' => ['/section-assignment/index']],
                        ['label' => Yii::t('app','My Profile'), 'icon' => 'file-code-o', 'url' => ['/profile/index']],
                        ['label' => Yii::t('app','Access'), 'icon' => 'file-code-o', 'url' => ['/admin/assignment']],
                        // ['label' => Yii::t('app','Activity Log'), 'icon' => 'file-code-o', 'url' => ['/activity-log/index']],
                        // ['label' => Yii::t('app','System Maintenance'), 'icon' => 'file-code-o', 'url' => ['/system/maintenance']],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => Yii::t('app','Report'),
                    // 'visible' => Yii::$app->user->can('admin'),
                    'items' => [
                        ['label' => Yii::t('app','Direct Issue Report'), 'icon' => 'file-code-o', 'url' => ['/user/index'],'visible' => Yii::$app->user->can('manage-direct-issue')],
                        ['label' => Yii::t('app','Asset Registration Report'), 'icon' => 'file-code-o', 'url' => ['/profile/index'],'visible' => Yii::$app->user->can('manage-asset-checkin')],
                        ['label' => Yii::t('app','Asset Movement Report'), 'icon' => 'file-code-o', 'url' => ['/profile/index'],'visible' => Yii::$app->user->can('manage-asset-movement')],
                        ['label' => Yii::t('app','Dispose Report'), 'icon' => 'file-code-o', 'url' => ['/profile/index'],'visible' => Yii::$app->user->can('manage-asset-disposal')],
                    ],
                    // 'visible' => Yii::$app->user->isGuest,
                ],
                ['label' => Yii::t('app','Module Developer'), 'options' => ['class' => 'header'],'visible' => Yii::$app->user->can('superadmin')],
                [
                    'label' => Yii::t('app','Developer'), 'icon' => 'file-code-o',
                    'items' => [
                        ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                        ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                        [
                            'label' => 'Under Development', 'icon' => 'file-code-o',
                            'items' => [
                                ['label' => Yii::t('app','Workshop Report'), 'icon' => 'file-code-o', 'url' => ['/workshop-usage/index']],
                                ['label' => Yii::t('app','Store Report'), 'icon' => 'file-code-o', 'url' => ['/store-usage/index']],
                            ]
                        ],
                        [
                            'label' => 'Database', 'icon' => 'database',
                            'items' => $items
                        ],
                    ],
                ],
            ],
        ]
        ) ?>

    </section>

</aside>
