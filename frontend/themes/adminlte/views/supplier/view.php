<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Supplier */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Supplier'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supplier-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Supplier').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'address',
        'contact_no',
        'email:email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

        <div class="col-sm-12">
<?php
if($providerAssetCheckin->totalCount){
    $gridColumnAssetCheckin = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'rnvr',
            [
                'attribute' => 'budget.name',
                'label' => Yii::t('app', 'Budget')
            ],
            [
                'attribute' => 'asset.description',
                'label' => Yii::t('app', 'Asset')
            ],
                        'check_date',
            'purchased_date',
            'refference_file',
            'detail:ntext',
            [
                'attribute' => 'approved.name',
                'label' => Yii::t('app', 'Approved')
            ],
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAssetCheckin,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-asset-checkin']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Asset Checkin')),
        ],
        'export' => false,
        'columns' => $gridColumnAssetCheckin
    ]);
}
?>

        </div>

        <div class="col-sm-12">
<?php
if($providerDirectIssue->totalCount){
    $gridColumnDirectIssue = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'ivr',
            [
                'attribute' => 'type.name',
                'label' => Yii::t('app', 'Type')
            ],
            'refference_no',
            'particulars:ntext',
            'amount',
            'total',
                        'officer_name',
            'date',
            'receipt_no',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDirectIssue,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-direct-issue']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Direct Issue')),
        ],
        'export' => false,
        'columns' => $gridColumnDirectIssue
    ]);
}
?>

        </div>
    <?php if ($model->createdBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email:email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->updatedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email:email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    </div>
    </div>
    </div>
    </div>
</div>
