<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SectionAssignment */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Section Assignment',
]) . ' ' . $model->section_id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Section Assignment'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->section_id, 'url' => ['view', 'section_id' => $model->section_id, 'user_id' => $model->user_id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="section-assignment-update">
	<div class="panel panel-danger">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
