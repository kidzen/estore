<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SectionAssignment */

$this->title = Yii::t('app', 'Create Section Assignment');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Section Assignment'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-assignment-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
