<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Dispose */

?>
<div class="dispose-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->description) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'refference_no',
        [
            'attribute' => 'category.name',
            'label' => Yii::t('app', 'Category'),
        ],
        [
            'attribute' => 'method.name',
            'label' => Yii::t('app', 'Method'),
        ],
        [
            'attribute' => 'reason.name',
            'label' => Yii::t('app', 'Reason'),
        ],
        'quantity',
        'current_revenue',
        'cost',
        'returns',
        'description',
        'requested_date',
        [
            'attribute' => 'requestedBy.username',
            'label' => Yii::t('app', 'Requested By'),
        ],
        'disposed_date',
        [
            'attribute' => 'disposedBy.username',
            'label' => Yii::t('app', 'Disposed By'),
        ],
        [
            'attribute' => 'approved.name',
            'label' => Yii::t('app', 'Approved'),
        ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
