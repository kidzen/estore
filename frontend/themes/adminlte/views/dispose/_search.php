<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\DisposeSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-dispose-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'refference_no')->textInput(['maxlength' => true, 'placeholder' => 'Refference No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'category_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeCategory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\DisposeCategory::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose category')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'method_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeMethod::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\DisposeMethod::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose method')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'reason_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeReason::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\DisposeReason::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose reason')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'quantity')->textInput(['placeholder' => 'Quantity']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'current_revenue')->textInput(['placeholder' => 'Current Revenue']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'cost')->textInput(['placeholder' => 'Cost']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'returns')->textInput(['placeholder' => 'Returns']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'requested_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Requested Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'requested_by')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'data' => \common\models\User::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'disposed_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Disposed Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'disposed_by')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'data' => \common\models\User::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'approved_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\ApproveLevel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\ApproveLevel::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Approve level')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
