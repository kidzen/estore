<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Dispose */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispose'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dispose-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Dispose').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'refference_no',
        [
            'attribute' => 'category.name',
            'label' => Yii::t('app', 'Category'),
        ],
        [
            'attribute' => 'method.name',
            'label' => Yii::t('app', 'Method'),
        ],
        [
            'attribute' => 'reason.name',
            'label' => Yii::t('app', 'Reason'),
        ],
        'quantity',
        'current_revenue',
        'cost',
        'returns',
        'description',
        'requested_date',
        [
            'attribute' => 'requestedBy.username',
            'label' => Yii::t('app', 'Requested By'),
        ],
        'disposed_date',
        [
            'attribute' => 'disposedBy.username',
            'label' => Yii::t('app', 'Disposed By'),
        ],
        [
            'attribute' => 'approved.name',
            'label' => Yii::t('app', 'Approved'),
        ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

        <div class="col-sm-12">
<?php
if($providerAssetItem->totalCount){
    $gridColumnAssetItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'section.name',
                'label' => Yii::t('app', 'Section')
            ],
            [
                'attribute' => 'checkin.id',
                'label' => Yii::t('app', 'Checkin')
            ],
            [
                'attribute' => 'orderItem.id',
                'label' => Yii::t('app', 'Order Item')
            ],
                        'serial_no',
            'stock_no',
            'unit_price',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAssetItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-asset-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Asset Item')),
        ],
        'export' => false,
        'columns' => $gridColumnAssetItem
    ]);
}
?>

        </div>

        <div class="col-sm-12">
<?php
if($providerDirectIssueItem->totalCount){
    $gridColumnDirectIssueItem = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'directIssue.id',
                'label' => Yii::t('app', 'Direct Issue')
            ],
                        'serial_no',
            'unit_price',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDirectIssueItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-direct-issue-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Direct Issue Item')),
        ],
        'export' => false,
        'columns' => $gridColumnDirectIssueItem
    ]);
}
?>

        </div>
    <?php if ($model->approved) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">ApproveLevel</h4>
    <?php 
    $gridColumnApproveLevel = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->approved,
        'attributes' => $gridColumnApproveLevel    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->transaction) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">Transaction</h4>
    <?php 
    $gridColumnTransaction = [
        ['attribute' => 'id', 'visible' => false],
        'type_id',
        'section_id',
        'check_date',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->transaction,
        'attributes' => $gridColumnTransaction    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->category) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">DisposeCategory</h4>
    <?php 
    $gridColumnDisposeCategory = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->category,
        'attributes' => $gridColumnDisposeCategory    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->method) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">DisposeMethod</h4>
    <?php 
    $gridColumnDisposeMethod = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->method,
        'attributes' => $gridColumnDisposeMethod    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->reason) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">DisposeReason</h4>
    <?php 
    $gridColumnDisposeReason = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->reason,
        'attributes' => $gridColumnDisposeReason    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->requestedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->requestedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->disposedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->disposedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->approvedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->approvedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->createdBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->updatedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>

        <div class="col-sm-12">
<?php
if($providerDisposeDetailValue->totalCount){
    $gridColumnDisposeDetailValue = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'key.description',
                'label' => Yii::t('app', 'Key')
            ],
            'value',
            ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDisposeDetailValue,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-dispose-detail-value']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Dispose Detail Value')),
        ],
        'export' => false,
        'columns' => $gridColumnDisposeDetailValue
    ]);
}
?>

        </div>
    </div>
    </div>
    </div>
    </div>
</div>
