<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AssetModel */

$this->title = Yii::t('app', 'Create Asset Model');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asset Model'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-model-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
