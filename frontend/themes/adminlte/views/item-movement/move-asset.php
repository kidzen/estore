<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ItemMovement */

$this->title = Yii::t('app', 'Item Movement');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Item Movement'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-movement-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
            <div class="item-movement-form">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->errorSummary($model); ?>

                <div class="col-md-4">
                    <?= $form->field($model, 'order_no')->textInput(['maxlength' => true, 'placeholder' => 'Order No', 'readOnly' => true, 'value' => $model->generateBaucer()]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'movement_type')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\ItemMovementType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                        'options' => ['placeholder' => Yii::t('app', 'Choose Item movement type')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-4">
                    <?= $form->field($model, 'asset_item_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \common\models\AssetItem::arrayList(),
                        'options' => ['id'=>'asset-item-id', 'placeholder' => Yii::t('app', 'Choose Asset item')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'section_id')->widget(\kartik\widgets\DepDrop::classname(), [
                        'type'=>\kartik\widgets\DepDrop::TYPE_SELECT2,
                        'options' => ['id'=>'brand-id', 'placeholder' => Yii::t('app', 'Choose Section')],
                        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                        'pluginOptions'=>[
                    // 'initialize'=>true,
                            'allowClear' => true,
                            'depends'=>['asset-item-id'],
                            'url' => \yii\helpers\Url::to(['/item-movement/section-list','select'=>'id,name'])
                        ],
                    ])->label(Yii::t('app','Section'));
                    ?>

                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'instruction_id')->textInput(['placeholder' => 'Instruction']) ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <?= $form->field($model, 'required_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                        'saveFormat' => 'php:Y-m-d',
                        'ajaxConversion' => true,
                        'options' => [
                            'pluginOptions' => [
                                'placeholder' => Yii::t('app', 'Choose Required Date'),
                                'autoclose' => true
                            ]
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'order_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                        'saveFormat' => 'php:Y-m-d',
                        'ajaxConversion' => true,
                        'options' => [
                            'pluginOptions' => [
                                'placeholder' => Yii::t('app', 'Choose Order Date'),
                                'autoclose' => true
                            ]
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'ordered_by')->textInput(['placeholder' => 'Ordered By']) ?>
                </div>


                <div class="clearfix"></div>

                <div class="col-md-4">
                    <?= $form->field($model, 'checkout_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                        'saveFormat' => 'php:Y-m-d',
                        'ajaxConversion' => true,
                        'options' => [
                            'pluginOptions' => [
                                'placeholder' => Yii::t('app', 'Choose Checkout Date'),
                                'autoclose' => true
                            ]
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'checkout_by')->textInput(['placeholder' => 'Checkout By']) ?>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-success']) ?>
                        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>		</div>
        </div>
    </div>
