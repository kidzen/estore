<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form kartik\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'OrderItem',
        'relID' => 'order-item',
        'value' => \yii\helpers\Json::encode($model->orderItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

$this->title = Yii::t('app', 'Item Movement');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Order'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="panel-body">
            <div class="order-form">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->errorSummary($model); ?>

                <div class="col-md-4">
                    <?= $form->field($model, 'order_no')->textInput(['maxlength' => true, 'placeholder' => 'Order No', 'readOnly' => true, 'value' => $model->generateBaucer()]) ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'instruction_id')->textInput(['placeholder' => 'Instruction']) ?>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-4">
                    <?= $form->field($itemMovement, 'type_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\ItemMovementType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                        'data' => \common\models\ItemMovementType::arrayList(),
                        'options' => ['placeholder' => Yii::t('app', 'Choose Item movement type')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($itemMovement, 'section_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(
                            \common\models\Section::find()
                            ->andWhere('id != '. \Yii::$app->user->activeSection['id'])->orderBy('id')->asArray()->all(), 'id', 'name'),
                        // 'data' => \common\models\Section::arrayList(\common\models\Section::find()->orderBy('id')->andWhere(['not',['id' => \Yii::$app->user->activeSection]])->asArray()->all()),
                        'options' => ['placeholder' => Yii::t('app', 'Choose Section')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-4">
                    <?= $form->field($model, 'order_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                        'saveFormat' => 'php:Y-m-d',
                        'ajaxConversion' => true,
                        'options' => [
                            'pluginOptions' => [
                                'placeholder' => Yii::t('app', 'Choose Order Date'),
                                'autoclose' => true
                            ]
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'ordered_by')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
                        'data' => \common\models\User::arrayList(),
                        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'required_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                        'saveFormat' => 'php:Y-m-d',
                        'ajaxConversion' => true,
                        'options' => [
                            'pluginOptions' => [
                                'placeholder' => Yii::t('app', 'Choose Required Date'),
                                'autoclose' => true
                            ]
                        ],
                    ]);
                    ?>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-4">
                    <?= $form->field($model, 'checkout_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                        'saveFormat' => 'php:Y-m-d',
                        'ajaxConversion' => true,
                        'options' => [
                            'pluginOptions' => [
                                'placeholder' => Yii::t('app', 'Choose Checkout Date'),
                                'autoclose' => true
                            ]
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'checkout_by')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
                        'data' => \common\models\User::arrayList(),
                        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'approved_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\ApproveLevel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                        'data' => \common\models\ApproveLevel::arrayList(),
                        'options' => ['placeholder' => Yii::t('app', 'Choose Approve level')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12">
                    <?php
                    $forms = [
                        [
                            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'OrderItem')),
                            'content' => $this->render('move-asset-item', [
                                'row' => \yii\helpers\ArrayHelper::toArray($model->orderItems),
                            ]),
                        ],
                    ];
                    echo kartik\tabs\TabsX::widget([
                        'items' => $forms,
                        'position' => kartik\tabs\TabsX::POS_ABOVE,
                        'encodeLabels' => false,
                        'pluginOptions' => [
                            'bordered' => true,
                            'sideways' => true,
                            'enableCache' => false,
                        ],
                    ]);
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>
</div>

