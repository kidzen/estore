<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ReportAllSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\helpers\Html;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Report All');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="report-index">

	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'KEWPA Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Generate KEWPA Form report base on preference through, years, month, inventory details or orders number.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/report/kewps'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Inventory Frequency Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Generate report for inventory frequency usage for current store.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/report/frequency'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Transaction Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Review all transaction check in and check out made through current store.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/transaction/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Yearly Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Review yearly report on stock position base on formula from KEWPA 14. (Stock Positioning Report)') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/report/yearly'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'System Maintenance Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Report description') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/all-maintenance-check/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Usage Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Review all fault and maintenance to be done for current store value.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/report/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Audit Log Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Review audit log.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/log/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Store Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Consist of all transaction and detail for all store.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/store/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Disposal Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Consist of all dispose transaction and detail for current store.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/dispose/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Vendor Usage Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Consist of all dispose transaction and detail for current store.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/dispose/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Vehicle Usage Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Consist of all dispose transaction and detail for current store.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/dispose/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Workshop Usage Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Consist of all dispose transaction and detail for current store.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/dispose/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><?= Yii::t('app', 'Budget																							 Report') ?></div>
				<div class="panel-body">
					<div class="text-center">
						<?= Yii::t('app', 'Consist of all dispose transaction and detail for current store.') ?>

						<div style="padding-top: 10px">
							<?= Html::a('Generate',['/dispose/index'],['class'=>'btn btn-primary']) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
