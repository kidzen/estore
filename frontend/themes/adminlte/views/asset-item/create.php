<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AssetItem */

$this->title = Yii::t('app', 'Create Asset Item');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asset Item'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-item-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
