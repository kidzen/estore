<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AssetItem */

?>
<div class="asset-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'section.name',
            'label' => Yii::t('app', 'Section'),
        ],
        [
            'attribute' => 'checkin.id',
            'label' => Yii::t('app', 'Checkin'),
        ],
        [
            'attribute' => 'orderItem.id',
            'label' => Yii::t('app', 'Order Item'),
        ],
        [
            'attribute' => 'dispose.description',
            'label' => Yii::t('app', 'Dispose'),
        ],
        'serial_no',
        'stock_no',
        'unit_price',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
