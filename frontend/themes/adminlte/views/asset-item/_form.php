<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AssetItem */
/* @var $form kartik\widgets\ActiveForm */

?>

<div class="asset-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'section_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Section::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\Section::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Section')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'checkin_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\AssetCheckin::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'data' => \common\models\AssetCheckin::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Asset checkin')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'order_item_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\OrderItem::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'data' => \common\models\OrderItem::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Order item')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'dispose_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Dispose::find()->orderBy('id')->asArray()->all(), 'id', 'description'),
        'data' => \common\models\Dispose::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'serial_no')->textInput(['maxlength' => true, 'placeholder' => 'Serial No']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'stock_no')->textInput(['maxlength' => true, 'placeholder' => 'Stock No']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'unit_price')->textInput(['maxlength' => true, 'placeholder' => 'Unit Price']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
</div>

    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
