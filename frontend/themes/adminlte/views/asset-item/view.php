<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AssetItem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asset Item'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-item-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Asset Item').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'section.name',
            'label' => Yii::t('app', 'Section'),
        ],
        [
            'attribute' => 'checkin.id',
            'label' => Yii::t('app', 'Checkin'),
        ],
        [
            'attribute' => 'orderItem.id',
            'label' => Yii::t('app', 'Order Item'),
        ],
        [
            'attribute' => 'dispose.description',
            'label' => Yii::t('app', 'Dispose'),
        ],
        'serial_no',
        'stock_no',
        'unit_price',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <?php if ($model->checkin) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">AssetCheckin</h4>
    <?php 
    $gridColumnAssetCheckin = [
        ['attribute' => 'id', 'visible' => false],
        'rnvr',
        'budget_id',
        'asset_id',
        'supplier_id',
        'check_date',
        'purchased_date',
        'refference_file',
        'detail',
        'approved_id',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->checkin,
        'attributes' => $gridColumnAssetCheckin    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->section) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">Section</h4>
    <?php 
    $gridColumnSection = [
        ['attribute' => 'id', 'visible' => false],
        'category_id',
        'name',
        'address',
        'contact_no',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->section,
        'attributes' => $gridColumnSection    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->orderItem) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">OrderItem</h4>
    <?php 
    $gridColumnOrderItem = [
        ['attribute' => 'id', 'visible' => false],
        'asset_id',
        'order_id',
        'rq_quantity',
        'app_quantity',
        'current_balance',
        'total_price',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->orderItem,
        'attributes' => $gridColumnOrderItem    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->dispose) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">Dispose</h4>
    <?php 
    $gridColumnDispose = [
        ['attribute' => 'id', 'visible' => false],
        'refference_no',
        'category_id',
        'method_id',
        'reason_id',
        'quantity',
        'current_revenue',
        'cost',
        'returns',
        'description',
        'requested_date',
        'requested_by',
        'disposed_date',
        'disposed_by',
        'approved_id',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->dispose,
        'attributes' => $gridColumnDispose    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->createdBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->updatedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    </div>
    </div>
    </div>
    </div>
</div>
