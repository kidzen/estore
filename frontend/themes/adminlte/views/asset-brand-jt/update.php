<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AssetBrandJt */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Asset Brand Jt',
]) . ' ' . $model->brand_id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asset Brand Jt'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->brand_id, 'url' => ['view', 'brand_id' => $model->brand_id, 'category_id' => $model->category_id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asset-brand-jt-update">
	<div class="panel panel-danger">
		<div class="panel-heading">
		    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
