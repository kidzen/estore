<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DirectIssue */
/* @var $form kartik\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'DirectIssueItem',
        'relID' => 'direct-issue-item',
        'value' => \yii\helpers\Json::encode($model->directIssueItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="direct-issue-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'ivr')->textInput(['maxlength' => true, 'placeholder' => 'Ivr', 'readOnly' => true, 'value' => $model->generateBaucer()]) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'type_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\DirectIssueType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\DirectIssueType::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Direct issue type')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'refference_no')->textInput(['maxlength' => true, 'placeholder' => 'Refference No']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'particulars')->textarea(['rows' => 6]) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'amount')->textInput(['placeholder' => 'Amount']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'total')->textInput(['placeholder' => 'Total']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'supplier_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Supplier::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\Supplier::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Supplier')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'officer_name')->textInput(['maxlength' => true, 'placeholder' => 'Officer Name']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'receipt_no')->textInput(['maxlength' => true, 'placeholder' => 'Receipt No']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
</div>

    <div class="clearfix"></div>
<div class="col-md-12">
    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'DirectIssueItem')),
            'content' => $this->render('_formDirectIssueItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->directIssueItems),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
</div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
