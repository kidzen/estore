<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DirectIssue */

?>
<div class="direct-issue-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'ivr',
        [
            'attribute' => 'type.name',
            'label' => Yii::t('app', 'Type'),
        ],
        'refference_no',
        'particulars:ntext',
        'amount',
        'total',
        [
            'attribute' => 'supplier.name',
            'label' => Yii::t('app', 'Supplier'),
        ],
        'officer_name',
        'date',
        'receipt_no',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
