<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\DirectIssueSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-direct-issue-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'ivr')->textInput(['maxlength' => true, 'placeholder' => 'Ivr']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'type_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\DirectIssueType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\DirectIssueType::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Direct issue type')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'refference_no')->textInput(['maxlength' => true, 'placeholder' => 'Refference No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'particulars')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'amount')->textInput(['placeholder' => 'Amount']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'total')->textInput(['placeholder' => 'Total']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'supplier_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Supplier::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\Supplier::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Supplier')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'officer_name')->textInput(['maxlength' => true, 'placeholder' => 'Officer Name']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'receipt_no')->textInput(['maxlength' => true, 'placeholder' => 'Receipt No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
