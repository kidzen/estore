<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DirectIssueItem */

?>
<div class="direct-issue-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'directIssue.id',
            'label' => Yii::t('app', 'Direct Issue'),
        ],
        [
            'attribute' => 'dispose.description',
            'label' => Yii::t('app', 'Dispose'),
        ],
        'serial_no',
        'unit_price',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
