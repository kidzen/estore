<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ApproveLevel */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Approve Level'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="approve-level-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Approve Level').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <?php if ($model->createdBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->updatedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>

        <div class="col-sm-12">
<?php
if($providerAsset->totalCount){
    $gridColumnAsset = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'category.name',
                'label' => Yii::t('app', 'Category')
            ],
            [
                'attribute' => 'brand.name',
                'label' => Yii::t('app', 'Brand')
            ],
            [
                'attribute' => 'model.name',
                'label' => Yii::t('app', 'Model')
            ],
            'card_no',
            'code_no',
            'description',
            'quantity',
            'min_stock',
                        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAsset,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-asset']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Asset')),
        ],
        'export' => false,
        'columns' => $gridColumnAsset
    ]);
}
?>

        </div>

        <div class="col-sm-12">
<?php
if($providerAssetCheckin->totalCount){
    $gridColumnAssetCheckin = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'rnvr',
            [
                'attribute' => 'budget.name',
                'label' => Yii::t('app', 'Budget')
            ],
            [
                'attribute' => 'asset.description',
                'label' => Yii::t('app', 'Asset')
            ],
            [
                'attribute' => 'supplier.name',
                'label' => Yii::t('app', 'Supplier')
            ],
            'check_date',
            'purchased_date',
            'refference_file',
            'detail:ntext',
                        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAssetCheckin,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-asset-checkin']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Asset Checkin')),
        ],
        'export' => false,
        'columns' => $gridColumnAssetCheckin
    ]);
}
?>

        </div>

        <div class="col-sm-12">
<?php
if($providerDispose->totalCount){
    $gridColumnDispose = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'refference_no',
            [
                'attribute' => 'category.name',
                'label' => Yii::t('app', 'Category')
            ],
            [
                'attribute' => 'method.name',
                'label' => Yii::t('app', 'Method')
            ],
            [
                'attribute' => 'reason.name',
                'label' => Yii::t('app', 'Reason')
            ],
            'quantity',
            'current_revenue',
            'cost',
            'returns',
            'description',
            'requested_date',
            [
                'attribute' => 'requestedBy.username',
                'label' => Yii::t('app', 'Requested By')
            ],
            'disposed_date',
            [
                'attribute' => 'disposedBy.username',
                'label' => Yii::t('app', 'Disposed By')
            ],
                        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDispose,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-dispose']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Dispose')),
        ],
        'export' => false,
        'columns' => $gridColumnDispose
    ]);
}
?>

        </div>

        <div class="col-sm-12">
<?php
if($providerOrder->totalCount){
    $gridColumnOrder = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'instruction_id',
            'order_date',
            [
                'attribute' => 'orderedBy.username',
                'label' => Yii::t('app', 'Ordered By')
            ],
            'order_no',
            [
                'attribute' => 'usage.id',
                'label' => Yii::t('app', 'Usage')
            ],
            'required_date',
            'checkout_date',
            [
                'attribute' => 'checkoutBy.username',
                'label' => Yii::t('app', 'Checkout By')
            ],
                        ['attribute' => 'status', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerOrder,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-order']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Order')),
        ],
        'export' => false,
        'columns' => $gridColumnOrder
    ]);
}
?>

        </div>
    </div>
    </div>
    </div>
    </div>
</div>
