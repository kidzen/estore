<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\OrderItemSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-order-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'asset_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Asset::find()->orderBy('id')->asArray()->all(), 'id', 'description'),
        'data' => \common\models\Asset::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Asset')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'order_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Order::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'data' => \common\models\Order::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Order')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'rq_quantity')->textInput(['placeholder' => 'Rq Quantity']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'app_quantity')->textInput(['placeholder' => 'App Quantity']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'current_balance')->textInput(['placeholder' => 'Current Balance']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'total_price')->textInput(['placeholder' => 'Total Price']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
