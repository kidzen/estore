<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\ProfileSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-profile-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'avatar')->textInput(['maxlength' => true, 'placeholder' => 'Avatar']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'ic_no')->textInput(['maxlength' => true, 'placeholder' => 'Ic No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'contact')->textInput(['maxlength' => true, 'placeholder' => 'Contact']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
