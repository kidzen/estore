<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\OrderSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'instruction_id')->textInput(['placeholder' => 'Instruction']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'order_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Order Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'ordered_by')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'data' => \common\models\User::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'order_no')->textInput(['maxlength' => true, 'placeholder' => 'Order No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'usage_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Usage::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'data' => \common\models\Usage::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Usage')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'required_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Required Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'checkout_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Checkout Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'checkout_by')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'data' => \common\models\User::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'approved_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\ApproveLevel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\ApproveLevel::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Approve level')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
