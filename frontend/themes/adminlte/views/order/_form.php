<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form kartik\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'ItemMovement', 
        'relID' => 'item-movement', 
        'value' => \yii\helpers\Json::encode($model->itemMovements),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'OrderItem', 
        'relID' => 'order-item', 
        'value' => \yii\helpers\Json::encode($model->orderItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'instruction_id')->textInput(['placeholder' => 'Instruction']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'order_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Order Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'ordered_by')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'data' => \common\models\User::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'order_no')->textInput(['maxlength' => true, 'placeholder' => 'Order No']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'usage_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Usage::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'data' => \common\models\Usage::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Usage')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'required_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Required Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'checkout_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Checkout Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'checkout_by')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'data' => \common\models\User::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'approved_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\ApproveLevel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\ApproveLevel::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Approve level')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
</div>

    <div class="clearfix"></div>
<div class="col-md-12">
    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'ItemMovement')),
            'content' => $this->render('_formItemMovement', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->itemMovements),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'OrderItem')),
            'content' => $this->render('_formOrderItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->orderItems),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
</div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
