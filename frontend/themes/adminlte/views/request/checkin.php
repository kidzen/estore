<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AssetCheckin */

$this->title = Yii::t('app', 'Create Asset Checkin');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asset Checkin'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-checkin-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">

			<div class="asset-checkin-form">

				<?php $form = ActiveForm::begin(); ?>

				<?= $form->errorSummary($model); ?>

				<div class="col-md-4">
					<?= $form->field($model, 'budget_id')->widget(\kartik\widgets\Select2::classname(), [
						'data' => \yii\helpers\ArrayHelper::map(\common\models\Budget::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
						'options' => ['placeholder' => Yii::t('app', 'Choose Budget')],
						'pluginOptions' => [
							'allowClear' => true
						],
					]
					); ?>
				</div>

				<div class="col-md-4">
					<?= $form->field($model, 'section_id')->widget(\kartik\widgets\Select2::classname(), [
						'data' => \yii\helpers\ArrayHelper::map(\common\models\Section::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
						'options' => ['placeholder' => Yii::t('app', 'Choose Section')],
						'pluginOptions' => [
							'allowClear' => true
						],
					]
					); ?>
				</div>

				<div class="col-md-4">
					<?= $form->field($model, 'supplier_id')->widget(\kartik\widgets\Select2::classname(), [
						'data' => \yii\helpers\ArrayHelper::map(\common\models\Supplier::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
						'options' => ['placeholder' => Yii::t('app', 'Choose Supplier')],
						'pluginOptions' => [
							'allowClear' => true
						],
					]
					); ?>
				</div>

				<div class="col-md-4">
					<?= $form->field($model, 'asset_id')->widget(\kartik\widgets\Select2::classname(), [
						'data' => \yii\helpers\ArrayHelper::map(\common\models\Asset::find()->orderBy('id')->asArray()->all(), 'id', 'description'),
						'options' => ['placeholder' => Yii::t('app', 'Choose Asset')],
						'pluginOptions' => [
							'allowClear' => true
						],
					]
					); ?>
				</div>

				<div class="col-md-4">
					<?= $form->field($model, 'items_quantity')->textInput(['placeholder' => 'Items Quantity']) ?>
				</div>

				<div class="col-md-4">
					<?= $form->field($model, 'items_unit_price')->textInput(['placeholder' => 'Items Unit Price']) ?>
				</div>

				<div class="clearfix"></div>
				<div class="col-md-4">
					<div class="form-group">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						<?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
					</div>
				</div>

				<?php ActiveForm::end(); ?>

			</div>
		</div>
	</div>
</div>
