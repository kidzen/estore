<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AssetItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Asset Item');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
$quo = "
<h3>#{$order['order_no']}</h3>
<table class=\"table table-condensed\">
<tr>
<td width=\"10px\">{$orderLabel['order_date']}</td>
<td width=\"10px\">:</td>
<td min-width=\"30px\">{$order['order_date']}</td>
<td style=\"padding: 0px 10px\"></td>
<td width=\"10px\">{$orderLabel['required_date']}</td>
<td width=\"10px\">:</td>
<td style=\"min-width:100px\">{$order['required_date']}</td>
</tr>
<tr>
<td>{$orderLabel['orderBy']}</td>
<td width=\"10px\">:</td>
<td>{$order['orderedBy']['profile']['name']}</td>
</tr>
<tr>
<td>{$orderLabel['detail']}</td>
</tr>
<tr>
<td colspan=\"5\">
<div  style=\"padding-left:20px\">
<table class=\"table table-condensed\">
<tr>
<td class=\"text-center\">#</td>
<td class=\"text-center\">Description</td>
<td class=\"text-center\">Requested</td>
<td class=\"text-center\">Approved</td>
</tr>";
foreach ($order->orderItems as $key => $value) {
    $i = $key+1;
    $orderItem[$value->asset_id] = $value;
    $quo .= "<tr>
    <td class=\"text-center\">{$i}</td>
    <td>{$value['asset']['code_no']} - {$value['asset']['description']}</td>
    <td class=\"text-center\">{$value['rq_quantity']}</td>
    <td class=\"text-center\">{$value['app_quantity']}</td>
    </tr>";
}
$quo .= "</table>
</div>
</td>
</tr>
</table>
";
?>
<div class="asset-item-index">

    <?php
    $gridColumn = [
        ['class' => 'kartik\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'section_id',
            'label' => Yii::t('app', 'Section'),
            'value' => function($model){
                if ($model->section)
                    {return $model->section->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Section::find()->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Section', 'id' => 'grid-asset-item-search-section_id']
        ],
        [
            'label' => 'Inventory Details',
            'attribute' => 'INVENTORY_DETAILS',
            'format' => 'raw',
            'value' => function($model) {
                $card = 'Card No : ' . $model->asset->card_no;
                $code = 'Code No : ' . $model->asset->code_no;
                $description = 'Description : ' . $model->asset->description;
                return Html::tag('span', $card . '<br>' . $code . '<br>' . $description);
            },
            'hAlign' => 'left', 'vAlign' => 'middle',
            'pageSummary' => 'Total',
        ],
        [
            'attribute' => 'serial_no',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'attribute' => 'stock_no',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
            // [
            //     'label' => 'Store Balance',
            //     'attribute' => 'inventory.quantity',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],
        [
            'attribute' => 'checkin.transaction.check_date',
            'label' => 'Check In Date',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
            // [
            //     'attribute' => 'checkout.order.transaction.check_date',
            //     'label' => 'Check Out Date',
            //     'hAlign' => 'center', 'vAlign' => 'middle',
            // ],

        [
            'attribute' => 'statusLabel',
            'format' => 'raw',
            'label' => Yii::t('app','Status'),
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'unit_price',
            'label' => 'UNIT PRICE (RM)',
            'format' => ['decimal', 2],
                // 'format' => 'currency',
            'width' => '60px', 'hAlign' => 'center', 'vAlign' => 'middle',
            'pageSummary' => true,
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{approve} {cancel}',
            'buttons' => [
                'cancel' => function ($url,$model) {
                    if($model['order_item_id']) {
                        return Html::a('<span class="glyphicon glyphicon-remove" style="color:red"></span>', $url, ['title' => 'Cancel']);
                    }
                },
                'approve' => function ($url,$model) use ($order,$orderItem) {
                    $visible = !$model['order_item_id'] && ($orderItem[$model->asset->id]['rq_quantity'] > $orderItem[$model->asset->id]['app_quantity']);
                    if($visible) {
                        return Html::a('<span class="glyphicon glyphicon-check" style="color:green"></span>', ['/request/approve','id'=>$model['id'],'orderId'=>$order['id']], ['title' => 'Approve']);
                    }
                },
            ],
            'visibleButtons' => [
                'delete-permanent' => \Yii::$app->user->can('admin'),
            ]
        ],
    ];
    ?>
    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel2,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        // 'toolbar' => false,
        'toolbar' => [
            [
                'content' =>
                Html::a('Back', ['/order/index'], ['class' => 'btn btn-default', 'title' => Yii::t('app', 'Back to list'),]). ' ' .
                Html::a('Update', ['/request/update-checkout','id' => $order['id']], ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Update'),]). ' ' .
                // Html::a('<i class="glyphicon glyphicon-check"></i> <span>Pengesahan <i>FIFO</i></span>', ['smart-approve', 'orderId' => $order['id']], ['id' => 'approve-transaction', 'class' => 'btn bg-maroon-gradient', 'title' => Yii::t('app', 'FIFO Approval'),]) . ' ' .
                Html::a('<i class="glyphicon glyphicon-check"></i> <span>'.Yii::t('app','Approve Transaction').'</span>', ['approve-transaction', 'orderId' => $order['id']], ['id' => 'approve-transaction', 'class' => 'btn bg-green-gradient', 'title' => Yii::t('app', 'Approval Transaction'),])
            ],
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'panel' => [
            'heading' => false,
            'footer' => false,
            'before' => $quo,
        ],
        'responsiveWrap' => false,
        'persistResize' => false,
        'showPageSummary'=> true,
        //        'exportConfig' => $exportConfig,
        'columns'=>$gridColumn,
    ]);
    ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider2,
        // 'filterModel' => $searchModel2,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        // 'toolbar' => false,
        'toolbar' => [
            [
                'content' =>
                Html::button('<i class="glyphicon glyphicon-barcode"></i>', ['class' => 'btn bg-purple-gradient', 'title' => Yii::t('app', 'Scan'), 'data-toggle' => 'modal', 'data-target' => '.modalApprovalScan']) . ' ' .
                Html::button('<i class="glyphicon glyphicon-search"></i>', ['class' => 'btn bg-blue-gradient', 'title' => Yii::t('app', 'Search'), 'data-toggle' => 'modal', 'data-target' => '.modalApprovalSearch'])
            ],
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'panel' => [
            'heading' => false,
            'footer' => false,
            // 'before' => $quo,
        ],
        'responsiveWrap' => false,
        'persistResize' => false,
        'showPageSummary'=> true,
        //        'exportConfig' => $exportConfig,
        'columns'=>$gridColumn,
    ]);
    ?>

</div>
