<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\AssetItem */

$this->title = Yii::t('app', 'Create Asset Item');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asset Item'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-item-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
			<div class="asset-item-form">

				<?php $form = ActiveForm::begin(); ?>

				<?= $form->errorSummary($model); ?>


				<div class="col-md-4">
					<?= $form->field($model, 'asset_id')->textInput(['maxlength' => true, 'placeholder' => 'Serial No']) ?>
				</div>

				<div class="col-md-4">
					<?= $form->field($model, 'serial_no')->textInput(['maxlength' => true, 'placeholder' => 'Serial No']) ?>
				</div>

				<div class="col-md-4">
					<?= $form->field($model, 'stock_no')->textInput(['maxlength' => true, 'placeholder' => 'Stock No']) ?>
				</div>

				<div class="col-md-4">
					<?= $form->field($model, 'unit_price')->textInput(['maxlength' => true, 'placeholder' => 'Unit Price']) ?>
				</div>


				<div class="clearfix"></div>
				<div class="col-md-4">
					<div class="form-group">
						<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						<?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
					</div>
				</div>

				<?php ActiveForm::end(); ?>

			</div>
		</div>
	</div>
</div>
