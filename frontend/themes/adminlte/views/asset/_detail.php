<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Asset */

?>
<div class="asset-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->description) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'category.name',
            'label' => Yii::t('app', 'Category'),
        ],
        [
            'attribute' => 'brand.name',
            'label' => Yii::t('app', 'Brand'),
        ],
        [
            'attribute' => 'model.name',
            'label' => Yii::t('app', 'Model'),
        ],
        'card_no',
        'code_no',
        'description',
        'quantity',
        'min_stock',
        [
            'attribute' => 'approved.name',
            'label' => Yii::t('app', 'Approved'),
        ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
