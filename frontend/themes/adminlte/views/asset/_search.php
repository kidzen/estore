<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AssetSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-asset-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'category_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\AssetCategory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\AssetCategory::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Asset category')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'brand_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\AssetBrand::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\AssetBrand::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Asset brand')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'model_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\AssetModel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\AssetModel::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Asset model')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'card_no')->textInput(['maxlength' => true, 'placeholder' => 'Card No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'code_no')->textInput(['maxlength' => true, 'placeholder' => 'Code No']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'quantity')->textInput(['placeholder' => 'Quantity']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'min_stock')->textInput(['placeholder' => 'Min Stock']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'approved_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\ApproveLevel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\ApproveLevel::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Approve level')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
