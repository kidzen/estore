<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Asset */
/* @var $form kartik\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'AssetCheckin', 
        'relID' => 'asset-checkin', 
        'value' => \yii\helpers\Json::encode($model->assetCheckins),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'OrderItem', 
        'relID' => 'order-item', 
        'value' => \yii\helpers\Json::encode($model->orderItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="asset-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<div class="col-md-4">
    <?= $form->field($model, 'category_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\AssetCategory::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\AssetCategory::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Asset category')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'brand_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\AssetBrand::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\AssetBrand::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Asset brand')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'model_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\AssetModel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\AssetModel::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Asset model')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'card_no')->textInput(['maxlength' => true, 'placeholder' => 'Card No']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'code_no')->textInput(['maxlength' => true, 'placeholder' => 'Code No']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'quantity')->textInput(['placeholder' => 'Quantity']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'min_stock')->textInput(['placeholder' => 'Min Stock']) ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'approved_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\ApproveLevel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\ApproveLevel::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Approve level')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
</div>

    <div class="clearfix"></div>
<div class="col-md-12">
    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'AssetCheckin')),
            'content' => $this->render('_formAssetCheckin', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->assetCheckins),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'OrderItem')),
            'content' => $this->render('_formOrderItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->orderItems),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
</div>
    <div class="clearfix"></div>
    <div class="col-md-4">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
