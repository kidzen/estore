<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AssetCheckin */

?>
<div class="asset-checkin-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'rnvr',
        [
            'attribute' => 'budget.name',
            'label' => Yii::t('app', 'Budget'),
        ],
        [
            'attribute' => 'asset.description',
            'label' => Yii::t('app', 'Asset'),
        ],
        [
            'attribute' => 'supplier.name',
            'label' => Yii::t('app', 'Supplier'),
        ],
        'check_date',
        'purchased_date',
        'refference_file',
        'detail:ntext',
        [
            'attribute' => 'approved.name',
            'label' => Yii::t('app', 'Approved'),
        ],
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
