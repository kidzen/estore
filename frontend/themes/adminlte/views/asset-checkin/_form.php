<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AssetCheckin */
/* @var $form kartik\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'AssetItem',
        'relID' => 'asset-item',
        'value' => \yii\helpers\Json::encode($model->assetItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="asset-checkin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <div class="col-md-4">
        <?= $form->field($model, 'rnvr')->textInput(['maxlength' => true, 'placeholder' => 'Rnvr', 'readOnly' => true, 'value' => $model->generateBaucer()]) ?>
    </div>


    <div class="col-md-4">
        <?= $form->field($model, 'asset_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Asset::find()->orderBy('id')->asArray()->all(), 'id', 'description'),
            'data' => \common\models\Asset::arrayList(),
            'options' => ['placeholder' => Yii::t('app', 'Choose Asset')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'supplier_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Supplier::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
            'data' => \common\models\Supplier::arrayList(),
            'options' => ['placeholder' => Yii::t('app', 'Choose Supplier')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'check_date')->widget(\kartik\datecontrol\DateControl::classname(), [
            'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
            'saveFormat' => 'php:Y-m-d',
            'ajaxConversion' => true,
            'options' => [
                'pluginOptions' => [
                    'placeholder' => Yii::t('app', 'Choose Check Date'),
                    'autoclose' => true,
                ],
            ],
        ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'purchased_date')->widget(\kartik\datecontrol\DateControl::classname(), [
            'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
            'saveFormat' => 'php:Y-m-d',
            'ajaxConversion' => true,
            'options' => [
                'pluginOptions' => [
                    'placeholder' => Yii::t('app', 'Choose Purchased Date'),
                    'autoclose' => true
                ]
            ],
        ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'refference_file')->textInput(['maxlength' => true, 'placeholder' => 'Refference File']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'approved_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\ApproveLevel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
            'data' => \common\models\ApproveLevel::arrayList(),
            'options' => ['placeholder' => Yii::t('app', 'Choose Approve level')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12">
        <?php
        $forms = [
            [
                'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'AssetItem')),
                'content' => $this->render('_formAssetItem', [
                    'row' => \yii\helpers\ArrayHelper::toArray($model->assetItems),
                ]),
            ],
        ];
        echo kartik\tabs\TabsX::widget([
            'items' => $forms,
            'position' => kartik\tabs\TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'pluginOptions' => [
                'bordered' => true,
                'sideways' => true,
                'enableCache' => false,
            ],
        ]);
        ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
