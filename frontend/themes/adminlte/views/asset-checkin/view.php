<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\AssetCheckin */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asset Checkin'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-checkin-view">

    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title"><?= Yii::t('app', 'Asset Checkin').' '. Html::encode($this->title) ?></h2>
                </div>
                <div class="box-body">
                    <div class="col-sm-4">

                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>

                    <div class="col-sm-12">
                        <?php
                        $gridColumn = [
                            ['attribute' => 'id', 'visible' => false],
                            'rnvr',
                            [
                                'attribute' => 'budget.name',
                                'label' => Yii::t('app', 'Budget'),
                            ],
                            [
                                'attribute' => 'asset.description',
                                'label' => Yii::t('app', 'Asset'),
                            ],
                            [
                                'attribute' => 'supplier.name',
                                'label' => Yii::t('app', 'Supplier'),
                            ],
                            'check_date',
                            'purchased_date',
                            'refference_file',
                            'detail:ntext',
                            [
                                'attribute' => 'approved.name',
                                'label' => Yii::t('app', 'Approved'),
                            ],
                            ['attribute' => 'status', 'visible' => false],
                        ];
                        echo DetailView::widget([
                            'model' => $model,
                            'attributes' => $gridColumn
                        ]);
                        ?>
                    </div>
                    <?php if ($model->asset) : ?>
                        <div class="col-sm-6">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h4 class="title">Asset</h4>
                                    <?php
                                    $gridColumnAsset = [
                                        ['attribute' => 'id', 'visible' => false],
                                        'category_id',
                                        'brand.name:text:Brand',
                                        'model.name:text:Model',
                                        'card_no',
                                        'code_no',
                                        'description',
                                        'quantity',
                                        'min_stock',
                                        [
                                            'attribute' => 'approved.name',
                                            'label' => Yii::t('app', 'Approved'),
                                        ],
                                        ['attribute' => 'status', 'visible' => false],
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model->asset,
                                        'attributes' => $gridColumnAsset
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->supplier) : ?>
                        <div class="col-sm-6">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h4 class="title">Supplier</h4>
                                    <?php
                                    $gridColumnSupplier = [
                                        ['attribute' => 'id', 'visible' => false],
                                        'name',
                                        'address',
                                        'contact_no',
                                        'email',
                                        ['attribute' => 'status', 'visible' => false],
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model->supplier,
                                        'attributes' => $gridColumnSupplier
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->approved) : ?>
                        <div class="col-sm-6">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h4 class="title">ApproveLevel</h4>
                                    <?php
                                    $gridColumnApproveLevel = [
                                        ['attribute' => 'id', 'visible' => false],
                                        'name',
                                        'description',
                                        ['attribute' => 'status', 'visible' => false],
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model->approved,
                                        'attributes' => $gridColumnApproveLevel
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->transaction) : ?>
                        <div class="col-sm-6">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h4 class="title">Transaction</h4>
                                    <?php
                                    $gridColumnTransaction = [
                                        ['attribute' => 'id', 'visible' => false],
                                        'type.name:text:Type',
                                        'section.name:text:Section',
                                        'check_date',
                                        ['attribute' => 'status', 'visible' => false],
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model->transaction,
                                        'attributes' => $gridColumnTransaction
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->budget) : ?>
                        <div class="col-sm-6">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h4 class="title">Budget</h4>
                                    <?php
                                    $gridColumnBudget = [
                                        ['attribute' => 'id', 'visible' => false],
                                        'name',
                                        'description',
                                        'revenue',
                                        'balance',
                                        'min_balance',
                                        ['attribute' => 'status', 'visible' => false],
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model->budget,
                                        'attributes' => $gridColumnBudget
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="col-sm-12">
                        <?php
                        if($providerAssetItem->totalCount){
                            $gridColumnAssetItem = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                'stock_no',
                                'serial_no',
                                [
                                    'attribute' => 'post.name',
                                    'label' => Yii::t('app', 'Section')
                                ],
                                [
                                    'attribute' => 'orderItem.order.order_no',
                                    'label' => Yii::t('app', 'Order No')
                                ],
                                [
                                    'attribute' => 'dispose.description',
                                    'label' => Yii::t('app', 'Dispose')
                                ],
                                'unit_price',
                                'statusLabel:raw:Status',
                                ['attribute' => 'status', 'visible' => false],
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerAssetItem,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-asset-item']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Asset Item')),
                                ],
                                'export' => false,
                                'columns' => $gridColumnAssetItem
                            ]);
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
