<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AssetCheckinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\helpers\Html;
use kartik\export\ExportMenu;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Asset Checkin');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="asset-checkin-index">

    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true,
            'visible' => true,
        ],
        ['attribute' => 'id', 'visible' => false],
        'rnvr',
        [
            'visible' => false,
            'attribute' => 'budget_id',
            'label' => Yii::t('app', 'Budget'),
            'value' => function($model){
                if ($model->budget)
                    {return $model->budget->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Budget::find()->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Budget', 'id' => 'grid-asset-checkin-search-budget_id']
        ],
        [
            'attribute' => 'asset_id',
            'label' => Yii::t('app', 'Asset'),
            'value' => function($model){
                if ($model->asset)
                    {return $model->asset->description;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Asset::find()->asArray()->all(), 'id', 'description'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Asset', 'id' => 'grid-asset-checkin-search-asset_id']
        ],
        [
            'attribute' => 'supplier_id',
            'label' => Yii::t('app', 'Supplier'),
            'value' => function($model){
                if ($model->supplier)
                    {return $model->supplier->name;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\Supplier::find()->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Supplier', 'id' => 'grid-asset-checkin-search-supplier_id']
        ],
        'check_date',
        'purchased_date',
        'refference_file',
        'detail:ntext',
        [
            'attribute' => 'approved_id',
            'format' => 'raw',
            'label' => Yii::t('app', 'Approved'),
            'value' => function($model){
                if ($model->approved)
                    {return $model->approved->label;}
                else
                    {return NULL;}
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\common\models\ApproveLevel::find()->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Approve level', 'id' => 'grid-asset-checkin-search-approved_id']
        ],
        ['attribute' => 'status', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {delete-permanent}',
            'buttons' => [
                'delete-permanent' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-trash" style="color:red"></span>', $url, ['title' => 'Delete Permanent']);
                },
            ],
            'visibleButtons' => [
                'delete-permanent' => \Yii::$app->user->can('admin'),
            ]
        ],
    ];
    ?>
    <?= DynaGrid::widget([
        'columns'=>$gridColumn,
        'storage'=>DynaGrid::TYPE_COOKIE,
        'theme'=>'panel-danger',
        'showPersonalize'=>true,
        'gridOptions'=>[
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            // 'showPageSummary'=>true,
            //'floatHeader'=>true,
            //'responsiveWrap'=>false,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-asset-checkin']],
            'panel' => [
                'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
                'before' =>  '<div style="padding-top: 7px;"><em>* The table header sticks to the top in this demo as you scroll</em></div>',
                'after' => false,
            ],
            // 'export' => false,
            // your toolbar can include the additional full export menu
            'toolbar' => [
                ['content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title'=>Yii::t('app', 'Create Asset Checkin')]) . ' '.
                Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button'])
            ],
            ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
            '{export}',
            '{toggleData}',
            'exportConfig' => [
                    // ExportMenu::FORMAT_PDF => false
            ]
        ],
    ],
        'options'=>['id'=>'dynagrid-1'] // a unique identifier is important
        ]); ?>

    </div>
