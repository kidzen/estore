<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AssetCheckinSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-asset-checkin-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'rnvr')->textInput(['maxlength' => true, 'placeholder' => 'Rnvr']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'budget_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Budget::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\Budget::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Budget')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'asset_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Asset::find()->orderBy('id')->asArray()->all(), 'id', 'description'),
        'data' => \common\models\Asset::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Asset')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'supplier_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Supplier::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\Supplier::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Supplier')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'check_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Check Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'purchased_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Choose Purchased Date'),
                'autoclose' => true
            ]
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'refference_file')->textInput(['maxlength' => true, 'placeholder' => 'Refference File']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'approved_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\ApproveLevel::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'data' => \common\models\ApproveLevel::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Approve level')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
