<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->orders,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'instruction_id',
        'order_date',
        [
                'attribute' => 'orderedBy.username',
                'label' => Yii::t('app', 'Ordered By')
            ],
        'order_no',
        [
                'attribute' => 'usage.id',
                'label' => Yii::t('app', 'Usage')
            ],
        'required_date',
        'checkout_date',
        [
                'attribute' => 'checkoutBy.username',
                'label' => Yii::t('app', 'Checkout By')
            ],
        [
                'attribute' => 'approved.name',
                'label' => Yii::t('app', 'Approved')
            ],
        ['attribute' => 'status', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'order'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
