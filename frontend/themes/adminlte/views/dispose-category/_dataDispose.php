<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->disposes,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'refference_no',
        [
                'attribute' => 'method.name',
                'label' => Yii::t('app', 'Method')
            ],
        [
                'attribute' => 'reason.name',
                'label' => Yii::t('app', 'Reason')
            ],
        'quantity',
        'current_revenue',
        'cost',
        'returns',
        'description',
        'requested_date',
        [
                'attribute' => 'requestedBy.username',
                'label' => Yii::t('app', 'Requested By')
            ],
        'disposed_date',
        [
                'attribute' => 'disposedBy.username',
                'label' => Yii::t('app', 'Disposed By')
            ],
        [
                'attribute' => 'approved.name',
                'label' => Yii::t('app', 'Approved')
            ],
        ['attribute' => 'status', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'dispose'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
