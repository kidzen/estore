<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\DisposeDetailValueSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-dispose-detail-value-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'dispose_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\Dispose::find()->orderBy('id')->asArray()->all(), 'id', 'description'),
        'data' => \common\models\Dispose::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'key_id')->widget(\kartik\widgets\Select2::classname(), [
        //'data' => \yii\helpers\ArrayHelper::map(\common\models\DisposeDetailKey::find()->orderBy('id')->asArray()->all(), 'id', 'description'),
        'data' => \common\models\DisposeDetailKey::arrayList(),
        'options' => ['placeholder' => Yii::t('app', 'Choose Dispose detail key')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'value')->textInput(['maxlength' => true, 'placeholder' => 'Value']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'status', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
