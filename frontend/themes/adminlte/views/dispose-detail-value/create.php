<?php

use kartik\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DisposeDetailValue */

$this->title = Yii::t('app', 'Create Dispose Detail Value');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispose Detail Value'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dispose-detail-value-create">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
		</div>
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
