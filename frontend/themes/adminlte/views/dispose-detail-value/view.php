<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\DisposeDetailValue */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispose Detail Value'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dispose-detail-value-view">

    <div class="row">
    <div class="col-sm-12">
    <div class="box">
        <div class="box-header">
            <h2 class="box-title"><?= Yii::t('app', 'Dispose Detail Value').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="box-body">
        <div class="col-sm-4">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>

        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'dispose.description',
            'label' => Yii::t('app', 'Dispose'),
        ],
        [
            'attribute' => 'key.description',
            'label' => Yii::t('app', 'Key'),
        ],
        'value',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <?php if ($model->dispose) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">Dispose</h4>
    <?php 
    $gridColumnDispose = [
        ['attribute' => 'id', 'visible' => false],
        'refference_no',
        'category_id',
        'method_id',
        'reason_id',
        'quantity',
        'current_revenue',
        'cost',
        'returns',
        'description',
        'requested_date',
        'requested_by',
        'disposed_date',
        'disposed_by',
        'approved_id',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->dispose,
        'attributes' => $gridColumnDispose    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->key) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">DisposeDetailKey</h4>
    <?php 
    $gridColumnDisposeDetailKey = [
        ['attribute' => 'id', 'visible' => false],
        'key',
        'description',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->key,
        'attributes' => $gridColumnDisposeDetailKey    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->createdBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($model->updatedBy) : ?>
    <div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header">
            <h4 class="title">User</h4>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        ['attribute' => 'status', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
    </div>
    </div>
    </div>
    </div>
</div>
