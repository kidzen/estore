<?php

use kartik\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\LogSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="form-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'level')->textInput(['placeholder' => 'Level']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'category')->textInput(['maxlength' => true, 'placeholder' => 'Category']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'log_time')->textInput(['placeholder' => 'Log Time']) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'prefix')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>
    </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-4">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
