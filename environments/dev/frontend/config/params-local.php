<?php
return [
	'version'=>2.0,
    'appName' => 'FEMS Fire Extinguisher Management System', //size 132x132
    'leftMenuImg' => 'dist/img/logo.png', //size 132x132
    'companyNameShort' => 'FEMS',
    'companyNameFull' => 'Aito Firework Holdings',
    // 'companyLogoIcon' => 'dist/img/favicon-32x32.png', //size 32x32
    'companyLogoIcon' => 'dist/img/logo.png', //size 32x32
];
