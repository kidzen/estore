<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost:3307;dbname=estore',
            'username' => 'root',
            'password' => '',
            // 'enableQueryCache' => true,
            // 'enableSchemaCache' => true,
            // 'schemaCacheDuration' => false,
            'charset' => 'utf8',
        ],
        // 'db2' => [
        //     'class' => 'yii\db\Connection',
        //     'dsn' => 'oci:host=localhost:1521;dbname=estore',
        //     'username' => 'root',
        //     'password' => '',
        //     // 'enableQueryCache' => true,
        //     // 'enableSchemaCache' => true,
        //     // 'schemaCacheDuration' => false,
        //     'charset' => 'utf8',
        // ],
        'dbStore' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost:3307;dbname=estor_pdrm',
            'username' => 'root',
            'password' => '',
            // 'enableQueryCache' => true,
            // 'enableSchemaCache' => true,
            // 'schemaCacheDuration' => false,
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
