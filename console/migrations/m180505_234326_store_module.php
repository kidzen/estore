<?php

use common\components\Migration;

/**
 * Class m180505_234326_store_module
 */
class m180505_234326_store_module extends Migration
{

    public function init()
    {
        parent::init();
        $this->db = Yii::$app->dbStore;
        $this->db->getSchema()->refresh();
        $this->db->enableSlaves = false;
    }

    public function safeUp() {
        $this->customDrop();
        //db mysql
        // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
        switch ($this->db->driverName) {
            case 'oci':
            $tableOptions = '';
            break;
            case 'mysql':
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            break;
            default:
            $tableOptions = '';
            break;
        }
        // user
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->unique(),
            'email' => $this->string(),
            'profile_pic' => $this->string(),
            'password' => $this->string(),
            'role_id' => $this->integer()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'auth_key' => $this->string(32),

            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ], $tableOptions);
        // activity_log
        $this->createTable('{{%activity_log}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'remote_ip' => $this->string(),
            'action' => $this->string(),
            'controller' => $this->string(),
            'params' => $this->string(),
            'route' => $this->string(),
            'status' => $this->string(),
            'messages' => $this->string(),
            'created_at' => $this->timestamp(),
        ], $tableOptions);
        // store
        $this->createTable('{{%store}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'name' => $this->string()->unique(),
            'address' => $this->string(),
            'contact_no' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // store_assignment
        $this->createTable('{{%store_assignment}}', [
            'id' => $this->primaryKey(),
            'store_id' => $this->integer(),
            'user_id' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // store_category
        $this->createTable('{{%store_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // category
        $this->createTable('{{%inventory_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // inventory
        $this->createTable('{{%inventory}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'card_no' => $this->string(),
            'code_no' => $this->string()->unique(),
            'description' => $this->string(),
            'quantity' => $this->integer(),
            'min_stock' => $this->integer(),
            'location' => $this->string(),
            'approved' => $this->smallinteger(),
            'approved_at' => $this->timestamp(),
            'approved_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // inventory_checkin
        $this->createTable('{{%inventory_checkin}}', [
            'id' => $this->primaryKey(),
            'transaction_id' => $this->integer(),
            'budget_id' => $this->integer(),
            'store_id' => $this->integer(),
            'inventory_id' => $this->integer(),
            'vendor_id' => $this->integer(),
            'items_quantity' => $this->integer(),
            'items_total_price' => $this->float(),
            'check_date' => $this->date(),
            'check_by' => $this->integer(),
            'approved' => $this->smallinteger(),
            'approved_at' => $this->timestamp(),
            'approved_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // inventory_item
        $this->createTable('{{%inventory_item}}', [
            'id' => $this->primaryKey(),
            'inventory_id' => $this->integer(),
            'checkin_id' => $this->integer(),
            'checkout_id' => $this->integer(),
            'dispose_id' => $this->integer(),
            'sku' => $this->string(),
            'unit_price' => $this->decimal(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // order
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'transaction_id' => $this->integer(),
            'instruction_id' => $this->integer(),
            'order_date' => $this->date(),
            'ordered_by' => $this->integer(),
            'ordered_by_temp' => $this->string(),
            'order_no' => $this->string(),
                // 'store_id' => $this->integer(),
            'usage_id' => $this->integer(),
            'required_date' => $this->date(),
            'checkout_date' => $this->date(),
            'checkout_by' => $this->integer(),
            'approved' => $this->smallinteger(),
            'approved_at' => $this->timestamp(),
            'approved_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // order_item
        $this->createTable('{{%order_item}}', [
            'id' => $this->primaryKey(),
            'inventory_id' => $this->integer(),
            'order_id' => $this->integer(),
            'rq_quantity' => $this->integer(),
            'app_quantity' => $this->integer(),
            'current_balance' => $this->integer(),
            'unit_price' => $this->float(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // package
        $this->createTable('{{%package}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'detail' => $this->string(),
            'delivery' => $this->string(),
            'package_by' => $this->integer(),
            'package_date' => $this->date(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // role
        $this->createTable('{{%role}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // transaction
        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'store_id' => $this->integer(),
            'type' => $this->integer(),
            'check_date' => $this->date(),
            'check_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // budget
        $this->createTable('{{%budget}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'revenue' => $this->double(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose
        $this->createTable('{{%dispose}}', [
            'id' => $this->primaryKey(),
            'refference_no' => $this->string(),
            'transaction_id' => $this->integer(),
            'category_id' => $this->integer(),
            'method_id' => $this->integer(),
            'reason_id' => $this->integer(),
            'quantity' => $this->integer(),
            'current_revenue' => $this->double(),
            'cost' => $this->double(),
            'returns' => $this->double(),
            'description' => $this->string(),
            'requested_at' => $this->timestamp(),
            'requested_by' => $this->integer(),
            'disposed_at' => $this->timestamp(),
            'disposed_by' => $this->integer(),
            'approved' => $this->integer(),
            'approved_at' => $this->timestamp(),
            'approved_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_detail_key
        $this->createTable('{{%dispose_detail_key}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_detail_value
        $this->createTable('{{%dispose_detail_value}}', [
            'id' => $this->primaryKey(),
            'dispose_id' => $this->integer(),
            'key_id' => $this->integer(),
            'value' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_category
        $this->createTable('{{%dispose_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_method
        $this->createTable('{{%dispose_method}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_reason
        $this->createTable('{{%dispose_reason}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // usage
        $this->createTable('{{%usage}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'item_id' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // usage_category
        $this->createTable('{{%usage_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
            // usage_detail_key
        $this->createTable('{{%usage_detail_key}}', [
            'id' => $this->primaryKey(),
                'category_id' => $this->integer(),  // usage_category
                'name' => $this->string(),
                'status' => $this->integer()->defaultValue(1),
                'deleted_by' => $this->integer()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
            ], $tableOptions);
        // usage_detail_value
        $this->createTable('{{%usage_detail_value}}', [
            'id' => $this->primaryKey(),
                'key_id' => $this->integer(),   // usage_key
                'usage_id' => $this->integer(),     // col detail
                'value' => $this->string(),
                'refference' => $this->string(),
                'status' => $this->integer()->defaultValue(1),
                'deleted_by' => $this->integer()->defaultValue(0),
                'deleted_at' => $this->timestamp(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
            ], $tableOptions);
        // vehicle
        $this->createTable('{{%vehicle}}', [
            'id' => $this->primaryKey(),
            'reg_no' => $this->string(),
            'model' => $this->string(),
            'type' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // workshop
        $this->createTable('{{%workshop}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'address' => $this->string(),
            'contact' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // vendor
        $this->createTable('{{%vendor}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'address' => $this->string(),
            'contact_no' => $this->string(),
            'email' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        if($this->db->driverName == 'mysql'){

            // profile
            $this->createTable('{{%profile}}', [
                'id' => $this->primaryKey(),
                'staff_no' => $this->string(),
                'name' => $this->string(),
                'position' => $this->string(),
                'department' => $this->string(),
            ], $tableOptions);
        }




        $this->createView();
    }

    public function safeDown() {
        $this->customDrop($this->db);
    }

    public function createView() {
        switch ($this->db->driverName) {
            case 'oci':
            //Approval
            $view['approval'] = '
            CREATE VIEW "approval" AS
            select
            rownum as "id"
            ,ii."id" "id_inventory_item"
            ,i."id" "id_inventory"
            ,c."id" "id_category"
            ,ic."id" "id_inventory_checkin"
            ,oi."id" "id_order_item"
            ,o."id" "id_order"
            ,ii."sku"
            ,o."order_no"
            ,c."name" "items_category_name"
            ,i."description" "items_inventory_name"
            ,oi."rq_quantity"
            ,oi."app_quantity"
            ,o."approved"
            ,oi."current_balance"
            ,ii."unit_price"
            ,oi."unit_price" "total_price"
            ,o."order_date"
            ,o."required_date"
            from "inventory_item" ii
            left join "inventory_checkin" ic on ic."id" = ii."checkin_id"
            left join "inventory" i on i."id" = ic."inventory_id"
            left join "inventory_category" c on c."id" = i."category_id"
            left join "order_item" oi on oi."inventory_id" = i."id"
            left join "order" o on o."id" = oi."order_id"
            ';

            //Usage List
            $view['usage_list'] = '
            CREATE VIEW "usage_list" AS
            SELECT
            rownum AS "id", t.*
            FROM (
            SELECT "id" "ref_id",\'vehicle\' "ref_cat","reg_no" "name" FROM "vehicle"
            UNION
            SELECT "id" "ref_id",\'workshop\' "ref_cat","name" "name" FROM "workshop"
            UNION
            SELECT "id" "ref_id",\'store\' "ref_cat","name" "name" FROM "store") t
            ';

            //sisken
            $view['sisken'] = '
            CREATE VIEW "sisken" AS
            select TAHUN "tahun",BULAN "bulan",ID "id",NO_KERJA "no_kerja",ARAHAN_KERJA "arahan_kerja",JK_JG "jk_jg",BENGKEL_PANEL_ID "bengkel_panel_id",NAMA_PANEL "nama_panel",KATEGORI_PANEL "category_panel",SELENGGARA_ID "selenggara_id",TEMPOH_SELENGGARA "tempoh_selenggara",ALASAN_ID "alasan_id",ALASAN "alasan",TARIKH_ARAHAN "tarikh_arahan",NO_SEBUTHARGA "no_sebutharga",TARIKH_KENDERAAN_TIBA "tarikh_kenderaan_tiba",TARIKH_JANGKA_SIAP "tarikh_jangka_siap",TARIKH_SEBUTHARGA "tarikh_sebut_harga",STATUS_SEBUTHARGA "status_sebut_harga",NO_DO "no_do",TARIKH_SIAP "tarikh_siap",TEMPOH_JAMINAN "tempoh_jaminan",TARIKH_DO "tarikh_do",TARIKH_CETAK "tarikh_cetakan",STATUS "status",CATATAN_SEBUTHARGA "catatan_sebutharga",CATATAN "catatan",NAMA_PIC "nama_pic",TARIKH_PERMOHONAN "tarikh_permohonan",ID_KENDERAAN "id_kenderaan",KATEGORI_KEROSAKAN_ID "kategori_kerosakan_id",KATEGORI_KEROSAKAN "kategori_kerosakan",KATEGORI "kategori",ISSERVICE "isservice",ISPANCIT "ispancit",ODOMETER_TERKINI "odometer_terkini",NO_PLAT "no_plat",MODEL "model",KOD_JABATAN "kod_jabatan",NAMA_JABATAN "nama_jabatan",JUMLAH_KOS "jumlah_kos" from usersisken.LAPORAN_PENYELENGGARAAN_ESTOR';

            //report_in_quarter
            $view['report_in_quarter'] = '
            CREATE VIEW "report_in_quarter" AS
            WITH DATA1 AS (
            select
            ii."sku"
            ,ii."unit_price"
            ,ic."inventory_id"
            ,t."check_date"
            ,extract(year from t."check_date") "year"
            ,to_char(t."check_date",\'Q\') "quarter"
            from "inventory_item" ii
            left join "inventory_checkin" ic on ic."id" = ii."checkin_id"
            left join "transaction" t on t."id" = ic."transaction_id"
            where ii."deleted_by" = 0)

            SELECT
            distinct "year",
            -- rownum as id,
            "quarter",
            COUNT(*) OVER (PARTITION BY "year","quarter") "count",
            SUM("unit_price") OVER (PARTITION BY "year","quarter") "total_price"
            FROM DATA1
            ORDER BY "year","quarter"
            ';

            //report_out_quarter
            $view['report_out_quarter'] = '
            CREATE VIEW "report_out_quarter" AS
            WITH DATA1 AS (
            select
            ii."sku"
            ,ii."unit_price"
            ,oi."inventory_id"
            ,t."check_date"
            ,extract(year from t."check_date") "year"
            ,to_char(t."check_date",\'Q\') "quarter"
            from "inventory_item" ii
            left join "order_item" oi on oi."id" = ii."checkout_id"
            left join "order" o on o."id" = oi."order_id"
            left join "transaction" t on t."id" = o."transaction_id"
            where ii."deleted_by" = 0 and ii."checkout_id" is not null)

            SELECT
            DISTINCT "year",
            -- rownum as id,
            "quarter",
            count(*) over (partition by "year","quarter") "count",
            sum("unit_price") over (partition by "year","quarter") "total_price"
            FROM DATA1
            ORDER BY "year","quarter"
            ';

            //list_year
            $view['list_year'] = '
            CREATE VIEW "list_year" AS
            select "year","quarter" from "report_in_quarter" union
            select "year","quarter" from "report_out_quarter"
            ';

            //report_all
            $view['report_all'] = '
            CREATE VIEW "report_all" AS
            select
            rownum as "id",
            "list_year"."year",
            "list_year"."quarter",
            nvl("report_in_quarter"."count", \'0\') "count_in",
            nvl("report_in_quarter"."total_price", \'0\') "price_in",
            nvl("report_out_quarter"."count", \'0\') count_out,
            nvl("report_out_quarter"."total_price", \'0\') price_out,

            sum(nvl("report_in_quarter"."count", \'0\')) over (order by "list_year"."year" rows unbounded preceding) -
            sum(nvl("report_out_quarter"."count", \'0\')) over (order by "list_year"."year" rows unbounded preceding)
            "count_current",
            sum(nvl("report_in_quarter"."total_price", \'0\')) over (order by "list_year"."year" rows unbounded preceding) -
            sum(nvl("report_out_quarter"."total_price", \'0\')) over (order by "list_year"."year" rows unbounded preceding)
            "price_current"

            from "list_year"
            left join "report_in_quarter"
            on ("report_in_quarter"."year" = "list_year"."year" and "list_year"."quarter" = "report_in_quarter"."quarter")
            left join "report_out_quarter"
            on ("list_year"."year" = "report_out_quarter"."year" and "list_year"."quarter" = "report_out_quarter"."quarter")
            order by "list_year"."year", "list_year"."quarter"
            ';

            //inventory_checkout_freq_report
            $view['inventory_checkout_freq_report'] = '
            CREATE VIEW "inventory_checkout_freq_report" as
            select
            t2.*
            ,rank() OVER (ORDER BY "by_inventory" DESC) "rank_inventory"
            ,rank() OVER (ORDER BY "by_year" DESC) "rank_year"
            ,rank() OVER (ORDER BY "by_month" DESC) "rank_month"
            ,rank() OVER (ORDER BY "by_date" DESC) "rank_date"
            from (
            select
            t1."inventory_id"
            ,"year","month","date"
            ,count(*) over (partition by "inventory_id") "by_inventory"
            ,count(*) over (partition by "inventory_id","year") "by_year"
            ,count(*) over (partition by "inventory_id","year","month") "by_month"
            ,count(*) over (partition by "inventory_id","date") "by_date"
            from (
            select
            ii."id",oi."inventory_id",o."approved",o."approved_at"
            ,extract(year from o."approved_at") "year"
            ,extract(month from o."approved_at") "month"
            ,to_char(cast(o."approved_at" as date),\'DD-MON-YYYY\') "date"
            from "inventory_item" ii
            left join "order_item" oi on oi."id" = ii."checkout_id"
            left join "order" o on o."id" = oi."order_id"
            where 1=1
            and ii."checkout_id" is not null
            and ii."deleted_by" = 0
            and o."approved" = 1
            ) t1
            ) t2
            ';

            //vendor_usage
            $view['vendor_usage'] = '
            CREATE VIEW "vendor_usage" as
            select
            rownum as "id", t.*
            from (
            select
            t1.*
            ,SUM ("items_total_price") over (partition by "year") "by_year"
            ,SUM ("items_total_price") over (partition by "vendor") "by_vendor"
            ,SUM ("items_total_price") over (partition by "year","vendor") "by_year_vendor"
            ,SUM ("items_total_price") over (partition by "year","month","vendor") "by_month_vendor"
            ,SUM ("items_total_price") over (partition by "date","vendor") "by_date_vendor"
            from (
            select
            v."name" "vendor"
            ,extract(year from ic."created_at") "year"
            ,extract(month from ic."created_at") "month"
            ,to_char(cast(ic."created_at" as date),\'DD-MON-YYYY\') "date"
            ,ic."created_at"
            ,ic."inventory_id"
            ,ic."items_total_price"
            from "vendor" v
            left join "inventory_checkin" ic on ic."vendor_id" = v."id"
            where 1=1 and ic."deleted_by" = 0 and v."deleted_by" = 0
            ) t1
            )t
            ';


            //vehicle_usage
            // $view['vehicle_usage'] = '
            // CREATE VIEW "vehicle_usage" as


            // ';

            // transaction_in
            $view['transaction_in'] = '
            CREATE VIEW "transaction_in" as
            WITH DATA1 AS (
            select
            t."id"
            ,t."type"
            ,ii."sku"
            ,ii."unit_price"
            ,ic."inventory_id"
            ,v."id" "vendor_id"
            ,v."name" "vendor_name"
            ,t."check_date"
            ,t."check_by"
            ,ic."approved_at"
            ,ic."approved_by"
            ,extract(year from t."check_date") "year"
            ,to_char(t."check_date",\'Q\') "quarter"
            ,count(*) over (partition by t."id") "count"
            ,sum(ii."unit_price") over (partition by t."id") "total_price"
            from "inventory_item" ii
            left join "inventory_checkin" ic on ic."id" = ii."checkin_id"
            left join "transaction" t on t."id" = ic."transaction_id"
            left join "vendor" v on v."id" = ic."vendor_id"
            where ii."deleted_by" = 0
            )

            SELECT
            "id",
            "type",
            "check_date",
            "check_by",
            "vendor_id" "refference_id",
            "vendor_name" "refference",
            "approved_at",
            "approved_by",
            "inventory_id",
            --  rownum as id,
            --  quarter,
            "unit_price",
            data1."count",
            "total_price"
            FROM data1
            GROUP BY "id","count","unit_price","total_price", "type", "check_date", "check_by",
            "approved_at", "approved_by", "inventory_id", "vendor_id", "vendor_name"
            ORDER BY "id"

            ';

            //transaction_out
            $view['transaction_out'] = '
            CREATE VIEW "transaction_out" as
            with data1 as (
            select distinct --//comment out due to tickets #1
            t."id"
            ,o."id" "o_id"
            ,oi."id" "oi_id"
            ,t."type"
            --  ,listagg(ii."sku", \', \') within group (order by ii."id") item
            --  ,ii."sku"
            ,ii."unit_price"
            ,oi."inventory_id"
            ,t."check_date"
            ,t."check_by"
            ,o."approved_at"
            ,o."order_no"
            ,o."approved_by"
            ,extract(year from t."check_date") "year"
            ,to_char(t."check_date",\'Q\') "quarter"
            ,count(*) over (partition by t."id",oi."id",ii."unit_price") "count"
            ,sum(ii."unit_price") over (partition by t."id",oi."id",ii."unit_price") "total_price"
            from "inventory_item" ii
            left join "order_item" oi on oi."id" = ii."checkout_id"
            left join "order" o on o."id" = oi."order_id"
            left join "transaction" t on t."id" = o."transaction_id"
            --where ii."deleted_by" = 0 and ii."checkout_id" is not null
            where ii."deleted_by" = 0 and ii."checkout_id" is not null and o."approved" = 1
            order by o."id"
            )

            select
            --  *
            "id",
            "type",
            "check_date",
            "check_by",
            "oi_id" "refference_id",
            "order_no" "refference",
            "approved_at",
            "approved_by",
            "inventory_id",
            --  rownum as "id",
            --  "quarter",
            "unit_price",
            data1."count",
            "total_price"
            from data1
            group by "id","oi_id","count","unit_price","total_price", "type", "check_date", "check_by",
            "approved_at", "approved_by", "inventory_id","order_no"   --//comment out due to tickets #1
            --group by
            order by "id"
            ';

            //transaction_all
            $view['transaction_all'] = '
            CREATE VIEW "transaction_all" as
            select
            t."id"
            , t."type"
            , t."check_date"
            , trunc(t."approved_at") "approved_date"
            , t."check_by"
            , t."refference"
            , t."inventory_id"
            , t."unit_price"
            , t."count_in"
            , t."price_in"
            , t."count_out"
            , t."price_out"
            , sum("count_in" - "count_out") over (partition by "inventory_id" order by "approved_at","id" rows unbounded preceding) "count_current"
            , sum("price_in" - "price_out") over (partition by "inventory_id" order by "approved_at","id" rows unbounded preceding) "price_current"
            from (
            select
            --    rownum as id,
            ids."id",
            ids."type",
            ids."check_date",
            ids."approved_at",
            ids."check_by",
            ids."refference_id",
            ids."refference",
            ids."inventory_id",
            ids."unit_price",
            case when ids."type" = 1 then ids."count" else 0 end "count_in",
            case when ids."type" = 1 then ids."total_price" else 0 end "price_in",
            case when ids."type" = 2 then ids."count" else 0 end "count_out",
            case when ids."type" = 2 then ids."total_price" else 0 end "price_out"
            from (select * from "transaction_out" union
            select * from "transaction_in"
            ) ids
            order by ids."approved_at"
            ) t

            ';

            //checkout
            $view['checkout'] = '
            CREATE VIEW "checkout" as
            with data1 as (
            select distinct
            --select
            --  rownum id
            o."id" "order_id"
            ,ii."id"
            ,o."required_date" "order_required_date"
            ,oi."id" "order_item_id"
            , o."order_no"
            , o."ordered_by"
            , o."order_date"
            ,ta."id" "transaction_id"
            , i."card_no"
            , i."code_no"
            , i."description"
            , oi."rq_quantity"
            --  , count(*) over (partition by oi."id",ii."unit_price") app_quantity
            , oi."current_balance"
            , ii."unit_price" "unit_price"
            --  , avg(ii."unit_price") over (partition by oi."id",ii."unit_price") average_unit_price
            --  , sum(ii."unit_price") over (partition by oi."id",ii."unit_price") batch_total_price
            , trunc(oi."created_at") "created_date"
            , oi."created_by"
            , o."approved_by"
            , trunc(o."approved_at") "approved_date"
            from "inventory_item" ii
            left join "inventory_checkin" ic on ic."id" = ii."checkin_id"
            left join "inventory" i on i."id" = ic."inventory_id"
            left join "order_item" oi on oi."id" = ii."checkout_id"
            left join "order" o on o."id" = oi."order_id"
            left join "transaction" t on t."id" = o."transaction_id"
            left join "transaction_all" ta on ta."id" = t."id"
            where ii."checkout_id"  is not null and ii."deleted_by" = 0
            -- and order_id = 1174
            )
            select distinct
            "order_id"
            , "order_required_date"
            , "order_item_id"
            , "order_no"
            , "ordered_by"
            , "order_date"
            , "transaction_id"
            , "card_no"
            , "code_no"
            , "description"
            , "rq_quantity"
            , count(*) over (partition by "order_item_id","unit_price") "app_quantity"
            , "current_balance"
            , "unit_price"
            , avg("unit_price") over (partition by "order_item_id","unit_price") "average_unit_price"
            , sum("unit_price") over (partition by "order_item_id","unit_price") "batch_total_price"
            , "created_date"
            , "created_by"
            , "approved_by"
            , "approved_date"
            from data1

            ';

            //TRANSACTION_OUT_SISKEN
            $view['TRANSACTION_OUT_SISKEN'] = '
            CREATE VIEW "TRANSACTION_OUT_SISKEN" as
            select
            rownum as id
            ,t."ARAHAN_KERJA_ID"
            ,t."ORDER_NO"
            ,t."CHECK_DATE"
            ,t."STATUS"
            ,t."ORDER_ITEMS_ID"
            ,t."CATEGORY_NAME"
            ,t."CARD_NO"
            ,t."CODE_NO"
            ,t."INVENTORY_DESCRIPTION"
            ,t."ITEMS"
            ,t."UNIT_PRICE"
            ,t."BATCH_QUANTITY"
            ,t."BATCH_TOTAL_PRICE"
            ,t."TOTAL_QUANTITY"
            ,t."TOTAL_PRICE" from
            (
            select
            arahan_kerja_id
            ,order_no
            ,check_date
            ,status
            ,order_items_id
            ,category_name
            ,card_no
            ,code_no
            ,inventory_description
            , LISTAGG(sku, \', \') WITHIN GROUP (ORDER BY sku) items
            ,unit_price
            ,batch_quantity
            ,batch_total_price
            ,total_quantity
            ,total_price
            from
            (
            select
            o."instruction_id" ARAHAN_KERJA_ID
            ,o."order_no" order_no
            ,to_char(o."approved_at",\'dd-MON-yy\') check_date
            ,o."approved" status
            ,oi."id" order_items_id
            ,c."name" category_name
            ,i."card_no" card_no
            ,i."code_no" code_no
            ,i."description" inventory_description
            ,ii."sku" sku
            ,ii."unit_price" unit_price
            ,count(*) over (partition by oi."id",ii."unit_price") batch_quantity
            ,sum(ii."unit_price") over (partition by oi."id",ii."unit_price") batch_total_price
            ,count(*) over (partition by o."order_no") total_quantity
            ,sum(ii."unit_price") over (partition by o."order_no") total_price
            from "inventory_item" ii
            left join "order_item" oi on oi."id" = ii."checkout_id"
            left join "order" o on o."id" = oi."order_id"
            left join "transaction" t on t."id" = o."transaction_id"
            left join "inventory" i on i."id" = oi."inventory_id"
            left join "inventory_category" c on c."id" = i."category_id"
            where ii."checkout_id" is not null
            and o."instruction_id" is not null
            order by o."order_no" desc
            )
            group by
            arahan_kerja_id
            , order_no
            , check_date
            , status
            , order_items_id
            , category_name
            , card_no
            , code_no
            , inventory_description
            -- , sku
            , unit_price
            , batch_quantity
            , batch_total_price
            , total_quantity
            , total_price
            order by order_no desc,order_items_id desc
            ) t
            ';

            //kewps10
            $view['kewps10'] = '
            CREATE VIEW "kewps10" as
            with data1 as (
            select distinct
            --select
            --  rownum id
              o."id" "order_id"
              ,ii."id"
              ,o."required_date" "order_required_date"
              ,oi."id" "order_item_id"
              , o."order_no"
              , o."ordered_by"
              , o."order_date"
              ,ta."id" "transaction_id"
              , i."card_no"
              , i."code_no"
              , i."description"
              , oi."rq_quantity"
            --  , count(*) over (partition by oi."id",ii."unit_price") "app_quantity"
              , oi."current_balance"
              , ii."unit_price" "unit_price"
            --  , avg(ii."unit_price") over (partition by oi."id",ii."unit_price") average_unit_price
            --  , sum(ii."unit_price") over (partition by oi."id",ii."unit_price") batch_total_price
              , trunc(oi."created_at") "created_date"
              , oi."created_by"
              , o."approved_by"
              , trunc(o."approved_at") "approved_date"
            from "inventory_item" ii
            left join "inventory_checkin" ic on ic."id" = ii."checkin_id"
            left join "inventory" i on i."id" = ic."inventory_id"
            left join "order_item" oi on oi."id" = ii."checkout_id"
            left join "order" o on o."id" = oi."order_id"
            left join "transaction" t on t."id" = o."transaction_id"
            left join "transaction_all" ta on ta."id" = t."id"
            where ii."checkout_id"  is not null
            -- and order_id = 1174
            )
            select distinct
              "order_id"
              , "order_required_date"
              , "order_item_id"
              , "order_no"
              , "ordered_by"
              , "order_date"
              , "transaction_id"
              , "card_no"
              , "code_no"
              , "description"
              , "rq_quantity"
              , count(*) over (partition by "order_item_id","unit_price") "app_quantity"
              , "current_balance"
              , "unit_price"
              , avg("unit_price") over (partition by "order_item_id","unit_price") "average_unit_price"
              , sum("unit_price") over (partition by "order_item_id","unit_price") "batch_total_price"
              , "created_date"
              , "created_by"
              , "approved_by"
              , "approved_date"
            from data1
            ';

            //credential
            $view['credential'] = '
            CREATE VIEW "credential" as (select rownum as "id",no_pekerja "staff_no", \'admin1\' "password" from payroll.paymas_estor_view)
            ';

            //profile
            $view['profile'] = '
            CREATE VIEW "profile" as (select rownum as "id",no_pekerja "staff_no", nama "name",jawatan "position",keterangan "department" from payroll.paymas_estor_view)
            ';

            //all_maintenance_check
            $view['all_maintenance_check'] = '
            CREATE VIEW "all_maintenance_check" as
            /* ALL CHECKIN TRANSACTIONS */
            with "checkin" as (
            select
            ii."updated_by" "ii_updated_by",ii."sku" "ii_sku",ii."inventory_id" "ii_inventory_id",ii."created_at" "ii_created_at",ii."checkout_id" "ii_checkout_id",ii."deleted_by" "ii_deleted_by",ii."status" "ii_status",ii."unit_price" "ii_unit_price",ii."id" "ii_id",ii."created_by" "ii_created_by",ii."updated_at" "ii_updated_at",ii."dispose_id" "ii_dispose_id",ii."deleted_at" "ii_deleted_at",ii."checkin_id" "ii_checkin_id"
            ,ic."approved_at" "ic_approved_at",ic."inventory_id" "ic_inventory_id",ic."transaction_id" "ic_transaction_id",ic."approved_by" "ic_approved_by",ic."deleted_by" "ic_deleted_by",ic."check_date" "ic_check_date",ic."updated_at" "ic_updated_at",ic."status" "ic_status",ic."check_by" "ic_check_by",ic."approved" "ic_approved",ic."items_quantity" "ic_items_quantity",ic."vendor_id" "ic_vendor_id",ic."store_id" "ic_store_id",ic."budget_id" "ic_budget_id",ic."id" "ic_id",ic."created_by" "ic_created_by",ic."created_at" "ic_created_at",ic."updated_by" "ic_updated_by",ic."deleted_at" "ic_deleted_at",ic."items_total_price" "ic_items_total_price"
            ,i."id" "i_id",i."deleted_at" "i_deleted_at",i."card_no" "i_card_no",i."code_no" "i_code_no",i."updated_by" "i_updated_by",i."created_at" "i_created_at",i."approved_by" "i_approved_by",i."description" "i_description",i."category_id" "i_category_id",i."status" "i_status",i."location" "i_location",i."min_stock" "i_min_stock",i."deleted_by" "i_deleted_by",i."approved_at" "i_approved_at",i."quantity" "i_quantity",i."approved" "i_approved",i."created_by" "i_created_by",i."updated_at" "i_updated_at"
            ,c."deleted_at" "c_deleted_at",c."name" "c_name",c."created_by" "c_created_by",c."id" "c_id",c."updated_at" "c_updated_at",c."updated_by" "c_updated_by",c."created_at" "c_created_at",c."deleted_by" "c_deleted_by",c."status" "c_status"
            ,t."updated_by" "t_updated_by",t."check_date" "t_check_date",t."store_id" "t_store_id",t."check_by" "t_check_by",t."id" "t_id",t."updated_at" "t_updated_at",t."created_by" "t_created_by",t."deleted_at" "t_deleted_at",t."deleted_by" "t_deleted_by",t."status" "t_status",t."type" "t_type",t."created_at" "t_created_at"
            from "inventory_item" ii
            left join "inventory_checkin" ic on ic."id" = ii."checkin_id"
            left join "transaction" t on t."id" = ic."transaction_id"
            left join "inventory" i on i."id" = ic."inventory_id"
            left join "inventory_category" c on c."id" = i."category_id"
            where ii."deleted_by"= 0
            )

            /* ALL CHECKOUT TRANSACTIONS */
            , "checkout" as (
            select
            ii."updated_by" "ii_updated_by",ii."sku" "ii_sku",ii."inventory_id" "ii_inventory_id",ii."created_at" "ii_created_at",ii."checkout_id" "ii_checkout_id",ii."deleted_by" "ii_deleted_by",ii."status" "ii_status",ii."unit_price" "ii_unit_price",ii."id" "ii_id",ii."created_by" "ii_created_by",ii."updated_at" "ii_updated_at",ii."dispose_id" "ii_dispose_id",ii."deleted_at" "ii_deleted_at",ii."checkin_id" "ii_checkin_id"
            ,oi."updated_by" "oi_updated_by",oi."order_id" "oi_order_id",oi."current_balance" "oi_current_balance",oi."rq_quantity" "oi_rq_quantity",oi."inventory_id" "oi_inventory_id",oi."unit_price" "oi_unit_price",oi."updated_at" "oi_updated_at",oi."created_by" "oi_created_by",oi."created_at" "oi_created_at",oi."deleted_at" "oi_deleted_at",oi."deleted_by" "oi_deleted_by",oi."status" "oi_status",oi."app_quantity" "oi_app_quantity",oi."id" "oi_id"
            ,o."approved_by" "o_approved_by",o."order_date" "o_order_date",o."approved_at" "o_approved_at",o."checkout_by" "o_checkout_by",o."required_date" "o_required_date",o."usage_id" "o_usage_id",o."order_no" "o_order_no",o."instruction_id" "o_instruction_id",o."status" "o_status",o."checkout_date" "o_checkout_date",o."deleted_at" "o_deleted_at",o."approved" "o_approved",o."updated_by" "o_updated_by",o."deleted_by" "o_deleted_by",o."id" "o_id",o."transaction_id" "o_transaction_id",o."created_by" "o_created_by",o."updated_at" "o_updated_at",o."created_at" "o_created_at",o."ordered_by_temp" "o_ordered_by_temp",o."ordered_by" "o_ordered_by"
            ,c."deleted_at" "c_deleted_at",c."name" "c_name",c."created_by" "c_created_by",c."id" "c_id",c."updated_at" "c_updated_at",c."updated_by" "c_updated_by",c."created_at" "c_created_at",c."deleted_by" "c_deleted_by",c."status" "c_status"
            ,t."updated_by" "t_updated_by",t."check_date" "t_check_date",t."store_id" "t_store_id",t."check_by" "t_check_by",t."id" "t_id",t."updated_at" "t_updated_at",t."created_by" "t_created_by",t."deleted_at" "t_deleted_at",t."deleted_by" "t_deleted_by",t."status" "t_status",t."type" "t_type",t."created_at" "t_created_at"
            from "inventory_item" ii
            left join "order_item" oi on oi."id" = ii."checkout_id"
            left join "order" o on o."id" = oi."order_id"
            left join "transaction" t on t."id" = o."transaction_id"
            left join "inventory" i on i."id" = oi."inventory_id"
            left join "inventory_category" c on c."id" = i."category_id"
            where ii."checkout_id" is not null and ii."deleted_by"= 0
            )

            /*INVENTORY QUANTITY COLUMN INTEGRIRTY CHECK*/
            , "i_quantity_check" as (
            select * from (
            select * from (
            select distinct
            \'int\' "type",
            \'inventory\' "table_name",
            \'quantity\' "col_name",
            "i_id" "data_id",
            "i_quantity" "fault_value",
            count(*) over (partition by "i_id") "true_value"
            from "checkin"
            where "ii_checkout_id" is null
            and "ii_deleted_by" = 0
            ) t
            union
            select distinct
            \'int\' "type",
            \'inventory\' "table_name",
            \'quantity\' "col_name",
            i."id" "data_id"
            ,i."quantity" "fault_value"
            , 0 "true_value"
            from "inventory" i
            left join "inventory_item" ii on ii."inventory_id" = i."id"
            and ii."deleted_by" = 0
            and ii."checkout_id" is null
            where ii."id" is null and i."quantity" != 0
            ) t2
            where "fault_value" != "true_value"
            )

            -- , "i_quantity_check" as (
            -- select * from (
            -- select distinct
            -- \'int\' "type",
            -- \'inventory\' "table_name",
            -- \'quantity\' "col_name",
            -- "i_id" "data_id",
            -- "i_quantity" "fault_value",
            -- count(*) over (partition by "i_id") "true_value"
            -- from "checkin"
            -- where "ii_checkout_id" is null
            -- )
            -- where "fault_value" != "true_value"
            -- )

            /*INVENTORY_CHECKIN ITEMS_TOTAL_PRICE COLUMN INTEGRIRTY CHECK*/
            , "ic_price_check" as (
            select * from (
            select distinct
            \'int\' "type",
            \'inventory_checkin\' "table_name",
            \'items_total_price\' "col_name",
            "ic_id" "data_id",
            "ic_items_total_price" "fault_value",
            sum("ii_unit_price") over (partition by "ic_id") "true_value"

            from (
            select
            "i_id"
            ,"ic_id"
            ,"i_quantity"
            ,"ic_items_quantity"
            ,"ii_unit_price"
            ,"ic_items_total_price"
            from "checkin"
            )
            )
            where "fault_value" != "true_value"
            )

            /*INVENTORY_CHECKIN ITEMS_QUANTITY COLUMN INTEGRIRTY CHECK*/
            , "ic_quantity_check" as (
            select *
            from (
            select distinct
            \'int\' "type"
            ,\'inventory_checkin\' "table_name"
            ,\'items_quantity\' "col_name"
            ,"ic_id" "data_id"
            ,"ic_items_quantity" "fault_value"
            ,count(*) over (partition by "ic_id") "true_value"
            from (
            select
            "i_id"
            ,"ic_id"
            ,"i_quantity"
            ,"ic_items_quantity"
            ,"ii_unit_price"
            ,"ic_items_total_price"
            from "checkin"
            )
            )
            where "fault_value" != "true_value"
            )

            /*ORDER_ITEM APP_QUANTITY COLUMN INTEGRIRTY CHECK*/
            ,"oi_app_quantity_check" as (
            select distinct
            "type","table_name","col_name","data_id","fault_value","true_value"
            from (
            select
            \'int\' "type"
            ,\'order_item\' "table_name"
            ,\'app_quantity\' "col_name"
            ,"oi_id" "data_id"
            ,"oi_inventory_id"
            ,"oi_app_quantity" "fault_value"
            ,"ii_batch_quantity_checkout" "true_value"
            from (
            select
            "ii_unit_price", "ii_checkout_id", "oi_inventory_id"
            ,"oi_id"
            , count(*) over (partition by "ii_checkout_id") "ii_batch_quantity_checkout"
            , "oi_app_quantity"
            from "checkout"
            order by "ii_updated_at"
            )
            where "ii_checkout_id" is not null
            )
            where "fault_value" != "true_value"
            )

            /*ORDER_ITEM UNIT_PRICE COLUMN INTEGRIRTY CHECK*/
            ,"oi_unit_price_check" as (
            select distinct
            "type","table_name","col_name","data_id","fault_value","true_value"
            from (
            select distinct
            \'int\' "type"
            ,\'order_item\' "table_name"
            ,\'unit_price\' "col_name"
            ,"oi_id" "data_id"
            ,"oi_batch_price" "fault_value"
            ,"ii_batch_price" "true_value"
            ,"ii_unit_price","ii_checkout_id","oi_inventory_id"
            from (
            select
            "oi_id","ii_unit_price", "ii_checkout_id", "oi_inventory_id"
            , "oi_unit_price" "oi_batch_price"
            , sum("ii_unit_price") over (partition by "ii_checkout_id") "ii_batch_price"
            from "checkout"
            order by "ii_updated_at"
            )
            where "ii_checkout_id" is not null
            )
            where "fault_value" != "true_value"
            )

            /*ALL COLUMN INTEGRITY CHECK*/
            , all_col_check as (
            select
            "type","table_name","col_name","data_id"
            ,CAST("fault_value" AS VARCHAR2(200)) "fault_value"
            ,CAST("true_value" AS VARCHAR2(200)) "true_value"
            from "ic_quantity_check"
            union all
            select
            "type","table_name","col_name","data_id"
            ,CAST("fault_value" AS VARCHAR2(200)) "fault_value"
            ,CAST("true_value" AS VARCHAR2(200)) "true_value"
            from "ic_price_check"
            union all
            select
            "type","table_name","col_name","data_id"
            ,CAST("fault_value" AS VARCHAR2(200)) "fault_value"
            ,CAST("true_value" AS VARCHAR2(200)) "true_value"
            from "i_quantity_check"
            union all
            /* check oi price integrity*/
            select
            "type","table_name","col_name","data_id"
            ,CAST("fault_value" AS VARCHAR2(200)) "fault_value"
            ,CAST("true_value" AS VARCHAR2(200)) "true_value"
            from "oi_unit_price_check"
            union all
            /*check oi app_quantity integrity*/
            select
            "type","table_name","col_name","data_id"
            ,CAST("fault_value" AS VARCHAR2(200)) "fault_value"
            ,CAST("true_value" AS VARCHAR2(200)) "true_value"
            from "oi_app_quantity_check"
            )

            /*SUMMARY*/
            select
            ROWNUM AS "id",all_col_check."type",all_col_check."table_name",all_col_check."col_name",all_col_check."data_id"
            ,all_col_check."fault_value",all_col_check."true_value"
            from all_col_check

            ';



            break;

            case 'mysql':
                //Approval
            $view['approval'] = "
            CREATE VIEW approval AS
            SELECT
            row_number() over (ORDER BY ii.id)  AS id
            ,ii.id id_inventory_item
            ,i.id id_inventory
            ,c.id id_category
            ,ic.id id_inventory_checkin
            ,oi.id id_order_item
            ,o.id id_order
            ,ii.sku
            ,ii.unit_price
            ,o.order_no
            ,c.name items_category_name
            ,i.description items_inventory_name
            ,oi.rq_quantity
            ,oi.app_quantity
            ,o.approved
            ,oi.current_balance
            ,oi.unit_price total_price
            ,o.order_date
            ,o.required_date
            from inventory_item ii
            left join inventory_checkin ic on ic.id = ii.checkin_id
            left join inventory i on i.id = ic.inventory_id
            left join inventory_category c on c.id = i.category_id
            left join order_item oi on oi.inventory_id = i.id
            left join `order` o on o.id = oi.order_id
            where ii.checkout_id is null
            and ii.deleted_by = 0
            and ifnull(oi.app_quantity,0) < oi.rq_quantity";

            //Usage List
            $view['usage_list'] = "
            CREATE VIEW usage_list AS
            SELECT
            row_number() over ()  AS id
            , t.*
            FROM (
            SELECT id ref_id,'vehicle' ref_cat,reg_no name FROM vehicle
            UNION
            SELECT id ref_id,'workshop' ref_cat,name name FROM workshop
            UNION
            SELECT id ref_id,'store' ref_cat,name name FROM store
            ) t
            ";


            //sisken
            // $view['sisken'] = "
            // CREATE VIEW sisken AS
            // select TAHUN,BULAN,ID,NO_KERJA,ARAHAN_KERJA,JK_JG,BENGKEL_PANEL_ID,NAMA_PANEL,KATEGORI_PANEL,SELENGGARA_ID,TEMPOH_SELENGGARA,ALASAN_ID,ALASAN,TARIKH_ARAHAN,NO_SEBUTHARGA,TARIKH_KENDERAAN_TIBA,TARIKH_JANGKA_SIAP,TARIKH_SEBUTHARGA,STATUS_SEBUTHARGA,NO_DO,TARIKH_SIAP,TEMPOH_JAMINAN,TARIKH_DO,TARIKH_CETAK,STATUS,CATATAN_SEBUTHARGA,CATATAN,NAMA_PIC,TARIKH_PERMOHONAN,ID_KENDERAAN,KATEGORI_KEROSAKAN_ID,KATEGORI_KEROSAKAN,KATEGORI,ISSERVICE,ISPANCIT,ODOMETER_TERKINI,NO_PLAT,MODEL,KOD_JABATAN,NAMA_JABATAN,JUMLAH_KOS from usersisken.LAPORAN_PENYELENGGARAAN_ESTOR";


            //report_in_quarter
            // $view['report_in_quarter'] = '
            // CREATE VIEW "report_in_quarter" AS
            // WITH DATA1 AS (
            // select
            // ii."sku"
            // ,ii."unit_price"
            // ,ic."inventory_id"
            // ,t."check_date"
            // ,extract(year from t."check_date") "year"
            // ,to_char(t."check_date",\'Q\') "quarter"
            // from "inventory_item" ii
            // left join "inventory_checkin" ic on ic."id" = ii."checkin_id"
            // left join "transaction" t on t."id" = ic."transaction_id"
            // where ii."deleted_by" = 0)

            // SELECT
            // distinct "year",
            // -- rownum as id,
            // "quarter",
            // COUNT(*) OVER (PARTITION BY "year","quarter") "count",
            // SUM("unit_price") OVER (PARTITION BY "year","quarter") "total_price"
            // FROM DATA1
            // ORDER BY "year","quarter"
            // ';

            // //report_out_quarter
            // $view['report_out_quarter'] = '
            // CREATE VIEW "report_out_quarter" AS
            // WITH DATA1 AS (
            // select
            // ii."sku"
            // ,ii."unit_price"
            // ,oi."inventory_id"
            // ,t."check_date"
            // ,extract(year from t."check_date") "year"
            // ,to_char(t."check_date",\'Q\') "quarter"
            // from "inventory_item" ii
            // left join "order_item" oi on oi."id" = ii."checkout_id"
            // left join "order" o on o."id" = oi."order_id"
            // left join "transaction" t on t."id" = o."transaction_id"
            // where ii."deleted_by" = 0 and ii."checkout_id" is not null)

            // SELECT
            // DISTINCT "year",
            // -- rownum as id,
            // "quarter",
            // count(*) over (partition by "year","quarter") "count",
            // sum("unit_price") over (partition by "year","quarter") "total_price"
            // FROM DATA1
            // ORDER BY "year","quarter"
            // ';

            // //list_year
            // $view['list_year'] = '
            // CREATE VIEW "list_year" AS
            // select "year","quarter" from "report_in_quarter" union
            // select "year","quarter" from "report_out_quarter"
            // ';

            // //report_all
            // $view['report_all'] = '
            // CREATE VIEW "report_all" AS
            // select
            // rownum as "id",
            // "list_year"."year",
            // "list_year"."quarter",
            // nvl("report_in_quarter"."count", \'0\') "count_in",
            // nvl("report_in_quarter"."total_price", \'0\') "price_in",
            // nvl("report_out_quarter"."count", \'0\') count_out,
            // nvl("report_out_quarter"."total_price", \'0\') price_out,

            // sum(nvl("report_in_quarter"."count", \'0\')) over (order by "list_year"."year" rows unbounded preceding) -
            // sum(nvl("report_out_quarter"."count", \'0\')) over (order by "list_year"."year" rows unbounded preceding)
            // "count_current",
            // sum(nvl("report_in_quarter"."total_price", \'0\')) over (order by "list_year"."year" rows unbounded preceding) -
            // sum(nvl("report_out_quarter"."total_price", \'0\')) over (order by "list_year"."year" rows unbounded preceding)
            // "price_current"

            // from "list_year"
            // left join "report_in_quarter"
            // on ("report_in_quarter"."year" = "list_year"."year" and "list_year"."quarter" = "report_in_quarter"."quarter")
            // left join "report_out_quarter"
            // on ("list_year"."year" = "report_out_quarter"."year" and "list_year"."quarter" = "report_out_quarter"."quarter")
            // order by "list_year"."year", "list_year"."quarter"
            // ';

            break;
            default:
            echo "no view created";
            break;
        }
        if(isset($view)){
            foreach ($view as $key => $value) {
                $time = microtime(true);
                    // echo "    > $value";
                echo "    > create view for $key ...";
                $this->db->createCommand($value)->execute();
                echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
            }
        }

    }}
