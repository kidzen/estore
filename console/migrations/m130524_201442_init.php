<?php

use common\components\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $this->customDrop();
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_by' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'avatar' => $this->string(),
            'staff_no' => $this->string()->notNull()->unique(),
            'position' => $this->string(),
            'department' => $this->string(),
            'avatar' => $this->string(),
            'ic_no' => $this->string()->unique(),
            'contact' => $this->string(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_by' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%setting}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique(),
            'label' => $this->string(),
            'value' => $this->string(),
            'description' => $this->string(),

            'remark' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_by' => $this->integer()->defaultValue(0),
        ], $tableOptions);
        // section
        $this->createTable('{{%section}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'name' => $this->string()->notNull()->unique(),
            'address' => $this->string(),
            'contact_no' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // section_assignment
        $this->createTable('{{%section_assignment}}', [
            // 'id' => $this->primaryKey(),
            'section_id' => $this->integer(),
            'user_id' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        $i = 1;
        $this->addPrimaryKey('pk'.$i++,'{{section_assignment}}',['section_id','user_id']);

        // section_category
        $this->createTable('{{%section_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // category
        $this->createTable('{{%asset_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // category
        $this->createTable('{{%asset_brand_jt}}', [
            'brand_id' => $this->integer(),
            'category_id' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        $this->addPrimaryKey('pkc1','asset_brand_jt',['brand_id','category_id']);
        // category
        $this->createTable('{{%asset_brand}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // category
        $this->createTable('{{%asset_model}}', [
            'id' => $this->primaryKey(),
            'brand_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // asset
        $this->createTable('{{%asset}}', [
            'id' => $this->primaryKey(),
            'transaction_id' => $this->integer(),
            'category_id' => $this->integer(),
            'brand_id' => $this->integer(),
            'model_id' => $this->integer(),
            'card_no' => $this->string()->unique(),
            'code_no' => $this->string()->unique(),
            'description' => $this->string()->notNull()->unique(),
            'quantity' => $this->integer(),
            'min_stock' => $this->integer(),
            'approved_id' => $this->integer(),
            'approved_at' => $this->timestamp(),
            'approved_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // asset_checkin
        $this->createTable('{{%asset_checkin}}', [
            'id' => $this->primaryKey(),
            'rnvr' => $this->string()->notNull()->unique(),
            'transaction_id' => $this->integer(),
            'budget_id' => $this->integer(),
            // 'section_id' => $this->integer(),
            'asset_id' => $this->integer(),
            'supplier_id' => $this->integer(),
            // 'items_quantity' => $this->integer(),
            // 'items_unit_price' => $this->float(),
            'check_date' => $this->date(),
            'check_by' => $this->integer(),
            'purchased_date' => $this->date(),
            'refference_file' => $this->string(),
            'detail' => $this->text(),
            'approved_id' => $this->integer(),
            'approved_at' => $this->timestamp(),
            'approved_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // asset_item
        $this->createTable('{{%asset_item}}', [
            'id' => $this->primaryKey(),
            'section_id' => $this->integer(),
            'checkin_id' => $this->integer(),
            'order_item_id' => $this->integer(),
            'dispose_id' => $this->integer(),
            'serial_no' => $this->string()->unique(),
            'stock_no' => $this->string()->unique(),
            'unit_price' => $this->decimal(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // item_movement
        $this->createTable('{{%item_movement}}', [
            'id' => $this->primaryKey(),
            // 'transaction_id' => $this->integer(),
            'order_id' => $this->integer(),
            'type_id' => $this->integer(),
            'section_id' => $this->integer(),
            'asset_id' => $this->integer(),
            // 'from' => $this->integer(),
            // 'checkin_id' => $this->integer(),
            // 'checkout_id' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // item_movement_type
        $this->createTable('{{%item_movement_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // order
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'transaction_id' => $this->integer(),
            'instruction_id' => $this->integer(),
            'order_date' => $this->date(),
            'ordered_by' => $this->integer(),
            'order_no' => $this->string(),
            'usage_id' => $this->integer(),
            'required_date' => $this->date(),
            'checkout_date' => $this->date(),
            'checkout_by' => $this->integer(),
            'approved_id' => $this->integer(),
            'approved_at' => $this->timestamp(),
            'approved_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // order_item
        $this->createTable('{{%order_item}}', [
            'id' => $this->primaryKey(),
            'asset_id' => $this->integer(),
            // 'section_id' => $this->integer(),
            'order_id' => $this->integer(),
            'rq_quantity' => $this->integer(),
            'app_quantity' => $this->integer(),
            'current_balance' => $this->integer(),
            'total_price' => $this->float(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // package
        $this->createTable('{{%package}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'detail' => $this->string(),
            'delivery' => $this->string(),
            'package_by' => $this->integer(),
            'package_date' => $this->date(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // transaction
        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer(),
            'section_id' => $this->integer(),
            'check_date' => $this->date(),
            'check_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // transaction_type
        $this->createTable('{{%transaction_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // budget
        $this->createTable('{{%budget}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'revenue' => $this->double(),
            'balance' => $this->double(),
            'min_balance' => $this->double(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose
        $this->createTable('{{%dispose}}', [
            'id' => $this->primaryKey(),
            'refference_no' => $this->string(),
            'transaction_id' => $this->integer(),
            'category_id' => $this->integer(),
            'method_id' => $this->integer(),
            'reason_id' => $this->integer(),
            'quantity' => $this->integer(),
            'current_revenue' => $this->double(),
            'cost' => $this->double(),
            'returns' => $this->double(),
            'description' => $this->string(),
            'requested_date' => $this->date(),
            'requested_by' => $this->integer(),
            'disposed_date' => $this->date(),
            'disposed_by' => $this->integer(),
            'approved_id' => $this->integer(),
            'approved_at' => $this->timestamp(),
            'approved_by' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_detail_key
        $this->createTable('{{%dispose_detail_key}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_detail_value
        $this->createTable('{{%dispose_detail_value}}', [
            'id' => $this->primaryKey(),
            'dispose_id' => $this->integer(),
            'key_id' => $this->integer(),
            'value' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_category
        $this->createTable('{{%dispose_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_method
        $this->createTable('{{%dispose_method}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // dispose_reason
        $this->createTable('{{%dispose_reason}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // usage
        $this->createTable('{{%usage}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'item_id' => $this->integer(),
            'reason' => $this->text(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // usage_category
        $this->createTable('{{%usage_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // usage_detail_key
        $this->createTable('{{%usage_detail_key}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),  // usage_category
            'name' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // usage_detail_value
        $this->createTable('{{%usage_detail_value}}', [
            'id' => $this->primaryKey(),
            'key_id' => $this->integer(),   // usage_key
            'usage_id' => $this->integer(),     // col detail
            'value' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // supplier
        $this->createTable('{{%supplier}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'address' => $this->string(),
            'contact_no' => $this->string(),
            'email' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // direct_issue
        $this->createTable('{{%direct_issue}}', [
            'id' => $this->primaryKey(),
            'ivr' => $this->string()->notNull()->unique(),
            'transaction_id' => $this->integer(),
            'type_id' => $this->integer(),
            // 'serial_no' => $this->string(),
            'refference_no' => $this->string(),
            'particulars' => $this->text(),
            'amount' => $this->integer(),
            'total' => $this->double(),
            'supplier_id' => $this->integer(),
            // 'section_id' => $this->integer(),
            'officer_name' => $this->string(),
            'date' => $this->date(),
            'receipt_no' => $this->string(),

            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // asset_item
        $this->createTable('{{%direct_issue_item}}', [
            'id' => $this->primaryKey(),
            'direct_issue_id' => $this->integer(),
            'dispose_id' => $this->integer(),
            'serial_no' => $this->string(),
            'unit_price' => $this->decimal(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // direct_issue_type
        $this->createTable('{{%direct_issue_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
        // approve_level
        $this->createTable('{{%approve_level}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer(),
            'name' => $this->string(),
            'color_code' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        // approve_level_type
        $this->createTable('{{%approve_level_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'status' => $this->integer()->defaultValue(1),
            'deleted_by' => $this->integer()->defaultValue(0),
            'deleted_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->insertFk();
        // $this->insertIdx();
        $this->defaultScenario();
    }

    public function down()
    {
        $this->customDrop();
    }

    public function insertFk() {
        $i = 1;
        // $this->addForeignKey('fk'.$i++,'{{profile}}','id','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{section}}','category_id','{{section_category}}','id');
        $this->addForeignKey('fk'.$i++,'{{section_assignment}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$i++,'{{section_assignment}}','user_id','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset}}','category_id','{{asset_category}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset}}','approved_id','{{approve_level}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset_checkin}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset_checkin}}','budget_id','{{budget}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset_checkin}}','asset_id','{{asset}}','id');
        // $this->addForeignKey('fk'.$i++,'{{asset_checkin}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset_checkin}}','supplier_id','{{supplier}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset}}','brand_id','{{asset_brand}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset}}','model_id','{{asset_model}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset_item}}','checkin_id','{{asset_checkin}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset_item}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset_item}}','order_item_id','{{order_item}}','id');
        $this->addForeignKey('fk'.$i++,'{{asset_item}}','dispose_id','{{dispose}}','id');
        // $this->addForeignKey('fk'.$i++,'{{item_movement}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{item_movement}}','type_id','{{item_movement_type}}','id');
        $this->addForeignKey('fk'.$i++,'{{item_movement}}','order_id','{{order}}','id');
        $this->addForeignKey('fk'.$i++,'{{item_movement}}','asset_id','{{asset_item}}','id');
        $this->addForeignKey('fk'.$i++,'{{item_movement}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$i++,'{{order}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{order}}','usage_id','{{usage}}','id');
        // $this->addForeignKey('fk'.$i++,'{{order}}','instruction_id','{{instruction}}','id');
        // $this->addForeignKey('fk'.$i++,'{{order_item}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$i++,'{{order_item}}','asset_id','{{asset}}','id');
        $this->addForeignKey('fk'.$i++,'{{order_item}}','order_id','{{order}}','id');
        $this->addForeignKey('fk'.$i++,'{{transaction}}','type_id','{{transaction_type}}','id');
        $this->addForeignKey('fk'.$i++,'{{dispose}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{dispose}}','category_id','{{dispose_category}}','id');
        $this->addForeignKey('fk'.$i++,'{{dispose}}','method_id','{{dispose_method}}','id');
        $this->addForeignKey('fk'.$i++,'{{dispose}}','reason_id','{{dispose_reason}}','id');
        $this->addForeignKey('fk'.$i++,'{{dispose}}','approved_id','{{approve_level}}','id');
        $this->addForeignKey('fk'.$i++,'{{dispose_detail_value}}','dispose_id','{{dispose}}','id');
        $this->addForeignKey('fk'.$i++,'{{dispose_detail_value}}','key_id','{{dispose_detail_key}}','id');
        $this->addForeignKey('fk'.$i++,'{{direct_issue}}','type_id','{{direct_issue_type}}','id');
        $this->addForeignKey('fk'.$i++,'{{direct_issue}}','supplier_id','{{supplier}}','id');
        // $this->addForeignKey('fk'.$i++,'{{direct_issue}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$i++,'{{direct_issue_item}}','direct_issue_id','{{direct_issue}}','id');
        $this->addForeignKey('fk'.$i++,'{{direct_issue_item}}','dispose_id','{{dispose}}','id');
        $this->addForeignKey('fk'.$i++,'{{direct_issue}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{transaction}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$i++,'{{approve_level}}','type_id','{{approve_level_type}}','id');

        // not needed
        // $this->addForeignKey('fk'.$i++,'{{package}}','order_id','{{order}}','id');
        // $this->addForeignKey('fk'.$i++,'{{usage}}','category_id','{{usage_category}}','id');
        // $this->addForeignKey('fk'.$i++,'{{usage}}','item_id','{{usage_item}}','id');

        $this->addExtraFk($i);
    }
    public function addExtraFk($i) {

        $dbName = $this->getDbname($this->db);
        switch ($this->db->driverName) {
            case 'mysql':
            $userBy = "
            SELECT
            -- table_name as TABLE_NAME
            column_name as COLUMN_NAME
            , table_name as TABLE_NAME
            FROM information_schema.`COLUMNS`
            WHERE table_schema = '$dbName'
            AND table_name NOT IN  ('log')
            AND column_name LIKE '%by'
            AND column_name NOT LIKE '%deleted%'
            ";
            $approved = "
            SELECT
            -- table_name as TABLE_NAME
            column_name as COLUMN_NAME
            , table_name as TABLE_NAME
            FROM information_schema.`COLUMNS`
            WHERE table_schema = '$dbName'
            AND table_name NOT IN  ('log')
            AND column_name LIKE 'approved_id'
            ";
            break;
            case 'oci':
            $userBy = "
            -- SELECT
            -- TABLE_NAME
            -- FROM USER_TABLES
            -- UNION ALL
            -- SELECT
            -- VIEW_NAME AS TABLE_NAME
            -- FROM USER_VIEWS
            -- UNION ALL
            -- SELECT
            -- MVIEW_NAME AS TABLE_NAME
            -- FROM USER_MVIEWS
            -- ORDER BY TABLE_NAME
            ";

            break;
            default:
            $userBy = false;
            break;
        }
        if($userBy){
            $query = $this->db->createCommand($userBy)->queryAll();
            foreach ($query as $key => $value) {
                $this->addForeignKey('fk'.$i++,'{{'.$value['TABLE_NAME'].'}}',$value['COLUMN_NAME'],'{{user}}','id');
            }
        }
        if($approved){
            $query = $this->db->createCommand($approved)->queryAll();
            foreach ($query as $key => $value) {
                $this->addForeignKey('fk'.$i++,'{{'.$value['TABLE_NAME'].'}}',$value['COLUMN_NAME'],'{{approve_level}}','id');
            }
        }

    }
    public function insertFk2() {
        $i = 1;
        $this->addForeignKey('fk'.$i++,'{{profile}}','id','{{user}}','id');

            // section
        $this->addForeignKey('fk'.$i++,'{{%section}}','category_id','{{section_category}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%section}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%section}}','created_by','{{user}}','id');
            // section_assignment
        $this->addForeignKey('fk'.$i++,'{{%section_assignment}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$i++,'{{%section_assignment}}','user_id','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%section_assignment}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%section_assignment}}','created_by','{{user}}','id');
            // asset_category
        // $this->addForeignKey('fk'.$i++,'{{%asset_category}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%asset_category}}','created_by','{{user}}','id');
            // asset
        // $this->addForeignKey('fk'.$i++,'{{%asset}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%asset}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset}}','approved_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset}}','category_id','{{asset_category}}','id');
            // asset_checkin
        $this->addForeignKey('fk'.$i++,'{{%asset_checkin}}','budget_id','{{budget}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%asset_checkin}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%asset_checkin}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_checkin}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_checkin}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_checkin}}','asset_id','{{asset}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_checkin}}','supplier_id','{{supplier}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_checkin}}','check_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_checkin}}','approved_by','{{user}}','id');
            // asset_item
        // $this->addForeignKey('fk'.$i++,'{{%asset_item}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%asset_item}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_item}}','asset_id','{{asset}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_item}}','checkin_id','{{asset_checkin}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_item}}','checkout_id','{{order_item}}','id');
        $this->addForeignKey('fk'.$i++,'{{%asset_item}}','dispose_id','{{dispose}}','id');
            // order
        // $this->addForeignKey('fk'.$i++,'{{%order}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%order}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%order}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{%order}}','ordered_by','{{user}}','id');
        if($this->DbDriver == 'mysql'){
            $this->addForeignKey('fk'.$i++,'{{%order}}','usage_id','{{usage}}','id');
        }
        $this->addForeignKey('fk'.$i++,'{{%order}}','checkout_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%order}}','approved_by','{{user}}','id');
            // order_item
        // $this->addForeignKey('fk'.$i++,'{{%order_item}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%order_item}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%order_item}}','asset_id','{{asset}}','id');
        $this->addForeignKey('fk'.$i++,'{{%order_item}}','order_id','{{order}}','id');
            // package
        // $this->addForeignKey('fk'.$i++,'{{%package}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%package}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%package}}','order_id','{{order}}','id');
        $this->addForeignKey('fk'.$i++,'{{%package}}','package_by','{{order}}','id');
            // role
        // $this->addForeignKey('fk'.$i++,'{{%role}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%role}}','created_by','{{user}}','id');
            // transaction
        // $this->addForeignKey('fk'.$i++,'{{%transaction}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%transaction}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%transaction}}','check_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%transaction}}','section_id','{{section}}','id');
            // usage_category
        // $this->addForeignKey('fk'.$i++,'{{%usage_category}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_category}}','created_by','{{user}}','id');
            // usage
        $this->addForeignKey('fk'.$i++,'{{%usage}}','category_id','{{usage_category}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage}}','created_by','{{user}}','id');
            // usage_detail_key
        $this->addForeignKey('fk'.$i++,'{{%usage_detail_key}}','category_id','{{usage_category}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_detail_key}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_detail_key}}','created_by','{{user}}','id');
            // usage_detail_value
        $this->addForeignKey('fk'.$i++,'{{%usage_detail_value}}','usage_id','{{usage}}','id');
        $this->addForeignKey('fk'.$i++,'{{%usage_detail_value}}','key_id','{{usage_detail_key}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_detail_value}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_detail_value}}','created_by','{{user}}','id');
            // vehicle
        // $this->addForeignKey('fk'.$i++,'{{%vehicle}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%vehicle}}','created_by','{{user}}','id');
            // workshop
        // $this->addForeignKey('fk'.$i++,'{{%workshop}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%workshop}}','created_by','{{user}}','id');
            // supplier
        // $this->addForeignKey('fk'.$i++,'{{%supplier}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%supplier}}','created_by','{{user}}','id');
            // dispose
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','category_id','{{dispose_category}}','id');
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','method_id','{{dispose_method}}','id');
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','reason_id','{{dispose_reason}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','requested_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','disposed_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','approved_by','{{user}}','id');
            // dispose_item
        // $this->addForeignKey('fk'.$i++,'{{%dispose_item}}','item_id','{{asset_item}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_item}}','dispose_id','{{dispose}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_item}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_item}}','created_by','{{user}}','id');
            // dispose_method
        // $this->addForeignKey('fk'.$i++,'{{%dispose_method}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_method}}','created_by','{{user}}','id');
            // dispose_category
        // $this->addForeignKey('fk'.$i++,'{{%dispose_category}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_category}}','created_by','{{user}}','id');
            // dispose_reason
        // $this->addForeignKey('fk'.$i++,'{{%dispose_reason}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_reason}}','created_by','{{user}}','id');
    }

    public function insertIdx() {
        $i = 1;
        $this->createIndex('idx'.$i++,'user','username');
        $this->createIndex('idx'.$i++,'setting','key');

        $this->addExtraIdx($i);
    }

    public function defaultScenario() {
        $this->batchInsert('{{user}}',['username','email','password_hash','auth_key'],[
            ['admin1','admin1@mail.com',\Yii::$app->security->generatePasswordHash('admin1'),\Yii::$app->security->generateRandomString()],
        ]);
        $this->batchInsert('{{profile}}',['name','ic_no','staff_no','contact','email'],[
            ['admin1','910207065155','T3007612','0132059953','admin1@mail.com'],
        ]);
        $this->batchInsert('{{setting}}',['label','description','key','value'],[
            ['Default themes','Default theme will be use if no theme is set.','defaultTheme','adminlte'],
            ['Testing email','Testing email are use for testing mailing purpose.','testEmail','admin@example.com'],
        ]);

/*        $this->batchInsert('{{%asset_brand}}', ['name'],
            [
                ['SIMOCO'],
                ['MOTOROLA'],
                ['SINGER'],
                ['PANASONIC'],
                ['HICOM'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%asset_model}}', ['brand_id','name'],
            [
                [$i++, 'SIMOCO A'],
                [$i++, 'MOTOROLA A'],
                [$i++, 'SINGER A'],
                [$i++, 'TV 14'],
                [$i++, '3 TON'],
            ]
        );*/
        $i = 1;
        $this->batchInsert('{{%section}}', ['id','name'],
            [
                [$i++, 'CAW Khas'],
                [$i++, 'Stor'],
                [$i++, 'Muzzam'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%transaction_type}}', ['id','name'],
            [
                [$i++, 'Asset Registration'],
                [$i++, 'Checkin'],
                [$i++, 'Direct Issue'],
                [$i++, 'Checkout'],
                [$i++, 'Movement'],
                [$i++, 'Dispose'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%section_assignment}}', ['user_id','section_id'],
            [
                [1, 1],
                // [1, 2],
                [1, 3],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%asset_category}}', ['id','name'],
            [
                [$i++, 'Equipment'],
                [$i++, 'Vehicle'],
                [$i++, 'Radio'],
            ]
        );
/*        $i = 1;
        $this->batchInsert('{{%asset_brand_jt}}', ['category_id','brand_id'],
            [
                [3,1],
                [3,2],
                [3,3],
                [1,4],
                [2,5],
            ]
        );*/
        $i = 1;
        $this->batchInsert('{{%approve_level}}',['name'],
            [
                ['Pending'],
                ['In Progress'],
                ['Approved'],
                ['Rejected'],
            ]
        );
/*        $i = 1;
        $this->batchInsert('{{%asset}}',
            ['category_id','card_no','code_no','brand_id','model_id','description','quantity','min_stock','approved_id'],
            [
                [1,'E001','EA001',4,4,'PANASONIC TV 14',0,5,1,],
                [2,'V001','VA002',5,5,'JKV 0561',0,5,1,],
                [3,'R001','RA002',1,1,'SIMOCO 30Mhz 100m',0,5,1,],
                [3,'R002','RA003',2,2,'MOTOROLA 30Mhz 100m',0,5,1,],
                [1,'E002','EA003',3,3,'SINGER MACHINE',0,5,1,],
            ]
        );*/
        $i = 1;
        $this->batchInsert('{{%supplier}}',
            ['id','name'],
            [
                [$i++,'Muzzam'],
                [$i++,'Giant'],
                [$i++,'Stor Pusat'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%direct_issue_type}}',
            ['name'],
            [
                ['Direct Issue'],
                ['Software'],
                ['Jobsheet'],
                ['Telephone'],
                ['Transportation'],
                ['Admin'],
                ['Others'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%item_movement_type}}',
            ['name'],
            [
                ['Rental'],
                ['Placement / Transfer'],
                ['Maintenance By Supplier'],
                ['Maintenance By Outsource'],
                ['Others'],
            ]
        );
        $this->batchInsert('{{%budget}}',
            ['name'],
            [
                ['SS'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%dispose_category}}',
            ['id','name'],
            [
                [$i++,'Hapus Kira'],
                [$i++,'Lupus'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%dispose_reason}}',
            ['id','name'],
            [
                [$i++,'Tidak Ekonomi Dibaiki'],
                [$i++,'Usang'],
                [$i++,'Rosak dan tidak boleh digunakan'],
                [$i++,'Luput tempoh penggunaan'],
                [$i++,'Keupayaan aset tidak lagi di peringkat optimum'],
                [$i++,'Tiada alat ganti di pasaran'],
                [$i++,'Pembekal tidak lagi memberi khidmat sokongan'],
                [$i++,'Di syor selepas pemeriksaan aset'],
                [$i++,'Tidak lagi diperlukan oleh Jabatan'],
                [$i++,'Perubahan teknologi'],
                [$i++,'Melebihi keperluan'],
                [$i++,'Hilang'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%dispose_method}}',
            ['id','name'],
            [
                [$i++,'Jualan secara tender'],
                [$i++,'Jualan secara sebut harga'],
                [$i++,'Jualan secara lelong'],
                [$i++,'E-Waste'],
                [$i++,'Buangan Terjadual'],
                [$i++,'Sisa Pepejal'],
                [$i++,'Jualan Sisa'],
                [$i++,'Tukar Barang atau Perkhidmatan'],
                [$i++,'Tukar Beli'],
                [$i++,'Tukar Ganti'],
                [$i++,'Hadiah'],
                [$i++,'Musnah'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%usage_category}}',
            ['id','name'],
            [
                [$i++,'Bengkel'],
                [$i++,'Kenderaan'],
                [$i++,'Stor'],
            ]
        );
        $commands[] = Yii::$app->db->createCommand("INSERT INTO asset (description) SELECT DISTINCT detail AS description FROM pdrm_old.`equipment` WHERE detail IS NOT NULL AND detail != ''");
        $commands[] = Yii::$app->db->createCommand("INSERT INTO supplier (name,address) SELECT DISTINCT detailsup,address FROM pdrm_old.supplier WHERE detailsup IS NOT NULL AND detailsup != ''");
        foreach ($commands as $key => $command) {
            $command->execute();
            var_dump($command);
        }
    }

    protected function insertDefaultData() {
/*        $i = 1;
        $this->batchInsert('{{%role}}', ['id','name','created_at','updated_at'],
            [
                [$i++,'Administrator',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'Pegawai Stor',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'Jurugegas',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'Vendor',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'Admin Stor',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'User Jabatan',$this->initialTimeStamp,$this->initialTimeStamp],
            ]
        );*/
        $i = 1;
        $this->batchInsert('{{%user}}', ['id','username','email','password','role_id','auth_key'],
            [
                // [1,'test','test@gmail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [10001,'admin3','admin3@gmail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [19875,'nafishah','nafishah.sulaiman@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [12260,'admin1','afizah@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [70202,'admin2','admin2@gmail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [37784,'rosli','rosli@mail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [90399,'awie','rawiramlan@gmail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [19892,'shima','nurhashima.ishak@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [17658,'17658','rajamashitah@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [11274,'yusri','yusripuskeb@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
            ]
        );



        $i = 1;
        $this->batchInsert('{{%section_category}}',
            ['id','name'],
            [
                [$i++,'Pusat'],
                [$i++,'Utama'],
                [$i++,'Unit'],
            ]
        );
            // $this->batchInsert('{{%workflow}}',
            //     ['name'],
            //     [
            //         ['Requested'],
            //         ['Pending'],
            //         ['Approved'],
            //         ['Checkout'],
            //         ['Checkin'],
            //         ['Rejected'],
            //         ['Disposed'],
            //     ]
            // );
            // $this->batchInsert('{{%workflow_module}}',
            //     ['module_id','module_name','wf_order','status_id'],
            //     [
            //         [1,1],
            //         [2,2],
            //         [3,3],
            //         [3,4],
            //     ]
            // );
        $i = 1;
        $this->batchInsert('{{%section}}',
            ['id','category_id','name'],
            [
                [$i++,1,'Stor Pusat A'],
                [$i++,2,'Stor Utama B'],
                [$i++,3,'Stor Unit C'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%dispose_category}}',
            ['id','name'],
            [
                [$i++,'Hapus Kira'],
                [$i++,'Lupus'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%dispose_reason}}',
            ['id','name'],
            [
                [$i++,'Tidak Ekonomi Dibaiki'],
                [$i++,'Usang'],
                [$i++,'Rosak dan tidak boleh digunakan'],
                [$i++,'Luput tempoh penggunaan'],
                [$i++,'Keupayaan aset tidak lagi di peringkat optimum'],
                [$i++,'Tiada alat ganti di pasaran'],
                [$i++,'Pembekal tidak lagi memberi khidmat sokongan'],
                [$i++,'Di syor selepas pemeriksaan aset'],
                [$i++,'Tidak lagi diperlukan oleh Jabatan'],
                [$i++,'Perubahan teknologi'],
                [$i++,'Melebihi keperluan'],
                [$i++,'Hilang'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%dispose_method}}',
            ['id','name'],
            [
                [$i++,'Jualan secara tender'],
                [$i++,'Jualan secara sebut harga'],
                [$i++,'Jualan secara lelong'],
                [$i++,'E-Waste'],
                [$i++,'Buangan Terjadual'],
                [$i++,'Sisa Pepejal'],
                [$i++,'Jualan Sisa'],
                [$i++,'Tukar Barang atau Perkhidmatan'],
                [$i++,'Tukar Beli'],
                [$i++,'Tukar Ganti'],
                [$i++,'Hadiah'],
                [$i++,'Musnah'],
            ]
        );
            // $this->batchInsert('{{%supplier}}',
            //     ['name'],
            //     [
            //         ['Sarra Global Sdn Bhd'],
            //     ]
            // );
            // $this->batchInsert('{{%asset_category}}',
            //     ['name'],
            //     [
            //         ['Air Filter'],
            //         ['Sticker'],
            //     ]
            // );
            // $this->batchInsert('{{%asset}}',
            //     ['category_id','card_no','code_no','description',],
            //     [
            //         [1,'CA1','CO1','NANO'],
            //         [1,'CA2','CO2','MICRO'],
            //         [2,'KA1','KO1','KILO'],
            //         [2,'KA2','KO2','MEGA'],
            //     ]
            // );



            // $this->batchInsert('{{%vehicle}}',
            //     ['reg_no','model','type_id'],
            //     [
            //         ['LA 1111','DAIHATSU','TRAK'],
            //         ['LA 2222','HICOM','LORI'],
            //     ]
            // );

        // $i = 1;
        // $this->batchInsert('{{%workshop}}',
        //     ['id','name','address','contact'],
        //     [
        //         [$i++,"BENGKEL","PUSKEB","MPSP"],
        //         [$i++,"BILIK SERVIS","PUSKEB","MPSP"],
        //         [$i++,"Blower","PUSKEB",""],
        //         [$i++,"Depo Asas Murni","MPSP",""],
        //         [$i++,"Depo Berapit","MPSP",""],
        //         [$i++,"Depo Bertam","MPSP",""],
        //         [$i++,"Depo Jalan Betik","MPSP",""],
        //         [$i++,"Depo Tasek Gelugor","MPSP",""],
        //         [$i++,"High PressureCleaner","",""],
        //         [$i++,"KIMPALAN","PUSKEB","MPSP"],
        //         [$i++,"LUPUS","PUSKEB",""],
        //         [$i++,"MESIN RUMPUT","PUSKEB",""],
        //     ]
        // );


        $i = 1;
        $this->batchInsert('{{%usage_category}}',
            ['id','name'],
            [
                [$i++,'Bengkel'],
                [$i++,'Kenderaan'],
                [$i++,'Stor'],
            ]
        );
            // $this->batchInsert('{{%usage_detail_key}}',
            //     ['category_id','name'],
            //     [
            //         [1,'Nama Bengkel'],
            //         [1,'Lokasi Bengkel'],
            //         [2,'No Plat Kenderaan'],
            //         [2,'Model Kenderaan'],
            //         [3,'Nama Stor'],
            //         [3,'Lokasi Stor'],
            //         [3,'No Rujukan Stor'],
            //     ]
            // );
            // $this->batchInsert('{{%usage}}',
            //     ['category_id','item_id'],
            //     [
            //         [1,1],
            //         [1,2],
            //         [1,3],
            //         [2,1],
            //         [2,2],
            //         [3,1],
            //         [3,2],
            //     ]
            // );
            // $this->batchInsert('{{%usage_detail_value}}',
            //     ['usage_id','key_id','value'],
            //     [
            //         [2,1,'BENGKEL A'],
            //         [2,2,'PUSKEB'],
            //         [1,3,'LA 1233'],
            //         [1,4,'TRAK'],
            //         [3,5,'STOR UNIT A'],
            //         [3,6,'PUSKEB'],
            //     ]
            // );

        $commands[] = Yii::$app->db->createCommand("INSERT INTO asset (description) SELECT DISTINCT detail AS description FROM pdrm_old.`equipment` WHERE detail IS NOT NULL AND detail != ''");
        $commands[] = Yii::$app->db->createCommand("INSERT INTO supplier (name,address) SELECT DISTINCT detailsup,address FROM pdrm_old.supplier WHERE detailsup IS NOT NULL AND detailsup != ''");
        foreach ($commands as $key => $command) {
            $command->execute();
            var_dump($command);
        }
        return true;
        if($this->db->driverName == 'oci'){
            $this->alterIdentitySequence();
        }

    }

    public function addExtraIdx($i) {
        $dbName = $this->getDbname($this->db);
        switch ($this->db->driverName) {
            case 'mysql':
            $idx = "
            SELECT
            -- table_name as TABLE_NAME
            column_name as COLUMN_NAME
            , table_name as TABLE_NAME
            FROM information_schema.`COLUMNS`
            WHERE table_schema = '$dbName'
            AND table_name NOT IN  ('log')
            AND column_name LIKE '%_id'
            AND column_name LIKE '%_by'
            AND column_name LIKE 'deleted'
            AND column_name LIKE 'approved'
            AND column_name LIKE 'status'
            ";
            break;
            case 'oci':
            $userBy = "
            -- SELECT
            -- TABLE_NAME
            -- FROM USER_TABLES
            -- UNION ALL
            -- SELECT
            -- VIEW_NAME AS TABLE_NAME
            -- FROM USER_VIEWS
            -- UNION ALL
            -- SELECT
            -- MVIEW_NAME AS TABLE_NAME
            -- FROM USER_MVIEWS
            -- ORDER BY TABLE_NAME
            ";

            break;
            default:
            $userBy = false;
            break;
        }
/*        if($userBy){
            $query = $this->db->createCommand($userBy)->queryAll();
            foreach ($query as $key => $value) {
                $this->createIndex('fk'.$i++,'{{'.$value['TABLE_NAME'].'}}',$value['COLUMN_NAME']);
            }
        }
        if($approved){
            $query = $this->db->createCommand($approved)->queryAll();
            foreach ($query as $key => $value) {
                $this->createIndex('fk'.$i++,'{{'.$value['TABLE_NAME'].'}}',$value['COLUMN_NAME']);
            }
        }*/
        if($idx){
            $query = $this->db->createCommand($idx)->queryAll();
            foreach ($query as $key => $value) {
                $this->createIndex('idx'.$i++,'{{'.$value['TABLE_NAME'].'}}',$value['COLUMN_NAME']);
            }
        }

    }
}
