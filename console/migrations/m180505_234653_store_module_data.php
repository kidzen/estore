<?php

use common\components\Migration;

/**
 * Class m180505_234653_store_module_data
 */
class m180505_234653_store_module_data extends Migration
{
    public function init()
    {
        parent::init();
        $this->db = Yii::$app->dbStore;
        $this->db->getSchema()->refresh();
        $this->db->enableSlaves = false;
    }
    /**
     * @inheritdoc
     */
    public function safeUp() {
        try {
            $this->emptyDatabase();
            switch ($this->db->driverName) {
                case 'oci':
                $this->insertDefaultData();
                $this->migrateByScript();
                // // $this->migrateBySqlFile();
                // $this->insertUsage();
                $this->alterIdentitySequence();
                $this->migrateBugFix();
                $this->insertIndex();
                break;
                case 'mysql':
                // $this->insertProfile();
                $this->insertDefaultData();
                $this->insertTestData();
                // $this->migrateByScript();
                // $this->migrateBySqlFile();
                // $this->insertUsage();
                $this->insertIndex();
                break;

                default:
                    # code...
                break;
            }
            $this->insertFk();
            // $this->execute('commit');

            return true;
        } catch (\yii\db\Exception $e) {
            echo $e->getMessage();
            return false;
        }
        // foreach (array_filter(array_map('trim', explode(';', $this->getSchemaSql()))) as $query) {
        //     var_dump($query);
        //     $this->execute($query);
        // }
    }

    public function safeDown() {
        // db oracle
        if($this->db->driverName == 'oci'){
            $this->customDrop();
        }
        $this->emptyDatabase();

    }

    private function insertUsage() {
        // db oracle
        switch ($this->db->driverName) {
            case 'oci':
            $queryDb = 'insert into "usage" ("item_id","category_id") select "ref_id" "item_id" , (CASE WHEN ("ref_cat" = \'workshop\') THEN 1 WHEN "ref_cat" = \'vehicle\' THEN 2 WHEN "ref_cat" = \'store\' THEN 3 ELSE null END) "category_id" from "usage_list"';
            $insertUsage = Yii::$app->db->createCommand($queryDb)->execute();
            break;
            case 'mysql':
            $queryDb = 'insert into `usage` (`item_id`,`category_id`) select `ref_id` `item_id` , (CASE WHEN (`ref_cat` = \'workshop\') THEN 1 WHEN `ref_cat` = \'vehicle\' THEN 2 WHEN `ref_cat` = \'store\' THEN 3 ELSE null END) `category_id` from `usage_list`';
            $insertUsage = Yii::$app->db->createCommand($queryDb)->execute();
            break;
            default:
            break;
        }
    }

    private function getSchemaSql() {
        // db oracle
        if($this->db->driverName == 'oci'){
            $fileName = 'data-20171013.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);

            $sql = $commandR;
            return "$sql";
        } else if($this->db->driverName == 'mysql'){
            $fileName = 'data-mysql-v3.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);

            $sql = $commandR;
            return "$sql";
        } else {
            return 'fail';die;
        }

    }

    private function insertProfile() {
        $time = $this->beginCommand("inserting profile");
        // db oracle
        if($this->db->driverName == 'oci'){
            $fileName = 'profile-oci.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);

            $commandR;
        } else if($this->db->driverName == 'mysql'){
            $fileName = 'profile.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);

            $commandR;
        } else {
            return 'fail';die;
        }
        foreach (array_filter(array_map('trim', explode(';', $commandR))) as $query) {
            Yii::$app->db->createCommand($query)->execute();
            // $this->execute($query);
        }
        $this->endCommand($time);
    }

    private function migrateBySqlFile() {
        $time = $this->beginCommand("migrating old data");
        // db oracle
        if($this->db->driverName == 'oci'){
            // migrate using script since have backup
            // $fileName = 'olddata-oci2.sql';
            // $myfile = fopen($fileName, "r") or die("Unable to open file!");
            // $commandR = fread($myfile, filesize($fileName));
            // fclose($myfile);

            $commandR = '';

            $commandR;
        } else if($this->db->driverName == 'mysql'){
            $fileName = 'olddata-mysql2.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);

            $commandR;
        } else {
            echo 'fail to read file';
            return false;
        }
        foreach (array_filter(array_map('trim', explode(';', $commandR))) as $query) {
            Yii::$app->db->createCommand($query)->execute();
            // $this->execute($query);
        }
        $this->endCommand($time);
    }

    protected function insertTestData() {
        $this->batchInsert('{{%inventory_category}}', ['name'],
            [
                ['Category 1'],
            ]
        );
        $this->batchInsert('{{%inventory}}', ['card_no','code_no','description'],
            [
                ['C1','001','Inventory 1'],
            ]
        );
        $this->batchInsert('{{%vendor}}', ['name'],
            [
                ['Muzzam'],
            ]
        );
    }
    protected function insertDefaultData() {
/*        $i = 1;
        $this->batchInsert('{{%role}}', ['id','name','created_at','updated_at'],
            [
                [$i++,'Administrator',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'Pegawai Stor',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'Jurugegas',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'Vendor',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'Admin Stor',$this->initialTimeStamp,$this->initialTimeStamp],
                [$i++,'User Jabatan',$this->initialTimeStamp,$this->initialTimeStamp],
            ]
        );*/
        $i = 1;
        $this->batchInsert('{{%user}}', ['id','username','email','password','role_id','auth_key'],
            [
                // [1,'test','test@gmail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [10001,'admin3','admin3@gmail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [19875,'nafishah','nafishah.sulaiman@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [12260,'admin1','afizah@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [70202,'admin2','admin2@gmail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [37784,'rosli','rosli@mail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [90399,'awie','rawiramlan@gmail.com',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [19892,'shima','nurhashima.ishak@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [17658,'17658','rajamashitah@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                [11274,'yusri','yusripuskeb@mpsp.gov.my',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
            ]
        );



        $i = 1;
        $this->batchInsert('{{%store_category}}',
            ['id','name'],
            [
                [$i++,'Pusat'],
                [$i++,'Utama'],
                [$i++,'Unit'],
            ]
        );
            // $this->batchInsert('{{%workflow}}',
            //     ['name'],
            //     [
            //         ['Requested'],
            //         ['Pending'],
            //         ['Approved'],
            //         ['Checkout'],
            //         ['Checkin'],
            //         ['Rejected'],
            //         ['Disposed'],
            //     ]
            // );
            // $this->batchInsert('{{%workflow_module}}',
            //     ['module_id','module_name','wf_order','status_id'],
            //     [
            //         [1,1],
            //         [2,2],
            //         [3,3],
            //         [3,4],
            //     ]
            // );
        $i = 1;
        $this->batchInsert('{{%store}}',
            ['id','category_id','name'],
            [
                [$i++,1,'Stor Pusat A'],
                [$i++,2,'Stor Utama B'],
                [$i++,3,'Stor Unit C'],
            ]
        );

        // $i = 1;
        // $this->batchInsert('{{%store_assignment}}',
        //     ['store_id','user_id'],
        //     [
        //         [1,12260],
        //         [2,12260],
        //         [3,12260],
        //     ]
        // );
        $i = 1;
        $this->batchInsert('{{%dispose_category}}',
            ['id','name'],
            [
                [$i++,'Hapus Kira'],
                [$i++,'Lupus'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%dispose_reason}}',
            ['id','name'],
            [
                [$i++,'Tidak Ekonomi Dibaiki'],
                [$i++,'Usang'],
                [$i++,'Rosak dan tidak boleh digunakan'],
                [$i++,'Luput tempoh penggunaan'],
                [$i++,'Keupayaan aset tidak lagi di peringkat optimum'],
                [$i++,'Tiada alat ganti di pasaran'],
                [$i++,'Pembekal tidak lagi memberi khidmat sokongan'],
                [$i++,'Di syor selepas pemeriksaan aset'],
                [$i++,'Tidak lagi diperlukan oleh Jabatan'],
                [$i++,'Perubahan teknologi'],
                [$i++,'Melebihi keperluan'],
                [$i++,'Hilang'],
            ]
        );
        $i = 1;
        $this->batchInsert('{{%dispose_method}}',
            ['id','name'],
            [
                [$i++,'Jualan secara tender'],
                [$i++,'Jualan secara sebut harga'],
                [$i++,'Jualan secara lelong'],
                [$i++,'E-Waste'],
                [$i++,'Buangan Terjadual'],
                [$i++,'Sisa Pepejal'],
                [$i++,'Jualan Sisa'],
                [$i++,'Tukar Barang atau Perkhidmatan'],
                [$i++,'Tukar Beli'],
                [$i++,'Tukar Ganti'],
                [$i++,'Hadiah'],
                [$i++,'Musnah'],
            ]
        );
            // $this->batchInsert('{{%vendor}}',
            //     ['name'],
            //     [
            //         ['Sarra Global Sdn Bhd'],
            //     ]
            // );
            // $this->batchInsert('{{%inventory_category}}',
            //     ['name'],
            //     [
            //         ['Air Filter'],
            //         ['Sticker'],
            //     ]
            // );
            // $this->batchInsert('{{%inventory}}',
            //     ['category_id','card_no','code_no','description',],
            //     [
            //         [1,'CA1','CO1','NANO'],
            //         [1,'CA2','CO2','MICRO'],
            //         [2,'KA1','KO1','KILO'],
            //         [2,'KA2','KO2','MEGA'],
            //     ]
            // );



            // $this->batchInsert('{{%vehicle}}',
            //     ['reg_no','model','type'],
            //     [
            //         ['LA 1111','DAIHATSU','TRAK'],
            //         ['LA 2222','HICOM','LORI'],
            //     ]
            // );

        // $i = 1;
        // $this->batchInsert('{{%workshop}}',
        //     ['id','name','address','contact'],
        //     [
        //         [$i++,"BENGKEL","PUSKEB","MPSP"],
        //         [$i++,"BILIK SERVIS","PUSKEB","MPSP"],
        //         [$i++,"Blower","PUSKEB",""],
        //         [$i++,"Depo Asas Murni","MPSP",""],
        //         [$i++,"Depo Berapit","MPSP",""],
        //         [$i++,"Depo Bertam","MPSP",""],
        //         [$i++,"Depo Jalan Betik","MPSP",""],
        //         [$i++,"Depo Tasek Gelugor","MPSP",""],
        //         [$i++,"High PressureCleaner","",""],
        //         [$i++,"KIMPALAN","PUSKEB","MPSP"],
        //         [$i++,"LUPUS","PUSKEB",""],
        //         [$i++,"MESIN RUMPUT","PUSKEB",""],
        //     ]
        // );


        $i = 1;
        $this->batchInsert('{{%usage_category}}',
            ['id','name'],
            [
                [$i++,'Bengkel'],
                [$i++,'Kenderaan'],
                [$i++,'Stor'],
            ]
        );
            // $this->batchInsert('{{%usage_detail_key}}',
            //     ['category_id','name'],
            //     [
            //         [1,'Nama Bengkel'],
            //         [1,'Lokasi Bengkel'],
            //         [2,'No Plat Kenderaan'],
            //         [2,'Model Kenderaan'],
            //         [3,'Nama Stor'],
            //         [3,'Lokasi Stor'],
            //         [3,'No Rujukan Stor'],
            //     ]
            // );
            // $this->batchInsert('{{%usage}}',
            //     ['category_id','item_id'],
            //     [
            //         [1,1],
            //         [1,2],
            //         [1,3],
            //         [2,1],
            //         [2,2],
            //         [3,1],
            //         [3,2],
            //     ]
            // );
            // $this->batchInsert('{{%usage_detail_value}}',
            //     ['usage_id','key_id','value'],
            //     [
            //         [2,1,'BENGKEL A'],
            //         [2,2,'PUSKEB'],
            //         [1,3,'LA 1233'],
            //         [1,4,'TRAK'],
            //         [3,5,'STOR UNIT A'],
            //         [3,6,'PUSKEB'],
            //     ]
            // );


        return true;
        if($this->db->driverName == 'oci'){
            $this->alterIdentitySequence();
        }

    }

    public function migrateByScript() {
        switch ($this->db->driverName) {
            case 'oci':
            //vendor
            $insert['vendor'] = 'insert into "vendor"
            ("id","name","address","contact_no","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","NAME","ADDRESS","CONTACT_NO","CREATED_AT","UPDATED_AT",
            (select staff_no from estor.people where id = "CREATED_BY") "created_by",
            (select staff_no from estor.people where id = "UPDATED_BY") "updated_by",
            "DELETED" "deleted_by","DELETED_AT" from estor.vendors';

            //inventory_category
            $insert['inventory_category'] = 'insert into "inventory_category" ("id","name","deleted_by","deleted_at","created_at","updated_at","created_by","updated_by")
            select "ID","NAME","DELETED","DELETED_AT","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY") from estor.categories
            ';

            //inventory
            $insert['inventory'] = 'insert into "inventory" ("id","category_id","card_no","code_no","description","quantity","min_stock","location","approved","approved_at","approved_by","created_at","created_by","updated_at","updated_by","deleted_by","deleted_at")
            select "ID","CATEGORY_ID","CARD_NO","CODE_NO","DESCRIPTION","QUANTITY","MIN_STOCK","LOCATION","APPROVED","APPROVED_AT",(select staff_no from estor.people where id = "APPROVED_BY"),"CREATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),"UPDATED_AT",(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.inventories
            ';

            //vehicle
            $insert['vehicle'] = 'insert into "vehicle"
            ("id","reg_no","model","type","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","REG_NO","MODEL","TYPE","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.vehicle_list
            where "ID" in (select id  from estor.vehicle_list where REGEXP_LIKE (substr(reg_no,-1,1),\'^[[:digit:]]+$\'))';

            //workshop
            $insert['workshop'] = 'insert into "workshop"
            ("id","name","address","contact","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","REG_NO","MODEL","TYPE","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.vehicle_list
            where "ID" not in (select id  from estor.vehicle_list where REGEXP_LIKE (substr(reg_no,-1,1),\'^[[:digit:]]+$\'))';

            //transaction
            $insert['transaction'] = 'insert into "transaction"
            ("id","check_date","store_id","check_by","type","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","CHECK_DATE",1 "store_id",(select staff_no from estor.people where id = "CHECK_BY"),"TYPE","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.transactions';

            // //inventory_checkin
            $insert['inventory_checkin'] = 'insert into "inventory_checkin"
            ("id","transaction_id","inventory_id","vendor_id","items_quantity","items_total_price","check_date","check_by","approved","approved_by","approved_at","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","TRANSACTION_ID","INVENTORY_ID","VENDOR_ID","ITEMS_QUANTITY","ITEMS_TOTAL_PRICE","CHECK_DATE",(select staff_no from estor.people where id = "CHECK_BY"),"APPROVED",(select staff_no from estor.people where id = "APPROVED_BY"),"APPROVED_AT","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.inventories_checkin';

            // //order
            $insert['order'] = 'insert into "order"
            ("id","transaction_id","instruction_id","order_date","ordered_by_temp","order_no","approved","approved_by","approved_at","usage_id","required_date","checkout_date","checkout_by","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","TRANSACTION_ID","ARAHAN_KERJA_ID","ORDER_DATE","ORDERED_BY","ORDER_NO","APPROVED",(select staff_no from estor.people where id = "APPROVED_BY"),"APPROVED_AT","VEHICLE_ID","REQUIRED_DATE","CHECKOUT_DATE",(select staff_no from estor.people where id = "CHECKOUT_BY"),"CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.orders';

            //order_item
            $insert['order_item'] = 'insert into "order_item"
            ("id","inventory_id","order_id","rq_quantity","app_quantity","current_balance","unit_price","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","INVENTORY_ID","ORDER_ID","RQ_QUANTITY","APP_QUANTITY","CURRENT_BALANCE","UNIT_PRICE","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.order_items';
                    // $insert['order_item2'] = 'insert into "order_item"
                    // ("id","inventory_id","order_id","rq_quantity","app_quantity","current_balance","unit_price","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
                    // select "ID","INVENTORY_ID","ORDER_ID","RQ_QUANTITY","APP_QUANTITY","CURRENT_BALANCE","UNIT_PRICE","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.order_items
                    // and id not in (select "id" from "order_item")';

            //inventory_item
            $insert['inventory_item'] = 'insert into "inventory_item"
            ("id","inventory_id","checkin_id","checkout_id","sku","unit_price","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","INVENTORY_ID","CHECKIN_TRANSACTION_ID","CHECKOUT_TRANSACTION_ID","SKU","UNIT_PRICE","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.inventory_items';
            break;

            case 'mysql': //mysql fail here
            //vendor
            $insert['vendor'] = 'insert into "vendor"
            ("id","name","address","contact_no","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","NAME","ADDRESS","CONTACT_NO","CREATED_AT","UPDATED_AT",
            (select staff_no from estor.people where id = "CREATED_BY") "created_by",
            (select staff_no from estor.people where id = "UPDATED_BY") "updated_by",
            "DELETED" "deleted_by","DELETED_AT" from estor.vendors';

            //inventory_category
            $insert['inventory_category'] = 'insert into "inventory_category" ("id","name","deleted_by","deleted_at","created_at","updated_at","created_by","updated_by")
            select "ID","NAME","DELETED","DELETED_AT","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY") from estor.categories';


            //inventory
            $insert['inventory'] = 'insert into "inventory" ("id","category_id","card_no","code_no","description","quantity","min_stock","location","approved","approved_at","approved_by","created_at","created_by","updated_at","updated_by","deleted_by","deleted_at")
            select "ID","CATEGORY_ID","CARD_NO","CODE_NO","DESCRIPTION","QUANTITY","MIN_STOCK","LOCATION","APPROVED","APPROVED_AT",(select staff_no from estor.people where id = "APPROVED_BY"),"CREATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),"UPDATED_AT",(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.inventories';



            //vehicle
            $insert['vehicle'] = 'insert into "vehicle"
            ("id","reg_no","model","type","created_at","updated_at","created_by","updated_by","deleted_by","deleted_at")
            select "ID","REG_NO","MODEL","TYPE","CREATED_AT","UPDATED_AT",(select staff_no from estor.people where id = "CREATED_BY"),(select staff_no from estor.people where id = "UPDATED_BY"),"DELETED","DELETED_AT" from estor.vehicle_list';

            break;
            default:
            echo "no data migrated";
            break;
        }
        // if(isset($insert)) {
        if($this->db->driverName == 'oci') {
            foreach ($insert as $key => $value) {
                $time = $this->beginCommand("migrating old data from old database for table $key");
                Yii::$app->db->createCommand($value)->execute();
                // $this->execute($value);
                $this->endCommand($time);
            }
        }
    }

    public function migrateBugFix() {
        if($this->db->driverName == 'oci') {
            $bugFix['missing_inventory_item_missing_inventory_id'] = '
            UPDATE "inventory_item" ori set "inventory_id" = (
            select ic."inventory_id" from "inventory_item" ii
            left join "inventory_checkin" ic on ic."id" = ii."checkin_id"
            where ii."inventory_id" is null and ori."id" = ii."id")
            where ori."inventory_id" is null
            ';
        }

        if($this->db->driverName == 'oci') {
            foreach ($bugFix as $key => $value) {
                $time = $this->beginCommand("fixing $key");
                Yii::$app->db->createCommand($value)->execute();
                // $this->execute($value);
                $this->endCommand($time);
            }
        }

    }
    public function insertFk() {
        $i = 1;
        // $this->addForeignKey('fk'.$i++,'{{user}}','role_id','{{role}}','id');
        // $this->addForeignKey('fk'.$i++,'{{activity_log}}','user_id','{{user}}','id');

            // store
        $this->addForeignKey('fk'.$i++,'{{%store}}','category_id','{{store_category}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%store}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%store}}','created_by','{{user}}','id');
            // store_assignment
        $this->addForeignKey('fk'.$i++,'{{%store_assignment}}','store_id','{{store}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%store_assignment}}','user_id','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%store_assignment}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%store_assignment}}','created_by','{{user}}','id');
            // inventory_category
        // $this->addForeignKey('fk'.$i++,'{{%inventory_category}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%inventory_category}}','created_by','{{user}}','id');
            // inventory
        // $this->addForeignKey('fk'.$i++,'{{%inventory}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%inventory}}','created_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%inventory}}','approved_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%inventory}}','category_id','{{inventory_category}}','id');
            // inventory_checkin
        $this->addForeignKey('fk'.$i++,'{{%inventory_checkin}}','budget_id','{{budget}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%inventory_checkin}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%inventory_checkin}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%inventory_checkin}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{%inventory_checkin}}','store_id','{{store}}','id');
        $this->addForeignKey('fk'.$i++,'{{%inventory_checkin}}','inventory_id','{{inventory}}','id');
        $this->addForeignKey('fk'.$i++,'{{%inventory_checkin}}','vendor_id','{{vendor}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%inventory_checkin}}','check_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%inventory_checkin}}','approved_by','{{user}}','id');
            // inventory_item
        // $this->addForeignKey('fk'.$i++,'{{%inventory_item}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%inventory_item}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%inventory_item}}','inventory_id','{{inventory}}','id');
        $this->addForeignKey('fk'.$i++,'{{%inventory_item}}','checkin_id','{{inventory_checkin}}','id');
        $this->addForeignKey('fk'.$i++,'{{%inventory_item}}','checkout_id','{{order_item}}','id');
        $this->addForeignKey('fk'.$i++,'{{%inventory_item}}','dispose_id','{{dispose}}','id');
            // order
        // $this->addForeignKey('fk'.$i++,'{{%order}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%order}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%order}}','transaction_id','{{transaction}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%order}}','ordered_by','{{user}}','id');
        if($this->db->driverName == 'mysql'){
            $this->addForeignKey('fk'.$i++,'{{%order}}','usage_id','{{usage}}','id');
        }
        // $this->addForeignKey('fk'.$i++,'{{%order}}','checkout_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%order}}','approved_by','{{user}}','id');
            // order_item
        // $this->addForeignKey('fk'.$i++,'{{%order_item}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%order_item}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%order_item}}','inventory_id','{{inventory}}','id');
        $this->addForeignKey('fk'.$i++,'{{%order_item}}','order_id','{{order}}','id');
            // package
        // $this->addForeignKey('fk'.$i++,'{{%package}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%package}}','created_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%package}}','order_id','{{order}}','id');
        $this->addForeignKey('fk'.$i++,'{{%package}}','package_by','{{order}}','id');
            // role
        // $this->addForeignKey('fk'.$i++,'{{%role}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%role}}','created_by','{{user}}','id');
            // transaction
        // $this->addForeignKey('fk'.$i++,'{{%transaction}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%transaction}}','created_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%transaction}}','check_by','{{user}}','id');
        $this->addForeignKey('fk'.$i++,'{{%transaction}}','store_id','{{store}}','id');
            // usage_category
        // $this->addForeignKey('fk'.$i++,'{{%usage_category}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_category}}','created_by','{{user}}','id');
            // usage
        $this->addForeignKey('fk'.$i++,'{{%usage}}','category_id','{{usage_category}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage}}','created_by','{{user}}','id');
            // usage_detail_key
        $this->addForeignKey('fk'.$i++,'{{%usage_detail_key}}','category_id','{{usage_category}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_detail_key}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_detail_key}}','created_by','{{user}}','id');
            // usage_detail_value
        $this->addForeignKey('fk'.$i++,'{{%usage_detail_value}}','usage_id','{{usage}}','id');
        $this->addForeignKey('fk'.$i++,'{{%usage_detail_value}}','key_id','{{usage_detail_key}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_detail_value}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%usage_detail_value}}','created_by','{{user}}','id');
            // vehicle
        // $this->addForeignKey('fk'.$i++,'{{%vehicle}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%vehicle}}','created_by','{{user}}','id');
            // workshop
        // $this->addForeignKey('fk'.$i++,'{{%workshop}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%workshop}}','created_by','{{user}}','id');
            // vendor
        // $this->addForeignKey('fk'.$i++,'{{%vendor}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%vendor}}','created_by','{{user}}','id');
            // dispose
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','transaction_id','{{transaction}}','id');
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','category_id','{{dispose_category}}','id');
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','method_id','{{dispose_method}}','id');
        $this->addForeignKey('fk'.$i++,'{{%dispose}}','reason_id','{{dispose_reason}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose}}','created_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose}}','requested_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose}}','disposed_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose}}','approved_by','{{user}}','id');
            // dispose_item
        // $this->addForeignKey('fk'.$i++,'{{%dispose_item}}','item_id','{{inventory_item}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_item}}','dispose_id','{{dispose}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_item}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_item}}','created_by','{{user}}','id');
            // dispose_method
        // $this->addForeignKey('fk'.$i++,'{{%dispose_method}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_method}}','created_by','{{user}}','id');
            // dispose_category
        // $this->addForeignKey('fk'.$i++,'{{%dispose_category}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_category}}','created_by','{{user}}','id');
            // dispose_reason
        // $this->addForeignKey('fk'.$i++,'{{%dispose_reason}}','updated_by','{{user}}','id');
        // $this->addForeignKey('fk'.$i++,'{{%dispose_reason}}','created_by','{{user}}','id');
    }
    public function insertIndex() {
        $i = 0;
        $this->addExtraIdx($i);
    }

    public function addExtraIdx($i) {
        $dbName = $this->getDbname($this->db);
        switch ($this->db->driverName) {
            case 'mysql':
            $idx = "
            SELECT
            -- table_name as TABLE_NAME
            column_name as COLUMN_NAME
            , table_name as TABLE_NAME
            FROM information_schema.`COLUMNS`
            WHERE table_schema = '$dbName'
            AND table_name NOT IN  ('log')
            AND column_name LIKE '%_id'
            AND column_name LIKE '%_by'
            AND column_name LIKE 'deleted'
            AND column_name LIKE 'approved'
            AND column_name LIKE 'status'
            ";
            break;
            case 'oci':
            $userBy = "
            -- SELECT
            -- TABLE_NAME
            -- FROM USER_TABLES
            -- UNION ALL
            -- SELECT
            -- VIEW_NAME AS TABLE_NAME
            -- FROM USER_VIEWS
            -- UNION ALL
            -- SELECT
            -- MVIEW_NAME AS TABLE_NAME
            -- FROM USER_MVIEWS
            -- ORDER BY TABLE_NAME
            ";

            break;
            default:
            $userBy = false;
            break;
        }
        if($idx){
            $query = $this->db->createCommand($idx)->queryAll();
            foreach ($query as $key => $value) {
                $this->createIndex('idx'.$i++,'{{'.$value['TABLE_NAME'].'}}',$value['COLUMN_NAME']);
            }
        }

    }
    public function getInitialTimeStamp() {
        switch ($this->db->driverName) {
            case 'oci':
            return '12-APR-16 03.54.20 AM';
            break;
            case 'mysql':
            return new \yii\db\Expression("str_to_date('12-APR-16 03.54.20 AM','%d-%b-%Y %h.%i.%s %p')");
            break;

            default:
                # code...
            break;
        }
    }
}
